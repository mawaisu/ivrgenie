<div class="col-md-6 col-xs-12">
    <form  id="reportForm" class="form-inline pull-right" name="reportForm" action="{{route('report.distro.create')}}" method="POST">
        {{csrf_field()}}
        <div class="form-group form-md-line-input" style="padding-top:0px">
            <div class="input-group">
                <div class="input-group-control">
                    <select class="form-control myselect" id="appName" name="appName" onchange="getReportList()" placeholder="Application" data-size="8">
                        <option value="">Select Application</option>
                      @if(!empty($apps))
                        @foreach($apps as $app)
                             <option value="{{$app->AppID}}"/>{{$app->AppName}}</option>
                            @endforeach
                          @endif
                    </select>
                </div>
                <div class="input-group-control">
                    <select class="form-control myselect" id="reportName" name="reportName" onchange="getEmailList()" placeholder="Title" disabled="true">
                        <option value="">Select Report Name</option>
                    </select>
                </div>
                <span class="input-group-btn btn-right">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </span>
            </div>
        </div>


</div>