@extends('ivr.layouts.master')
@section('page_title')
Reports Email Distro
@endsection
@section('custom-styles')
    <style>
        ul.dt-button-collection.dropdown-menu {
            opacity: 1;
        }
    </style>
@endsection
@section('content')
    <div class="portlet light page-header">
        <div class="row no-gutter">
            @include('ivr.report_distro.partials.heading')
            @include('ivr.report_distro.partials.search')
        </div>
    </div>
    <div id="alert" align="center">
        @include('ivr.layouts.error')
        @include('ivr.layouts.success')
    </div>
    <div class="portlet light">
        <div class="row no-gutter">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group" style="width:100%;">
                    <textarea class="form-control" id="emailList" name="emailList" placeholder="Write here" rows="10"
                              style="width:100%; height:250px;resize:none"></textarea>
                </div>
            </div>
        </div>
    </div>
    </form>

@endsection
@section('custom-js')
    <script type="text/javascript" src="/js/reports/reports.js"></script>
@endsection