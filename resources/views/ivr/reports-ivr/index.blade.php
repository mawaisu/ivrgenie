@extends('ivr.layouts.master')
@section('page_title')
    IVR-Reports
@endsection
@section('custom-styles')
    <style>
        ul.dt-button-collection.dropdown-menu {
            opacity: 1;
        }
    </style>
@endsection
@section('content')
    <div class="portlet light page-header">
        <div class="row no-gutter">
            @include('ivr.reports-ivr.partials.heading')
        </div>
    </div>
    <div class="row portlet light no-gutter">
        <div class="col-md-12">
            @include('ivr.layouts.error')
            @include('ivr.layouts.success')
            <div class="table-responsive">
                <table class="table table-hover" data-page-length='10' id="html_table">
                    <thead>
                    <tr>
                        @if(!empty($headings))
                            @foreach($headings as $heading)
                                <th>{{$heading}}</th>
                            @endforeach
                        @endif
                    </tr>
                    </thead>
                    <tbody>

                    @if(!empty($tableData))
                        @foreach ($tableData as $value)
                            <tr>
                                @foreach ($value as $key)
                                    <td>{{$key}}</td>
                                @endforeach
                            </tr>
                        @endforeach
                    @endif


                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('custom-js')
    <script type="text/javascript" src="/js/companies/companies.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#html_table').DataTable(
                {
                    dom: 'Bfrtip',
                    //rowReorder: true,
                    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    buttons: [
                      /*    {
                              extend: 'copy',
                              className: 'btn btn-danger'

                          }
                          , */{
                              extend: 'csv',
                              className: 'btn btn-danger'
                          }
                          , {
                              extend: 'excel',
                                text:'  Excel',
                              className: 'btn btn-danger'
                          }
                          /*, {
                              extend: 'pdf',
                              className: 'btn btn-danger'
                          }
                          , {
                              extend: 'print',
                              className: 'btn btn-danger'
                          }*/,
                        {
                            extend: 'colvis',
                            className: 'btn btn-danger fa fa-columns',
                            text: ''

                        }
                    ]
                }
            );
        });
    </script>
@endsection