@extends('ivr.layouts.master')
@section('page_title')
    Role Assignment
@endsection
@section('custom-styles')
    <style>
        ul.dt-button-collection.dropdown-menu {
            opacity: 1;
        }

        p {
            font-size: 20px;
        }

        .my-check-label {

            border-radius: 3px;
            width: 17px;
            height: 17px;
            padding: 7px 10px;
            margin-right: 10px;
            position: relative;
            top: 6px;
            cursor:pointer ;
        }

        .my-check-label input {
            margin-left: 5px;
            position: relative;
            top: 3px;
        }
    </style>
@endsection
@section('content')
    <div class="portlet light page-header">
        <div class="row no-gutter">
            @include('ivr.role_assignment.partials.heading')
        </div>
    </div>
    <div class="row portlet light no-gutter">

        <div class="col-md-12">
            <select name="roles" id="rolesList">
                <option value="0"></option>
                @if(!empty($roles))
                    @foreach($roles as $role)
                        <option value="{{$role->id}}">{{$role->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="col-md-12">
            @include('ivr.layouts.error')
            @include('ivr.layouts.success')
            <div style="margin:40px auto;">

                <div id="list"></div>
            </div>
            <div class="col-md-5"></div>
            <div class="col-md-2">
                <div class="buttons">
                    <input type="button" id="sbmtBtn" class="btn btn-sm btn-block btn-info" value="Save">
                </div>
            </div>
            <div class="col-md-5"></div>

        </div>
    </div>
@endsection
@section('custom-js')

    <script src="/js/picklist/picklist.js"></script>
    <script type="text/javascript" src="/js/role_assignment/role_assignment.js"></script>
    <script type="text/javascript">
        //console.log( window.location.hostname );
        /**
         * @void display or hide loader on screen
         * @param action(show/hide)
         */
        function loader(action) {
            if (action == "show") {
                $("#loader").show();
            }
            else {
                $("#loader").hide();
            }
        }

        var data;
        $('#rolesList').change(function () {

            loader("show")

            var listValue = $('#rolesList').val();
            console.log(listValue);
            $('#sbmtBtn').click(function () {
                console.log(data);
                loader("show")
                $.ajax({
                    type: 'GET',
                    url: '/role/assignment/set',
                    data: {
                        data: data,
                    },
                    success: function (response) {
                        try {
                            var status = JSON.parse(response);
                            loader("hide")

                            if (status.Status == 100) {
                            console.log("response" + response);
                            swal({
                                title: 'Successful',
                                text: 'Role Permissions Assignment went successful.',
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonText: 'OK'
                            });
                        }
                        else {
                            swal({
                                title: 'Failure',
                                text: 'Something went wrong.Contact IBEX IVR team.',
                                type: 'warning',
                                showCancelButton: false,
                                confirmButtonText: 'OK'
                            });
                        }

                        }catch (e) {
                            swal({
                                title: 'Failure',
                                text: 'Something went wrong with service.Contact IBEX IVR team.',
                                type: 'warning',
                                showCancelButton: false,
                                confirmButtonText: 'OK'
                            });
                            loader("hide")
                        }
                    }
                });

            });

            if (listValue == 0) {

                data = {
                    available: [],
                    selected: []
                };
                $('#list').empty();
                loader("hide")
            }
            else {
                loader("show")
                $.ajax({
                    type: 'GET',
                    url: '/role/assignment/ajax',
                    data: {
                        roleId: listValue,
                    },
                    success: function (response) {

                        console.log("response" + response)

                        data = JSON.parse(response);

                        console.log("hello" + (data));
                        $('#list').empty();
                        $(function () {

                            $(document).ready(function () {

                                //console.log(data.selected);

                                // $('.select.selected').append("<label>Edit</label>");
                                // creating checkboxs for selected list
                                $('.select.selected').append("<label class='label label-danger my-check-label ' id='deleteLabel' style='display:none;'>Delete</label>");
                                $('.select.selected').append("  ");
                                $('.select.selected').append("<label class='label label-primary my-check-label ' id='editLabel'  style='display:none;'>Edit</label>");
                                if (data.selected) {
                                    for (var i = 0; i < data.selected.length; i++) {

                                        //delete checkbox
                                        //delete checkbox
                                        var txt2 = $("<input type='checkbox'>").attr('name', data.selected[i].id + 'deleteCheck');
                                        //edit checkbox
                                        var txt1 = $("<input type='checkbox'>").attr('name', data.selected[i].id + 'editCheck');

                                        //adding class to checkbox
                                        txt2.addClass('my-check');
                                        txt1.addClass('my-check');

                                        //appending checboxes
                                        $('#deleteLabel').append(txt2);

                                        // alert('here')
                                        $('#editLabel').append(txt1);

                                        //checking if edit and delete value is checked or not and populating them
                                        if (data.selected[i].edit == "false") {
                                            //txt2.attr('checked','checked');
                                            txt1.attr('value', false);
                                        }
                                        else {
                                            txt1.attr('checked', 'checked');
                                            txt1.attr('value', true);
                                        }

                                        if (data.selected[i].delete == "false") {
                                            //txt2.attr('checked','checked');
                                            txt2.attr('value', false);
                                        }
                                        else {
                                            txt2.attr('checked', 'checked');
                                            txt2.attr('value', true);
                                        }

                                        // assiging id to checkboxes
                                        txt2.attr('id', data.selected[i].id + 'deleteCheck');
                                        txt1.attr('id', data.selected[i].id + 'editCheck');

                                        //adding click event to checkbox
                                        txt2.attr('onclick', 'checkevent(this);');
                                        txt1.attr('onclick', 'editevent(this);');
                                        //hiding check boxes
                                        txt2.hide();
                                        txt1.hide();
                                        console.log(data.selected);
                                    }

                                }
                            });

                            var a = $('#list').pickList({
                                data: data,
                            });

                            $('#button').on('click', function () {
                                console.log(a.pickList('getSelected'));
                            });


                            //remove button action
                            a.on('picklist.remove', function (event, v) {
                                console.log(v)
                                for (var i = 0; i < v.length; i++) {
                                    var isFound = false;
                                    var counter = 0;
                                    for (var j = 0; j < data.selected.length; j++) {

                                        if (data.selected[j].id === v[i].id) {

                                            isFound = true;
                                            counter = j;
                                            break;
                                        }

                                    }

                                    if (isFound == true) {
                                        //data.selected.push(v[i]);
                                        //data.available.pop(v[i]);
                                        data.selected.splice(counter, 1);
                                    }
                                    else {
                                        //data.available.push(v[i]);
                                    }


                                    var isFound2 = false;
                                    var counter2 = 0;
                                    for (var j = 0; j < data.available.length; j++) {

                                        if (data.available[j].id === v[i].id) {

                                            isFound2 = true;
                                            break;
                                        }

                                    }

                                    if (isFound2 == true) {
                                        //data.selected.push(v[i]);
                                        //data.available.pop(v[i]);

                                        //data.available.splice(counter2,1);

                                    }
                                    else {
                                        data.available.push(v[i]);
                                    }

                                    console.log(data);
                                }
                                $('#deleteLabel').hide();
                                $('#editLabel').hide();
                                $("input:checkbox").hide();
                            });

                            a.on('picklist.add', function (event, v) {
                                console.log(v)
                                //loop through seleted items and create there respective checkboxes
                                for (var i = 0; i < v.length; i++) {

                                    // alert($('#' + v[i].id + 'deleteCheck').length);

                                    //delete checkbox creation
                                    if ($('#' + v[i].id + 'deleteCheck').length) {

                                    }
                                    else {
                                        var txt = $("<input type='checkbox'>").attr('name', v[i].id + 'deleteCheck');
                                        $('#deleteLabel').append(txt);
                                        if (v[i].delete == 'false') {
                                            txt.attr('checked', false);
                                        }
                                        else {
                                            txt.attr('checked', true);
                                        }
                                        //assiging id to checkbox
                                        txt.attr('id', v[i].id + 'deleteCheck');
                                        //binding click event to checkbox
                                        txt.attr('onclick', 'checkevent(this);');

                                        //adding class to checkbox
                                        txt.addClass('my-check');

                                        //hiding checkbox
                                        txt.hide();
                                    }

                                    //edit checkbox creation
                                    if ($('#' + v[i].id + 'editCheck').length) {

                                    }
                                    else {
                                        var txt = $("<input type='checkbox'>").attr('name', v[i].id + 'editCheck');
                                        $('#editLabel').append(txt);
                                        if (v[i].edit == 'false') {
                                            txt.attr('checked', false);
                                        }
                                        else {
                                            txt.attr('checked', true);
                                        }
                                        //assiging id to checkbox
                                        txt.attr('id', v[i].id + 'editCheck');
                                        //binding event
                                        txt.attr('onclick', 'editevent(this);');
                                        //adding class to checkbox
                                        txt.addClass('my-check');

                                        //hiding element
                                        txt.hide();
                                    }

                                    //// alert(v[i]);


                                    console.log(data.selected.length);
                                    console.log(v.length);
                                    var isFound = false;
                                    var counter = 0;
                                    for (var j = 0; j < data.selected.length; j++) {

                                        if (data.selected[j].id === v[i].id) {

                                            isFound = true;

                                            break;
                                        }

                                    }

                                    if (isFound == true) {
                                        //data.selected.push(v[i]);
                                        //data.available.pop(v[i]);

                                    }
                                    else {
                                        data.selected.push(v[i]);
                                    }


                                    var isFound2 = false;
                                    var counter2 = 0;
                                    for (var j = 0; j < data.available.length; j++) {

                                        if (data.available[j].id === v[i].id) {

                                            isFound2 = true;
                                            counter2 = j;

                                            break;
                                        }

                                    }

                                    if (isFound2 == true) {
                                        //data.selected.push(v[i]);
                                        //data.available.pop(v[i]);

                                        data.available.splice(counter2, 1);

                                    }
                                    else {
                                        //data.selected.push(v[i]);
                                    }

                                    console.log(data);

                                }
                            });
                        });
                        $(document).ready(function () {

                            $('.select.available select').attr('name', 'availabe_permissions');
                            $('.select.selected select').attr('name', 'seleted_permissions');
                            $('.select.selected select').dblclick(function () {

                                var temp = this.value + "deleteCheck";
                                var temp2 = this.value + "editCheck";

                                for (var i = 0; i < data.selected.length; i++) {
                                    var checkbox = $('#' + data.selected[i].id + 'deleteCheck');
                                    var checkbox2 = $('#' + data.selected[i].id + 'editCheck');
                                    checkbox.hide();
                                    checkbox2.hide();
                                }
                                // alert(temp);
                                // alert(temp2);
                                $('#deleteLabel').show();
                                $('#editLabel').show();
                                $('#' + temp).show();

                                if ($('#editLabel').length > 0) {

                                }
                                else
                                //  $('.select.selected').append("<label class='label label-primary' id='editLabel'>Edit</label>");
                                    $('.select.selected').append("  ");
                                $('#' + temp2).show();
                                //so far apend input checkbox is done now i have to add value to to that checkbox and on submit i have to gather all values
                            });
                            $('.select.available select').click(function () {

                                // alert('avaliable on click')
                                $('#deleteLabel').hide();
                                $('#editLabel').hide();
                                $("input:checkbox").hide();
                            });
                        });
                        loader("hide")

                    }
                });
            }

        });

        // json data from service

        function checkevent(val) {

            var ch = val;

            // alert($(ch).attr('id'));

            // replace a dash by a colon
            var id = $(ch).attr('id').replace("deleteCheck", "");
            var status = $(ch).prop('checked');
            var value = $(ch).attr('value');
            // alert(id);
            // alert("status:" + status);
            // alert("value:" + value);

            console.log(data.selected);
            for (var i = 0; i < data.selected.length; i++) {

                if (id == data.selected[i].id) {
                    data.selected[i].delete = status;
                }
            }

            console.log(data.selected);

            <!-- }); -->
            <!-- var id=$(this).attr('id'); -->
            // // alert("ID:"+$input.attr('id'));
        }


        function editevent(val) {

            var ch = val;

            // alert($(ch).attr('id'));

            // replace a dash by a colon
            var id = $(ch).attr('id').replace("editCheck", "");
            var status = $(ch).prop('checked');
            var value = $(ch).attr('value');
            // alert(id);
            // alert("status:" + status);
            // alert("value:" + value);

            console.log(data.selected);
            for (var i = 0; i < data.selected.length; i++) {

                if (id == data.selected[i].id) {
                    data.selected[i].edit = status;
                }
            }

            console.log(data.selected);
        }

        function loadList(data) {
            data = data;
            $(document).ready(function () {

                console.log(data.selected);

                // $('.select.selected').append("<label>Edit</label>");
                // creating checkboxs for selected list
                for (var i = 0; i < data.selected.length; i++) {

                    //delete checkbox
                    var txt2 = $("<input type='checkbox'>").attr('name', data.selected[i].id + 'deleteCheck');
                    //edit checkbox
                    var txt1 = $("<input type='checkbox'>").attr('name', data.selected[i].id + 'editCheck');

                    //adding class to checkbox
                    txt2.addClass('my-check');
                    txt1.addClass('my-check');

                    //appending checboxes
                    $('.select.selected').append(txt2);
                    $('.select.selected').append(txt1);

                    //checking if edit and delete value is checked or not and populating them
                    if (data.selected[i].edit == 'false') {
                        //txt2.attr('checked','checked');
                        txt1.attr('value', false);
                    }
                    else {
                        txt1.attr('checked', 'checked');
                        txt1.attr('value', true);
                    }

                    if (data.selected[i].delete == 'false') {
                        //txt2.attr('checked','checked');
                        txt2.attr('value', false);
                    }
                    else {
                        txt2.attr('checked', 'checked');
                        txt2.attr('value', true);
                    }

                    // assiging id to checkboxes
                    txt2.attr('id', data.selected[i].id + 'deleteCheck');
                    txt1.attr('id', data.selected[i].id + 'editCheck');

                    //adding click event to checkbox
                    txt2.attr('onclick', 'editevent(this);');
                    //hiding check boxes
                    txt2.hide();
                    txt1.hide();
                }


            });

            var a = $('#list').pickList({
                data: data,
            });

            $('#button').on('click', function () {
                console.log(a.pickList('getSelected'));
            });


            //remove button action
            a.on('picklist.remove', function (event, v) {
                console.log(v)
                for (var i = 0; i < v.length; i++) {
                    var isFound = false;
                    var counter = 0;
                    for (var j = 0; j < data.selected.length; j++) {

                        if (data.selected[j].id === v[i].id) {

                            isFound = true;
                            counter = j;
                            break;
                        }

                    }

                    if (isFound == true) {
                        //data.selected.push(v[i]);
                        //data.available.pop(v[i]);
                        data.selected.splice(counter, 1);
                    }
                    else {
                        //data.available.push(v[i]);
                    }


                    var isFound2 = false;
                    var counter2 = 0;
                    for (var j = 0; j < data.available.length; j++) {

                        if (data.available[j].id === v[i].id) {

                            isFound2 = true;
                            break;
                        }

                    }

                    if (isFound2 == true) {
                        //data.selected.push(v[i]);
                        //data.available.pop(v[i]);

                        //data.available.splice(counter2,1);

                    }
                    else {
                        data.available.push(v[i]);
                    }

                    console.log(data);
                }
            });

            a.on('picklist.add', function (event, v) {
                console.log(v)
                //loop through seleted items and create there respective checkboxes
                for (var i = 0; i < v.length; i++) {

                    // alert($('#' + v[i].id + 'deleteCheck').length);

                    //delete checkbox creation
                    if ($('#' + v[i].id + 'deleteCheck').length) {

                    }
                    else {
                        var txt = $("<input type='checkbox'>").attr('name', v[i].id + 'deleteCheck');
                        $('.select.selected').append(txt);
                        if (v[i].delete == 'false') {
                            txt.attr('checked', false);
                        }
                        else {
                            txt.attr('checked', true);
                        }
                        //assiging id to checkbox
                        txt.attr('id', v[i].id + 'deleteCheck');
                        //binding click event to checkbox
                        txt.attr('onclick', 'checkevent(this);');

                        //adding class to checkbox
                        txt.addClass('my-check');

                        //hiding checkbox
                        txt.hide();
                    }

                    //edit checkbox creation
                    if ($('#' + v[i].id + 'editCheck').length) {

                    }
                    else {
                        var txt = $("<input type='checkbox'>").attr('name', v[i].id + 'editCheck');
                        $('.select.selected').append(txt);
                        if (v[i].edit == 'false') {
                            txt.attr('checked', false);
                        }
                        else {
                            txt.attr('checked', true);
                        }
                        //assiging id to checkbox
                        txt.attr('id', v[i].id + 'editCheck');
                        //binding event
                        txt.attr('onclick', 'editevent(this);');
                        //adding class to checkbox
                        txt.addClass('my-check');

                        //hiding element
                        txt.hide();
                    }

                    //// alert(v[i]);


                    console.log(data.selected.length);
                    console.log(v.length);
                    var isFound = false;
                    var counter = 0;
                    for (var j = 0; j < data.selected.length; j++) {

                        if (data.selected[j].id === v[i].id) {

                            isFound = true;

                            break;
                        }

                    }

                    if (isFound == true) {
                        //data.selected.push(v[i]);
                        //data.available.pop(v[i]);

                    }
                    else {
                        data.selected.push(v[i]);
                    }


                    var isFound2 = false;
                    var counter2 = 0;
                    for (var j = 0; j < data.available.length; j++) {

                        if (data.available[j].id === v[i].id) {

                            isFound2 = true;
                            counter2 = j;

                            break;
                        }

                    }

                    if (isFound2 == true) {
                        //data.selected.push(v[i]);
                        //data.available.pop(v[i]);

                        data.available.splice(counter2, 1);

                    }
                    else {
                        //data.selected.push(v[i]);
                    }

                    console.log(data);

                }
            });
        }


    </script>
@endsection