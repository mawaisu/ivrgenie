@extends('ivr.layouts.master')
@section('page_title')
    Manage Applications
@endsection
@section('custom-styles')
    <style>
        ul.dt-button-collection.dropdown-menu {
            opacity: 1;
        }
    </style>
@endsection
@section('content')
    <div class="portlet light page-header">
        <div class="row no-gutter">
            @include('ivr.applications.partials.heading')
        </div>
    </div>
    <div class="row portlet light no-gutter">
        <div class="col-md-12">
            @include('ivr.layouts.error')
            @include('ivr.layouts.success')
            <div class="table-responsive">
                <table class="table table-hover" data-page-length='10' id="html_table">
                    <thead>
                    <tr>
                        <th>App Id</th>
                        <th>Owner</th>
                        <th>App Name</th>
                        <th>App Abrv</th>
                        <th>App Type</th>
                        <th>Status</th>
                        <th>Time Zone</th>
                        <th>Environment</th>
                        <th class="action-column">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($applications as $app)
                        <tr>
                            <td>{{$app->AppID}}</td>

                            <td>{{$app->user==null?'N/A':$app->user->firstname}}
                                &nbsp;{{$app->user==null?'N/A':$app->user->lastname}}
                            </td>
                            <td>{{$app->AppName}}</td>
                            <td>{{$app->AppAbrv}}</td>
                            @if($app->AppType=='I')
                                <td>Inbound</td>@elseif($app->AppType=='O')
                                <td>Outbound</td>@elseif($app->AppType=='B')
                                <td>Blended</td>
                            @elseif($app->AppType=='N')
                                <td>Inbound-Plus</td>@elseif($app->AppType=='U')
                                <td>Outbound-Plus</td>@elseif($app->AppType=='L')
                                <td>Blended-Plus</td>
                            @else
                                <td>Blended</td>@endif
                            <td>{{$app->Status=='L'?'Live':'Down'}}</td>
                            <td>{{$app->TimeZone>=0?'EST+'.$app->TimeZone:'EST'.$app->TimeZone}}</td>
                            <td>{{$app->Environment=='S'?'Staging':'Production'}}</td>
                            <td><a href="#"
                                   onclick="editApplication({{$app->AppID}})"><span
                                            class="btn btn-xs"><i
                                                class="fa fa-pencil"></i></span></a>
                                @if($app->Status=='L')
                                <a class="btn bt-xs text-gray" href="#" title="Disable"
                                   onclick="messageBox(this,function(){loader('show');window.location='{{route('application.statusChange',$app->AppID)}}';})"
                                   data-type="warning" data-text="You are about to disable this application!"
                                   data-confirm-text="Yes, disable it!"
                                   data-success-text="Your record has been disabled!"
                                   data-href="{{route('application.statusChange',$app->AppID)}}" data-toggle="tooltip"
                                   data-placement="top"><i class="fa fa-ban app"></i>
                                </a>
                                @else
                                <a class="btn bt-xs text-green" href="#" title="Enable"
                                   onclick="messageBox(this,function(){loader('show');window.location='{{route('application.statusChange',$app->AppID)}}';})"
                                   data-type="warning" data-text="You are about to enable this application!"
                                   data-confirm-text="Yes, enable it!"
                                   data-success-text="Your record has been Enabled!"
                                   data-href="{{route('application.statusChange',$app->AppID)}}" data-toggle="tooltip"
                                   data-placement="top"><i class="fa fa-check app"></i>
                                </a>
                                @endif
                                <a class="btn bt-xs text-red" href="#" title="Delete"
                                   onclick="messageBox(this,function(){loader('show');window.location='{{route('application.delete',$app->AppID)}}';})"
                                   data-type="warning" data-text="You will not be able to recover this record!"
                                   data-confirm-text="Yes, delete it!"
                                   data-success-text="Your record has been deleted!"
                                   data-href="{{route('application.delete',$app->AppID)}}" data-toggle="tooltip"
                                   data-placement="top"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('ivr.applications.partials.addEdit-modal')
@endsection
@section('custom-js')
    <script type="text/javascript" src="/js/application/application.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#html_table').DataTable(
                {
                    dom: 'Bfrtip',
                    //rowReorder: true,
                    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    buttons: [
                        /*  {
                              extend: 'copy',
                              className: 'btn btn-primary'

                          }
                          , {
                              extend: 'csv',
                              className: 'btn btn-primary'
                          }
                          , {
                              extend: 'excel',
                              className: 'btn btn-primary'
                          }
                          , {
                              extend: 'pdf',
                              className: 'btn btn-primary'
                          }
                          , {
                              extend: 'print',
                              className: 'btn btn-primary'
                          },*/
                        {
                            extend: 'colvis',
                            className: 'btn btn-danger fa fa-columns',
                            text: ''
                        }
                    ]
                }
            );
        });
    </script>
@endsection