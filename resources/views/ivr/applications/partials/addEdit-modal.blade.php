
<!--====== CONTENT ======-->
<div class="modal fade" id="addAppPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form class="form-horizontal" id="addAppForm" action="{{route('application.edit')}}" method="post">
        {{csrf_field()}}
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="applicationModalLabel">Add Application</h4>
                </div>
                <div class="modal-body noSpace">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-4 control-label" for="customerID">Owner</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="customername" id="customername" readonly value="{{\Illuminate\Support\Facades\Auth::user()->username}}" required>
                            <input type="hidden" class="form-control" name="userID" id="userID" readonly value="1" required>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="application_name" class="col-md-4 control-label">App Name</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control appname" name="appName" id="appName" maxlength="50" required>
                            <div class="pull-right" id="appnameloader"></div>
                        </div>
                    </div>

                    <div class="form-group form-md-line-input" id="appnameavailability" style="display: none;">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-7">
                            <strong>Application with this name already Exists!</strong>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="app_abbrv" class="col-md-4 control-label">App Abbreviation</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control appabrv" name="appAbrv" id="appAbrv" maxlength="50" required>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="app_type" class="col-md-4 control-label">App Type</label>
                        <div class="col-md-7">
                            <select class="form-control apptype myselect" name="appType" id="appType">
                                @foreach(\App\CommonUtils\CommonStrings::$appType as  $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="status" class="col-md-4 control-label">Status</label>
                        <div class="col-md-7">
                            <select class="form-control appstatus myselect" name="appStatus" id="appStatus">
                                @foreach(\App\CommonUtils\CommonStrings::$appStatus as  $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="timezone" class="col-md-4 control-label">Time Zone</label>
                        <div class="col-md-7">
                            <select class="form-control apptime myselect" name="timeZone" id="timeZone">
                                @foreach(\App\CommonUtils\CommonStrings::$appTime as  $key => $value)
                                    <option value="{{$key}}" {{$key=='0'?'selected':''}} >{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="environment" class="col-md-4 control-label">Environment</label>
                        <div class="col-md-7">
                            <select class="form-control appenv myselect" name="environment" id="environment">
                                @foreach(\App\CommonUtils\CommonStrings::$appEnv as  $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!--input type="button" class="btn btn-green submit" value="Save"-->
                    <input type="hidden" name="teamID" value="" />
                    <button type="submit" id="submitButton" class="btn btn-success">Save</button>
                    <button type="button" id="cancelmodal" data-dismiss="modal" class="btn btn-primary">Cancel</button>
                </div>
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
        <input type="hidden"  value="-1" name="appID" id="appID" >
    </form>
</div>