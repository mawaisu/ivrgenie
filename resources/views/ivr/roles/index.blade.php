@extends('ivr.layouts.master')
@section('page_title')
    Roles
@endsection
@section('custom-styles')
    <style>
        ul.dt-button-collection.dropdown-menu {
            opacity: 1;
        }
    </style>
@endsection
@section('content')
    <div class="portlet light page-header">
        <div class="row no-gutter">
            @include('ivr.roles.partials.heading')
        </div>
    </div>
    <div class="row portlet light no-gutter">
        <div class="col-md-12">
            @include('ivr.layouts.error')
            @include('ivr.layouts.success')
            <div class="table-responsive">
                <table class="table table-hover" data-page-length='10' id="html_table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Role Name</th>
                        <th>Status</th>
                        <th>Created_At</th>
                        <th class="action-column">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($roles))
                        @foreach($roles as $role)
                            <tr>
                                <td>{{$role->id}}</td>
                                <td>{{$role->name}}</td>
                                <td>{{$role->status==0?'In-Active':'Active'}}</td>
                                <td>{{$role->created_at->diffForHumans()}}</td>
                                <td> <a
                                            href="#" onclick="editRole({{$role->id}})"><span class="btn btn-xs"><i
                                                    class="fa fa-pencil" ></i></span></a> <a
                                            href="{{route('role.delete',$role->id)}}" title="Delete" aria-label="Delete"
                                            onclick="return confirm('Are you sure you want to Delete this role?')"
                                            data-pjax="0"><span class="btn bt-xs text-red"><i
                                                    class="fa fa-times"></i></span></a></td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('ivr.roles.partials.addEdit-modal')
@endsection
@section('custom-js')
    <script type="text/javascript" src="/js/role/role.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#html_table').DataTable(
                {
                    dom: 'Bfrtip',
                    //rowReorder: true,
                    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    buttons: [
                        /*  {
                              extend: 'copy',
                              className: 'btn btn-primary'

                          }
                          , {
                              extend: 'csv',
                              className: 'btn btn-primary'
                          }
                          , {
                              extend: 'excel',
                              className: 'btn btn-primary'
                          }
                          , {
                              extend: 'pdf',
                              className: 'btn btn-primary'
                          }
                          , {
                              extend: 'print',
                              className: 'btn btn-primary'
                          },*/
                        {
                            extend: 'colvis',
                            className: 'btn btn-danger fa fa-columns',
                            text: ''

                        }
                    ]
                }
            );
        });
    </script>
@endsection