<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 pull-right searchCrt">

    <div class="dropdown pull-right">
        <button class="btn btn-primary dropdown-toggle" id="adSearch" type="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
            Advanced Search <span class="caret"></span>
        </button>
        <ul class="dropdown-menu dd-menu-md dropdown-menu-right pull-right" role="menu"
            aria-labelledby="adSearch">
            <form action="" method="get">
                <div class="addSpace form">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">

                                <input type="text" class="form-control" id="searchAppID" name="searchAppID"
                                       placeholder="App ID"
                                       value="<?php echo isset($_GET['searchAppID']) ? $_GET['searchAppID'] : '' ?>">

                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">


                                <input type="text" class="form-control" id="searchAppName"
                                       name="searchAppName" placeholder="App Name"
                                       value="<?php echo isset($_GET['searchAppName']) ? $_GET['searchAppName'] : '' ?>">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col-xs-12 col-md-6">
                            <div class="select-wrapper form-group form-md-line-input">
                                <select name="searchStatus" id="searchStatus" class="form-control myselect">
                                    <option value="">Select Status</option>
                                    <option value="L">Live</option>
                                    <option value="D">Down</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <select name="searchEnvironment" id="searchEnvironment"
                                        class="form-control myselect">

                                    <option value="">Select Environment</option>
                                    <option value="S">Staging</option>
                                    <option value="P">Production</option>
                                </select>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="actionPanel">
                    <button type="submit" class="btn btn-primary pull-right">Search</button>
                </div>
            </form>
        </ul>
    </div>
</div>