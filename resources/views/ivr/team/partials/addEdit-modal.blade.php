
<div class="modal fade" id="addTeamPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <form class="form-horizontal" id="addTeamForm" action="{{route('team.edit')}}" method="post">
        {{csrf_field()}}
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="teamTitle">Add Team</h4>
                </div>
                <div class="modal-body noSpace">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label">Company</label>
                        <div class="col-md-8">
                            <select id="teamCompany" class="form-control myselect" name="teamCompany" required="true">
                                <option value="">Please Select a company</option>
                             @if(!empty($companies=\App\Company::all()))
                                 @foreach($companies as $company)
                                        <option value="{{$company->ID}}">{{$company->Name}}</option>
                                        @endforeach
                                 @endif
                            </select>
                        </div>
                    </div>

                    <div class="form-group form-md-line-input">
                        <label for="app_abbrv" class="col-md-3 control-label">Team Name</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control teamName" name="Name" maxlength="150"
                                   id="teamName" required>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="app_abbrv" class="col-md-3 control-label">Description</label>
                        <div class="col-md-8">
                            <textarea class="form-control appabrv" rows="5" name="teamDesc" id="teamDesc"
                                      required></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!--input type="button" class="btn btn-green submit" value="Save"-->
                    <button type="submit" id="submitButton" class="btn btn-success">Save</button>
                    <button type="button" id="cancelmodal" data-dismiss="modal" class="btn btn-primary">Cancel</button>
                </div>
            </div>
        </div>
        <input type="hidden" value="" name="teamID" id="teamID">
    </form>
</div>