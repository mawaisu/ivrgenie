@extends('ivr.layouts.master')
@section('page_title')
   Teams
@endsection
@section('custom-styles')
    <style>
        ul.dt-button-collection.dropdown-menu {
            opacity: 1;
        }
    </style>
@endsection
@section('content')
    <div class="portlet light page-header">
        <div class="row no-gutter">
            @include('ivr.team.partials.heading')
        </div>
    </div>
    <div class="row portlet light no-gutter">
        <div class="col-md-12">
            @include('ivr.layouts.error')
            @include('ivr.layouts.success')
            <div class="table-responsive">
                <table class="table table-hover" data-page-length='10' id="html_table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Company Name</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th class="action-column">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($teams))

                        @foreach($teams as $team)
                            <tr>
                                <td>{{$team->Team_ID}}</td>
                                <td>{{$team->company->Name}}</td>
                                <td>{{$team->Name}}</td>
                                <td>{{$team->Description}}</td>
                                <td><a
                                            href="#" onclick="editTeam({{$team->Team_ID}})"><span class="btn btn-xs"><i
                                                    class="fa fa-pencil"></i></span></a>
                                    <a class="btn bt-xs text-red"
                                       href="#" title="Delete"
                                       onclick="messageBox(this,function(){loader('show');
                                       window.location='{{route('team.delete',$team->Team_ID)}}';})"
                                       data-type="warning"
                                       data-text="You will not be able to recover this record!"
                                       data-confirm-text="Yes, delete it!"
                                       data-success-text="Your record has been deleted!"
                                       data-href="{{route('team.delete',$team->Team_ID)}}"
                                       data-toggle="tooltip"
                                       data-placement="top"><i
                                                class="fa fa-times"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('ivr.team.partials.addEdit-modal')
@endsection
@section('custom-js')
    <script type="text/javascript" src="/js/teams/teams.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#html_table').DataTable(
                {
                    dom: 'Bfrtip',
                    //rowReorder: true,
                    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    buttons: [
                        /*  {
                              extend: 'copy',
                              className: 'btn btn-primary'

                          }
                          , {
                              extend: 'csv',
                              className: 'btn btn-primary'
                          }
                          , {
                              extend: 'excel',
                              className: 'btn btn-primary'
                          }
                          , {
                              extend: 'pdf',
                              className: 'btn btn-primary'
                          }
                          , {
                              extend: 'print',
                              className: 'btn btn-primary'
                          },*/
                        {
                            extend: 'colvis',
                            className: 'btn btn-danger fa fa-columns',
                            text: ''

                        }
                    ]
                }
            );
        });
    </script>
@endsection