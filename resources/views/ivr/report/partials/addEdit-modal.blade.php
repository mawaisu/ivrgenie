<div class="modal fade" id="addRptPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <form class="form-horizontal" id="addRptForm" action="{{route('report.edit')}}" method="post">
        {{csrf_field()}}
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="reportModalLabel">Add Report</h4>
                </div>
                <div class="modal-body noSpace">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-4 control-label" for="customerID">Owner</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="customername" id="customername" readonly
                                   value="{{\Illuminate\Support\Facades\Auth::user()->username}}" required>
                            <input type="hidden" class="form-control" name="userID" id="userID" readonly value=""
                                   required>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="app_type" class="col-md-4 control-label">Application</label>
                        <div class="col-md-7">
                            <select class="form-control myselect" id="appID" name="appID" placeholder="Application"
                                    data-size="8" required>
                                <option value="">Select Application</option>
                                @if(!empty($apps))
                                    @foreach($apps as $row)
                                        <option value="{{$row->AppID}}"/>{{$row->AppName}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="form-group form-md-line-input">
                        <label for="app_abbrv" class="col-md-4 control-label">Name</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control appabrv" name="rptName" id="rptName" maxlength="50"
                                   required>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="app_abbrv" class="col-md-4 control-label">Subject</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control appabrv" name="rptSubject" id="rptSubject"
                                   maxlength="150" required>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="status" class="col-md-4 control-label">Hour</label>
                        <div class="col-md-7">
                            <select class="form-control myselect" name="rptHour" id="rptHour" required>
                                <option value="" selected>Execution Hour EST</option>
                                @foreach(\App\CommonUtils\CommonStrings::$hours as  $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="timezone" class="col-md-4 control-label">Frequency</label>
                        <div class="col-md-7">
                            <select class="form-control myselect" name="rptFrq" id="rptFrq" required>
                                <option value="" selected> Select Days</option>

                                @foreach(\App\CommonUtils\CommonStrings::$reportFrequency as  $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="timezone" class="col-md-4 control-label">Day</label>
                        <div class="col-md-7">
                            <select class="form-control myselect" name="rptDay" id="rptDay">
                                <option value="0" selected> Select Day</option>
                                @foreach(\App\CommonUtils\CommonStrings::$days as  $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="environment" class="col-md-4 control-label">Status</label>
                        <div class="col-md-7">
                            <select class="form-control myselect appenv" name="rptStatus" id="rptStatus">
                                @foreach(\App\CommonUtils\CommonStrings::$appStatus as  $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!--input type="button" class="btn btn-green submit" value="Save"-->
                    <input type="hidden" name="teamID" value=""/>
                    <input type="hidden" name="rptID" id="rptID" value="0"/>
                    <button type="submit" id="submitButton" class="btn btn-success">Save</button>
                    <button type="button" id="cancelmodal" data-dismiss="modal" class="btn btn-primary">Cancel</button>
                </div>
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->


    </form>
</div>