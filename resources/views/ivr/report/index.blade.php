@extends('ivr.layouts.master')
@section('page_title')Manage Report
@endsection
@section('custom-styles')
    <style>
        ul.dt-button-collection.dropdown-menu {
            opacity: 1;
        }
    </style>
@endsection
@section('content')
    <div class="portlet light page-header">
        <div class="row no-gutter">
            @include('ivr.report.partials.heading')
        </div>
    </div>
    <div class="row portlet light no-gutter">
        <div class="col-md-12">
            @include('ivr.layouts.error')
            @include('ivr.layouts.success')
            <div class="table-responsive">
                <table class="table table-hover" data-page-length='10' id="html_table">
                    <thead>
                    <tr>
                        <th>Rpt Id</th>
                        <th>App Name</th>
                        <th>Rpt Name</th>
                        <th>Rpt Subject</th>
                        <th>Rpt Hh</th>
                        <th>Frequency</th>
                        <th>Day</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($reports))
                        @foreach($reports as $report)
                            <tr>
                                <td>{{$report->RptID}}</td>
                                <td>{{$report->app==null?'N/A':$report->app->AppName}}</td>
                                <td>{{$report->RptName}}</td>
                                <td>{{$report->RptSubject}}</td>
                                <td>{{$report->RptHH}}</td>
                                @if($report->ReportEntryIndicator==1)
                                    <td>{{'Single Day'}}</td>
                                @elseif($report->ReportEntryIndicator==7)
                                    <td>{{'Mon-Sun'}}</td>
                                @elseif($report->ReportEntryIndicator==6)
                                    <td>{{'Mon-Sat'}}</td>
                                @elseif($report->ReportEntryIndicator==5)
                                    <td>{{'Mon-Fri'}}</td>
                                @elseif($report->ReportEntryIndicator==9)
                                    <td>{{'Monthly'}}</td>
                                @else
                                    <td>N/A</td>
                                @endif
                                @if($report->RptEntryDayIndicator >'0' and $report->RptEntryDayIndicator <='7')
                                    <td>{{\App\CommonUtils\CommonStrings::$days[$report->RptEntryDayIndicator]}}</td>
                                @else
                                    <td>N/A</td>
                                @endif
                                <td>{{$report->Status=='L'?'Live':'Down'}}</td>
                                <td><a href="#"
                                       onclick="editReport({{$report->RptID}}{{','}}{{$report->AppID}}{{',\''}}{{$report->RptName}}{{'\',\''}}{{$report->RptSubject}}{{'\','}}{{$report->RptHH}}{{','}}{{$report->ReportEntryIndicator}}{{','}}{{$report->RptEntryDayIndicator}}{{',\''}}{{$report->Status}}{{'\''}})"><span
                                                class="btn btn-xs"><i
                                                    class="fa fa-pencil"></i></span></a>

                                    <a class="btn bt-xs text-red" href="#" title="Delete"
                                       onclick="messageBox(this,function(){loader('show');window.location='{{route('report.delete',$report->RptID)}}';})"
                                       data-type="warning" data-text="You will not be able to recover this record!"
                                       data-confirm-text="Yes, delete it!"
                                       data-success-text="Your record has been deleted!"
                                       data-href="{{route('report.delete',$report->RptID)}}" data-toggle="tooltip"
                                       data-placement="top"><i class="fa fa-times"></i></a>
                                   </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('ivr.report.partials.addEdit-modal')
@endsection
@section('custom-js')
    <script type="text/javascript" src="/js/report/report.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#html_table').DataTable(
                {
                    dom: 'Bfrtip',
                    //rowReorder: true,
                    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    buttons: [
                        /*  {
                              extend: 'copy',
                              className: 'btn btn-primary'

                          }
                          , {
                              extend: 'csv',
                              className: 'btn btn-primary'
                          }
                          , {
                              extend: 'excel',
                              className: 'btn btn-primary'
                          }
                          , {
                              extend: 'pdf',
                              className: 'btn btn-primary'
                          }
                          , {
                              extend: 'print',
                              className: 'btn btn-primary'
                          },*/
                        {
                            extend: 'colvis',
                            className: 'btn btn-danger fa fa-columns',
                            text: ''

                        }
                    ]
                }
            );
        });
    </script>
@endsection