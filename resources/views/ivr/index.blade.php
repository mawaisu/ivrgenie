@extends('ivr.layouts.master')
@section('page_title')
    Dashboard
@endsection
@section('custom-styles')

    <style>
        ul.dt-button-collection.dropdown-menu {
            opacity: 1;
        }
    </style>
@endsection
@section('content')
    <div class="row no-gutter noLRmargin">
        <div class="col-md-12">
            <div class="pull-left">
                <h3 class="inline-element pull-left no-MarginPadding">Dashboard</h3>
            </div>
        </div>
    </div>
    <div class="row no-gutter">
        <div class="col-md-12">
            @include('ivr.layouts.error')
            @include('ivr.layouts.success')
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-leftPadding">
                <div class="dashboard-stat2 boxBorder boxOne">


                    <div class="display">
                        <div class="number">
                            <h3>
                                    <span id="favApp1Count">
                                              @if(isset($response->favoriteApps[0]))
                                            {{$response->favoriteApps[0]->count}}
                                        @endif
                                    </span>
                            </h3>
                            <small id="favApp1Name"> @if(isset($response->favoriteApps[0]))
                                    {{$response->favoriteApps[0]->name}}    @endif</small>
                            <div class="icon"></div>
                        </div>


                    </div>

                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-leftPadding no-RightPadding-res">
                <div class="dashboard-stat2 boxBorder boxTwo">


                    <div class="display">
                        <div class="number">
                            <h3>
                                    <span id="favApp2Count">  @if(isset($response->favoriteApps[1]))
                                            {{$response->favoriteApps[1]->count}}
                                        @endif
                                    </span>
                            </h3>
                            <small id="favApp2Name">
                                @if(isset($response->favoriteApps[1]))
                                    {{$response->favoriteApps[1]->name}}
                                @endif
                            </small>
                            <div class="icon"></div>
                        </div>


                    </div>

                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-leftPadding">
                <div class="dashboard-stat2 boxBorder boxThree">

                    <div class="display">
                        <div class="number">
                            <h3>

                                    <span id="favApp3Count">
                                          @if(isset($response->favoriteApps[2]))
                                            {{$response->favoriteApps[2]->count}}  @endif</span>
                            </h3>
                            <small id="favApp3Name">@if(isset($response->favoriteApps[2])){{$response->favoriteApps[2]->name}}  @endif</small>
                            <div class="icon"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-leftPadding no-RightPadding-res">
                <div class="dashboard-stat2 boxBorder boxFour">

                        <div class="display">
                            <div class="number">
                                <h3>
                                    <span id="favApp4Count">
                                        @if(isset($response->favoriteApps[3]))
                                        {{$response->favoriteApps[3]->count}}
                                        @endif
                                    </span>
                                </h3>
                                <small id="favApp4Name">
                                    @if(isset($response->favoriteApps[3]))
                                    {{$response->favoriteApps[3]->name}}
                                    @endif
                                </small>
                                <div class="icon"></div>
                            </div>

                        </div>

                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-leftPadding  no-RightPadding-res bottomMargin">
                <div class="portlet boxBorder" style="margin-bottom:5px">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject">Applications</span>
                        </div>

                        {{--     <div class="actions">
                                   <div class="btn-group">
                                       <a class="btn btn-default btn-sm" href="javascript:;" data-toggle="dropdown">
                                           <i class="fa fa-bars"></i>
                                       </a>
                                       <ul class="dropdown-menu pull-right">
                                           <li>
                                               <a href="javascript:;"> <i class="fa fa-print"></i> Print </a>
                                           </li>
                                           <li class="divider"> </li>
                                           <li>
                                               <a href="javascript:;"> <i class="fa fa-file-image-o"></i> Export PNG </a>
                                           </li>
                                           <li>
                                               <a href="javascript:;"> <i class="fa fa-file-image-o"></i> Export Jpg </a>
                                           </li>
                                           <li>
                                               <a href="javascript:;"> <i class="fa fa-file-image-o"></i> Export Pdf </a>
                                           </li>
                                           <li>
                                               <a href="javascript:;"> <i class="fa fa-file-image-o"></i> Export Svg </a>
                                           </li>
                                       </ul>
                                   </div>
                               </div>--}}

                    </div>
                    <div class="portlet-body" id="applications" style="padding-bottom:0px">

                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-leftPadding no-RightPadding-res no-gutter">
                <div class="portlet boxBorder">
                    <div class="portlet-title no-bottomMargin">
                        <div class="caption">
                            <span class="caption-subject">Today Calls Stats</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row no-gutter">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                @if(isset($response->totalCalls))
                                    <div class="serverBox"><h3 id="totalCalls1">{{$response->totalCalls}}</h3>
                                        <small></small>
                                    </div>
                                    <div class="serverTotal">Total:{{$response->totalCalls}}
                                        <span id="totalCalls2"></span>
                                        @endif
                                    </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                @if(isset($response->inbound))
                                    <div class="serverBox serverBoxMiddle"><h3 id="inBound1">{{$response->inbound}}</h3>
                                        <small></small>
                                    </div>
                                    <div class="serverTotal serverThreads">Inbound:{{$response->inbound}}<span
                                                id="inBound2"></span>
                                        @endif
                                    </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                @if(isset($response->outbound))
                                    <div class="serverBox"><h3 id="outBound1">{{$response->outbound}}</h3>
                                        <small></small>
                                    </div>
                                    <div class="serverTotal serverProcesses">Outbound:{{$response->outbound}}<span
                                                id="outBound2"></span></div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 boxBorder">
            <div class="portlet light nopadding" style="margin-bottom:0px">
                <div class="portlet-title no-bottomMargin">
                    <div class="caption">
                        <span class="caption-subject">Recent Calls</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive scroller" style="height:887px;">
                        <table class="table no-bottomMargin" id="recentCallTable">
                            <thead>
                            <tr>
                                <th>Application Name</th>
                                <th>DNIS</th>
                                <th>Call-In Time</th>
                                <th>Call-End Time</th>
                                <th>Call Duration</th>
                                <th>Hangup Code</th>

                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($response->recentCalls))
                                @foreach($response->recentCalls as $recentCall)
                                    <tr>
                                        <td>{{$recentCall->appName}}</td>
                                        <td>{{$recentCall->dnis}}</td>
                                        <td>{{$recentCall->callInTime}}</td>
                                        <td>{{$recentCall->callEndTime}}</td>
                                        <td>{{$recentCall->callDuration}}</td>
                                        <td>{{$recentCall->hangupCode}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <?php
    $data=array();
    foreach($response->apps as $app)
    {
        $obj=new stdClass();
        $obj->name=$app->name;
        $obj->y=(int)$app->y;
        $data[]=	$obj;
    }
    ?>

@endsection
@section('custom-js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>
    <script src="/js/highcharts/highcharts.js"></script>
    <script src="/js/highcharts/highcharts-3d.js"></script>
    <script src="/js/highcharts/exporting.js"></script>
    <script src="/js/dashboard/dashboard.js"></script>
    <script type="text/javascript">

        var appsData =<?php  echo  json_encode($data); ?>;

        $('#applications').highcharts({
            credits: {
                enabled: false
            },
            chart: {
                backgroundColor: '#ffffff',
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ' '
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: 'black'
                        }
                    }
                }
            },
            series: [{
                name: 'Application',
                colorByPoint: true,
                data: appsData
            }]
        });
    </script>
@endsection
