@extends('ivr.layouts.master')
@section('page_title')
    TFN Inventory
@endsection
@section('custom-styles')
    <style>
        ul.dt-button-collection.dropdown-menu {
            opacity: 1;
        }
    </style>
@endsection
@section('content')
    <div class="portlet light page-header">
        <div class="row no-gutter">
            @include('ivr.tfn.partials.heading')
        </div>
    </div>
    <div class="row portlet light no-gutter">
        <div class="col-md-12">
            @include('ivr.layouts.error')
            @include('ivr.layouts.success')
            <div class="table-responsive">
                <table class="table table-hover table-striped" data-page-length='10' id="html_table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>App Name</th>
                        <th>Owner</th>
                        <th>Dnis</th>
                        <th>Status</th>
                        <th>Tfn</th>
                        <th>Type</th>
                        <th>Remarks</th>
                        <th class="action-column">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($tfns))
                        @foreach($tfns as $tfn)
                            <tr>
                                <td>{{$tfn->ID}}</td>
                                <td>{{$tfn->application['AppName']}}</td>
                                <td>{{$tfn->TeamID==null?'Free':$tfn->team->Name}}</td>
                                <td>{{$tfn->DNIS}}</td>
                                @if($tfn->Status=='L')
                                    <td>Live</td>@elseif($tfn->Status=='D')
                                    <td>Down</td>@else
                                    <td>Free</td>@endif
                                <td>{{$tfn->TFN}}</td>
                                @if($tfn->Type=='P')
                                    <td>Production</td>
                                @elseif($tfn->Type=='S')
                                    <td>Staging</td>
                                @else
                                    <td>N/A</td>@endif

                                <td>{{$tfn->Remarks}}</td>
                                <td>
                                    <a href="#"
                                       onclick="editTfn({{$tfn->ID}})"><span
                                                class="btn btn-xs"><i
                                                    class="fa fa-pencil"></i></span></a>
                                    @if($tfn->Status=='L')
                                        <a class="btn bt-xs text-gray" href="#" title="Disable"
                                           onclick="messageBox(this,function(){loader('show');window.location='{{route('tfn.statusChange',$tfn->ID)}}';})"
                                           data-type="warning" data-text="You are about to disable this application!"
                                           data-confirm-text="Yes, disable it!"
                                           data-success-text="Your record has been disabled!"
                                           data-href="{{route('tfn.statusChange',$tfn->ID)}}" data-toggle="tooltip"
                                           data-placement="top"><i class="fa fa-ban app"></i>
                                        </a>
                                    @else
                                        <a class="btn bt-xs text-green" href="#" title="Enable"
                                           onclick="messageBox(this,function(){loader('show');window.location='{{route('tfn.statusChange',$tfn->ID)}}';})"
                                           data-type="warning" data-text="You are about to enable this application!"
                                           data-confirm-text="Yes, enable it!"
                                           data-success-text="Your record has been Enabled!"
                                           data-href="{{route('application.statusChange',$tfn->ID)}}"
                                           data-toggle="tooltip"
                                           data-placement="top"><i class="fa fa-check app"></i>
                                        </a>
                                    @endif
                                    <a class="btn bt-xs text-red" href="#" title="Delete"
                                       onclick="messageBox(this,function(){loader('show');window.location='{{route('tfn.delete',$tfn->ID)}}';})"
                                       data-type="warning" data-text="You will not be able to recover this record!"
                                       data-confirm-text="Yes, delete it!"
                                       data-success-text="Your record has been deleted!"
                                       data-href="{{route('tfn.delete',$tfn->ID)}}" data-toggle="tooltip"
                                       data-placement="top"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('ivr.tfn.partials.addEdit-modal')
@endsection
@section('custom-js')
    <script type="text/javascript" src="/js/tfn/tfn.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#html_table').DataTable(
                {
                    dom: 'Bfrtip',
                    //rowReorder: true,
                    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    buttons: [
                        /*  {
                              extend: 'copy',
                              className: 'btn btn-primary'

                          }
                          , {
                              extend: 'csv',
                              className: 'btn btn-primary'
                          }
                          , {
                              extend: 'excel',
                              className: 'btn btn-primary'
                          }
                          , {
                              extend: 'pdf',
                              className: 'btn btn-primary'
                          }
                          , {
                              extend: 'print',
                              className: 'btn btn-primary'
                          },*/
                        {
                            extend: 'colvis',
                            className: 'btn btn-danger fa fa-columns',
                            text: ''
                        }
                    ]
                }
            );
        });
    </script>
@endsection