<div class="modal fade" id="addTfnPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <form class="form-horizontal" id="addEditTfnForm" action="{{route('tfn.edit')}}" method="post">
        {{csrf_field()}}
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="tfnModalLabel">Add TFN</h4>
                </div>
                <div class="modal-body noSpace">

                    <div class="form-group form-md-line-input">
                        <label class="col-md-4 control-label">Owner</label>
                        <div class="col-md-7">
                            <select name="customerList" id="customerList" class="form-control customerlist myselect"
                                  onchange="getApplications()"  required>
                                <option value="">Select Owner</option>
                                <option value="0">Free</option>
                                @if(!empty($teams))
                                    @foreach($teams as $team)
                                        <option value="{{$team->Team_ID}}">{{$team->Name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div id="customer_application" class="form-group form-md-line-input">
                        <label class="col-md-4 control-label">Application</label>
                        <div class="col-md-7">
                            <select name="customerAppList" id="customerAppList"
                                    class="form-control customerapplist myselect" required>
                                <option value="">Select Application</option>
                            </select>
                        </div>
                        <div id="inlineloader">
                        </div>
                    </div>



                    <div class="form-group form-md-line-input">
                        <label for="dnis" class="col-md-4 control-label">DNIS</label>
                        <div class="col-md-7">
                            <input type="number" class="numeric dnis" name="dnis" id="dnis" max="9999999999" required>
                        </div>
                        <div id="dnisloader">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input" id="availability" style="display: none;">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-7">
                            <strong>DNIS Already Exists!</strong>
                        </div>
                    </div>

                    <div class="form-group form-md-line-input">
                        <label for="tfn" class="col-md-4 control-label">TFN</label>
                        <div class="col-md-7">
                            <input type="number" class="numeric tfn" name="dnistfn" id="dnistfn" max="9999999999"
                                   required>
                        </div>
                        <div id="tfnloader">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input" id="tfnavailability" style="display: none;">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-7">
                            <strong>TFN Already Exists!</strong>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="status" class="col-md-4 control-label">Status</label>
                        <div class="col-md-7">
                            <select class="myselect form-control tfnstatus" name="tfnstatus" id="tfnstatus" required>
                                <option value="">Select a Status</option>
                                <option value="L">Live</option>
                                <option value="D">Down</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="type" class="col-md-4 control-label">Type</label>
                        <div class="col-md-7">
                            <select class="myselect form-control tfntype" name="tfntype" id="tfntype">
                                <option value="S" selected>Staging</option>
                                <option value="P">Production</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="remarks" class="col-md-4 control-label">Remarks</label>
                        <div class="col-md-7">
                            <textarea rows="4" cols="5" class="form-control remarks" name="tfnremarks" id="tfnremarks"
                                      maxlength="250"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!--input type="button" class="btn btn-green submit" value="Save"-->
                    <button type="submit" class="btn btn-success" id="submitButton">Save</button>
                    <button id="cancelmodal" type="button" data-dismiss="modal" class="btn btn-primary">Cancel</button>
                </div>
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
        <input type="hidden" name="teamID" id="teamID" value=""/>
        <input type="hidden" name="userID" id="userID" value=""/>
        <input type="hidden" value="" name="tfnRecordID" id="tfnRecordID">
        <input type="hidden" value="sysadmin" name="role" id="role">
        <input type="hidden" value="user" name="role" id="role">
        <input type="hidden" value="0" name="role" id="role">
    </form>
</div>