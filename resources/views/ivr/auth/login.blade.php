{{--

'/js/auth/auth.js';--}}
@extends('ivr.layouts.auth.master')
@section('content')
<div class="logo center-block"></div>
<div class="formContainer">
    <p class="text-center">Sign in to continue to IBEX IVR</p>
    <form role="form" class="form-inline" action = "{{route('auth.login')}}" method="post">
        {{csrf_field()}}
        @include('ivr.layouts.error')
        @include('ivr.layouts.success')
        <div class="input-group top slashBottom">
            <div class="input-group-addon"><i class="fa fa-user"></i></div>
            <input type="text" id="id" name="id" class="form-control" placeholder="NT Login ID" required maxlength="15">
        </div>
        <div class="input-group slashBottom">
            <div class="input-group-addon"><i class="fa fa-lock"></i></div>
            <input type="password" id="pass" name="pass" class="form-control" placeholder="Password" required>
        </div>
        {{--<div class="input-group bottom">
            <div class="input-group-addon"><i class="fa fa-globe"></i></div>
            <select id="domain" name="domain" class="form-control" style="background:#F8F8F8 !important">
                <option value="trgworld">TRGWORLD</option>
                <option value="corp" selected>CORP</option>
            </select>
        </div>--}}

        <div class="form-group form-md-checkboxes">
            <div class="md-checkbox remembercheck" style="line-height: 20px">
                <input type="checkbox" id="remember" class="md-check">
                <label for="remember">
                    <span></span>
                    <span class="check"></span>
                    <span class="box"></span> Remember me </label>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-danger">Sign In</button>
        </div>

    </form>

    <div class="lblforgetPass">

        <a href="" class="forgotpwd">Forgot Password?</a>

    </div>
</div> <!-- /.formContainer -->

<div class="text-center">
    Don't have an account yet? <a href="">Sign Up</a>
</div>

@endsection