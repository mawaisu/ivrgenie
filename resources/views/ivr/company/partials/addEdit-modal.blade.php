<div class="modal fade" id="addCompanyPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form class="form-horizontal" id="addCompanyForm" action="{{route('company.edit')}}" method="post">
        {{csrf_field()}}
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="companyTitle">Add Company</h4>
                </div>
                <div class="modal-body noSpace">
                    <div class="form-group form-md-line-input">
                        <label for="app_abbrv" class="col-md-4 control-label">Company Name</label>
                        <div class="col-md-8">
                            <input maxlength="150" type="text" class="form-control companyName" name="companyName" id="companyName" required>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="app_abbrv" class="col-md-4 control-label">Description</label>
                        <div class="col-md-8">
                            <textarea maxlength="250" class="form-control appabrv" rows="5" name="companyDesc" id="companyDesc" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!--input type="button" class="btn btn-green submit" value="Save"-->
                    <button type="submit" id="submitButton" class="btn btn-success">Save</button>
                    <button type="button" id="cancelmodal" data-dismiss="modal" class="btn btn-primary">Cancel</button>
                </div>
            </div>
        </div>
        <input type="hidden"  value="" name="companyID" id="companyID" >
    </form>
</div>