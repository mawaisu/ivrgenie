<!--====== CONTENT ======-->
<div class="modal fade" id="editFavoriteAppPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <form class="form-horizontal" id="editUser" action="{{route('favorite.app.edit')}}" method="post">
        {{csrf_field()}}
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Add Favorite Applications</h4>
                </div>
                <div class="modal-body noSpace">
                    <div class="form-group form-md-line-input">
                        <label for="AppID" class="col-md-3 control-label">Applications</label>
                        <div class="col-md-7">
                            <select class="form-control myselect" id="AppID" name="AppID" required=true
                                    class='form-control'>
                                <option value="0">Select an application</option>
                                @if(!empty($applications))
                                    @foreach($applications as $application)
                                        <option value="{{$application['AppID']}}">{{$application['AppName']}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="userID" id="userID" value="">
                    <button type="submit" id="submitButton" class="btn btn-success">Save</button>
                    <button type="button" id="cancelmodal" data-dismiss="modal" class="btn btn-primary">Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>