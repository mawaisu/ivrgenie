<!--====== CONTENT ======-->
<div class="modal fade" id="editUserPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <form class="form-horizontal" id="editUser" action="{{route('user.edit')}}" method="post">
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="userTitle">Edit User</h4>
                </div>
                <div class="modal-body noSpace">

                    <div class="form-group form-md-line-input">
                        <label for="userName" class="col-md-3 control-label">User Name</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" maxlength="150" name="username" id="userName"
                                   value="" readonly>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label">First Name</label>

                        <div class="col-md-8">
                            <input class="form-control" type="text" maxlength="150" name="firstName" id="firstName"
                                   value="">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label">Last Name</label>

                        <div class="col-md-8">
                            <input class="form-control" type="text" maxlength="150" name="lastName" id="lastName" value="">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="" class="col-md-3 control-label">Role</label>
                        <div class="col-md-8">
                            <select id="role" name="role" class="form-control" required>
                                @if(!empty($roles))
                                    <option value="">Please Select Role</option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                @endif
                            </select>

                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="" class="col-md-3 control-label">Company</label>
                        <div class="col-md-8">
                            <select id="company" name="company" class="form-control" required onchange="populateTeams()">
                                @if(!empty($companies))
                                    <option value="">Please Select Company</option>
                                    @foreach($companies as $company)
                                        <option value="{{$company->ID}}">{{$company->Name}}</option>
                                    @endforeach
                                @endif
                            </select>

                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="" class="col-md-3 control-label">Default Team</label>
                        <div class="col-md-8">
                            <select class="form-control" name="teams" id="teams" required="true">
                                <option value="">Select your team</option>

                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="userID" id="userID" value="">
                    <button type="submit" id="submitButton" class="btn btn-success">Save</button>
                    <button type="button" id="cancelmodal" data-dismiss="modal" class="btn btn-primary">Cancel</button>
                </div>
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->

        {{csrf_field()}}
    </form>
</div>