@extends('ivr.layouts.master')
@section('page_title')
    Manage Users
@endsection
@section('custom-styles')
    <style>
        ul.dt-button-collection.dropdown-menu {
            opacity: 1;
        }
    </style>
@endsection
@section('content')
    <div class="portlet light page-header">
        <div class="row no-gutter">
            @include('ivr.users.partials.heading')
        </div>
    </div>
    <div class="row portlet light no-gutter">
        <div class="col-md-12">
            @include('ivr.layouts.error')
            @include('ivr.layouts.success')
            <div class="table-responsive">
                <table class="table table-hover" data-page-length='10' id="html_table">
                    <thead>
                    <tr>
                        <th>User Name</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Role</th>
                        <th>Team</th>
                        <th>Company</th>
                        <th class="action-column">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($users))

                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->username}}</td>
                                <td>{{$user->firstname}}</td>
                                <td>{{$user->lastname}}</td>
                                <td>{{$user->role->name}}</td>
                                <td>{{$user->team->Name}}</td>
                                <td>{{$user->company->Name}}</td>
                                <td><a
                                            href="#" onclick="editUser({{$user->id}})">
                                        <span class="btn btn-xs"><i
                                                    class="fa fa-pencil"></i></span></a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('ivr.users.partials.addEdit-modal')
@endsection
@section('custom-js')
    <script type="text/javascript" src="/js/users/users.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#html_table').DataTable(
                {
                    dom: 'Bfrtip',
                    //rowReorder: true,
                    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    buttons: [
                        /*  {
                              extend: 'copy',
                              className: 'btn btn-primary'

                          }
                          , {
                              extend: 'csv',
                              className: 'btn btn-primary'
                          }
                          , {
                              extend: 'excel',
                              className: 'btn btn-primary'
                          }
                          , {
                              extend: 'pdf',
                              className: 'btn btn-primary'
                          }
                          , {
                              extend: 'print',
                              className: 'btn btn-primary'
                          },*/
                        {
                            extend: 'colvis',
                            className: 'btn btn-danger fa fa-columns',
                            text: ''

                        }
                    ]
                }
            );
        });
    </script>
@endsection