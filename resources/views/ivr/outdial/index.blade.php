@extends('ivr.layouts.master')
@section('page_title')
    CWO Out-Dial
@endsection
@section('custom-styles')
    <style>
        ul.dt-button-collection.dropdown-menu {
            opacity: 1;
        }
    </style>
@endsection
@section('content')
    <div class="portlet light page-header">
        <div class="row no-gutter">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 pull-left">
                <h3 class="inline-element pull-left">CWO Out-Dial</h3>
            </div>
        </div>
    </div>
    <div class="row portlet light no-gutter">
        <div class="col-md-12">
            @include('ivr.layouts.error')
            @include('ivr.layouts.success')

            <form class="form-horizontal" id="cwoForm" action="{{route('cwo.call')}}" method="post">
                {{csrf_field()}}
                <div class="modal-dialog lg">
                    <div class="modal-content">
                        <div class="modal-header">

                            <h4 class="modal-title" id="teamTitle">CWO Dial</h4>
                        </div>
                        <div class="modal-body noSpace">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-3 control-label" for="serverIp">Outdial server IP</label>
                                <div class="col-md-8">
                                    <select id="serverIp" class="form-control myselect" name="serverIp"
                                            required="true">
                                        <option value="">Please Select a Server</option>
                                        @foreach(\App\CommonUtils\CommonStrings::$outDialServers as $value)
                                            <option value="{{$value}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group form-md-line-input">
                                <label for="name" class="col-md-3 control-label">Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="name" maxlength="150"
                                           id="name" required>
                                </div>
                            </div>

                            <div class="form-group form-md-line-input">
                                <label for="number" class="col-md-3 control-label">Number To Dial</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="number" maxlength="150"
                                           id="number" required>
                                </div>
                            </div>

                            <div class="form-group form-md-line-input">
                                <label for="param1" class="col-md-3 control-label">Param 1:</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="param1" maxlength="150"
                                           id="param1" required placeholder="Policy Number">
                                </div>
                            </div>

                            <div class="form-group form-md-line-input">
                                <label for="param2" class="col-md-3 control-label">Param 2:</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="param2" maxlength="150"
                                           id="param2" required placeholder="Policy Group">
                                </div>
                            </div>

                            <div class="form-group form-md-line-input">
                                <label class="col-md-3 control-label" for="extension">Extension</label>
                                <div class="col-md-8">
                                    <select id="extension" class="form-control myselect" name="extension"
                                            required="true">
                                        <option value="">Please Select an Extension</option>
                                        @foreach(\App\CommonUtils\CommonStrings::$extensions as $value)
                                            <option value="{{$value}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">

                            <button type="submit" id="submitButton" class="btn btn-success">Dial <i class="icon-call-out"></i> </button>
                            </button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection
@section('custom-js')

@endsection