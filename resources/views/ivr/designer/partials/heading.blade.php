<div class="col-xs-12 col-sm-6 col-md-9 col-lg-9 pull-left">
    <h3 class="inline-element pull-left">IVR For</h3>
    <div class="btn-group ivrlist ivrDesignerBar no-topPadding">
        <div class="form-group form-md-line-input">
            <select class="form-control myselect" id="ivrList" data-size="8">
                <option value="0">Select an (IVR)</option>
                @if(!empty($data))
                    @foreach ($data as $row)
                        <option value="{{$row->AppID}}"/>{{$row->AppName}}</option>
                    @endforeach
                @endif
            </select>
            <div class="form-control-focus"></div>
        </div>
    </div>
    <div id="tfnNumbers" style="display:inline"></div>
</div>