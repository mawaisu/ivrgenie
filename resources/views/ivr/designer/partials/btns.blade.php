<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
    <div class="pull-right">
        <input type="button" class="btn btn-primary" value="Copy IVR" onclick="copyIVR();">
        <!-- 	<input type="button" class="btn btn-primary toggle-btn" value="Show Xml Editor"> -->
        <input type="button" class="btn btn-success" data-success-text="Your record has been saved!"
               data-confirm-text="Yes" data-text="Are you sure, you want to save this IVR" title="Save!"
               data-type="warning" onclick="messageBox(this,function(){treeDesigner.saveIVR();});"
               value="Save">
    </div>
</div>