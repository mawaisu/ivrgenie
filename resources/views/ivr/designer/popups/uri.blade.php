<?php
   /* 
   * To change this license header, choose License Headers in Project Properties.
   * To change this template file, choose Tools | Templates
   * and open the template in the editor.
   */
   ?>
<div class="modal fade" id="uriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">URI</h4>
            {{csrf_field()}}
         </div>
         <form class="" id="uriForm" action="" method="post">
            <div class="modal-footer modal-head">
               <div class="row no-gutter">
                  <div class="col-md-3">
                     <div class="form-group form-md-line-input">
                        <label class="col-md-4 control-label" for="uriID">ID</label>
                        <div class="col-md-8">
                           <input type="text" class="form-control input-sm prompt_ID" name="uriID" maxlength="5"  id="uriID">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="uriText">URL</label>
                        <div class="col-md-9">
                           <input type="text" class="form-control prompt_text" name="uriText" id="uriText" maxlength="150" placeholder="">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group form-md-line-input">
                        <label for="uriTimeOut" class="col-md-4 control-label">Timeout</label>
                        <div class="col-md-8">
                           <select  name="uriTimeOut"  id="uriTimeOut" class="form-control start_ivr myselect">
                              <option value="3000">3</option>
                              <option value="4000">4</option>
                              <option value="5000">5</option>
                              <option value="6000">6</option>
                              <option value="7000">7</option>
                              <option value="8000">8</option>
                              <option value="9000">9</option>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row no-gutter">
                  <div class="col-md-3">
                     <div class="form-group form-md-line-input">
                        <label for="uriGoto" class="col-md-4 control-label">Go to</label>
                        <div class="col-md-8">
                           <select class="form-control BR myselect" name="uriGoto" id="uriGoto" name="uriGoto">
                              <option value="0">Hangup</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-2 col-md-offset-1">
                     <div class="form-group form-md-checkboxes">
                        <div class="md-checkbox-list">
                           <div class="md-checkbox">
                              <input type="checkbox" name="continueHangup" id="continueHangup" class="md-check">
                              <label for="continueHangup">
                                 <span></span>
                                 <span class="check"></span>
                                 <span class="box"></span> Hangup Continue </label>
                              </div>
                        </div>
                      </div>
                  </div>
                     </div>
                  </div>
               </form>
               <div class="modal-body">
                  <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
                     <!-- Nav tabs -->
                     <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#inputParam" aria-controls="inputParam" role="tab" data-toggle="tab">Input Parameters</a></li>
                        <li role="presentation"><a href="#outputParam" aria-controls="outputParam" role="tab" data-toggle="tab">Output Parmeters</a></li>
                        <li role="presentation"><a href="#statusParam" aria-controls="statusParam" role="tab" data-toggle="tab">Status Parmeters</a></li>
                     </ul>
                     <!-- Tab panes -->
                     <div class="tab-content scroller" style="height:200px">
                        <div role="tabpanel" class="tab-pane active" id="inputParam">
                           <div class="table-responsive">
                              <form id="inputParamForm" action="" method="post">
                                 <table class="table table-condensed" id="inputParamTable">
                                    <thead>
                                       <tr>
                                          <th width="220px">Parameter Name</th>
                                          <th></th>
                                          <th></th>
                                          <th></th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <tr id="inputParamRow">
                                          <td>
                                             <div class="form-group form-md-line-input">
                                                <select class="form-control BR  myselect"  onchange="treeDesigner.crud.setInputParam(event);"  name="inputParamList" id="inputParamList">
                                                   <option value="0">Create New</option>
                                                </select>
                                             </div>
                                          </td>
                                          <td>
                                             <div class="form-group form-md-line-input">
                                                <input type="text"  class="form-control" placeholder="Enter input paramater"   id="inputParamNameHolder" name="inputParamNameHolder">
                                             </div> 
                                          </td>
                                          <td>
                                             <div class="form-group form-md-line-input">
                                                <input type="text"  class="form-control" placeholder="Enter value"   id="inputParamValueHolder" name="inputParamValueHolder">
                                             </div> 
                                          </td>
                                          <td class="paddingtop">
                                             <button class="btn btn-xs actionbuttons" type="button" title="Add Record" onclick="treeDesigner.crud.addInputParamRow();"><i class="fa fa-plus"></i></button>
                                          </td>
                                       </tr>
                                       <tr id="inputParamTemplate" class="hidden" data-param-type="VAR">
                                          <td>
                                             <div class="form-group form-md-line-input">
                                                <input type="text" readonly class="form-control"   id="inputParamName" name="inputParamName">
                                             </div>   
                                          </td>
                                          <td>
                                             <div class="form-group form-md-line-input">
                                                <input type="text" readonly class="form-control" id="inputParamValue" name="inputParamValue">
                                             </div>
                                          </td>
                                          <td class="paddingtop" >
                                             <button class="btn btn-xs actionbuttonsmedium" type="button" title="Move Up"  onclick="moveRowUp(this);" ><i class="fa fa-long-arrow-up"></i></button>
                                             <button class="btn btn-xs actionbuttonsmedium lastaction" type="button" title="Move Down"  onclick="moveRowDown(this);" ><i class="fa fa-long-arrow-down"></i></button>
                                             <button class="btn btn-xs actionbuttons" onclick="treeDesigner.crud.removeInputParamRow(this)" type="button" title="Remove Record"><i class="fa fa-minus"></i></button>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </form>
                           </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="outputParam">
                           <div class="table-responsive">
                              <form class="" id="outputParamForm" action="" method="post">
                                 <table id="outputParamTable" class="table table-condensed">
                                    <thead>
                                       <tr>
                                          <th width="220px">Parameter Name</th>
                                          <th></th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <tr id="outputParamRow">
                                          <td>
                                             <div class="form-group form-md-line-input">
                                                <select class="form-control BR  myselect"  onchange="treeDesigner.crud.setOutputParam(event);"  name="outputParamList" id="outputParamList">
                                                   <option value="0">Create New</option>
                                                </select>
                                             </div>  
                                          </td>
                                          <td>
                                             <div class="form-group form-md-line-input">
                                                <input type="text"  class="form-control" placeholder="Enter name of parameter"   id="outputParamNameHolder" name="outputParamNameHolder">
                                             </div>   
                                          </td>
                                          <td>
                                             <div class="form-group form-md-line-input">
                                                <input type="text"  class="form-control" placeholder="Enter value"   id="outputParamValueHolder" name="outputParamValueHolder">
                                             </div>
                                          </td>
                                          <td class="paddingtop" >
                                             <button class="btn btn-xs actionbuttons" type="button" title="Add Record" onclick="treeDesigner.crud.addOutputParamRow();"><i class="fa fa-plus"></i></button>
                                          </td>
                                       </tr>
                                       <tr id="outputParamTemplate" class="hidden">
                                          <td>
                                             <div class="form-group form-md-line-input">
                                                <input type="text" readonly class="form-control" placeholder="Enter name of parameter"   id="outputParamName" name="outputParamName">
                                             </div>
                                             <td>
                                                <div class="form-group form-md-line-input">
                                                   <input type="text" readonly class="form-control" id="outputParamValue" name="outputParamValue">
                                                </div>
                                             </td>
                                             <td class="paddingtop" >
                                                <button class="btn btn-xs actionbuttonsmedium" type="button"  onclick="moveRowUp(this)"  title="Move Up"><i class="fa fa-long-arrow-up"></i></button>
                                                <button class="btn btn-xs actionbuttonsmedium lastaction" onclick="moveRowDown(this)" type="button" title="Move Down"><i class="fa fa-long-arrow-down"></i></button>
                                                <button class="btn btn-xs actionbuttons" onclick="treeDesigner.crud.removeOutputParamRow(this)" type="button" title="Remove Record"><i class="fa fa-minus"></i></button>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </form>
                              </div>
                           </div>
                           <div role="tabpanel" class="tab-pane" id="statusParam">
                              <div class="table-responsive">
                                 <form id="statusParamForm" action="" method="post">
                                    <table class="table table-condensed" id="statusParamTable">
                                       <thead>
                                          <tr>
                                             <th width="220px">Parameter Name</th>
                                             <th></th>
                                             <th></th>
                                             <th></th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr id="statusParamRow">
                                             <td>
                                                <div class="form-group form-md-line-input">
                                                   <select class="form-control BR myselect "  onchange="treeDesigner.crud.setStatusParam(event);"  name="statusParamList" id="statusParamList">
                                                      <option value="0">Create New</option>
                                                   </select>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group form-md-line-input">
                                                   <input type="text"  class="form-control" placeholder="Enter input paramater"   id="statusParamNameHolder" name="statusParamNameHolder">
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group form-md-line-input">
                                                   <input type="text"  class="form-control" placeholder="Enter value"   id="statusParamValueHolder" name="statusParamValueHolder">
                                                </div>
                                             </td>
                                             <td class="paddingtop">
                                                <button class="btn btn-xs actionbuttons" type="button" title="Add Record" onclick="treeDesigner.crud.addStatusParamRow();"><i class="fa fa-plus"></i></button>
                                             </td>
                                          </tr>
                                          <tr id="statusParamTemplate" class="hidden" data-param-type="VAR">
                                             <td>
                                                <div class="form-group form-md-line-input">
                                                   <input type="text" readonly class="form-control"   id="statusParamName" name="statusParamName">
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group form-md-line-input">
                                                   <input type="text" readonly class="form-control" id="statusParamValue" name="statusParamValue">
                                                </div>
                                             </td>
                                             <td class="paddingtop" >
                                                <button class="btn btn-xs actionbuttonsmedium" type="button" title="Move Up"  onclick="moveRowUp(this);" ><i class="fa fa-long-arrow-up"></i></button>
                                                <button class="btn btn-xs actionbuttonsmedium lastaction" type="button" title="Move Down"  onclick="moveRowDown(this);" ><i class="fa fa-long-arrow-down"></i></button>
                                                <button class="btn btn-xs actionbuttons" onclick="treeDesigner.crud.removeStatusParamRow(this)" type="button" title="Remove Record"><i class="fa fa-minus"></i></button>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <input id="uriSaveButton" type="button" class="btn btn-success submit" onclick="treeDesigner.crud.saveURI();" value="Save">
                     <button type="button" class="btn btn-primary cancel"  onclick="closeModal('uriModal');">Cancel</button>
                  </div>
               </div>
            </div>
         </div>