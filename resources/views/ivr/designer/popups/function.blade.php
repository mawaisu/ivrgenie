<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="modal fade" id="functionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form class="form-horizontal" id="functionForm" action="" method="post">
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Function</h4>
                    {{csrf_field()}}
                </div>
                <div class="modal-body noSpace">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="function_ID">Function ID</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control prompt_text" name="functionID" id="functionID" maxlength="5" >
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="function_text">Function Text</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control prompt_text" name="functionText" maxlength="150" id="functionText" placeholder="">
                        </div>
                    </div>
                    
                    
                    <div class="form-group form-md-line-input">
                        <label for="functionType" class="col-md-3 control-label">Type</label>
                        <div class="col-md-8">
                            <select class="form-control apptype myselect" name="goto" id="functionType" name="functionType">
                                <option value="1">Mon To Sun</option>
                                <option value="3">Mon To Fri</option>
                                <option value="5">Mon To Sat</option>
                            </select>
                        </div>
                    </div>
                    <div class="row no-gutter">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-6 control-label" for="functionFromHour">From (Hour)</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control prompt_text" name="functionFromHour"  maxlength="2" id="functionFromHour" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-5 control-label" for="functionToHour">To (Hour)</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control prompt_text" name="functionToHour" maxlength="2" id="functionToHour" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutter">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-6 control-label" for="functionFromMin">From (min)</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="functionFromMin"  maxlength="2" id="functionFromMin" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-5 control-label" for="functionToMin">To (min)</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="functionToMin"  maxlength="2" id="functionToMin" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="form-group form-md-line-input">
                        <label for="functionGotoSuccess" class="col-md-3 control-label">On Success</label>
                        <div class="col-md-8">
                            <select class="form-control apptype myselect" name="goto" id="functionGotoSuccess" name="functionGotoSuccess">
                                <option value="0">Hangup</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="functionGotoFailure" class="col-md-3 control-label">On Failure</label>
                        <div class="col-md-8">
                            <select class="form-control apptype myselect" name="goto" id="functionGotoFailure" name="functionGotoFailure">
                                <option value="0">Hangup</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-success submit" value="Save" onclick="treeDesigner.crud.saveFunction();">
                    <button type="button" class="btn btn-primary cancel" onclick="closeModal('functionModal');;" >Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>
