<?php

/* 
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
?>

<div class="modal fade" id="ifModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<form class="" id="ifForm" action="" method="post">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">IF</h4>
			 {{csrf_field()}}
         </div>
         
         <div class="modal-footer modal-head">

         <div class="row no-gutter">
	          <div class="col-md-3">
	          <div class="form-group form-md-line-input">
	              <label class="col-md-4 control-label" for="ifID">ID</label>
	              <div class="col-md-8">
	                  <input type="text" class="form-control input-sm prompt_ID" name="ifID" maxlength="5"  id="ifID">
	              </div>
	          </div>
	          </div>

	          <div class="col-md-8">
	          <div class="form-group form-md-line-input">
	              <label class="col-md-2 control-label" for="ifText">IF Text</label>
	              <div class="col-md-10">
	                  <input type="text" class="form-control prompt_text" name="ifText" id="ifText" maxlength="150">
	              </div>
	          </div>
	          </div>
          </div>

         </div>


		 <div class="modal-body no-topPadding">
					<div class="table-responsive">
					   <table class="table table-striped">
						  <tbody>
							 <tr>
								<th scope="row" style="width:150px;">Left Operand</th>
								<td>
									<div class="form-group form-md-line-input" style="width:150px;display:inline-block">
			                            <select class="form-control BR myselect" style="width:150px;display:inline-block"  name="ifLeftOperand" id="ifLeftOperand" >
			                            <option value="0">Variable</option>
			                            </select>
				                    </div>
				                    <div class="form-group form-md-line-input" style="display:inline-block">
				                        <input type="text" class="form-control hidden input-sm" placeholder="Enter Value" name="ifVLI" maxlength="4" id="ifVLI" style="width:150px;display:inline-block">
				                    </div>
				                    <div class="form-group form-md-line-input" style="display:inline-block">
				                        <input type="text" class="form-control hidden input-sm" placeholder="Enter Value" name="ifVLS" id="ifVLS" style="width:150px;display:inline-block">
				                    </div>
								</td>
							 </tr>
							 <tr>
								<th scope="row" style="width:150px;">Operator</th>
								<td>
								<div class="form-group form-md-line-input" style="width:150px;display:inline-block">
								   <select class="form-control BR myselect" style="width:150px;" name="ifOperator" id="ifOperator">
									  <option value="0">>=</option>
									  <option value="1"><=</option>
									  <option value="2">==</option>
									  <option value="3"><</option>
									  <option value="4">></option>
								   </select>
								   </div>
								</td>
							 </tr>
							 <tr>
								<th scope="row" style="width:150px;">Right Operand</th>
								<td>
								   <div class="form-group form-md-line-input" style="width:150px;display:inline-block">
			                            <select class="form-control BR myselect" style="width:150px;display:inline-block" name="ifRightOperand" id="ifRightOperand" disabled="true">
			                            <option value="0">Variable</option>
			                            </select>
				                    </div>
				                    <div class="form-group form-md-line-input" style="display:inline-block">
				                        <input type="text" class="form-control hidden input-sm" placeholder="Enter value" name="ifVRI" maxlength="255" id="ifVRI" style="width:150px;display:inline-block">
				                    </div>
				                    <div class="form-group form-md-line-input" style="display:inline-block">
				                        <input type="text" class="form-control hidden input-sm" placeholder="Enter value" name="ifVRS" maxlength="255" id="ifVRS" style="width:150px;display:inline-block"> 
				                    </div>
								</td>
							 </tr>

							 <tr>
								<th scope="row" style="width:150px;">On Success</th>
								<td>
								<div class="form-group form-md-line-input" style="width:150px;display:inline-block">
									<select class="form-control BR myselect" style="width:150px;" name="ifGotoSuccess" id="ifGotoSuccess">
									  <option value="0">Hangup</option>
								   </select>
				                    </div>
								</td>
							</tr>

							<tr>
								<th scope="row" style="width:150px;">On Failure</th>
								<td>
									<div class="form-group form-md-line-input" style="width:150px;display:inline-block">
									<select class="form-control BR myselect" style="width:150px;" name="ifGotoFailure" id="ifGotoFailure">
									  <option value="0">Hangup</option>
								   </select>
				                    </div>
								</td>
							</tr>
						  </tbody>
					   </table>


					</div>
	      
		</div>
		
	<div class="modal-footer">
	<input id="ifSaveButton" type="button" class="btn btn-success submit" onclick="treeDesigner.crud.saveIF();" value="Save">
	<button type="button" class="btn btn-primary cancel"  onclick="closeModal('ifModal');">Cancel</button>
	</div> 
</div>
</div>
	</form> 
	</div>