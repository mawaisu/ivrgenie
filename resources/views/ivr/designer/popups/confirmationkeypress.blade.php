<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="modal fade" id="confirmationKeypressModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form class="form-horizontal" id="play_prompt" action="" method="post">
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Confirmation Keypress</h4>
                    {{csrf_field()}}
                </div>
                <div class="modal-body noSpace">
                    <div class="form-group form-md-line-input">
                        <label for="confirmationKeypress" class="col-md-3 control-label">Keypress</label>
                        <div class="col-md-8">
                            <input type="text" readonly="true" class="form-control keypress" name="confirmationKeypress" id="confirmationKeypress">
                            <input type="hidden" name="confirmationKeypressID" id="confirmationKeypressID" value="">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="confirmationKeypressGoto" class="col-md-3 control-label">Go to</label>
                        <div class="col-md-8">
                            <select class="form-control BR myselect" name="goto" id="confirmationKeypressGoto" name="confirmationKeypressGoto">
                                <option value="0">Hangup</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-success submit" value="Save" onclick="treeDesigner.crud.saveConfirmationKeypress();">
                    <button type="button" class="btn btn-primary cancel" onclick="closeModal('confirmationKeypressModal');" >Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>
