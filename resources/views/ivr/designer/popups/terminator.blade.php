<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="modal fade" id="terminatorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form class="form-horizontal" id="terminatorForm" action="" method="post">
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Call Terminator</h4>
                    {{csrf_field()}}
                </div>
                <div class="modal-body noSpace">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="prompt_ID">Prompt ID</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control prompt_text" name="terminatorpromptID" id="terminatorpromptID" maxlength="5">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="prompt_text">Prompt Text</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control prompt_text" name="terminatorpromptText" maxlength="150" id="terminatorpromptText" placeholder="">
                        </div>
                    </div>
                    
                    <div class="form-group form-md-line-input">
                        <label for="BR" class="col-md-3 control-label">BR</label>
                        <div class="col-md-8">
                            <select class="form-control apptype myselect" name="BR" id="terminatorpromptBargin" name="terminatorpromptBargin">
                                <option value="FALSE">False</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input hidden">
                        <label for="goto" class="col-md-3 control-label">Go to</label>
                        <div class="col-md-8">
                            <select class="form-control apptype myselect" name="goto" id="terminatorpromptGoto" name="terminatorpromptGoto">
                                <option value="0">Hangup</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-success submit" value="Save" onclick="treeDesigner.crud.saveTerminator();">
                    <button type="button" class="btn btn-primary cancel" onclick="closeModal('terminatorModal');;" >Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>
