<?php
   /* 
   * To change this license header, choose License Headers in Project Properties.
   * To change this template file, choose Tools | Templates
   * and open the template in the editor.
   */
   ?>
<div class="modal fade" id="legalModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Legal</h4>

         </div>
         <form class="" id="legalForm" action="" method="post">
            {{csrf_field()}}
            <div class="modal-footer modal-head">
               <div class="row no-gutter">
                  <div class="col-md-3">
                     <div class="form-group form-md-line-input">
                        <label class="col-md-4 control-label" for="legalID">ID</label>
                        <div class="col-md-8">
                           <input type="text" class="form-control input-sm prompt_ID" name="legalID" maxlength="5"  id="legalID">
                           <div class="form-control-focus">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="legalText">Prompt Text</label>
                        <div class="col-md-9">
                           <input type="text" class="form-control prompt_text" name="legalText" id="legalText" maxlength="150" placeholder="">
                           <div class="form-control-focus">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group form-md-line-input">
                        <label for="legalTimeOut" class="col-md-4 control-label">Timeout</label>
                        <div class="col-md-8">
                           <select  name="legalTimeOut"  id="legalTimeOut" class="form-control start_ivr myselect">
                              <option value="1000">1</option>
                              <option value="2000">2</option>
                              <option value="3000">3</option>
                              <option value="4000">4</option>
                              <option value="5000">5</option>
                              <option value="6000">6</option>
                              <option value="7000">7</option>
                              <option value="8000">8</option>
                              <option value="9000">9</option>
                              <option value="10000">10</option>
                              <option value="11000">11</option>
                              <option value="12000">12</option>
                              <option value="13000">13</option>
                              <option value="14000">14</option>
                              <option value="15000">15</option>
                              <option value="16000">16</option>
                              <option value="17000">17</option>
                              <option value="18000">18</option>
                              <option value="19000">19</option>
                              <option value="20000">20</option>
                           </select>
                           <div class="form-control-focus">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row no-gutter">
                  <div class="col-md-3">
                     <div class="form-group form-md-line-input">
                        <label for="legalLength" class="col-md-4 control-label">Length</label>
                        <div class="col-md-8">
                           <select  name="legalLength" id="legalLength" class="form-control start_ivr myselect">
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                              <option value="6">6</option>
                              <option value="7">7</option>
                              <option value="8">8</option>
                              <option value="9">9</option>
                              <option value="10">10</option>
                              <option value="11">11</option>
                              <option value="12">12</option>
                              <option value="13">13</option>
                              <option value="14">14</option>
                              <option value="15">15</option>
                              <option value="16">16</option>
                              <option value="17">17</option>
                              <option value="18">18</option>
                              <option value="19">19</option>
                              <option value="20">20</option>
                           </select>
                           <div class="form-control-focus">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group form-md-line-input">
                        <label for="legalASR" class="col-md-4 control-label">ASR</label>
                        <div class="col-md-8">
                           <select  name="legalASR"  id="legalASR" class="form-control start_ivr myselect">
                              <option value="FALSE">False</option>
                              <option value="TRUE">True</option>
                           </select>
                           <div class="form-control-focus">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group form-md-line-input">
                        <label for="legalGrammer" class="col-md-4 control-label">Grammer</label>
                        <div class="col-md-8">
                           <select  name="legalGrammer" id="legalGrammer" class="form-control start_ivr myselect">
                              <option value="DIGITS">Digits</option>
                              <option value="YESNO">Yes/No</option>
                           </select>
                           <div class="form-control-focus">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group form-md-line-input">
                        <label for="legalKeypressGoto" class="col-md-4 control-label">Go to</label>
                        <div class="col-md-8">
                           <select class="form-control BR myselect" name="goto" id="legalKeypressGoto" name="legalKeypressGoto">
                              <option value="0">Hangup</option>
                           </select>
                           <div class="form-control-focus">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group form-md-line-input">
                        <label for="appType" class="col-md-4 control-label">App-Type</label>
                        <div class="col-md-8">
                           <select  name="appType" id="appType" onchange="appTypeChange()" class="form-control myselect">
                              <option value="S">Standard</option>
                              <option value="C">Custom</option>
                           </select>
                           <div class="form-control-focus">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group form-md-line-input">
                        <label for="appMode" class="col-md-4 control-label">App-Mode</label>
                        <div class="col-md-8">
                           <select  name="appMode" id="appMode" onchange="appModeChange()" class="form-control start_ivr myselect">
                              <option value="S">State</option>
                              <option value="G">Governor</option>
                              <option value="F">Federal</option>
                           </select>
                           <div class="form-control-focus">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group form-md-line-input">
                        <label for="appMode" class="col-md-4 control-label">App-Tel</label>
                        <div class="col-md-8">
                           <select  name="appTel" id="appTel" class="form-control start_ivr myselect">
                              <option value="C">Capital</option>
                              <option value="D">District</option>
                           </select>
                           <div class="form-control-focus">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group form-md-line-input">
                        <label for="appMode" class="col-md-4 control-label">Mode</label>
                        <div class="col-md-8">
                           <select  name="mode" id="mode" onchange="modeChange()" class="form-control start_ivr myselect">
                              <option value="S">Senator</option>
                              <option value="R">Representative</option>
                              <option value="B">Both</option>
                           </select>
                           <div class="form-control-focus">
                           </div>
                        </div>
                     </div>
                  </div>
                   <div class="col-md-3">
                     <div class="form-group form-md-line-input">
                        <label for="appMode" class="col-md-4 control-label">List-Order</label>
                        <div class="col-md-8">
                           <select  name="listOrder" id="listOrder" class="form-control start_ivr myselect" disabled="true">
                              <option value="S">Senator</option>
                              <option value="R">Representative</option>
                           </select>
                           <div class="form-control-focus">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group form-md-line-input">
                        <label for="appState" class="col-md-4 control-label">App-State</label>
                        <div class="col-md-8">
                           <select  name="appState" id="appState" class="form-control myselect">
                              <option selected="selected" value="FD">------------</option>
                              <option value="AL">ALABAMA - AL</option>
                              <option value="AK">ALASKA - AK</option>
                              <option value="AZ">ARIZONA - AZ</option>
                              <option value="AR">ARKANSAS - AR</option>
                              <option value="CA">CALIFORNIA - CA</option>
                              <option value="CO">COLORADO - CO</option>
                              <option value="CT">CONNECTICUT - CT</option>
                              <option value="DE">DELAWARE - DE</option>
                              <option value="DC">DISTRICT OF COLUMBIA - DC</option>
                              <option value="FM">FEDERATED STATES OF MICRONESIA - FM</option>
                              <option value="FL">FLORIDA - FL</option>
                              <option value="GA">GEORGIA - GA</option>
                              <option value="GU">GUAM - GU</option>
                              <option value="HI">HAWAII - HI</option>
                              <option value="ID">IDAHO - ID</option>
                              <option value="IL">ILLINOIS - IL</option>
                              <option value="IN">INDIANA - IN</option>
                              <option value="IA">IOWA - IA</option>
                              <option value="KS">KANSAS - KS</option>
                              <option value="KY">KENTUCKY - KY</option>
                              <option value="LA">LOUISIANA - LA</option>
                              <option value="ME">MAINE - ME</option>
                              <option value="MH">MARSHALL ISLANDS - MH</option>
                              <option value="MD">MARYLAND - MD</option>
                              <option value="MA">MASSACHUSETTS - MA</option>
                              <option value="MI">MICHIGAN - MI</option>
                              <option value="MN">MINNESOTA - MN</option>
                              <option value="MS">MISSISSIPPI - MS</option>
                              <option value="MO">MISSOURI - MO</option>
                              <option value="MT">MONTANA - MT</option>
                              <option value="NE">NEBRASKA - NE</option>
                              <option value="NV">NEVADA - NV</option>
                              <option value="NH">NEW HAMPSHIRE - NH</option>
                              <option value="NJ">NEW JERSEY - NJ</option>
                              <option value="NM">NEW MEXICO - NM</option>
                              <option value="NY">NEW YORK - NY</option>
                              <option value="N1">NEW YORK - N1 Attorney General </option>
                              <option value="NC">NORTH CAROLINA - NC</option>
                              <option value="ND">NORTH DAKOTA - ND</option>
                              <option value="MP">NORTHERN MARIANA ISLANDS - MP</option>
                              <option value="OH">OHIO - OH</option>
                              <option value="OK">OKLAHOMA - OK</option>
                              <option value="OR">OREGON - OR</option>
                              <option value="PW">PALAU - PW</option>
                              <option value="PA">PENNSYLVANIA - PA</option>
                              <option value="PR">PUERTO RICO - PR</option>
                              <option value="RI">RHODE ISLAND - RI</option>
                              <option value="SC">SOUTH CAROLINA - SC</option>
                              <option value="SD">SOUTH DAKOTA - SD</option>
                              <option value="TN">TENNESSEE - TN</option>
                              <option value="TX">TEXAS - TX</option>
                              <option value="UT">UTAH - UT</option>
                              <option value="VT">VERMONT - VT</option>
                              <option value="VI">VIRGIN ISLANDS - VI</option>
                              <option value="VA">VIRGINIA - VA</option>
                              <option value="WA">WASHINGTON - WA</option>
                              <option value="WV">WEST VIRGINIA - WV</option>
                              <option value="WI">WISCONSIN - WI</option>
                              <option value="WY">WYOMING - WY</option>
                              <option value="01">CUSTOM - NMA 3388</option>
                              <option value="99">CUSTOM - VLT 9955</option>
                              <option value="72">CUSTOM - NJ 1225</option>
                              <option value="02">CUSTOM - ANH 1200</option>
                              <option value="03">CUSTOM - NHR 3407 - Reps</option>
                              <option value="04">CUSTOM - NHR 3407 - Reps New</option>
                           </select>
                           <div class="form-control-focus">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group form-md-line-input">
                        <label for="appMode" class="col-md-4 control-label">Custom-Src</label>
                        <div class="col-md-8">      
                           <div class="form-control-focus">
                              <input type="text" class="form-control input-sm" name="customText" id="customText" disabled="true">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-2 col-md-offset-1">
                     <div class="form-group form-md-checkboxes" style="padding-left:11px">
                        <div class="">
                           <div class="md-checkbox">
                              <input type="checkbox" name="zipPlayBack" id="zipPlayBack" class="md-check">
                              <label for="zipPlayBack" style="padding-left:0px; margin-right:50px">
                                 <span style="height:0px; width:0px"></span>
                                 <span class="check"></span>
                                 <span class="box"></span> PlayBack </label>
                              </div>
                        </div>
                      </div>
                  </div>
               </div>
            </div>
            <input name="PLAYPRIORLIST" id="PLAYPRIORLIST" type="hidden" value="filename"/>
            <input name="PLAYPRIORTRANSFER" id="PLAYPRIORTRANSFER" type="hidden" value="filename"/>
         </form> 

      
         
         
         <div class="row top-padding rowBorder modal-footer" style="background:#EBE7E4; padding:2px 4px 4px 10px;border-top:1px #fff solid !important">
                  <form style="padding-top:8px" name="myLegalUploadPriorForm" id="myLegalUploadPriorForm" action="/ajax/extrafile-upload" method="POST" enctype="multipart/form-data">
                     <input type="hidden" id="commandType" name="commandType" value="menu"/>
                     <div class="row no-topPadding">

                        {{csrf_field()}}

                        <div class="col-md-3" style="margin-top:-1px">
                           <div class="form-group form-md-line-input">
                              <label for="extraLanguage" class="col-md-4 control-label">Language</label>
                              <div class="col-md-8">
                                 <select class="form-control language myselect" name="extraLanguage" id="extraLanguage">
                                    <option value="English">English</option>
                                    <option value="Spanish">Spanish</option>
                                    <option value="German">German</option>
                                    <option value="French">French</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-4 form-group">
                           <div class="input-group">
                              <span class="input-group-btn">
                                 <label class="btn btn-primary btn-file" for="multiple_input_group">
                                    <div class="input required">
                                       <input type="hidden" id="extraFileAppId" name="extraFileAppId" value="" />
                                       <input type="hidden" id="extraOwnerPromptId" name="extraOwnerPromptId" value="" />
                                       <input type="hidden" id="extraFilePromptId" name="extraFilePromptId" value="" />
                                       <input name="extraFile_upload" id="extraFile_upload" type="file"/>
                                    </div>
                                    Browse
                                 </label>
                              </span>
                              <span class="file-input-label" style="border:1px #ccc solid;text-align:left"></span>
                           </div>
                        </div>

                        <div class="col-md-3">
                        <div class="md-checkbox-inline" style="margin-top:7px">
                           <div class="md-checkbox">
                               <input type="checkbox" id="list" class="md-check">
                               <label for="list">
                                   <span style="height:0px; width:0px"></span>
                                   <span class="check"></span>
                                   <span class="box"></span> List </label>
                           </div>
                           <div class="md-checkbox">
                               <input type="checkbox" id="transfer" class="md-check">
                               <label for="transfer">
                                   <span style="height:0px; width:0px"></span>
                                   <span class="check"></span>
                                   <span class="box"></span> Transfer </label>
                           </div>
                       </div>
                     </div>

                        <div class="col-md-2">
                           <input type="submit" id="submit-btn" value="Upload" class="btn btn-success"/>
                           <img src="/images/LoaderIcon.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
                        </div>
                     </div>
                  </form>
               </div>



         <div class="modal-body no-topPadding">
            <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
               <!-- Nav tabs -->
               <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#legalInvalidresponsetab" aria-controls="legalInvalidresponsetab" role="tab" data-toggle="tab">Invalid Response</a></li>
                  <li role="presentation"><a href="#legalNoresponsetab" aria-controls="legalNoresponsetab" role="tab" data-toggle="tab">No Response</a></li> 
               </ul>
               <!-- Tab panes -->
               <div class="row top-padding rowBorder ">
                  <form style="display:none;padding-top:8px" name="myLegalUploadExtraForm" id="myLegalUploadExtraForm" action="ajax/extrafile-upload" method="POST" enctype="multipart/form-data">
                     <input type="hidden" id="commandType" name="commandType" value="menu"/>
                     <div class="row no-gutter no-topPadding">
                        <div class="col-md-3">
                           <div class="form-group form-md-line-input">
                              <label for="extraLanguage" class="col-md-4 control-label">Language</label>
                              <div class="col-md-8">
                                 <select class="form-control language myselect" name="extraLanguage" id="extraLanguage">
                                    <option value="English">English</option>
                                    <option value="Spanish">Spanish</option>
                                    <option value="German">German</option>
                                    <option value="French">French</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-6 form-group">
                           <div class="input-group">
                              <span class="input-group-btn">
                                 <label class="btn btn-primary btn-file" for="multiple_input_group">
                                    <div class="input required">
                                       <input type="hidden" id="extraFileAppId" name="extraFileAppId" value="" />
                                       <input type="hidden" id="extraOwnerPromptId" name="extraOwnerPromptId" value="" />
                                       <input type="hidden" id="extraFilePromptId" name="extraFilePromptId" value="" />
                                       <input name="extraFile_upload" id="extraFile_upload" type="file"/>
                                    </div>
                                    Browse
                                 </label>
                              </span>
                              <span class="file-input-label"></span>
                           </div>
                        </div>
                        <div class="col-md-2">
                           <input type="submit" id="submit-btn" value="Upload" class="btn btn-success"/>
                           <img src="/images/LoaderIcon.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
                        </div>
                     </div>
                  </form>
               </div>
               <div class="tab-content scroller" style="height:200px">
                  <div role="tabpanel" class="tab-pane active" id="legalInvalidresponsetab">
                     <div class="table-responsive">
                        <form id="legalInvalidResponseForm" action="" method="post">
                           {{csrf_field()}}
                           <table class="table table-condensed" id="legalInvalidResponseTable">
                              <thead>
                                 <tr>
                                    <th width="100">Prompt ID</th>
                                    <th>Prompt</th>
                                    <th >Action</th>
                                    <th></th>
                                    <th></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr id="legalInvalidRow">
                                    <td>
                                       <div class="form-group form-md-line-input">
                                          <select class="form-control BR myselect " style="width:110px;" onchange="treeDesigner.crud.setLegalInvalidPromptText(event);" name="legalInvalidPromptList" id="legalInvalidPromptList">
                                             <option value="0">Create New</option>
                                          </select>
                                          <div class="form-control-focus"></div>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="form-group form-md-line-input">
                                          <input type="text" class="form-control" placeholder="Prompt" name="legalInvalidPromptText" maxlength="150" id="legalInvalidPromptText">
                                          <div class="form-control-focus"></div>
                                       </div>
                                    </td>
                                    <td  class="paddingtop radioCol">
                                       <label>
                                       <input type="radio" name="legalInvalidAction" checked id="legalInvalidRepeat" value="RP" class="md-radiobtn">
                                       Repeat </label>
                                       <label >
                                       <input type="radio" name="legalInvalidAction" id="legalInvalidGoto" value="0" class="md-radiobtn">
                                       Goto </label>
                                    </td>
                                    <td>
                                       <div class="form-group form-md-line-input">
                                          <select class="form-control BR myselect " style="width:150px;"  name="legalInvalidGotoList" id="legalInvalidGotoList" onchange="treeDesigner.crud.legalInvalidResponseGoto(this);">
                                             <option value="0">Hangup</option>
                                          </select>
                                          <div class="form-control-focus"></div>
                                       </div>
                                    </td>
                                    <td class="paddingtop" align="right">
                                       <button class="btn btn-xs actionbuttons" type="button" title="Add Record" onclick="treeDesigner.crud.addLegalInvalidResponseRow();"><i class="fa fa-plus"></i></button>
                                    </td>
                                 </tr>
                                 <tr id="legalInvalidResponseTemplate" class="hidden">
                                    <td>
                                       <div class="form-group form-md-line-input">
                                          <input type="text" readonly class="form-control" placeholder="Prompt"   id="legalInvalidPromptID" name="legalInvalidPromptID">
                                          <div class="form-control-focus"></div>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="form-group form-md-line-input">
                                          <input type="text" readonly class="form-control" placeholder="Prompt" id="legalInvalidPromptText" name="legalInvalidPromptText">
                                          <div class="form-control-focus"></div>
                                       </div>
                                    </td>
                                    <td  class="paddingtop radioCol">
                                       <label>
                                       <input type="radio" name="legalInvalidAction" id="legalInvalidRepeat" value="RP" class="md-radiobtn">
                                       Repeat </label>
                     </div>
                     <label>
                     <input type="radio" name="legalInvalidAction" id="legalInvalidGoto" value="0" class="md-radiobtn">
                     Goto </label>
                     </td>
                     <td>
                     <div class="form-group form-md-line-input">
                     <select class="form-control BR myselect " style="width:150px;"  name="legalInvalidGotoList" id="legalInvalidGotoList" onchange="treeDesigner.crud.legalInvalidResponseGoto(this);">
                     <option value="0">Hangup</option>
                     </select>
                     <div class="form-control-focus"></div>
                     </div>
                     </td>
                     <td class="paddingtop" align="right">
                     <button type="button" title="Select Primary Language" id="extraFileUploadBtn" onclick="extraFileUploadFormOpener(this)" class="btn btn-xs actionbuttonsmedium lastaction"><i class="fa fa-globe"></i></button>
                      <input name="extraFileName" id="extraFilename" type="hidden" value="filename"/>
                     <button class="btn btn-xs actionbuttonsmedium" type="button" title="Move Up"  onclick="moveRowUp(this);" ><i class="fa fa-long-arrow-up"></i></button>
                     <button class="btn btn-xs actionbuttonsmedium lastaction" type="button" title="Move Down"  onclick="moveRowDown(this);" ><i class="fa fa-long-arrow-down"></i></button>
                     <button class="btn btn-xs actionbuttons" onclick="treeDesigner.crud.removeInvalidResponseRow(this)" type="button" title="Remove Record"><i class="fa fa-minus"></i></button>
                     </td>
                     </tr>
                     </tbody>
                     </table>
                     </form>
                  </div>
               </div>
               <div role="tabpanel" class="tab-pane" id="legalNoresponsetab">
                  <div class="table-responsive">
                     <form class="" id="legalNoResponseForm" action="" method="post">
                        {{csrf_field()}}
                        <table id="legalNoResponseTable" class="table table-condensed">
                           <thead>
                              <tr>
                                 <th width="100">Prompt ID</th>
                                 <th>Prompt</th>
                                 <th >Action</th>
                                 <th></th>
                                 <th></th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr id="legalNoResponseRow">
                                 <td>
                                    <div class="form-group form-md-line-input">
                                       <select class="form-control BR myselect " style="width:110px;"  onchange="treeDesigner.crud.setlegalNoResponsePromptText(event);" name="legalNoResponsePromptList" id="legalNoResponsePromptList">
                                          <option value="0">Create New</option>
                                       </select>
                                       <div class="form-control-focus"></div>
                                    </div>
                                 </td>
                                 <td>
                                    <div class="form-group form-md-line-input">
                                       <input type="text" class="form-control" placeholder="Prompt" name="legalNoResponsePromptText" maxlength="150" id="legalNoResponsePromptText">
                                       <div class="form-control-focus"></div>
                                    </div>
                                 </td>
                                 <td  class="paddingtop radioCol">
                                    <label >
                                    <input type="radio" name="legalNoResponseAction" checked id="legalNoResponseRepeat" value="RP" class="md-radiobtn">
                                    Repeat </label>
                                    <label>
                                    <input type="radio" name="legalNoResponseAction" id="legalNoResponseGoto" value="0" class="md-radiobtn">
                                    Goto </label>
                                 </td>
                                 <td>
                                    <div class="form-group form-md-line-input">
                                       <select style="width:150px;"   class="form-control BR myselect " onchange="treeDesigner.crud.legalNoResponseGoto(this);" name="legalNoResponseGotoList" id="legalNoResponseGotoList">
                                          <option value="0">Hangup</option>
                                       </select>
                                       <div class="form-control-focus"></div>
                                    </div>
                                 </td>
                                 <td class="paddingtop" align="right">
                                    <button class="btn btn-xs actionbuttons" type="button" title="Add Record" onclick="treeDesigner.crud.addLegalNoResponseRow();"><i class="fa fa-plus"></i></button>
                                 </td>
                              </tr>
                              <tr id="legalNoResponseTemplate" class="hidden">
                                 <td>
                                    <div class="form-group form-md-line-input">
                                       <input type="text" readonly class="form-control" placeholder="Prompt"   id="legalNoResponsePromptID" name="legalNoResponsePromptID">
                                       <div class="form-control-focus"></div>
                                    </div>
                                 </td>
                                 <td>
                                    <div class="form-group form-md-line-input">
                                       <input type="text" readonly class="form-control" placeholder="Prompt" id="legalNoResponsePromptText" name="legalNoResponsePromptText">
                                       <div class="form-control-focus"></div>
                                    </div>
                                 </td>
                                 <td  class="paddingtop radioCol">
                                    <label >
                                    <input type="radio" name="legalNoResponseAction" id="legalNoResponseRepeat" value="RP" class="md-radiobtn">
                                    Repeat </label>
                                    <label >
                                    <input type="radio" name="legalNoResponseAction" id="legalNoResponseGoto" value="0" class="md-radiobtn">
                                    Goto </label>
                                 </td>
                                 <td>
                                    <div class="form-group form-md-line-input">
                                       <select class="form-control BR myselect" style="width:150px;" name="legalNoResponseGotoList" id="legalNoResponseGotoList" onchange="treeDesigner.crud.legalNoResponseGoto(this);">
                                          <option value="0">Hangup</option>
                                       </select>
                                       <div class="form-control-focus"></div>
                                    </div>
                                 </td>
                                 <td class="paddingtop" align="right">
                                    <button type="button" title="Select Primary Language" id="extraFileUploadBtn" onclick="extraFileUploadFormOpener(this)" class="btn btn-xs actionbuttonsmedium lastaction"><i class="fa fa-globe"></i></button>
                                       <input name="extraFileName" id="extraFilename" type="hidden" value="filename"/>
                                    <button class="btn btn-xs actionbuttonsmedium" type="button"  onclick="moveRowUp(this)"  title="Move Up"><i class="fa fa-long-arrow-up"></i></button>
                                    <button class="btn btn-xs actionbuttonsmedium lastaction" onclick="moveRowDown(this)" type="button" title="Move Down"><i class="fa fa-long-arrow-down"></i></button>
                                    <button class="btn btn-xs actionbuttons" onclick="treeDesigner.crud.removeInvalidResponseRow(this)" type="button" title="Remove Record"><i class="fa fa-minus"></i></button>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <input id="legalSaveButton" type="button" class="btn btn-success submit" onclick="treeDesigner.crud.saveLegal();" value="Save">
         <button type="button" class="btn btn-primary cancel"  onclick="closeModal('legalModal');">Cancel</button>
      </div>
   </div>
</div>
</div>