<?php
   /* 
   * To change this license header, choose License Headers in Project Properties.
   * To change this template file, choose Tools | Templates
   * and open the template in the editor.
   */
   ?>
<div class="modal fade" id="menuModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Menu</h4>

         </div>
         <form class="" id="menuForm" action="" method="post">
            {{csrf_field()}}
            <div class="modal-footer modal-head">
               <div class="row no-gutter">
                  <div class="col-xs-12 col-md-3">
                     <div class="form-group form-md-line-input">
                        <label class="col-xs-4 col-md-4 control-label" for="menuID">ID</label>
                        <div class="col-xs-8 col-md-8">
                           <input type="text" class="form-control input-sm prompt_ID" name="menuID" maxlength="5"  id="menuID">
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-12 col-md-6">
                     <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="menuText">Prompt Text</label>
                        <div class="col-md-9">
                           <input type="text" class="form-control prompt_text" name="menuText" id="menuText" maxlength="150" placeholder="">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group form-md-line-input">
                        <label for="menuTimeOut" class="col-md-4 control-label">Timeout</label>
                        <div class="col-md-8">
                           <select  name="menuTimeOut"  id="menuTimeOut" class="myselect form-control start_ivr">
                              <option value="1000">1</option>
                              <option value="2000">2</option>
                              <option value="3000">3</option>
                              <option value="4000">4</option>
                              <option value="5000">5</option>
                              <option value="6000">6</option>
                              <option value="7000">7</option>
                              <option value="8000">8</option>
                              <option value="9000">9</option>
                              <option value="10000">10</option>
                              <option value="11000">11</option>
                              <option value="12000">12</option>
                              <option value="13000">13</option>
                              <option value="14000">14</option>
                              <option value="15000">15</option>
                              <option value="16000">16</option>
                              <option value="17000">17</option>
                              <option value="18000">18</option>
                              <option value="19000">19</option>
                              <option value="20000">20</option>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row no-gutter">
                  <div class="col-md-3">
                     <div class="form-group form-md-line-input">
                        <label for="menuID" class="col-md-4 control-label">ASR</label>
                        <div class="col-md-8">
                           <select name="menuASR"  id="menuASR" class="form-control start_ivr myselect">
                              <option value="FALSE">False</option>
                              <option value="TRUE">True</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-9">
                     <div class="form-group form-md-line-input">
                        <label for="menuGrammer" class="col-md-2 control-label">Grammer</label>
                        <div class="col-md-10">
                           <select  name="menuGrammer"  id="menuGrammer" class="form-control start_ivr myselect" disabled="true">
                              <option value="DIGITS">Digits</option>
                              <option value="YESNO">Yes/No</option>
                              <option value="PhoneNumber">PhoneNumber</option>
                              <option value="Date">Date</option>
                              <option value="Time">Time</option>
                              <option value="Cust">Custom</option>
                           </select>
                        </div>
                     </div>
                  </div>

                  <div class="col-md-5">
                        <div class="md-checkbox-inline" style="margin-top:10px;">
                           <div class="md-checkbox" style="margin-right:27px;">
                               <input type="checkbox" name="trackINV" id="trackINV" class="md-check">
                               <label for="trackINV" style="margin-top:6px">
                                   <span style="height:0px; width:0px"></span>
                                   <span class="check"></span>
                                   <span class="box"></span> Track Invalid </label>
                           </div>
                           <div class="md-checkbox">
                               <input type="checkbox" name="trackNR" id="trackNR" class="md-check">
                               <label for="trackNR" style="margin-top:6px">
                                   <span style="height:0px; width:0px"></span>
                                   <span class="check"></span>
                                   <span class="box"></span> Track No Response </label>
                           </div>
                       </div>
                     </div>


               </div>
            </div>
         </form>
         <div class="modal-body no-topPadding">
            <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
               <!-- Nav tabs -->
               <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#invalidresponse" aria-controls="invalidresponse" role="tab" data-toggle="tab">Invalid Response</a></li>
                  <li role="presentation"><a href="#noresponse" aria-controls="noresponse" role="tab" data-toggle="tab">No Response</a></li>
               </ul>
               <!-- Tab panes -->
               <div class="row top-padding rowBorder ">
                  <form style="display:none;padding-top:8px" name="myUploadExtraForm" id="myUploadExtraForm" action="ajax/extrafile-upload" method="POST" enctype="multipart/form-data">
                     <input type="hidden" id="commandType" name="commandType" value="menu"/>
                     <div class="row no-gutter no-topPadding">
                        <div class="col-md-3">
                           <div class="form-group form-md-line-input">
                              <label for="extraLanguage" class="col-md-4 control-label">Language</label>
                              <div class="col-md-8">
                                 <select class="form-control language myselect" name="extraLanguage" id="extraLanguage">
                                    <option value="English">English</option>
                                    <option value="Spanish">Spanish</option>
                                    <option value="German">German</option>
                                    <option value="French">French</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <!--<div class="col-xs-12 col-sm-2 col-md-2 form-group">
                           <label for="file_upload" class="pull-right control-label">File </label>
                           </div>
                           <div class="col-xs-12 col-sm-4 col-md-4 form-group">
                           <div class="input-group">
                              <span class="input-group-btn">
                                 <span class="btn btn-info btn-file">
                                    <i class="fa fa-sticky-note-o"></i> Browse&hellip;
                                    <input type="hidden" id="extraFileAppId" name="extraFileAppId" value="" />
                                    <input type="hidden" id="extraOwnerPromptId" name="extraOwnerPromptId" value="" />
                                    <input type="hidden" id="extraFilePromptId" name="extraFilePromptId" value="" />
                                    <input name="extraFile_upload" id="extraFile_upload" type="file" class="form-control"/>
                                 </span>
                              </span>
                              <input type="text" class="form-control" readonly>
                           </div>
                           </div>-->
                        <div class="col-xs-12 col-sm-2 col-md-6 form-group">
                           <div class="input-group">
                              <span class="input-group-btn">
                                 <label class="btn btn-primary btn-file" for="multiple_input_group">
                                    <div class="input required">
                                       <input type="hidden" id="extraFileAppId" name="extraFileAppId" value="" />
                                       <input type="hidden" id="extraOwnerPromptId" name="extraOwnerPromptId" value="" />
                                       <input type="hidden" id="extraFilePromptId" name="extraFilePromptId" value="" />
                                       <input name="extraFile_upload" id="extraFile_upload" type="file"/>
                                    </div>
                                    Browse
                                 </label>
                              </span>
                              <span class="file-input-label"></span>
                           </div>
                        </div>
                        <div class="col-md-2">
                           <input type="submit" id="submit-btn" value="Upload" class="btn btn-success"/>
                           <img src="/images/LoaderIcon.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
                        </div>
                     </div>
                  </form>
               </div>
               <div class="tab-content scroller" style="height:200px">
                  <div role="tabpanel" class="tab-pane active" id="invalidresponse">
                     <div class="table-responsive">
                        <form id="invalidResponseForm" action="" method="post">
                           <table class="table table-condensed" id="invalidResponseTable">
                              <thead>
                                 <tr>
                                    <th width="100">Prompt ID</th>
                                    <th>Prompt</th>
                                    <th >Action</th>
                                    <th></th>
                                    <th></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr id="invalidRow">
                                    <td>
                                       <div class="form-group form-md-line-input">
                                          <select class="form-control BR myselect " style="width:110px;" onchange="treeDesigner.crud.setInvalidPromptText(event);" name="invalidPromptList" id="invalidPromptList">
                                             <option value="0">Create New</option>
                                          </select>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="form-group form-md-line-input">
                                          <input type="text" class="form-control" placeholder="Prompt" name="invalidPromptText" maxlength="150" id="invalidPromptText">
                                          <div class="form-control-focus"></div>
                                       </div>
                                    </td>
                                    <td class="paddingtop radioCol">
                                       <label >
                                       <input type="radio" name="invalidAction" checked id="invalidRepeat" value="RP" class="md-radiobtn">
                                       Repeat </label>
                                       <label >
                                       <input type="checkbox" name="playPrompt" id="playPrompt"class="md-check" checked="true">
                                       Play Prompt</label>             
                                       <label >
                                       <input type="radio" name="invalidAction" id="invalidGoto" value="0" class="md-radiobtn">
                                       Goto </label>        
                                    </td>
                                    <td>
                                       <div class="form-group form-md-line-input">
                                          <select class="form-control BR myselect " style="width:150px;" name="invalidGotoList" id="invalidGotoList" onchange="treeDesigner.crud.invalidResponseGoto(this);">
                                             <option value="0">Hangup</option>
                                          </select>
                                       </div>
                                    </td>
                                    <td class="paddingtop" align="right">
                                       <button class="btn btn-xs actionbuttons" type="button" title="Add Record" onclick="treeDesigner.crud.addInvalidResponseRow();"><i class="fa fa-plus"></i></button>
                                    </td>
                                 </tr>
                                 <tr id="invalidResponseTemplate" class="hidden">
                                    <td>
                                       <div class="form-group form-md-line-input">
                                          <input type="text" readonly class="form-control" placeholder="Prompt"   id="invalidPromptID" name="invalidPromptID">
                                       </div>
                                    </td>
                                    <td>
                                       <div class="form-group form-md-line-input">
                                          <input type="text" readonly class="form-control" placeholder="Prompt" id="invalidPromptText" name="invalidPromptText">
                                       </div>
                                    </td>
                                    <td  class="paddingtop radioCol">
                                       <input type="radio" name="invalidAction" id="invalidRepeat" value="RP" class="md-radiobtn">
                                       Repeat </label>
                                       <label>
                                       <label >
                                       <input type="checkbox" name="playPrompt" id="playPrompt" class="md-check" checked="true">
                                       Play Prompt</label> 
                                       <input type="radio" name="invalidAction" id="invalidGoto" value="0" class="md-radiobtn">
                                       Goto </label>
                                    </td>
                                    <td>
                                       <div class="form-group form-md-line-input">
                                          <select class="form-control BR myselect " style="width:150px;" name="invalidGotoList" id="invalidGotoList" onchange="treeDesigner.crud.invalidResponseGoto(this);">
                                             <option value="0">Hangup</option>
                                          </select>
                                       </div>
                                    </td>
                                    <td class="paddingtop" align="right">
                                       <button type="button" title="Select Primary Language" id="extraFileUploadBtn" onclick="extraFileUploadFormOpener(this)" class="btn btn-xs actionbuttonsmedium lastaction"><i class="fa fa-globe"></i></button>
                                       <input name="extraFileName" id="extraFilename" type="hidden" value="filename"/>
                                       <button class="btn btn-xs actionbuttonsmedium" type="button" title="Move Up"  onclick="moveRowUp(this);" ><i class="fa fa-long-arrow-up"></i></button>
                                       <button class="btn btn-xs actionbuttonsmedium lastaction" type="button" title="Move Down"  onclick="moveRowDown(this);" ><i class="fa fa-long-arrow-down"></i></button>
                                       <button class="btn btn-xs actionbuttons" onclick="treeDesigner.crud.removeInvalidResponseRow(this)" type="button" title="Remove Record"><i class="fa fa-times"></i></button>
                                    </td>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </form>
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="noresponse">
                     <div class="table-responsive">
                        <form class="" id="noResponseForm" action="" method="post">
                           <table id="noResponseTable" class="table table-condensed">
                              <thead>
                                 <tr>
                                    <th width="100">Prompt ID</th>
                                    <th>Prompt</th>
                                    <th >Action</th>
                                    <th></th>
                                    <th></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr id="noResponseRow">
                                    <td>
                                       <div class="form-group form-md-line-input">
                                          <select class="form-control BR myselect " style="width:110px;" onchange="treeDesigner.crud.setNoResponsePromptText(event);" name="noResponsePromptList" id="noResponsePromptList">
                                             <option value="0">Create New</option>
                                          </select>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="form-group form-md-line-input">
                                          <input type="text" class="form-control" placeholder="Prompt" name="noResponsePromptText" maxlength="150" id="noResponsePromptText">
                                       </div>
                                    </td>
                                    <td  class="paddingtop radioCol">
                                       <label >
                                       <input type="radio" name="noResponseAction" checked id="noResponseRepeat" value="RP" class="md-radiobtn">
                                       Repeat </label>
                                       <label >
                                       <input type="checkbox" name="playPrompt" id="playPrompt"class="md-check" checked="true">
                                       Play Prompt</label>  
                                       <label >
                                       <input type="radio" name="noResponseAction" id="noResponseGoto" value="0" class="md-radiobtn">
                                       Goto </label>
                                    </td>
                                    <td>
                                       <div class="form-group form-md-line-input">
                                          <select style="width:150px;"  class="form-control BR myselect " onchange="treeDesigner.crud.noResponseGoto(this);" name="noResponseGotoList" id="noResponseGotoList">
                                             <option value="0">Hangup</option>
                                          </select>
                                       </div>
                                    </td>
                                    <td class="paddingtop" align="right">
                                       <button class="btn btn-xs actionbuttons" type="button" title="Add Record" onclick="treeDesigner.crud.addNoResponseRow();"><i class="fa fa-plus"></i></button>
                                    </td>
                                 </tr>
                                 <tr id="noResponseTemplate" class="hidden">
                                    <td>
                                       <div class="form-group form-md-line-input">
                                          <input type="text" readonly class="form-control" placeholder="Prompt"   id="noResponsePromptID" name="noResponsePromptID">
                                       </div>
                                    </td>
                                    <td>
                                       <div class="form-group form-md-line-input">
                                          <input type="text" readonly class="form-control" placeholder="Prompt" id="noResponsePromptText" name="noResponsePromptText">
                                       </div>
                                    </td>
                                    <td  class="paddingtop radioCol">
                                       <label >
                                       <input type="radio" name="noResponseAction" id="noResponseRepeat" value="RP" class="md-radiobtn">
                                       Repeat </label>
                                       <label >
                                       <input type="checkbox" name="playPrompt" id="playPrompt"class="md-check" checked="true">
                                       Play Prompt</label>  
                                       <label>
                                       <input type="radio" name="noResponseAction" id="noResponseGoto" value="0" class="md-radiobtn">
                                       Goto </label>
                                    </td>
                                    <td>
                                       <div class="form-group form-md-line-input">
                                          <select class="form-control BR myselect" style="width:150px;"  name="noResponseGotoList" id="noResponseGotoList" onchange="treeDesigner.crud.noResponseGoto(this);">
                                             <option value="0">Hangup</option>
                                          </select>
                                       </div>
                                    </td>
                                    <td class="paddingtop" align="right">
                                       <button type="button" title="Select Primary Language" id="extraFileUploadBtn" onclick="extraFileUploadFormOpener(this)" class="btn btn-xs actionbuttonsmedium lastaction"><i class="fa fa-globe"></i></button>
                                       <input name="extraFileName" id="extraFilename" type="hidden" value="filename"/>
                                       <button class="btn btn-xs actionbuttonsmedium" type="button"  onclick="moveRowUp(this)"  title="Move Up"><i class="fa fa-long-arrow-up"></i></button>
                                       <button class="btn btn-xs actionbuttonsmedium lastaction" onclick="moveRowDown(this)" type="button" title="Move Down"><i class="fa fa-long-arrow-down"></i></button>
                                       <button class="btn btn-xs actionbuttons" onclick="treeDesigner.crud.removeNoResponseRow(this)" type="button" title="Remove Record"><i class="fa fa-times"></i></button>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <input id="menuSaveButton" type="button" class="btn btn-success submit" onclick="treeDesigner.crud.saveMenu();" value="Save">
            <button type="button" class="btn btn-primary cancel"  onclick="closeModal('menuModal');">Cancel</button>
         </div>
      </div>
   </div>
</div>