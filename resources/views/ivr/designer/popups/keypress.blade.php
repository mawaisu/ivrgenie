<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="modal fade" id="keypressModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form class="form-horizontal" id="play_prompt" action="" method="post">
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Menu Keypress</h4>
                    {{csrf_field()}}
                </div>
                <div class="modal-body noSpace">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="keypress">Keypress</label>
                        <div class="col-md-8">
                            <select class="form-control myselect" name="keypress" id="keypress">
                            </select>
                            <input type="hidden" name="keypressID" id="keypressID" value="">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="keypressGoto">Go to</label>
                        <div class="col-md-8">
                            <select class="form-control myselect" name="goto" id="keypressGoto" name="keypressGoto">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-success submit" value="Save" onclick="treeDesigner.crud.saveKeypress();">
                    <button type="button" class="btn btn-primary cancel" onclick="closeModal('keypressModal');" >Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>
