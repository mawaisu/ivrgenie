<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Created by M.Awais Ullah
?>

<div class="modal fade" id="setValueModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form class="form-horizontal" id="setValueForm" action="" method="post">
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Set Value</h4>
                  {{csrf_field()}}
                </div>
                <div class="modal-body noSpace">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="prompt_ID">Set Value ID</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control prompt_text" name="setValueID" id="setValueID" maxlength="5">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="prompt_text">Set Value Text</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control prompt_text" name="setValueText" maxlength="150" id="setValueText" placeholder="">
                        </div>
                    </div>
                    
                    <div class="form-group form-md-line-input">
                        <label for="BR" class="col-md-3 control-label">Variable</label>
                        <div class="col-md-8">
                            <select class="form-control BR myselect" name="cmdType" id="cmdType">
                            <option value="">Please select a value</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="BR" class="col-md-3 control-label">Value-Type</label>
                        <div class="col-md-8">
                            <select class="form-control BR myselect" name="valType" id="valType">
                                <option value="text">Text</option>
                                <option value="cmd">Command</option>
                                <option value="getCurrentYear">Current year</option>
                                <option value="getCurrentMonth">Current month</option>
                                <option value="getCurrentDay">Current day</option>
                                <option value="getCurrentHour">Current hour</option>
                                <option value="getCurrentMinute">Current Minute(s)</option>
                                <option value="getCurrentSeconds">Current Second(s)</option>
                                <option value="getCurrentDayOfWeek">Current Day of Week</option>
                                <option value="getCurrentANI">Current ANI</option>
                                <option value="getCurrentDNIS">Current DNIS</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="goto" class="col-md-3 control-label">Value</label>
                        <div class="col-md-8" id="setValueDiv">
                            <input type="text" class="form-control prompt_text" name="setValueVAL" maxlength="150" id="setValueVAL" placeholder="">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="goto" class="col-md-3 control-label">Go to</label>
                        <div class="col-md-8">
                            <select class="form-control apptype myselect" id="setValueGoto" name="setValueGoto">
                                <option value="0">Hangup</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-success submit" value="Save" onclick="treeDesigner.crud.saveSetValue();">
                    <button type="button" class="btn btn-primary cancel" onclick="closeModal('setValueModal');" >Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>
