<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="modal fade" id="languageKeyPressModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form class="form-horizontal" id="play_prompt" action="" method="post">
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Language Keypress</h4>
                    {{csrf_field()}}
                </div>
                <div class="modal-body noSpace">
                    <div class="form-group form-md-line-input">
                        <label for="languageKeypress" class="col-md-3 control-label">Keypress</label>
                        <div class="col-md-8">
                            <input type="text" readonly="true" class="form-control keypress" name="languageKeypress" id="languageKeypress">
                            <input type="hidden" name="languageKeypressID" id="languageKeypressID" value="">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="goto" class="col-md-3 control-label">Go to</label>
                        <div class="col-md-8">
                            <select class="form-control BR myselect" name="goto" id="languageKeypressList" name="languageKeypressList">
                                
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-success submit" value="Save" onclick="treeDesigner.crud.saveLanguageKeypress();">
                    <button type="button" class="btn btn-primary cancel" onclick="closeModal('languageKeyPressModal');" >Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>
