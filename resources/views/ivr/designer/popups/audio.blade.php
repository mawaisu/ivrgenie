<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="modal fade" id="audioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form class="form-horizontal" id="audioForm" action="" method="post">
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Audio</h4>
                    {{csrf_field()}}
                </div>
                <div class="modal-body noSpace">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="prompt_ID">Audio ID</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control prompt_text" name="audioID" id="audioID" maxlength="5">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="prompt_text">Audio Text</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control prompt_text" name="audioText" maxlength="150" id="audioText" placeholder="">
                        </div>
                    </div>
                    
                    <div class="form-group form-md-line-input">
                        <label for="BR" class="col-md-3 control-label">HANGUPKEY</label>
                        <div class="col-md-8">
                            <select class="form-control apptype myselect" name="audioHangupKey" id="audioHangupKey">
                                <option value="#">#</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="BR" class="col-md-3 control-label">PAUSEKEY</label>
                        <div class="col-md-8">
                            <select class="form-control apptype myselect" name="audioPauseKey" id="audioPauseKey">
                                <option value="0">0</option>
                            </select>
                        </div>
                    </div>
                      <div class="form-group form-md-line-input">
                        <label for="BR" class="col-md-3 control-label">SARTOVERKEY</label>
                        <div class="col-md-8">
                            <select class="form-control apptype myselect" name="audioStartOverKey" id="audioStartOverKey">
                                <option value="*">*</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="goto" class="col-md-3 control-label">Go to</label>
                        <div class="col-md-8">
                            <select class="form-control apptype myselect" id="audioGoto" name="audioGoto">
                                <option value="0">Hangup</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-success submit" value="Save" onclick="treeDesigner.crud.saveAudio();">
                    <button type="button" class="btn btn-primary cancel" onclick="closeModal('audioModal');;" >Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>
