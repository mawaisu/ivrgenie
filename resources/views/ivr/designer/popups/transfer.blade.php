<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="modal fade" id="transferModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form class="form-horizontal" id="transferForm" action="" method="post">
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Transfer</h4>
                    {{csrf_field()}}
                </div>
                <div class="modal-body noSpace">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="transfer_ID">Transfer ID</label>
                        <div class="col-md-8">
                            <input type="text" maxlength="5" class="form-control transfer_ID" name="transferID" id="transferID">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-Text" for="transfer_text">Transfer Text</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control transfer_text" name="transferText" maxlength="150" id="transferText">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="transferBargin" class="col-md-3 control-label">BR</label>
                        <div class="col-md-8">
                            <select class="form-control transferBargin myselect" id="transferBargin" name="transferBargin">
                                <option value="FALSE">False</option>
                                <option value="TRUE">True</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="transferA" class="col-md-3 control-label">All Errors</label>
                        <div class="col-md-8">
                            <select class="form-control transferA myselect"  id="transferA" name="transferA">
                                <option value="0">Hangup</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="transferB" class="col-md-3 control-label">Busy</label>
                        <div class="col-md-8">
                            <select class="form-control transferB myselect"  id="transferB" name="transferB">
                                <option value="0">Hangup</option>
                            </select>
                        </div> 
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="transferN" class="col-md-3 control-label">No Response</label>
                        <div class="col-md-8">
                            <select class="form-control transferN myselect"  id="transferN" name="transferN">
                                <option value="0">Hangup</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="transferC" class="col-md-3 control-label">Connected</label>
                        <div class="col-md-8">
                            <select class="form-control transferC myselect"  id="transferC" name="transferC">
                                <option value="0">Hangup</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="transferXT" class="col-md-3 control-label">Channel Out</label>
                        <div class="col-md-8">
                            <select class="form-control transferXT myselect"  id="transferXT" name="transferXT">
                                <option value="TFN">DID/TFN</option>
                                <option value="EXT">Extension</option>
                                <option value="CMD">Command</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="transferXD" class="col-md-3 control-label">Out Number</label>
                        <div id="transferOutDiv" class="col-md-8">
                            <input type="text" class="form-control transferXD" name="transferXD" maxlength="50" id="transferXD" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-success submit" value="Save" onclick="treeDesigner.crud.saveTransfer();">
                    <button type="button" class="btn btn-primary cancel" onclick="closeModal('transferModal');;" >Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>
