<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="modal fade" id="variableModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form class="form-horizontal" id="variableForm" action="" method="post">
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Variable</h4>
                    {{csrf_field()}}
                </div>
                <div class="modal-body noSpace">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="variableID">Variable ID</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control variable_ID" maxlength="5" name="variableID" id="variableID">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="variableText">Variable</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control variable_text" name="variableText" maxlength="150" id="variableText">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="variableValue">Value</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control variable_text" name="variableValue" maxlength="150" id="variableValue">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-success submit" value="Save" onclick="treeDesigner.crud.saveVariable();">
                    <button type="button" class="btn btn-primary cancel" onclick="closeModal('variableModal');;" >Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>