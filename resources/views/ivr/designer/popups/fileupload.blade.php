<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="modal fade" id="fileUploadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <form class="form-horizontal" id="MyUploadForm" action="{{route('designer.fileUpload')}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">File Upload</h4>

                </div>
                <div class="modal-body">
                    <div class="form-group form-md-line-input">
                        <label for="BR" class="col-md-3 control-label">Language</label>
                        <div class="col-md-8">
                            <select class="form-control language myselect" name="language" id="language">
                                <option value="English">English</option>
                                <option value="Spanish">Spanish</option>
                                <option value="German">German</option>
                                <option value="French">French</option>
                            </select>
                        </div>
                    </div>
                    <br/>

                    <!--<div class="form-group">
                      <label for="file_upload" class="col-md-3 control-label">File</label>
                      <div class="col-md-8">
                        <div class="input-group">
                          <span class="input-group-btn">
                            <span class="btn btn-info btn-file">
                              Browse&hellip;
                              <input type="hidden" id="fileAppId" name="fileAppId" value="" />
                              <input type="hidden" id="filePromptId" name="filePromptId" value="" />
                              <input name="file_upload" id="file_upload" type="file" class="form-control"/>
                            </span>
                          </span>
                          <input type="text" class="form-control" readonly>
                        </div>
                      </div>
                    </div>-->

                    <div class="col-xs-12 col-sm-12 col-md-10 form-group form-md-line-input"
                         style="margin-left:35px !important">
                        <div class="input-group">
               <span class="input-group-btn">
                 <label class="btn btn-primary btn-file" for="multiple_input_group">
                   <div class="input required">
                   <input type="hidden" id="fileAppId" name="fileAppId" value=""/>
                   <input type="hidden" id="filePromptId" name="filePromptId" value=""/>
                   <input name="file_upload" id="file_upload" type="file"/></div> Browse
                 </label>
               </span>
                            <span class="file-input-label"
                                  style="-webkit-border-radius: 0px 4px 4px 0px;-moz-border-radius: 0px 4px 4px 0px;border-radius: 0px 4px 4px 0px"></span>
                        </div>
                    </div>

                    <br/>
                    <br/>
                </div>
                <div class="modal-footer">
                    <input type="submit" id="submit-btn1" value="Upload" class="btn btn-success"/>
                    <img src="/images/LoaderIcon.gif" id="loading-img1" style="display:none;" alt="Please Wait"/>
                </div>
            </div>
        </div>
    </form>
</div>



