<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="modal fade" id="ttsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form class="form-horizontal" id="ttsForm" action="" method="post">
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">TTS</h4>
                    {{csrf_field()}}
                </div>
                <div class="modal-body noSpace">
                    <div class="form-group form-md-line-input">
                        <label for="tts_ID" class="col-md-3 control-label">TTS ID</label>
                        <div class="col-md-8">
                            <input type="text" maxlength="5" class="form-control tts_ID" name="ttsID" id="ttsID">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="tts_text" class="col-md-3 control-label">TTS Text</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control tts_text" name="ttsText"  maxlength="150" id="ttsText" placeholder="">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="BR" class="col-md-3 control-label">Variable</label>
                        <div class="col-md-8">
                            <select class="form-control BR myselect" name="BR" id="ttsVariable" name="ttsVariable">
                                
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="ttsType" class="col-md-3 control-label">Type</label>
                        <div class="col-md-8">
                            <select class="form-control BR myselect" name="BR" id="ttsType" name="ttsType">
                                <option value="DIGITS">Digits</option>
                                <option value="DATE">Date</option>
                                <option value="AMOUNT">Amount</option>
                                <option value="RECORD">Record</option>
                                <option value="PLAY">Play</option>
                                <option value="TTS">TTS</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="ttsGoto" class="col-md-3 control-label">Go to</label>
                        <div class="col-md-8">
                            <select class="form-control BR myselect" name="goto" id="ttsGoto" name="ttsGoto">
                                <option value="0">Hangup</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-success submit" value="Save" onclick="treeDesigner.crud.saveTts();">
                    <button type="button" class="btn btn-primary cancel" onclick="closeModal('ttsModal');;" >Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>
