<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="modal fade" id="recordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form class="form-horizontal" id="recordForm" action="" method="post">
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Record</h4>
                    {{csrf_field()}}
                </div>
                <div class="modal-body noSpace">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="recordID">Record ID</label>
                        <div class="col-md-8">
                            <input type="text" maxlength="5" class="form-control record_ID" name="recordID" id="recordID">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="recordText">Record Text</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control record_text" name="recordText"  maxlength="150" id="recordText" placeholder="">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="recordGoto" class="col-md-3 control-label">Go to</label>
                        <div class="col-md-8">
                            <select class="form-control BR myselect" name="goto" id="recordGoto" name="recordGoto">
                                <option value="0">Hangup</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="recordBargin" class="col-md-3 control-label">BR</label>
                        <div class="col-md-8">
                            <select class="form-control BR myselect"  id="recordBargin" name="recordBargin">
                                <option value="FALSE">False</option>
                                <option value="TRUE">True</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="recordML" class="col-md-3 control-label">Recording</label>
                        <div class="col-md-8">
                            <select class="form-control recordML myselect"  id="recordML" name="recordML">
                                <option value="30000">30 Seconds</option>
                                <option value="60000">1 Minute</option>
                                <option value="120000">2 Minutes</option>
                                <option value="180000">3 Minutes</option>
                                <option value="240000">4 Minutes</option>
                                <option value="300000">5 Minutes</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="recordIS" class="col-md-3 control-label">Silence</label>
                        <div class="col-md-8">
                            <select class="form-control recordIS myselect" id="recordIS" name="recordIS">
                                <option value="3">3 Seconds</option>
                                <option value="4">4 Seconds</option>
                                <option value="5">5 Seconds</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="recordBP" class="col-md-3 control-label">Beep</label>
                        <div class="col-md-8">
                            <select class="form-control recordBP myselect" id="recordBP" name="recordBP">
                                <option value="0">False</option>
                                <option value="1">True</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="recordPB" class="col-md-3 control-label">Play Back</label>
                        <div class="col-md-8">
                            <select class="form-control recordPB myselect" id="recordPB" name="recordPB">
                                <option value="0">False</option>
                                <option value="1">True</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="recordFileName" class="col-md-3 control-label">File Name</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control record_text" name="recordFileName"  maxlength="150" id="recordFileName" placeholder="File Name for Recording">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-success submit" value="Save" onclick="treeDesigner.crud.saveRecord();">
                    <button type="button" class="btn btn-primary cancel" onclick="closeModal('recordModal');;" >Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>
