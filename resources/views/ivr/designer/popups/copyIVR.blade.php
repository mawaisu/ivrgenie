<?php

//created by Muhammad Ahmed on 28th October 2015.
//view to display the add application modal. 
//this modal is submitted to application controller's add action. 

?>

<!--====== CONTENT ======-->
<div class="modal fade" id="copyIVRPopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form class="form-horizontal" id="copyIvrForm" action="" method="post">
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="ivrTitle">Copy IVR</h4>
                </div>
                <div class="modal-body noSpace">
                    <div class="form-group form-md-line-input">
                        <label for="app_abbrv" class="col-md-4 control-label">IVR Name</label>
                        <div class="col-md-8">
                            <input maxlength="150" type="text" class="form-control companyName" name="copyIVRPopupName" id="copyIVRPopupName" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!--input type="button" class="btn btn-green submit" value="Save"-->
                    <button type="submit" id="submitButton" class="btn btn-success">Save</button>
                    <button type="button" id="cancelmodal" data-dismiss="modal" class="btn btn-primary">Cancel</button>
                </div>
            </div>
        </div>
        <input type="hidden"  value="" name="copyIvrId" id="copyIvrId" >
        {{csrf_field()}}
    </form>
</div>