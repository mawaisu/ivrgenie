<?php
/* 
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
?>

<div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Confirmation</h4>

      </div>
      <form class="" id="confirmationForm" action="" method="post">
        <div class="modal-footer modal-head">
          <div class="row no-gutter">
              {{csrf_field()}}
            <div class="col-md-3">
              <div class="form-group form-md-line-input">
                <label class="col-md-4 control-label" for="confirmationID">ID</label>
                <div class="col-md-8">
                  <input type="text" class="form-control input-sm prompt_ID" name="confirmationID" maxlength="5"  id="confirmationID">
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label" for="confirmationText">Prompt Text</label>
                <div class="col-md-9">
                  <input type="text" class="form-control prompt_text" name="confirmationText" id="confirmationText" maxlength="150" placeholder="">
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group form-md-line-input">
                <label for="confirmationTimeOut" class="col-md-4 control-label">Timeout</label>
                <div class="col-md-8">
                  <select  name="confirmationTimeOut"  id="confirmationTimeOut" class="form-control start_ivr myselect">
                      <option value="1000">1</option>
                      <option value="2000">2</option>
                      <option value="3000">3</option>
                      <option value="4000">4</option>
                      <option value="5000">5</option>
                      <option value="6000">6</option>
                      <option value="7000">7</option>
                      <option value="8000">8</option>
                      <option value="9000">9</option>
                      <option value="10000">10</option>
                      <option value="11000">11</option>
                      <option value="12000">12</option>
                      <option value="13000">13</option>
                      <option value="14000">14</option>
                      <option value="15000">15</option>
                      <option value="16000">16</option>
                      <option value="17000">17</option>
                      <option value="18000">18</option>
                      <option value="19000">19</option>
                      <option value="20000">20</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row no-gutter">
            <div class="col-md-3">
              <div class="form-group form-md-line-input">
                <label for="confirmationASR" class="col-md-4 control-label">ASR</label>
                <div class="col-md-8">
                  <select  name="confirmationASR"  id="confirmationASR" class="form-control start_ivr myselect">
                    <option value="FALSE">False</option>
                    <option value="TRUE">True</option>                    
                  </select>
                </div>
              </div>
            </div>
            
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <label for="confirmationGrammer" class="col-md-3 control-label">Grammer</label>
                <div class="col-md-9">
                  <select  name="confirmationGrammer"  id="confirmationGrammer" class="form-control start_ivr myselect" disabled="true">
                    <option value="DIGITS">Digits</option>
                    <option value="YESNO">Yes/No</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
      <div class="modal-body no-topPadding">
        <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#invalidresponsetab" aria-controls="invalidresponsetab" role="tab" data-toggle="tab">Invalid Response</a></li>
            <li role="presentation"><a href="#noresponsetab" aria-controls="noresponsetab" role="tab" data-toggle="tab">No Response</a></li>
          </ul>
          <div class="row top-padding rowBorder">
            <form style="display:none;padding-top:8px" name="myConfirmationUploadExtraForm" id="myConfirmationUploadExtraForm" action="ajax/extrafile-upload" method="POST" enctype="multipart/form-data">
              <input type="hidden" id="commandType" name="commandType" value="confirmation"/>
                {{csrf_field()}}
              <div class="col-xs-12 col-sm-1 col-md-1 form-group">
                <label for="BR" class="pull-right">Language</label>
              </div>
              <div class="col-xs-12 col-sm-2 col-md-2 form-group">
                <select class="form-control language myselect" name="extraLanguage" id="extraLanguage">
                  <option value="English">English</option>
                  <option value="Spanish">Spanish</option>
                  <option value="German">German</option>
                  <option value="French">French</option>
                </select>
              </div>

              <!--<div class="col-xs-12 col-sm-2 col-md-2 form-group">
                <label for="file_upload" class="pull-right control-label">File</label>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-4 form-group">
                <div class="input-group">
                  <span class="input-group-btn">
                    <span class="input-group-btn">
                      <i class="fa fa-sticky-note-o"></i> Browse&hellip;
                      <input type="hidden" id="extraFileAppId" name="extraFileAppId" value="" />
                      <input type="hidden" id="extraOwnerPromptId" name="extraOwnerPromptId" value="" />
                      <input type="hidden" id="extraFilePromptId" name="extraFilePromptId" value="" />
                      <input name="extraFile_upload" id="extraFile_upload" type="file"/>
                    </span>
                  </span>
                  <input type="text" class="form-control" readonly>
                </div>
              </div>-->
              
              <div class="col-xs-12 col-sm-2 col-md-6 form-group">
                  <div class="input-group">
                    <span class="input-group-btn">
                      <label class="btn btn-primary btn-file" for="multiple_input_group">
                        <div class="input required">
                          <input type="hidden" id="extraFileAppId" name="extraFileAppId" value="" />
                        <input type="hidden" id="extraOwnerPromptId" name="extraOwnerPromptId" value="" />
                        <input type="hidden" id="extraFilePromptId" name="extraFilePromptId" value="" />
                        <input name="extraFile_upload" id="extraFile_upload" type="file"/></div> Browse
                      </label>
                    </span>
                    <span class="file-input-label"></span>
                  </div>
              </div>
              <div class="col-md-2">
                <input type="submit" id="submit-btn" value="Upload" class="btn btn-success"/>
                <img src="/images/LoaderIcon.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
              </div>  
            </form>
          </div>
          <!-- Tab panes -->
          <div class="tab-content scroller" style="height:200px">
            <div role="tabpanel" class="tab-pane active" id="invalidresponsetab">
              <div class="table-responsive">
                <form id="confirmationInvalidResponseForm" action="" method="post">
                  <table class="table table-condensed" id="confirmationInvalidResponseTable">
                    <thead>
                      <tr>
                        <th width="100">Prompt ID</th>
                        <th>Prompt</th>
                        <th >Action</th>
                        <th></th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr id="confirmationInvalidRow">
                        <td>
                         <div class="form-group form-md-line-input">
                            <select class="form-control BR myselect " style="width:110px;" onchange="treeDesigner.crud.setConfirmationInvalidPromptText(this);"  name="confirmationInvalidPromptList" id="confirmationInvalidPromptList">
                              <option value="0">Create New</option>
                            </select>
                          </div>
                          </td>
                          <td>
                            <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" placeholder="Prompt" name="confirmationInvalidPromptText" maxlength="150" id="confirmationInvalidPromptText">
                          </div>
                          </td>
                          <td class="paddingtop radioCol">
                             <label>
                                <input type="radio" name="confirmationInvalidAction" checked id="confirmationInvalidRepeat" value="RP" class="md-radiobtn">
                               
                                 Repeat </label>
                                 <label >
                                  <input type="radio" name="confirmationInvalidAction" id="confirmationInvalidGoto" value="0" class="md-radiobtn">
                                 
                                   Goto </label>
                                 
                          </td>
                          <td>
                            <div class="form-group form-md-line-input">
                                <select class="form-control BR myselect " name="confirmationInvalidGotoList" id="confirmationInvalidGotoList" onchange="treeDesigner.crud.confirmationInvalidResponseGoto(this);">
                                  <option value="0">Hangup</option>
                                </select>
                              </div>
                            </td>
                            <td class="paddingtop" align="right">
                              <button class="btn btn-xs actionbuttons" type="button" title="Add Record" onclick="treeDesigner.crud.addConfirmationInvalidResponseRow();"><i class="fa fa-plus"></i></button>
                            </td>
                          </tr>
                          <tr id="confirmationInvalidResponseTemplate" class="hidden">
                            <td>
                              <div class="form-group form-md-line-input">
                                <input type="text" readonly class="form-control" placeholder="Prompt"   id="confirmationInvalidPromptID" name="confirmationInvalidPromptID">
                              </div>
                              </td>
                              <td>
                                 <div class="form-group form-md-line-input">
                                <input type="text" readonly class="form-control" placeholder="Prompt" id="confirmationInvalidPromptText" name="confirmationInvalidPromptText">
                              </div>
                              </td>
                              <td  class="paddingtop radioCol">
                                <label>
                                    <input type="radio" name="confirmationInvalidAction" id="confirmationInvalidRepeat" value="RP" class="md-radiobtn">
                                    
                                     Repeat </label>
                                      <label>
                                      <input type="radio" name="confirmationInvalidAction" id="confirmationInvalidGoto" value="0" class="md-radiobtn">
                                    
                                       Goto </label>
                                      
                              </td>
                              <td>
                                 <div class="form-group form-md-line-input">
                                    <select class="form-control BR myselect " name="confirmationInvalidGotoList" id="confirmationInvalidGotoList" onchange="treeDesigner.crud.confirmationInvalidResponseGoto(this);">
                                      <option value="0">Hangup</option>
                                    </select>
                                  </div>
                                  </td>
                                <td class="paddingtop" align="right">
                                  <button type="button" title="Select Primary Language" id="extraFileUploadBtn" onclick="extraFileUploadFormOpener(this)" class="btn btn-xs actionbuttonsmedium lastaction"><i class="fa fa-globe"></i></button>
                                  <input name="extraFileName" id="extraFilename" type="hidden" value="filename"/>
                                  <button class="btn btn-xs actionbuttonsmedium" type="button" title="Move Up"  onclick="moveRowUp(this);" ><i class="fa fa-long-arrow-up"></i></button>
                                  <button class="btn btn-xs actionbuttonsmedium lastaction" type="button" title="Move Down"  onclick="moveRowDown(this);" ><i class="fa fa-long-arrow-down"></i></button>
                                  <button class="btn btn-xs actionbuttons" onclick="treeDesigner.crud.removeInvalidResponseRow(this)" type="button" title="Remove Record"><i class="fa fa-times"></i></button>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </form>
                      </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="noresponsetab">
                      <div class="table-responsive">
                        <form class="" id="confirmationNoResponseForm" action="" method="post">
                          <table id="confirmationNoResponseTable" class="table table-condensed">
                            <thead>
                              <tr>
                                <th width="100">Prompt ID</th>
                                <th>Prompt</th>
                                <th >Action</th>
                                <th></th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr id="confirmationNoResponseRow">
                                <td>
                                  <div class="form-group form-md-line-input">
                                    <select class="form-control BR myselect " style="width:110px;" onchange="treeDesigner.crud.setConfirmationNoResponsePromptText(this);" name="confirmationNoResponsePromptList" id="confirmationNoResponsePromptList">
                                      <option value="0">Create New</option>
                                    </select>
                                  </div>
                                  </td>
                                  <td>
                                    <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control" placeholder="Prompt" name="confirmationNoResponsePromptText" maxlength="150" id="confirmationNoResponsePromptText">
                                  </div>
                                  </td>
                                  <td  class="paddingtop radioCol">
                                     <label>
                                        <input type="radio" name="confirmationNoResponseAction" checked id="confirmationNoResponseRepeat" value="RP" class="md-radiobtn">
                                        Repeat </label>
                                        
                                              <label >
											  <input type="radio" name="confirmationNoResponseAction" id="confirmationNoResponseGoto" value="0" class="md-radiobtn">
                                      
                                            Goto </label>
                                          
                                  </td>
                                  <td>
                                    <div class="form-group form-md-line-input">
                                        <select style="width:150px;"   class="form-control BR myselect " onchange="treeDesigner.crud.confirmationNoResponseGoto(this);" name="confirmationNoResponseGotoList" id="confirmationNoResponseGotoList">
                                          <option value="0">Hangup</option>
                                        </select>
                                      </div>
                                  </td>
                                  <td class="paddingtop" align="right">
                                    <button class="btn btn-xs actionbuttons" type="button" title="Add Record" onclick="treeDesigner.crud.addConfirmationNoResponseRow();"><i class="fa fa-plus"></i></button>
                                  </td>
                                </tr>
                                <tr id="confirmationNoResponseTemplate" class="hidden">
                                  <td>
                                    <div class="form-group form-md-line-input">
                                        <input type="text" readonly class="form-control" placeholder="Prompt"   id="confirmationNoResponsePromptID" name="confirmationNoResponsePromptID">
                                      </div>
                                      </td>
                                    <td>
                                      <div class="form-group form-md-line-input">
                                        <input type="text" readonly class="form-control" placeholder="Prompt" id="confirmationNoResponsePromptText" name="confirmationNoResponsePromptText">
                                      </div>
                                    </td>
                                    <td  class="paddingtop radioCol">
                                      <label>
                                            <input type="radio" name="confirmationNoResponseAction" id="confirmationNoResponseRepeat" value="RP" class="md-radiobtn">
                                          
                                              Repeat </label>
                                             <label>
                                              <input type="radio" name="confirmationNoResponseAction" id="confirmationNoResponseGoto" value="0" class="md-radiobtn">
                                             
                                               Goto </label>
                
                                    </td>
                                    <td>
                                      <div class="form-group form-md-line-input">
                                            <select class="form-control BR myselect" style="width:150px;" name="confirmationNoResponseGotoList" id="confirmationNoResponseGotoList" onchange="treeDesigner.crud.confirmationNoResponseGoto(this);">
                                              <option value="0">Hangup</option>
                                            </select>
                                          </div>
                                          </td>
                                      <td class="paddingtop" align="right">
                                        <button type="button" title="Select Primary Language" id="extraFileUploadBtn" onclick="extraFileUploadFormOpener(this)" class="btn btn-xs actionbuttonsmedium lastaction"><i class="fa fa-globe"></i></button>
                                        <input name="extraFileName" id="extraFilename" type="hidden" value="filename"/>
                                        <button class="btn btn-xs actionbuttonsmedium" type="button"  onclick="moveRowUp(this)"  title="Move Up"><i class="fa fa-long-arrow-up"></i></button>
                                        <button class="btn btn-xs actionbuttonsmedium lastaction" onclick="moveRowDown(this)" type="button" title="Move Down"><i class="fa fa-long-arrow-down"></i></button>
                                        <button class="btn btn-xs actionbuttons" onclick="treeDesigner.crud.removeInvalidResponseRow(this)" type="button" title="Remove Record"><i class="fa fa-times"></i></button>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <input id="confirmationSaveButton" type="button" class="btn btn-success submit" onclick="treeDesigner.crud.saveConfirmation();" value="Save">
                      <button type="button" class="btn btn-primary cancel"  onclick="closeModal('confirmationModal');">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>


<style type="text/css">
  .btn-file {
  position: relative;
  overflow: hidden;
}
.btn-file input[type=file] {
  position: absolute;
  top: 0;
  right: 0;
  min-width: 100%;
  min-height: 100%;
  font-size: 100px;
  text-align: right;
  filter: alpha(opacity=0);
  opacity: 0;
  background: red;
  cursor: inherit;
  display: block;
}
.file-input-label {
  padding: 0px 10px;
  display: table-cell;
  vertical-align: middle;
  border: 1px solid #ddd;
  border-radius: 4px;
}
input[readonly] {
  background-color: white !important;
  cursor: text !important;
}

</style>