<?php

/* 
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
?>

<div class="modal fade" id="languageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Language</h4>
        {{csrf_field()}}
      </div>
      <form class="" id="languageForm" action="" method="post">
        <div class="modal-footer modal-head">
          <div class="row no-gutter">
            <div class="col-md-3">
              <div class="form-group form-md-line-input">
                <label class="col-md-4 control-label" for="languageID">ID</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="languageID" id="languageID" maxlength="5">
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label" for="languageText">Prompt Text</label>
                <div class="col-md-9">
                  <input type="text" class="form-control prompt_text" name="languageText" id="languageText" maxlength="150">
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group form-md-line-input">
                <label for="languageTimeOut" class="col-md-4 control-label">Timeout</label>
                <div class="col-md-8">
                  <select class="form-control apptype myselect" name="languageTimeOut" id="languageTimeOut">
                    <option value="1000">1</option>
                    <option value="2000">2</option>
                    <option value="3000">3</option>
                    <option value="4000">4</option>
                    <option value="5000">5</option>
                    <option value="6000">6</option>
                    <option value="7000">7</option>
                    <option value="8000">8</option>
                    <option value="9000">9</option>
                    <option value="10000">10</option>
                    <option value="11000">11</option>
                    <option value="12000">12</option>
                    <option value="13000">13</option>
                    <option value="14000">14</option>
                    <option value="15000">15</option>
                    <option value="16000">16</option>
                    <option value="17000">17</option>
                    <option value="18000">18</option>
                    <option value="19000">19</option>
                    <option value="20000">20</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row no-gutter">
            <div class="col-md-3">
              <div class="form-group form-md-line-input">
                <label for="languageGoto" class="col-md-4 control-label">Go to</label>
                <div class="col-md-8">
                  <select class="form-control apptype myselect" name="languageGoto" id="languageGoto">
                    <option value="0">Hangup</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-md-line-input">
                <label for="languageASR" class="col-md-3 control-label">ASR</label>
                <div class="col-md-9">
                  <select class="form-control apptype myselect" name="languageASR" id="languageASR">
                    <option value="TRUE">True</option>
                    <option value="FALSE">False</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group form-md-line-input">
                <label for="languageGrammer" class="col-md-4 control-label">Grammer</label>
                <div class="col-md-8">
                  <select class="form-control apptype myselect" name="languageGrammer" id="languageGrammer">
                    <option value="DIGITS">Digits</option>
                    <option value="YESNO">Yes/No</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
      <div class="modal-body no-topPadding">
        <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#languageInvalidresponsetab" aria-controls="invalidresponsetab" role="tab" data-toggle="tab">Invalid Response</a></li>
            <li role="presentation"><a href="#languageNoresponsetab" aria-controls="noresponsetab" role="tab" data-toggle="tab">No Response</a></li>
          </ul>
          <!-- Tab panes -->
          <div class="row top-padding rowBorder">
            <form style="display:none;padding-top:8px" name="myLanguageUploadExtraForm" id="myLanguageUploadExtraForm" action="ajax/extrafile-upload" method="POST" enctype="multipart/form-data">
              <input type="hidden" id="commandType" name="commandType" value="menu"/>
              <div class="col-xs-12 col-sm-1 col-md-1 form-group">
                <label for="BR" class="pull-right">Language</label>
              </div>
              <div class="col-xs-12 col-sm-2 col-md-2 form-group">
                <select class="form-control language myselect" name="extraLanguage" id="extraLanguage">
                  <option value="English">English</option>
                  <option value="Spanish">Spanish</option>
                  <option value="German">German</option>
                  <option value="French">French</option>
                </select>
              </div>
              
              <!--<div class="col-xs-12 col-sm-2 col-md-2 form-group">
                <label for="file_upload" class="pull-right control-label">File</label>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-4 form-group">
                <div class="input-group">
                  <span class="input-group-btn">
                    <span class="btn btn-info btn-file">
                      <i class="fa fa-sticky-note-o"></i> Browse&hellip;
                      <input type="hidden" id="extraFileAppId" name="extraFileAppId" value="" />
                      <input type="hidden" id="extraOwnerPromptId" name="extraOwnerPromptId" value="" />
                      <input type="hidden" id="extraFilePromptId" name="extraFilePromptId" value="" />
                      <input name="extraFile_upload" id="extraFile_upload" type="file" class="form-control"/>
                    </span>
                  </span>
                  <input type="text" class="form-control" readonly>
                </div>
              </div>-->

              <div class="col-xs-12 col-sm-2 col-md-6 form-group">
                  <div class="input-group">
                    <span class="input-group-btn">
                      <label class="btn btn-primary btn-file" for="multiple_input_group">
                        <div class="input required">
                          <input type="hidden" id="extraFileAppId" name="extraFileAppId" value="" />
                        <input type="hidden" id="extraOwnerPromptId" name="extraOwnerPromptId" value="" />
                        <input type="hidden" id="extraFilePromptId" name="extraFilePromptId" value="" />
                        <input name="extraFile_upload" id="extraFile_upload" type="file"/></div> Browse
                      </label>
                    </span>
                    <span class="file-input-label"></span>
                  </div>
              </div>
              
              <div class="col-md-2">
                <input type="submit" id="submit-btn" value="Upload" class="btn btn-success"/>
                <img src="/images/LoaderIcon.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
              </div>
            </form>
          </div>
          <div class="tab-content scroller" style="height:200px">
            <div role="tabpanel" class="tab-pane active" id="languageInvalidresponsetab">
              <div class="table-responsive">
                <form id="languageInvalidResponseForm" action="" method="post">
                  <table class="table table-condensed" id="languageInvalidResponseTable">
                    <thead>
                      <tr>
                        <th width="100">Prompt ID</th>
                        <th>Prompt</th>
                        <th >Action</th>
                        <th></th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr id="languageInvalidRow">
                        <td>
                          <div class="form-group form-md-line-input">
                            <select class="form-control BR myselect" style="width:110px" onchange="treeDesigner.crud.setlanguageInvalidPromptText(event);"  name="languageInvalidPromptList" id="languageInvalidPromptList">
                              <option value="0">Create New</option>
                            </select>
                          </div>
                        </td>
                        <td>
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control prompt_text" placeholder="Prompt" name="languageInvalidPromptText" maxlength="150" id="languageInvalidPromptText">
                          </div>
                        </td>
                        <td  class="paddingtop radioCol">
                          <label >
                                <input type="radio" name="languageInvalidAction" checked id="languageInvalidRepeat" value="RP" class="md-radiobtn">
                                Repeat </label>
                               <label >
                                  <input type="radio" name="languageInvalidAction" id="languageInvalidGoto" value="0" class="md-radiobtn">
                                 
                                    Goto </label>
                                 
                            </td>
                            <td>
                              <div class="form-group form-md-line-input">
                                <select class="form-control BR myselect" name="languageInvalidGotoList" id="languageInvalidGotoList" onchange="treeDesigner.crud.languageInvalidResponseGoto(this);">
                                  <option value="0">Hangup</option>
                                </select>
                              </div>
                            </td>
                            <td class="paddingtop" align="right">
                              <button class="btn btn-xs actionbuttons" type="button" title="Add Record" onclick="treeDesigner.crud.addLanguageInvalidResponseRow();"><i class="fa fa-plus"></i></button>
                            </td>
                          </tr>
                          <tr id="languageInvalidResponseTemplate" class="hidden">
                            <td>
                              <div class="form-group form-md-line-input">
                                <input type="text" readonly class="form-control" placeholder="Prompt" id="languageInvalidPromptID" name="languageInvalidPromptID">
                              </div>
                            </td>
                            <td>
                              <div class="form-group form-md-line-input">
                                <input type="text" readonly class="form-control" placeholder="Prompt" id="languageInvalidPromptText" name="languageInvalidPromptText">
                              </div>
                            </td>
                            <td  class="paddingtop radioCol">
                              <label>
                                    <input type="radio" name="languageInvalidAction" id="languageInvalidRepeat" value="RP" class="md-radiobtn">
                                 
                                    Repeat </label>
                                    </div>
                                        <label>
                                      <input type="radio" name="languageInvalidAction" id="languageInvalidGoto" value="0" class="md-radiobtn">
                                  
                                        Goto </label>
                                      
                                </td>
                                <td>
                                  <div class="form-group form-md-line-input">
                                    <select class="form-control BR myselect" name="languageInvalidGotoList" id="languageInvalidGotoList" onchange="treeDesigner.crud.languageInvalidResponseGoto(this);">
                                      <option value="0">Hangup</option>
                                    </select>
                                  </div>
                                </td>
                                <td class="paddingtop" align="right">
                                  <button type="button" title="Select Primary Language" id="extraFileUploadBtn" onclick="extraFileUploadFormOpener(this)" class="btn btn-xs actionbuttonsmedium lastaction"><i class="fa fa-globe"></i></button>
                                  <input name="extraFileName" id="extraFilename" type="hidden" value="filename"/>
                                  <button class="btn btn-xs actionbuttonsmedium" type="button" title="Move Up"  onclick="moveRowUp(this);" ><i class="fa fa-long-arrow-up"></i></button>
                                  <button class="btn btn-xs actionbuttonsmedium lastaction" type="button" title="Move Down"  onclick="moveRowDown(this);" ><i class="fa fa-long-arrow-down"></i></button>
                                  <button class="btn btn-xs actionbuttons" onclick="treeDesigner.crud.removeInvalidResponseRow(this)" type="button" title="Remove Record"><i class="fa fa-minus"></i></button>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </form>
                      </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="languageNoresponsetab">
                      <div class="table-responsive">
                        <form class="" id="languageNoResponseForm" action="" method="post">
                          <table id="languageNoResponseTable" class="table table-condensed">
                            <thead>
                              <tr>
                                <th width="100">Prompt ID</th>
                                <th>Prompt</th>
                                <th >Action</th>
                                <th></th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr id="languageNoResponseRow">
                                <td>
                                  <div class="form-group form-md-line-input">
                                    <select class="form-control BR myselect" style="width:110px" onchange="treeDesigner.crud.setlanguageNoResponsePromptText(event);" name="languageNoResponsePromptList" id="languageNoResponsePromptList">
                                      <option value="0">Create New</option>
                                    </select>
                                  </div>
                                </td>
                                <td>
                                  <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control prompt_text" placeholder="Prompt" name="languageNoResponsePromptText" maxlength="150" id="languageNoResponsePromptText">
                                  </div>
                                </td>
                                <td  class="paddingtop radioCol">
                                  <label >
                                        <input type="radio" name="languageNoResponseAction" checked id="languageNoResponseRepeat" value="RP" class="md-radiobtn">
                                       
                                         Repeat </label>
                                           <label>
                                          <input type="radio" name="languageNoResponseAction" id="languageNoResponseGoto" value="0" class="md-radiobtn">
                                      
                                           Goto </label>
                                          
                                    </td>
                                    <td>
                                      <div class="form-group form-md-line-input">
                                        <select class="form-control BR myselect" class="form-control BR  " onchange="treeDesigner.crud.languageNoResponseGoto(this);" name="languageNoResponseGotoList" id="languageNoResponseGotoList">
                                          <option value="0">Hangup</option>
                                        </select>
                                      </div>
                                    </td>
                                    <td class="paddingtop" align="right">
                                      <button class="btn btn-xs actionbuttons" type="button" title="Add Record" onclick="treeDesigner.crud.addLanguageNoResponseRow();"><i class="fa fa-plus"></i></button>
                                    </td>
                                  </tr>
                                  <tr id="languageNoResponseTemplate" class="hidden">
                                    <td>
                                      <div class="form-group form-md-line-input">
                                        <input type="text" readonly class="form-control prompt_text" placeholder="Prompt" id="languageNoResponsePromptID" name="languageNoResponsePromptID">
                                      </div>
                                    </td>
                                    <td>
                                      <div class="form-group form-md-line-input">
                                        <input type="text" readonly class="form-control prompt_text" placeholder="Prompt" id="languageNoResponsePromptText" name="languageNoResponsePromptText">
                                      </div>
                                    </td>
                                    <td  class="paddingtop radioCol">
                                       <label >
                                            <input type="radio" name="languageNoResponseAction" id="languageNoResponseRepeat" value="RP" class="md-radiobtn">
                                          
                                             Repeat </label>
                                                   <label >
                                              <input type="radio" name="languageNoResponseAction" id="languageNoResponseGoto" value="0" class="md-radiobtn">
                                      
                                                Goto </label>
                                             
                                        </td>
                                        <td>
                                          <div class="form-group form-md-line-input">
                                            <select class="form-control BR myselect" class="form-control BR  " name="languageNoResponseGotoList" id="languageNoResponseGotoList" onchange="treeDesigner.crud.languageNoResponseGoto(this);">
                                              <option value="0">Hangup</option>
                                            </select>
                                          </div>
                                        </td>
                                        <td class="paddingtop" align="right">
                                          <button type="button" title="Select Primary Language" id="extraFileUploadBtn" onclick="extraFileUploadFormOpener(this)" class="btn btn-xs actionbuttonsmedium lastaction"><i class="fa fa-globe"></i></button>
                                          <input name="extraFileName" id="extraFilename" type="hidden" value="filename"/>
                                          <button class="btn btn-xs actionbuttonsmedium" type="button"  onclick="moveRowUp(this)"  title="Move Up"><i class="fa fa-long-arrow-up"></i></button>
                                          <button class="btn btn-xs actionbuttonsmedium lastaction" onclick="moveRowDown(this)" type="button" title="Move Down"><i class="fa fa-long-arrow-down"></i></button>
                                          <button class="btn btn-xs actionbuttons" onclick="treeDesigner.crud.removeNoResponseRow(this)" type="button" title="Remove Record"><i class="fa fa-minus"></i></button>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <input id="languageSaveButton" type="button" class="btn btn-success submit" onclick="treeDesigner.crud.saveLanguage();" value="Save">
                        <button type="button" class="btn btn-primary cancel"  onclick="closeModal('languageModal');">Cancel</button>
                      </div>
                    </div>
                  </div>
                </div>
