@extends('ivr.layouts.master')
@section('page_title')
   IVR Designer
@endsection
@section('custom-styles')

    <link href="/css/jquery-ui.css" rel="stylesheet">
    <link href="/css/jstree.css" rel="stylesheet">
    <link href="/css/developer.css" rel="stylesheet">

    <style>
        /* For slimscroller in command navigation panel */

        .page-content .row {
            padding-left: 0px;
            padding-top: 0px;
            padding-bottom: 0px
        }
    </style>
@endsection
@section('content')
    <div class="portlet light page-header">
        <div class="row no-gutter">
            @include('ivr.designer.partials.heading')
            @include('ivr.designer.partials.btns')
        </div>
    </div>
    <div class="row portlet light no-gutter">
        <div class="col-md-12">
            @include('ivr.layouts.error')
            @include('ivr.layouts.success')
        </div>
    </div>
    <div class="col-md-12 panel-default nopadding portlet light">
        <div class="noleftrightpading">

            <div class="noleftrightpading menu ivrDesignerBarNav">
                <div class="subNav" id="command">
                    <div class="" style="height: 522px">
                        <ul>
                            <li command="prompt"><a href="javascript:;" class="playBtn tooltips" data-placement="right"
                                                    title="Play"></a></li>
                            <li command="menu"><a href="#" class="menuBtn tooltips" data-placement="left"
                                                  title="Menu"></a></li>
                            <li command="input"><a href="#" class="inputBtn tooltips" data-placement="left"
                                                   title="Input"></a></li>
                            <li command="record"><a href="#" class="recordBtn tooltips" data-placement="left"
                                                    title="Record"></a></li>
                            <li command="terminator"><a href="#" class="terminate tooltips" data-placement="left"
                                                        title="Terminator"></a></li>
                            <li command="transfer"><a href="#" class="transferBtn tooltips" data-placement="left"
                                                      title="Transfer"></a></li>
                            <li command="confirmation"><a href="#" class="confirmationBtn tooltips"
                                                          data-placement="left" title="Confirmation"></a></li>
                            <li command="keypress"><a href="#" class="keypressBtn tooltips" data-placement="left"
                                                      title="Keypress"></a></li>
                            <li command="function"><a href="#" class="functionBtn tooltips" data-placement="left"
                                                      title="isBusinessHour"></a></li>
                            <li command="if"><a href="#" class="ifBtn tooltips" data-placement="left" title="IF"></a>
                            </li>
                            <li command="variable"><a href="#" class="variableBtn tooltips" data-placement="left"
                                                      title="Variable"></a></li>
                            <li command="uri"><a href="#" class="URIBtn tooltips" data-placement="left" title="URI"></a>
                            </li>
                            <li command="legal"><a href="#" class="legalBtn tooltips" data-placement="left"
                                                   title="Legal"></a></li>
                            <li command="language"><a href="#" class="languageBtn tooltips" data-placement="left"
                                                      title="Language"></a></li>
                            <li command="tts"><a href="#" class="ttsBtn tooltips" data-placement="left" title="TTS"></a>
                            </li>
                            <li command="setvalue"><a href="#" class="setValue tooltips" data-placement="left"
                                                      title="Set Value"></a></li>
                            <li command="audio"><a href="#" class="audioBtn tooltips" data-placement="left"
                                                   title="Audio"></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="" style="margin-left:60px">
                <div class="contIVR">
                    <div class="redarrow_box">
                        <div id="xmlEditor" style="display:none">
                            <textarea></textarea>
                        </div>
                    </div>
                    <div class="IVRAcc ivrListing scroller" id="ivrContainer"></div>
                </div>
            </div>

            <!--<button type="button" class="btn btn-default btn-grey extend">Open Modal</button>-->
            <div id="popup">
                @include ('ivr.designer.popups.prompt')
                @include ('ivr.designer.popups.terminator')
                @include ('ivr.designer.popups.keypress')
                @include ('ivr.designer.popups.confirmationkeypress')
                @include ('ivr.designer.popups.languagekeypress')
                @include ('ivr.designer.popups.menu')
                @include ('ivr.designer.popups.record')
                @include ('ivr.designer.popups.transfer')
                @include ('ivr.designer.popups.confirmation')
                @include ('ivr.designer.popups.input')
                @include ('ivr.designer.popups.fileupload')
                @include ('ivr.designer.popups.function')
                @include ('ivr.designer.popups.if')
                @include ('ivr.designer.popups.uri')
                @include ('ivr.designer.popups.variable')
                @include ('ivr.designer.popups.legal')
                @include ('ivr.designer.popups.language')
                @include ('ivr.designer.popups.tts')
                @include ('ivr.designer.popups.assignValue')
                @include ('ivr.designer.popups.audio')
                @include ('ivr.designer.popups.copyIVR')
            </div>
        </div>
    </div>
{{csrf_field()}}
@endsection
@section('custom-js')
    <script src="/js/jquery.form.min.js"></script>
    <script src="/js/designer/fileupload.js"></script>
    <script src="/js/jstree/jstree.min.js"></script>
    <script src="/js/jstree/jstree.dnd.js"></script>
    <script src="/js/designer/IVRModel.js"></script>
    <script src="/js/designer/ScriptBuilder.js"></script>
    <script src="/js/designer/crud.js"></script>
    <script src="/js/designer/TreeDesigner.js"></script>
    <script src="/js/designer/designer.js"></script>
    <script src="/js/prettify.js"></script>
    {{--<script src="/js/tooltip.js"></script>--}}
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>

@endsection