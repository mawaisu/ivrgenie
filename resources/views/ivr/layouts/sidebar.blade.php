<?php $route = request()->route()->getName();?>
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed" data-keep-expanded="false" data-auto-scroll="true"
            data-slide-speed="200" style="padding-top: 0px">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler"></div>
            </li>
            <!--<li class="sidebar-search-wrapper">
               <form class="sidebar-search" action="#." method="POST">
                  <a href="javascript:;" class="remove">
                     <i class="icon-close"></i>
                  </a>
                  <div class="input-group">
                     <input type="text" class="form-control" placeholder="Search...">
                     <span class="input-group-btn">
                        <a href="javascript:;" class="btn submit">
                           <i class="icon-magnifier"></i>
                        </a>
                     </span>
                  </div>
               </form>
            </li>-->
            <li class="nav-item {{$route=='dashboard' || $route=='favorite.app.index'  ? 'active' : '' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Home</span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item {{$route=='dashboard' ? 'active' : '' }}">
                        <a href="{{route('dashboard')}}" class="nav-link nav-toggle"> Dashboard</a>
                    </li>
                    <li class="nav-item {{ $route=='favorite.app.index'  ? 'active' : '' }}">
                        <a href="{{route('favorite.app.index')}}" class="nav-link nav-toggle"> Favourite</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item  {{$route=='role.index' || $route=='user.index' || $route=='team.index' ||$route=='company.index' || $route=='permission.index' || $route=='role.assignment.index'  ? 'active' : '' }}">
                <a href="#." class="nav-link nav-toggle">
                    <i class="icon-wrench"></i>
                    <span class="title">Account Management</span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item {{$route=='user.index'?'active':''}}">
                        <a href="{{route('user.index')}}"> Users</a>
                    </li>
                    <li class="nav-item {{ $route=='company.index'?'active':''}}">
                        <a href="{{route('company.index')}}" class="nav-link"> Companies</a>
                    </li>
                    <li class="nav-item {{$route=='team.index'?'active':''}}">
                        <a href="{{route('team.index')}}" class="nav-link"> Teams</a>
                    </li>
                    <li class="nav-item {{$route=='role.index'?'active':''}}">
                        <a href="{{route('role.index')}}" class="nav-link">Roles</a>
                    </li>
                    <li class="nav-item  {{$route=='permission.index'?'active':''}}">
                        <a href="{{route('permission.index')}}" class="nav-link">Routes</a>
                    </li>
                    <li class="nav-item  {{$route=='role.assignment.index'?'active':''}}">
                        <a href="{{route('role.assignment.index')}}" class="nav-link"> Role Route Assignment</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item {{$route=='application.index' || $route=='tfn.index' || $route=='report.index'|| $route=='report.distro.index'  ? 'active' : '' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-layers"></i>
                    <span class="title">Manage Applications</span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item {{$route=='application.index'?'active':''}}">
                        <a href="{{route('application.index')}}" class="nav-link"> Application</a>
                    </li>
                    <li class="nav-item{{$route=='tfn.index'?'active':''}} ">
                        <a href="{{route('tfn.index')}}" class="nav-link"> TFN Inventory</a>
                    </li>
                    <li class="nav-item {{$route=='tfn.index'?'active':''}}">
                        <a href="#" class="nav-link"> TFN</a>
                    </li>
                    <li class="nav-item {{$route=='report.index'?'active':''}}">
                        <a href="{{route('report.index')}}" class="nav-link"> Reports</a>
                    </li>
                    <li class="nav-item {{$route=='report.distro.index'?'active':''}} ">
                        <a href="{{route('report.distro.index')}}" class="nav-link"> Reports Distro</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item  {{$route=='reports.ivr.index'  ? 'active' : '' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Reports</span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item {{$route=='reports.ivr.index'?'active':''}}">
                        <a href="{{route('reports.ivr.index')}}">IVR Reports</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item {{$route=='designer.index'?'active':''}} ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-wrench"></i>
                    <span class="title">Designer</span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item {{$route=='designer.index'?'active':''}}">
                        <a href="{{route('designer.index')}}" class="nav-link"> IVR Designer</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link"> Survey Designer</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-pie-chart"></i>
                    <span class="title">Analytics</span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="#" class="nav-link"> Transcription Status</a>
                    </li>
                    <li class="nav-item">
                        <a href="3" class="nav-link"> DataSpy (Haven)</a>
                    </li>

                    <li class="nav-item">
                        <a href="#" class="nav-link"> Haven Holiday</a>
                    </li>

                    <li class="nav-item">
                        <a href="#" class="nav-link"> Transcription US</a>
                    </li>

                    <li class="nav-item">
                        <a href="#" class="nav-link"> Top Record</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link"> Call Summary</a>
                    </li>

                    <li class="nav-item">
                        <a href="#" class="nav-link">CNA Field Heading</a>
                    </li>

                    <li class="nav-item">
                        <a href="#" class="nav-link">View Reports</a>
                    </li>

                    <li class="nav-item">
                        <a href="#" class="nav-link">Absence Line Distros</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item">
                <a href="#" class="nav-link">
                    <!-- <i class="icon-user-unfollow"></i> -->
                    <i class="icon-user-unfollow"></i>
                    <span class="title">AbsenceLine Dashboard</span>
                </a>
            </li>
            <li class="nav-item {{$route=='cwo.outdial'  ? 'active' : '' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-call-out"></i>
                    <span class="title">Out Dial</span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item {{$route=='cwo.outdial'  ? 'active' : '' }}">
                        <a href="{{route('cwo.outdial')}}" class="nav-link nav-toggle">CWO</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>