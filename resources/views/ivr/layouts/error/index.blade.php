@extends('ivr.layouts.master')
@section('custom-styles')
    <style>
        ul.dt-button-collection.dropdown-menu {
            opacity: 1;
        }
    </style>
@endsection
@section('page_title')
    Error Page
@endsection
@section('content')
    <div class="row portlet light no-gutter">
        <div class="col-md-12">

            <h1 style="color: #EB0045;font-size: 125px;">OOPS! <span class="pull-right">  <img src="/images/error.gif"
                                                                                               class="img-responsive"
                                                                                               width="256" height="256"></span>
            </h1>
            <h1> {{\App\CommonUtils\CommonStrings::$commonError}}</h1>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @include('ivr.layouts.error')
            @include('ivr.layouts.success')
        </div>

    </div>

@endsection
@section('custom-js')

@endsection