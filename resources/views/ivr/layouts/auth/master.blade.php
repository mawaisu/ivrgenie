<!DOCTYPE html>
<html lang=">
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="/images/favicon.ico">

<title>IVR-LOGIN</title>
<link rel="icon" href="/images/favicon.ico">
<link href="/css/bootstrap.min.css" rel="stylesheet">
{{--<link href="/js/datatables/dataTables.bootstrap.min.css" rel="stylesheet">--}}
{{--<link href="/js/datatables/responsive.bootstrap.min.css" rel="stylesheet">
<link href="/jPList/jplist.filter-toggle-bundle.min.css" rel="stylesheet">
<link href="/jPList/jplist.core.min.css" rel="stylesheet">--}}
{{--<link href="/assets/9b799ea/css/daterangepicker.css" rel="stylesheet">
<link href="/assets/9b799ea/css/daterangepicker-kv.css" rel="stylesheet">
<link href="/assets/587d7ed3/css/kv-widgets.css" rel="stylesheet">--}}
<link href="/css/styles.css" rel="stylesheet">
<link href="/css/developer.css" rel="stylesheet">
<link href="/css/sweetalert.css" rel="stylesheet">
<link href="/css/simple-line-icons.min.css" rel="stylesheet">
<link href="/css/icon.css" rel="stylesheet">
<link href="/css/material-design.css" rel="stylesheet">
<link href="/css/layout.css" rel="stylesheet">
<link href="/css/default.css" rel="stylesheet">
<link href="/css/components-md.css" rel="stylesheet">
<link href="/css/custom.css" rel="stylesheet">
<link href="/css/bootstrap-select.css" rel="stylesheet">
<link href="/css/jquery-validation/validation.css" rel="stylesheet">
</head>

<body>
<div class="loginBody">
    <section class="login">
        <div class="container">
          @yield('content')
        </div> <!-- /.container -->
    </section> <!-- /.login -->
    <footer class="text-center ">
        &copy; <?php echo date("Y") ?> IBEX | IVR. All Rights Reserved.
    </footer>
</div> <!-- /.loginBody -->
</body>
<script src="/js/jquery.min.js"></script>
<script src="/js/jquery-ui.js"></script>
<script src="/js/bootstrap.js"></script>
<script src="/js/auth/auth.js"></script>
<script src="/js/theme/modernizr.custom.js"></script>
<script src="/js/theme/classie.js"></script>
<script src="/js/theme/uisearch.js"></script>
<script src="/js/theme/bootstrap-hover-dropdown.min.js"></script>
<script src="/js/jquery.slimscroll.min.js"></script>
<script src="/js/theme/jquery.uniform.min.js"></script>
<script src="/js/theme/bootstrap-switch.min.js"></script>
<script src="/js/theme/morris.min.js"></script>
<script src="/js/theme/raphael-min.js"></script>
<script src="/js/app.min.js"></script>
<script src="/js/dashboard.js"></script>
<script src="/js/layout.min.js"></script>
<script src="/js/placeholder.js"></script>
<script src="/js/modernizr.js"></script>
<script src="/js/developer.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script src="/js/additional-methods.min.js"></script>
<script src="/js/sweetalert/sweetalert.min.js"></script>
<script src="/js/bootstrap-select.js"></script>
</html>



