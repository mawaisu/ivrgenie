<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <script language="javascript" type="text/javascript">
        function chngimg() {
            var img = document.getElementById('imgplus').src;
            if (img.indexOf('arrow_carrot-right.svg') != -1) {
                document.getElementById('imgplus').src = '/images/arrow_carrot-left.svg';
            }
            else {
                document.getElementById('imgplus').src = '/images/arrow_carrot-right.svg';
            }
        }
    </script>
    <title>@yield('page_title')</title>
    @include('ivr.layouts.styles-css')
    @yield('custom-styles')
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<div id="loader" class="overlay" style="display:none;">
    <div class="spinner">
    <div class="bounce1"></div>
    <div class="bounce2"></div>
    <div class="bounce3"></div>
</div>
</div>
<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner">
        @include('ivr.layouts.left-submenu')

        @include('ivr.layouts.logo-header')

        @include('ivr.layouts.top-navbar')

    </div>
</div>
<div class="clearfix"></div>
<div class="page-container">
    @include('ivr.layouts.sidebar')
    <div class="page-content-wrapper">
        <div class="page-content" style="min-height:579px">
            <div class="clearfix"></div>
            <div class="row">

                @yield('content')

            </div>
        </div>
    </div>
</div>


@include('ivr.layouts.footer')
</body>
@include('ivr.layouts.scripts')

<script type="text/javascript">
    $(document).on('change', '.btn-file :file', function () {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });

    $(document).ready(function () {
        $('.btn-file :file').on('fileselect', function (event, numFiles, label) {
            console.log("teste");
            var input_label = $(this).closest('.input-group').find('.file-input-label'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

            if (input_label.length) {
                input_label.text(log);
            } else {
                if (log) alert(log);
            }
        });
    });
</script>

<script>
    $(document).ready(function () {
        //$('.selectpicker').selectpicker('render');
        $('.selectpicker').on('changed.bs.select');
    });
</script>

@yield ('custom-js')

</html>