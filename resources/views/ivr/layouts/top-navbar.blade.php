<div class="top-menu">
    <ul class="nav navbar-nav pull-right">
       <li class="dropdown dropdown-user">
          <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
             <img alt="" class="img-circle" src="/images/avatar.jpg" />
             <span class="username username-hide-on-mobile">
             </span>
             <i class="fa fa-angle-down"></i>
          </a>
          <ul class="dropdown-menu dropdown-menu-default" style="left:-79px">
             <!--<li>
                <a href="#."><i class="icon-user"></i> My Profile </a>
             </li>
             <li>
                <a href="#.">
                <i class="icon-question"></i> Help</a>
             </li>-->
         {{--    <li>
                <a href="{{route('logout')}}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">

                <i class="icon-power"></i> Log Out</a>
             </li>--}}
              <li>
                  <a href="{{route('auth.logout')}}" >

                      <i class="icon-power"></i> Log Out</a>
              </li>
          </ul>
           {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
               {{ csrf_field() }}
           </form>--}}
       </li>
    </ul>
 </div>
