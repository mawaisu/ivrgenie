<link rel="icon" href="/images/favicon.ico">
<link href="/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/components.css" rel="stylesheet">
{{--<link href="/js/datatables/dataTables.bootstrap.min.css" rel="stylesheet">--}}
{{--<link href="/js/datatables/responsive.bootstrap.min.css" rel="stylesheet">
<link href="/jPList/jplist.filter-toggle-bundle.min.css" rel="stylesheet">
<link href="/jPList/jplist.core.min.css" rel="stylesheet">--}}
{{--<link href="/assets/9b799ea/css/daterangepicker.css" rel="stylesheet">
<link href="/assets/9b799ea/css/daterangepicker-kv.css" rel="stylesheet">
<link href="/assets/587d7ed3/css/kv-widgets.css" rel="stylesheet">--}}
<link href="/css/styles.css" rel="stylesheet">
<link href="/css/developer.css" rel="stylesheet">
<link href="/css/sweetalert.css" rel="stylesheet">
<link href="/css/simple-line-icons.min.css" rel="stylesheet">
<link href="/css/icon.css" rel="stylesheet">
<link href="/css/material-design.css" rel="stylesheet">
<link href="/css/layout.css" rel="stylesheet">
<link href="/css/default.css" rel="stylesheet">
<link href="/css/components-md.css" rel="stylesheet">
<link href="/css/custom.css" rel="stylesheet">
<link href="/css/bootstrap-select.css" rel="stylesheet">
<link href="/css/jquery-validation/validation.css" rel="stylesheet">
<link href="/css/datatables_colvis.min.css" rel="stylesheet">

{{--
<link href="/datatables/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>
--}}{{--
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.2/b-colvis-1.5.2/b-html5-1.5.2/b-print-1.5.2/rr-1.2.4/datatables.min.css"/>
--}}