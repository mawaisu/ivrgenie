<div class="top-menu pull-left">
    <ul class="nav navbar-nav pull-left">
       <li class="dropdown dropdown-extended dropdown-notification familyAppToggle">
          <a href="javascript:;" class="dropdown-toggle familyAppsMenu" data-toggle="dropdown" style="padding-bottom:11px">
             <i class="menu-toggler"></i>
          </a>
          <ul class="dropdown-menu familyApps">
             <li>
                <ul class="dropdown-menu-list">
                   <li>
                             <ul class="dropdown-menu-list">
                             <li>
                                 <a href="https://portal.trgworld.com" target="_blank">
                                     <span class="details"><i class="ibexFamily ibexCore"></i>Core</span>
                                 </a>
                             </li>
                             <li>
                                 <a href="http://portal.trgworld.com/insight" target="_blank">
                                     <span class="details"><i class="ibexFamily ibexInsight"></i>Insight</span>
                                 </a>
                             </li>
                             <li>
                                 <a href="#">
                                     <span class="details"><i class="ibexFamily ibexQuartz"></i>Quartz</span>
                                 </a>
                             </li>
                             <li>
                                 <a href="http://wdpurple.trgworld.com/WebDialer/4.1/MYSQL/Portal/" target="_blank">
                                     <span class="details"><i class="ibexFamily ibexWebdialer"></i>Web Dialer</span>
                                 </a>
                             </li>
                             <li>
                                 <a href="https://portal.trgworld.com/force" target="_blank">
                                     <span class="details"><i class="ibexFamily ibexFamily ibexForce"></i>Force</span>
                                 </a>
                             </li>
                             <li>
                                 <a href="http://status.ibexglobal.com" target="_blank">
                                     <span class="details"><i class="ibexFamily ibexStatus"></i>Status Dashboard</span>
                                 </a>
                             </li>
                             <li>
                                 <a href="http://10.80.10.167:8080/auth/login" target="_blank">
                                     <span class="details"><i class="ibexFamily ibexIvr"></i>IVR</span>
                                 </a>
                             </li>
                             <li>
                                 <a href="https://portal.trgworld.com/QualityControl/CallEvaluationWeb.aspx" target="_blank">
                                     <span class="details"><i class="ibexFamily ibexNr"></i>NR Player</span>
                                 </a>
                             </li>
                             <li>
                                 <a href="http://status.ibexglobal.com" target="_blank">
                                     <span class="details"><i class="ibexFamily ibexIsite"></i>iSite</span>
                                 </a>
                             </li>
                             <li>
                                 <a href="https://e23.ultipro.com" target="_blank">
                                     <span class="details"><i class="ibexFamily ibexUltipro"></i>US: Ultipro</span>
                                 </a>
                             </li>
                             
                             <li>
                                 <a href="https://portal.afiniti.com/afinitiportal/Login.aspx" target="_blank">
                                     <span class="details"><i class="ibexFamily ibexAfiniti"></i>Afiniti</span>
                                 </a>
                             </li>
                             <li>
                                 <a href="https://ibexmail.ibexglobal.com" target="_blank">
                                     <span class="details"><i class="icon-bar-chart"></i>IBEX Email</span>
                                 </a>
                             </li>
                             <li>
                                 <a href="http://cmos.trgworld.com/" target="_blank">
                                     <span class="details"><i class="icon-envelope-open"></i>CMOS</span>
                                 </a>
                             </li>
                </ul>
             </li>
          </ul>
       </li>
    </ul>
 </div>