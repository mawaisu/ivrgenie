
<!--====== CONTENT ======-->
<div class="modal fade" id="addPermissionPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form class="form-horizontal" id="addPermissionForm" action="{{route('permission.edit')}}" method="post">
        {{csrf_field()}}
        <div class="modal-dialog md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="permissionModalLabel">Add Permission</h4>
                </div>
                <div class="modal-body noSpace">
                    <div class="form-group form-md-line-input">
                        <label for="application_name" class="col-md-4 control-label">Display Name</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="display_name" id="permissionDisplayName" maxlength="100" required>
                            <div class="pull-right" id="appnameloader"></div>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="application_name" class="col-md-4 control-label ">Actual Name</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="actual_name" id="permissionActualName" maxlength="100" required>
                            <div class="pull-right" id="appnameloader"></div>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="app_type" class="col-md-4 control-label">Status</label>
                        <div class="col-md-7">
                            <select class="form-control" name="status" id="permissionStatus">
                                <option value="1" selected>Active</option>
                                <option value="0">In-Active</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!--input type="button" class="btn btn-green submit" value="Save"-->
                    <button type="submit" id="submitButton" class="btn btn-success">Save</button>
                    <button type="button" id="cancelmodal" data-dismiss="modal" class="btn btn-primary">Cancel</button>
                </div>
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
        <input type="hidden"  value="" name="permissionId" id="permissionId" >
    </form>
</div>