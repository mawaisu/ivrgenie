<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* View Logs Route */
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

/*Route for Applications*/
Route::group(['middleware' => ['auth']], function () {

    Route::get('/application/index', 'ApplicationController@index')->name('application.index');
    Route::get('/application/check', 'ApplicationController@checkApplicationAjax')->name('application.checkAjax');
    Route::get('/application/get', 'ApplicationController@getApplicationAjax')->name('application.getAjax');
    Route::post('/application/create', 'ApplicationController@store')->name('application.edit');
    Route::get('/application/{id}/delete', 'ApplicationController@destroy')->name('application.delete');
    Route::get('/application/{id}/status-change', 'ApplicationController@statusChange')->name('application.statusChange');

    //Routes for Roles
    Route::get('/role/index', 'RoleController@index')->name('role.index');
    Route::post('/role/create', 'RoleController@store')->name('role.edit');
    Route::get('/role/get', 'RoleController@getRoleAjax')->name('role.getRoleAjax');
    Route::get('/role/{id}/delete', 'RoleController@destroy')->name('role.delete');

    //Routes for Permissions
    Route::get('/permission/index', 'PermissionController@index')->name('permission.index');
    Route::post('/permission/create', 'PermissionController@store')->name('permission.edit');
    Route::get('/permission/get', 'PermissionController@getPermissionAjax')->name('permission.getRoleAjax');
    Route::get('/permission/{id}/delete', 'PermissionController@destroy')->name('permission.delete');

    //Routes for Roles Assignment
    Route::get('/role/assignment/index', 'RoleAssignmentController@index')->name('role.assignment.index');
    Route::get('/role/assignment/ajax', 'RoleAssignmentController@getRoleAssignmentAjax')->name('role.assignment.getAjax');
    Route::get('/role/assignment/set', 'RoleAssignmentController@setRoleAssignmentAjax')->name('role.assignment.setAjax');

    //Route Report Distro
    Route::get('/report/distro', 'ReportDistroController@index')->name('report.distro.index');
    Route::get('/report/list/ajax', 'ReportDistroController@getReportListAjax')->name('report.list.ajax');
    Route::get('/email/list/ajax', 'ReportDistroController@getEmailListAjax')->name('email.list.ajax');
    Route::post('/report/distro/save', 'ReportDistroController@store')->name('report.distro.create');

    //Route Manage Report
    Route::get('/report/index', 'AppReportController@index')->name('report.index');
    Route::post('/report/create/update', 'AppReportController@store')->name('report.edit');
    Route::get('/report/{id}/delete', 'AppReportController@destroy')->name('report.delete');

    /*Route for Tfn*/

    Route::get('/tfn/index', 'TfnController@index')->name('tfn.index');
    Route::get('/tfn/getTeamApps', 'TfnController@getTeamApplicationsAjax')->name('team.appsList.ajax');
    Route::get('/tfn/checkTfn', 'TfnController@checkTfnAjax')->name('checkTfn.ajax');
    Route::get('/tfn/checkDnis', 'TfnController@checkDnisAjax')->name('checkDnis.ajax');
    Route::post('/tfn/create/update', 'TfnController@store')->name('tfn.edit');
    Route::get('/tfn/{id}/delete', 'TfnController@destroy')->name('tfn.delete');
    Route::get('/tfn/{id}/status-change', 'TfnController@statusChange')->name('tfn.statusChange');
    Route::get('/tfn/getAjax', 'TfnController@getTfnAjax')->name('tfn.getTfnAjax');

    //Routes for Companies
    Route::get('/company/index', 'CompanyController@index')->name('company.index');
    Route::post('/company/create', 'CompanyController@store')->name('company.edit');
    Route::get('/company/get', 'CompanyController@getRoleAjax')->name('company.getRoleAjax');
    Route::get('/company/{id}/delete', 'CompanyController@destroy')->name('company.delete');
    Route::get('/ajax/get-company', 'CompanyController@getCompanyAjax')->name('company.getCompanyAjax');
    Route::get('/ajax/check-companyduplicate', 'CompanyController@checkDuplicateAjax')->name('company.checkDuplicateAjax');


    //Routes for teams
    Route::get('/team/index', 'TeamController@index')->name('team.index');
    Route::post('/team/create', 'TeamController@store')->name('team.edit');
    Route::get('/team/get', 'TeamController@getTeamAjax')->name('team.getTeamAjax');
    Route::get('/team/{id}/delete', 'TeamController@destroy')->name('team.delete');
    Route::get('/ajax/get-team', 'TeamController@getTeamAjax')->name('team.getTeamAjax');
    Route::get('/ajax/check-teamduplicate', 'TeamController@checkDuplicateAjax')->name('team.checkDuplicateAjax');


    //Routes for users
    Route::get('/user/index', 'UserController@index')->name('user.index');
    Route::post('/user/create', 'UserController@store')->name('user.edit');
    Route::get('/ajax/get-user', 'UserController@getTeamAjax')->name('user.getTeamAjax');
    Route::get('/ajax/check-user', 'UserController@checkDuplicateAjax')->name('user.checkDuplicateAjax');
    Route::get('/ajax/get-companyteams', 'UserController@getCompanyTeams')->name('user.getCompanyTeams');
    Route::get('/ajax/get-user', 'UserController@getUserAjax')->name('user.getUserAjax');


    //IVR Designer Routes

    Route::get('/designer/index', 'DesignerController@index')->name('designer.index');
    Route::get('/designer/getIvrScriptAjax', 'DesignerController@getIvrScriptAjax')->name('designer.getIvrScriptAjax');
    Route::post('/designer/setIvrScriptAjax', 'DesignerController@setIvrScriptAjax')->name('designer.setIvrScriptAjax');
    Route::post('/designer/ajax/file-upload', 'DesignerController@sftpConnect1')->name('designer.fileUpload');
    Route::post('/ajax/extrafile-upload', 'DesignerController@extraFileUpload')->name('designer.extraFileUpload');

    Route::get('/','HomeController@dashboard')->name('dashboard')->middleware('permission');

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/error/page', 'HomeController@errorPage')->name('error.page');
    Route::get('/error', 'HomeController@page404')->name('404.page');

   //out dial
    Route::get('/cwo/out-dial', 'OutDialController@getCwoDial')->name('cwo.outdial');
    Route::post('/cwo/out-dial', 'OutDialController@postCwoCall')->name('cwo.call');

    //Route Favorite Applications
    Route::get('/favorite/applications/index', 'FavoriteAppController@index')->name('favorite.app.index');
    Route::get('/favorite/app/{id}/delete', 'FavoriteAppController@destroy')->name('favorite.app.delete');
    Route::post('/favorite/applications/create', 'FavoriteAppController@store')->name('favorite.app.edit');

    //Route ReportsController
    Route::get('/ivr-reports/index', 'ReportsController@index')->name('reports.ivr.index');

});

Route::get('/auth/login', 'AuthController@showLoginForm')->name('auth.login');
Route::post('/auth/login', 'AuthController@login')->name('auth.login');
Route::get('/auth/logout', 'AuthController@logout')->name('auth.logout');

Route::get('/application', function () {

    $apps = \App\Application::all();
    // return view('ivr.applications.index',compact('apps'));
    return view('ivr.applications.index', compact('apps'));
});

Auth::routes();

Route::get('/mytest', function () {

    dd(phpinfo());
});

Route::get('/ssh2', 'DesignerController@sftpConnect');
Route::get('/ssh', 'DesignerController@sftpConnect1');