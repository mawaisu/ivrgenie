<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    //

    protected $fillable = ['AppID', 'CompanyID', 'AppName', 'AppAbrv', 'AppType', 'Status', 'TimeZone', 'Environment', 'UserID', 'TeamID', 'CNA'];

    protected $table = 'application';

    protected $primaryKey = 'AppID';

    public $timestamps = false;

    public function user()
    {

        return $this->hasOne('App\User', 'id', 'UserID');
    }

    public function appReports()
    {

        return $this->hasMany('App\AppReport','AppID','AppID');
    }

    public function tfns(){

        return $this->hasMany('App\Tfn','AppID','AppID');
    }

    public function team(){

        return $this->belongsTo('App\Team','TeamID','Team_ID')->withDefault(['Name'=>'Unknown Team']);;
    }

    public function favoriteApps(){

        return $this->hasMany('App\FavoriteApp','AppID','AppID');
    }

}
