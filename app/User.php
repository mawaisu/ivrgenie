<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password','auth_key','firstname','lastname','CompanyID','TeamID','role_id'
    ];

    protected $table = 'user';

    protected $primaryKey = 'id';

    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
        'password', 'password_reset_token',
    ];

    public function applications(){

        return $this->hasMany('App\Application','UserID','id');
    }

    public function role(){

        return $this->belongsTo('App\Role')->withDefault(['name'=>'Unknown Role']);
    }

    public function team(){
        return $this->belongsTo('App\Team','TeamID','Team_ID')->withDefault(['Name'=>'Unknown Team']);
    }

    public function company(){
        return $this->belongsTo('App\Company','CompanyID','ID')->withDefault(['Name'=>'Unknown Company']);
    }

    public function favoriteApps(){

        return $this->hasMany('App\FavoriteApp','UserID','id');
    }
}
