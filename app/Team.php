<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = ['Team_ID','Company_ID','Name','Description'];

    protected $table = 'teams';

    protected $primaryKey = 'Team_ID';

    public $timestamps = false;

    public function company(){

        return $this->belongsTo('App\Company','Company_ID','ID')->withDefault(['Name'=>'Unknown Company']);
    }


    public function tfns(){

        return $this->hasMany('App\Tfn','TeamID','Team_ID');
    }

    public function applications(){

        return $this->hasMany('App\Application','TeamID','Team_ID');
    }

    public function users(){

        return $this->hasMany('App\User','TeamID','Team_ID');
    }
}
