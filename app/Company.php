<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    protected $table ='companies';

    protected $fillable = ['Name','Description'];

    protected $primaryKey = 'ID';

    public $timestamps = false;

    public function teams(){

        return $this->hasMany('App\Team','Company_ID','ID');
    }

    public function users(){

        return $this->hasMany('App\User','CompanyID','ID');
    }
}
