<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppReport extends Model
{

    protected $fillable = ['RptID', 'AppID', 'RptName', 'RptSubject', 'FromDateIndicator', 'RptHH', 'ReportEntryIndicator', 'RptEntryDayIndicator', 'Status'];

    protected $table = 'apprpttypes';

    protected $primaryKey = 'RptID';

    public $timestamps = false;

    public function app()
    {

        return $this->belongsTo('App\Application', 'AppID', 'AppID')->withDefault(['AppName'=>'Unknown App']);
    }

    public function reportDistros()
    {

        return $this->hasMany('App\ReportDistro', 'RptID', 'RptID');
    }

}
