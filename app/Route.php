<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $fillable = ['status','display_name','actual_name'];

    protected $table = 'routes';
}
