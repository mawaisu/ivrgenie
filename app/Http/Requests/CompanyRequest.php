<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'companyName' =>'required',
            'companyDesc' =>'required',
        ];
    }

    public function messages()
    {
        return [
            'companyName.required' => 'Unique Company name is required.',
            'companyDesc.required'  => 'Company description is required.',
        ];
    }
}
