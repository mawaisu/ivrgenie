<?php

namespace App\Http\Requests;

use http\Env\Request;
use Illuminate\Foundation\Http\FormRequest;

class TfnRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'customerAppList' =>'required',
            'dnistfn' => 'required|max:50',
            'tfnstatus' => 'required',
            'tfntype' =>'required',
            'customerList'=>'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'customerAppList.required' => 'Please Select Application.',
            'dnistfn.required'  => 'Please enter a valid TFN.',
            'tfnstatus.required' => 'Please Select a valid status.',
            'tfntype.required' =>'Please Select a valid TFN type.',
            'customerList.required'=>'Please Select Application Owner.',
            'dnistfn.max'  => 'DNIS and TFN may not be greater than 50 characters.',
            'tfnremarks.max'  => 'Remarks cannot be greater than 250 Characters.',
        ];
    }
}
