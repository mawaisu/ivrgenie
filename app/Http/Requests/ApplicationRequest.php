<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ApplicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
       /* if (Auth::user())
        return true;
        else
            return false;*/

       return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userID' =>'required',
            'appName' => 'required|max:50',
            'appType' => 'required|in:I,O,B,N,U,L',
            'appAbrv' =>'required|max:5',
            'timeZone'=>'required',
            'appStatus'=>'required|in:D,L',
            'environment'=>'required|in:S,P',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'UserID.required' => 'A User is required.',
            'body.required'  => 'Application name is required',
            'appAbrv.required' => 'Application Abbreviation is required.',
            'appType.required' => 'Application type is required.',
            'appAbrv.max' =>'Application Abbreviation cannot be greater than 50 characters.',
            'appName.max' =>'Application Name cannot be greater than 50 characters.',
            'TimeZone.required'=>'Application Timezone is required .',
            'Status.required'=>'Application status is required field',
            'Environment.required'=>'Application Environment is required.',
        ];
    }
}
