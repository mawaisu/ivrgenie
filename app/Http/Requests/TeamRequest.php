<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Name' =>'required',
            'teamDesc' =>'required',
            'teamCompany' =>'required',
        ];
    }

    public function messages()
    {
        return [
            'Name.required' => 'Unique Team name is required.',
            'teamDesc.required'  => 'Team description is required.',
            'teamCompany.required'  => 'Company is required.',
        ];
    }
}
