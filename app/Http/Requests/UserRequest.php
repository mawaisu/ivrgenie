<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'firstName' => 'required',
            'lastName' =>'required',
            'role'=>'required|integer',
            'teams'=>'required|',
            'company'=>'required|integer',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'firstName.required' => 'Please Enter First Name.',
            'lastName.required'  => 'Please enter Last Name.',
            'role.required' => 'Please Select a valid Role.',
            'teams.required' =>'Please Select a valid Team.',
            'company.required'=>'Please Select a Company.',
            'username.required'=>'Please enter a unique given user name.',
        ];
    }
}
