<?php

namespace App\Http\Middleware;

use App\CommonUtils\CommonStrings;
use App\CommonUtils\HelperFunctions;
use Closure;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
            if(HelperFunctions::permissionCheck()){

                return $next($request);
            }
        HelperFunctions::writeLogInfo(CommonStrings::$accessError,"error");
        return redirect()->route('error.page')->withErrors(['Access Error' => CommonStrings::$accessError]);
    }
}
