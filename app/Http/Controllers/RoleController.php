<?php
/*Created By M Awais Ullah 25,Sep,2018*/

namespace App\Http\Controllers;

use App\CommonUtils\CommonStrings;
use App\CommonUtils\HelperFunctions;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission', ['only' => 'index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $roles = Role::all();
            HelperFunctions::writeLogInfo($desc = "RoleController@index Returning View with roles list : " . $roles);
            return view('ivr.roles.index', compact('roles'));
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required|' . Rule::unique('roles')->ignore($request->roleId),
                'status' => 'required'
            ]);
            HelperFunctions::writeLogInfo($desc = "RoleController@store Passed Validation.");

            if (empty($request->roleId)) {

                $role = Role::create(['name' => $request->name, 'status' => $request->status]);
                HelperFunctions::writeLogInfo($desc = "RoleController@store Create New Role : " . $role);
                session()->flash('success', 'Role Created Successfully');
            } else {

                $role = Role::find($request->roleId)->update(['status' => $request->status, 'name' => $request->name]);
                HelperFunctions::writeLogInfo($desc = "RoleController@store Update Role : " . $role);
                session()->flash('success', 'Role Updated Successfully');
            }

            return redirect()->route('role.index');
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }


    public function destroy($id)
    {
        try {
            $role = Role::findOrFail($id);
            HelperFunctions::writeLogInfo($desc = "RoleController@destroy Delete Role : " . $role);
            $role->status = 0;
            $role->update();
            session()->flash('success', 'Role Deactivated Successfully');
            return redirect()->route('role.index');
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    public function getRoleAjax(Request $request)
    {
        try {
            $role = Role::findOrFail($request->id);
            HelperFunctions::writeLogInfo($desc = "RoleController@getRoleAjax Get Role : " . $role);
            return response()->json(['role' => $role]);
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

}
