<?php
/*Created By M Awais Ullah 25,Sep,2018*/

namespace App\Http\Controllers;

use App\CommonUtils\CommonStrings;
use App\CommonUtils\HelperFunctions;
use App\Role;
use App\RoleAssignment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class RoleAssignmentController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $roles = Role::all();
            HelperFunctions::writeLogInfo($desc = "RoleAssignmentController@index! Returning View with roles list [ " . $roles . "]");
            return view('ivr.role_assignment.index', compact('roles'));
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }


    public function getRoleAssignmentAjax(Request $request)
    {
        try {
            $url = CommonStrings::$getIvrRoleAssignment;
            $id = Input::get("roleId");
            $param = array("ParamList" => $request->roleId, "RequestID" => "900");

            $data = json_encode($param);
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);

            $output = curl_exec($ch);

            curl_close($ch);

            return ($output);

        } catch (\Exception $exception) {
            report($exception);
        }
    }

    public function setRoleAssignmentAjax(Request $request)
    {
        try {
            $url = CommonStrings::$setIvrRoleAssignment;

            $param = ($request->data);

            $data = json_encode($param);
            $ch = curl_init();
            //  dd($data);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);

            $output = curl_exec($ch);

            curl_close($ch);

            return ($output);

        } catch (\Exception $exception) {
            report($exception);

        }
    }

}
