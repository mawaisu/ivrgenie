<?php
/*Created By M Awais Ullah 25,Sep,2018*/

namespace App\Http\Controllers;

use App\CommonUtils\CommonStrings;
use App\CommonUtils\HelperFunctions;
use App\Permission;
use App\Route;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class PermissionController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission', ['only' => 'index']);
    }

    public function index()
    {
        try {
            $permissions = Route::all();
            HelperFunctions::writeLogInfo($desc = " PermissionController@index  Get All Routes :" . $permissions);
            return view('ivr.permissions.index', compact('permissions'));
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'actual_name' => 'required|' . Rule::unique('routes')->ignore($request->permissionId),
            'display_name' => 'required|' . Rule::unique('routes')->ignore($request->permissionId),
            'status' => 'required'
        ]);

        try {
            if (empty($request->permissionId)) {

                $permission = Route::create(['actual_name' => $request->actual_name, 'display_name' => $request->display_name, 'status' => $request->status]);

                HelperFunctions::writeLogInfo($desc = " PermissionController@store Create Route :" . $permission);

                session()->flash('success', 'Permission Created Successfully');

            } else {

                $permission = Route::findOrFail($request->permissionId)->update(['actual_name' => $request->actual_name, 'display_name' => $request->display_name, 'status' => $request->status]);

                HelperFunctions::writeLogInfo($desc = " PermissionController@store Update Route :" . $permission);

                session()->flash('success', 'Permission Updated Successfully');

            }
            return redirect()->route('permission.index');
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }


    public function getPermissionAjax(Request $request)
    {

        $permission = Route::findOrFail($request->id);
        HelperFunctions::writeLogInfo($desc = " PermissionController@getPermissionAjax Return Route :" . $permission);
        return response()->json(['permission' => $permission]);
    }

    public function destroy($id)
    {
        try {
            $permission = Route::findOrFail($id);
            HelperFunctions::writeLogInfo($desc = " PermissionController@destroy Delete Route :" . $permission);
            $permission->status = 0;
            $permission->update();
            session()->flash('success', 'Permission Deactivated Successfully');
            return redirect()->route('permission.index');
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }
}
