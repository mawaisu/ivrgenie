<?php
/*Created By M Awais Ullah 25,Sep,2018*/

namespace App\Http\Controllers;

use App\Application;
use App\CommonUtils\CommonStrings;
use App\CommonUtils\HelperFunctions;
use App\Http\Requests\ApplicationRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('permission', ['only' => 'index']);
    }

    public function index()
    {
        try {
            $applications = Application::all();
            HelperFunctions::writeLogInfo($decs = "ApplicationController@index get apps list : " . $applications);
            return view('ivr.applications.index', compact('applications'));
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    public function checkApplicationAjax(Request $request)
    {

        try {
            if ($request->appRecId == null) {
                $app = Application::where('AppName', $request->name)->first();

            } else {
                $app = Application::where('AppName', $request->name)->where('AppID','!=', $request->appRecId)->first();
            }

            if ($app) {
                HelperFunctions::writeLogInfo($decs = "ApplicationController@checkApplicationAjax Check App : " . $app);
                return response()->json(false);
            } else {
                return response()->json(true);
            }

        } catch (\Exception $exception) {
            report($exception);
        }
    }

    public function getApplicationAjax(Request $request)
    {
        try {
            $application = Application::findOrFail($request->id);
            HelperFunctions::writeLogInfo($decs = "ApplicationController@getApplicationAjax Returning App : " . $application);
            return json_encode($application);
        } catch (\Exception $exception) {
            report($exception);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApplicationRequest $request)
    {
        try {
            $request->validated();
            $userID=Session::get('UserID');
            $teamID=Session::get('TeamID');
            $companyID=Session::get('CompanyID');
            if ($request['appID'] != "" || $request['appID'] != null) {

                $application = Application::findOrFail($request['appID']);

                HelperFunctions::writeLogInfo($decs = "ApplicationController@store Update App : " . $application);

                $application->update([
                    'AppName' => $request->appName,
                    'AppType' => $request->appType,
                    'AppAbrv' => $request->appAbrv,
                    'TimeZone' => $request->timeZone,
                    'Status' => $request->appStatus,
                    'Environment' => $request->environment,
                ]);

                if ($application) {
                    session()->flash('success', CommonStrings::$successUpdate);
                } else {
                    session()->flash('success', CommonStrings::$failureUpdate);
                }


            } else {
                $application = Application::create([
                    'UserID' => $userID,
                    'AppName' => $request->appName,
                    'AppType' => $request->appType,
                    'AppAbrv' => $request->appAbrv,
                    'TimeZone' => $request->timeZone,
                    'Status' => $request->appStatus,
                    'Environment' => $request->environment,
                    'CompanyID' => $companyID,
                    'TeamID' => $teamID,
                ]);
                HelperFunctions::writeLogInfo($decs = "ApplicationController@store Create New App : " . $application);

                if ($application) {
                    session()->flash('success', CommonStrings::$successSave);
                } else {
                    session()->flash('error', CommonStrings::$failureSave);
                }

            }
            return redirect()->route('application.index');
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    public function destroy($id)
    {
        try {
            $application = Application::findOrFail($id);
            HelperFunctions::writeLogInfo($decs = "ApplicationController@destroy Delete App : " . $application);

            if ($application) {
                $application->delete();
                HelperFunctions::writeLogInfo($decs = "ApplicationController@destroy App Deleted Successfully");
                session()->flash('success', CommonStrings::$successDelete);
                return redirect()->route('application.index');
            } else {
                return redirect()->route('application.index')->withErrors(['Error' => CommonStrings::$failureDelete]);
            }
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    public function statusChange($id)
    {
        try {
            $application = Application::findOrFail($id);
            HelperFunctions::writeLogInfo($decs = "ApplicationController@statusChange Disable/Enable App : " . $application);

            if ($application) {
                if ($application->Status == 'L') {

                    $application->Status = 'D';
                } else {
                    $application->Status = 'L';
                }
                $application->update();
                HelperFunctions::writeLogInfo($decs = "ApplicationController@statusChange Successfully Changed App.");

                session()->flash('success', CommonStrings::$successUpdate);
                return redirect()->route('application.index');
            } else {
                return redirect()->route('application.index')->withErrors(['Login Error' => CommonStrings::$failureUpdate]);
            }
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);

        }
    }
}
