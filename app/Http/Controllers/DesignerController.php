<?php

/*Created By M Awais Ullah 25,Sep,2018*/

namespace App\Http\Controllers;

use App\Application;
use App\CommonUtils\CommonStrings;
use App\CommonUtils\HelperFunctions;
use App\CommonUtils\RestCalls;
use App\Designer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use phpseclib\Net\SFTP;

class DesignerController extends Controller
{
    public function index()
    {

        //currently showing all Ivr Apps later on need to show all to admin, and team ivr to particular team users
        $data = Application::all();
        HelperFunctions::writeLogInfo($desc = "DesignerController@index : Return Designer view with applications list : ".$data);
        return view('ivr.designer.index', compact('data'));
    }

    /*Below are ajax functions used to perform certain actions in IVR*/
    public function getIvrScriptAjax(Request $request)
    {
        $designer = new Designer();
        $data = $designer->getScript($request->appID);
        HelperFunctions::writeLogInfo($desc = "DesignerController@getIvrScriptAjax getIVR :".$request->appID." script :".$data);
        return $data;
    }

    public function setIvrScriptAjax(Request $request)
    {
        $data = $request->data;
        $data=json_decode($data);
        $userID=Session::get('UserID');
        $data->UserID=$userID;
        $designer=new Designer();
        HelperFunctions::writeLogInfo($desc = "DesignerController@setIvrScriptAjax userID ".$userID." script :".json_encode($data));
        $data=$designer->setScript($data);
        HelperFunctions::writeLogInfo($desc = "DesignerController@setIvrScriptAjax | Rest call Response  userId: [".$userID."] response [".json_encode($data)."]");
        return json_encode($data);
    }


    public function fileUpload()
    {
        $ftp_server = CommonStrings::$fileUploadServer;
        $ftp_username = "ivribex";
        $ftp_userpass = '$#$$NB7GEb*GIVR{)';
        $ftp_conn = ftp_connect($ftp_server) or die("Could not connect to $ftp_server");
        $login = ftp_login($ftp_conn, $ftp_username, $ftp_userpass);
        ftp_pasv($ftp_conn, true);
        $file = $_FILES["file_upload"]["tmp_name"];
        $fileAppID = $_POST['fileAppId'];
        $filePromptID = $_POST['filePromptId'];
        //  $date=strtotime("now");
        $filename = $fileAppID . "-" . $filePromptID . "-" . ".gsm";

        if (isset($_POST['language'])) {
            $language = $_POST['language'];
            if ($language != "English") {
                $filename = $language . "/" . $filename;
            }
        }

        // upload file
        if (ftp_put($ftp_conn, $filename, $_FILES["file_upload"]["tmp_name"], FTP_BINARY)) {
            $filename = $fileAppID . "-" . $filePromptID . "-";
            echo $filename;

        } else {
            echo "Error uploading file.";
        }
        // close connection
        ftp_close($ftp_conn);
    }

    public function extraFileUpload()
    {

        // $ftp_server = "10.80.15.70";
        $ftp_server = CommonStrings::$fileUploadServer;
        $ftp_username = "ivribex";
        $ftp_userpass = '$#$$NB7GEb*GIVR{)';
        $ftp_conn = ftp_connect($ftp_server) or die("Could not connect to $ftp_server");
        $login = ftp_login($ftp_conn, $ftp_username, $ftp_userpass);
        ftp_pasv($ftp_conn, true);
        $file = $_FILES["extraFile_upload"]["tmp_name"];
        $fileAppID = $_POST['extraFileAppId'];
        $filePromptID = $_POST['extraOwnerPromptId'];
        $extraPromptID = $_POST['extraFilePromptId'];
        //  $date=strtotime("now");
        $filename = $fileAppID . "-" . $filePromptID . "-" . $extraPromptID . ".gsm";

        if (isset($_POST['extraLanguage'])) {
            $language = $_POST['extraLanguage'];
            if ($language != "English") {
                $filename = $language . "/" . $filename;
            }
        }
        // upload file
        if (ftp_put($ftp_conn, $filename, $_FILES["extraFile_upload"]["tmp_name"], FTP_BINARY)) {
            $filename = $fileAppID . "-" . $filePromptID . "-" . $extraPromptID;
            echo $filename;
        } else {
            echo "Error uploading file.";
        }
        // close connection
        ftp_close($ftp_conn);
    }


    public function sftpConnect()
    {

        $connection = ssh2_connect('10.80.10.80');
        $serverUser = 'ivrsftp';
        $serverPassword = 'IVRsftp1';
        if (ssh2_auth_password($connection, $serverUser, $serverPassword)) {
            echo "connected\n";
            //  ssh2_scp_send($connection, "/path/to/local/".$file, "/path/to/remote/".$file);
            echo "done\n";
        } else {
            echo "connection failed\n";
        }

    }

    public function sftpConnect1()
    {

        $sftp = new SFTP('10.80.10.80');
        $sftp2 = new SFTP('10.90.10.82');

        if (!$sftp->login('ivrsftp', 'IVRsftp1')) {
            die('Cannot connect to server' . $sftp->errors);
        } else {
            $file = $_FILES["file_upload"]["tmp_name"];
            $fileAppID = $_POST['fileAppId'];
            $filePromptID = $_POST['filePromptId'];
            //  $date=strtotime("now");
            $filename = $fileAppID . "-" . $filePromptID . "-" . ".gsm";

            if (isset($_POST['language'])) {
                $language = $_POST['language'];
                if ($language != "English") {
                    $filename = $language . "/" . $filename;
                }
            }
            // upload file
            if ($sftp->put($filename, file_get_contents($_FILES["file_upload"]["tmp_name"]))) {
                $filename = $fileAppID . "-" . $filePromptID . "-";
                echo $filename;
            } else {
                echo "Error uploading file.";
            }
        }
        if (!$sftp2->login('ivrsftp', 'IVRsftp1')) {
            die('Cannot connect to server' . $sftp->errors);
        } else {
            $filename = $fileAppID . "-" . $filePromptID . "-" . ".gsm";
            if ($sftp2->put($filename, file_get_contents($_FILES["file_upload"]["tmp_name"]))) {
                $filename = $fileAppID . "-" . $filePromptID . "-";
                echo $filename;
            } else {
                echo "Error uploading file.";
            }
        }
    }
}
