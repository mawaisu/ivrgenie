<?php
/**
 * Created By Muhammad Awais Ullah
 */

namespace App\Http\Controllers;

use App\Application;
use App\CommonUtils\CommonStrings;
use App\CommonUtils\HelperFunctions;
use App\Http\Requests\TfnRequest;
use App\Team;
use App\Tfn;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TfnController extends Controller
{


    public function __construct()
    {
        $this->middleware('permission', ['only' => 'index']);
    }

    /**
     * Display a listing of the TFN.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $tfns = Tfn::all();
            $teams = Team::all();
            // HelperFunctions::writeLogInfo($desc = "TfnController@index! Returning View with Tfn  : ".($tfns)."and Teams : ".($teams));
            return view('ivr.tfn.index', compact('tfns', 'teams'));
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => ['Something went wrong, Please contact IVR-Team!']]);
        } catch (\Symfony\Component\Debug\Exception\FatalErrorException $exception) {
            report($exception);
            return back()->withErrors(['Error' => ['Something went wrong, Please contact IVR-Team!']]);
        }
    }

    /**
     * GetApplications List of team .
     * @param teamId
     * @return  Ajax applications response json encoded
     */
    public function getTeamApplicationsAjax(Request $request)
    {

        $team = Team::find($request->customerID);
        $applications = $team->applications;

        return json_encode($applications);

    }

    public function checkTfnAjax(Request $request)
    {


        if ($request->tfnRecId == null || $request->tfnRecId == "") {
            $tfn = Tfn::where('TFN', $request->tfnNumber)->get()->first();
        } else {
            $tfn = Tfn::where('TFN', $request->tfnNumber)->where('ID', $request->tfnRecId)->get()->first();
        }

        if ($tfn) {
            return response()->json(false);
        } else {
            return response()->json(true);
        }

    }

    /**
     * checkDnisAjax Check if the DNIS already exist .
     * @param dnisID,dnisRecId
     * @return  true/false if dnis found or not
     */
    public function checkDnisAjax(Request $request)
    {


        if ($request->dnisRecId == null || $request->dnisRecId == "") {
            $tfn = Tfn::where('DNIS', $request->dnisNumber)->get()->first();
        } else {
            $tfn = Tfn::where('DNIS', $request->dnisNumber)->where('ID', $request->dnisRecId)->get()->first();
        }

        if ($tfn) {
            return response()->json(false);
        } else {
            return response()->json(true);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TfnRequest $request)
    {
        try {
            $request->validated();
            HelperFunctions::writeLogInfo($desc = " TfnController@store ");
            if ($request->tfnRecordID == null or $request->tfnRecordID == '') {
                $this->validate($request, ['dnis' => 'required|max:12']);

                $tfn = Tfn::create(['AppID' => $request->customerAppList,
                    'DNIS' => $request->dnis,
                    'TFN' => $request->dnistfn,
                    'Status' => $request->tfnstatus,
                    'Type' => $request->tfntype,
                    'Remarks' => $request->tfnremarks,
                    'CustomerID' => '1',
                    'TeamID' => $request->customerList
                ]);
                HelperFunctions::writeLogInfo($desc = " TfnController@store  Create:New Record : " . $tfn);
                if ($tfn) {
                    session()->flash('success', CommonStrings::$successSave);
                } else {
                    session()->flash('error', CommonStrings::$failureSave);
                }

            } else {

                HelperFunctions::writeLogInfo($desc = " TfnController@store");
                $tfn = Tfn::find($request->tfnRecordID)->update(['AppID' => $request->customerAppList,
                    'TFN' => $request->dnistfn,
                    'Status' => $request->tfnstatus,
                    'Type' => $request->tfntype,
                    'Remarks' => $request->tfnremarks,
                    'CustomerID' => '1',
                    'TeamID' => $request->customerList
                ]);
                HelperFunctions::writeLogInfo($desc = " TfnController@store  Updated: Record : " . $tfn);
                if ($tfn) {
                    session()->flash('success', CommonStrings::$successUpdate);
                } else {
                    session()->flash('error', CommonStrings::$failureUpdate);
                }
            }
            return redirect()->route('tfn.index');
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    public function destroy($id)
    {
        try {
            HelperFunctions::writeLogInfo($desc = " TfnController@destroy  Delete Record TFN ID  :" . $id);
            $tfn = Tfn::findOrFail($id);
            if ($tfn) {

                HelperFunctions::writeLogInfo($desc = " TfnController@destroy  Delete Record TFN ID  :" . $tfn);
                $tfn->delete();
                session()->flash('success', CommonStrings::$successDelete);
            } else {
                session()->flash('error', CommonStrings::$failureDelete);
            }
            return redirect()->route('tfn.index');
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }


    public function statusChange($id)
    {
        try {
            $tfn = Tfn::findOrFail($id);
            HelperFunctions::writeLogInfo($desc = " TfnController@statusChange  Change Status of TFN  :" . $tfn);
            if ($tfn) {
                if ($tfn->Status == 'L') {

                    $tfn->Status = 'D';
                } else {
                    $tfn->Status = 'L';
                }

                $tfn->update();
                HelperFunctions::writeLogInfo($desc = " TfnController@statusChange Good-bye after updating status.");
                session()->flash('success', CommonStrings::$successUpdate);

                return redirect()->route('tfn.index');


            } else {
                return redirect()->route('tfn.index')->withErrors(['Error' => CommonStrings::$failureUpdate]);
            }
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    public function getTfnAjax(Request $request)
    {

        $tfn = Tfn::find($request->id);

        $apps = Application::where('TeamID', $tfn->TeamID)->get();

        //return $tfn->application;
        return json_encode([$tfn, $apps]);
    }


}
