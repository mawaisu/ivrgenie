<?php

namespace App\Http\Controllers;

use App\Application;
use App\CommonUtils\CommonStrings;
use App\CommonUtils\HelperFunctions;
use App\FavoriteApp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FavoriteAppController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

            $favoriteApps = FavoriteApp::all()->where('UserID', Session::get('UserID'));
            //$favoriteApps = FavoriteApp::where('UserID',Session::get('UserID'))->with('user')->with('application')->get();

            $applications = Application::all();

            HelperFunctions::writeLogInfo($desc = "FavoriteAppController@index Returning View with favorite app list : " . $favoriteApps);

            return view('ivr.favorite_app.index', compact('favoriteApps', 'applications'));

        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, ['AppID' => 'required|integer']);

            $userId = Session::get('UserID');

            HelperFunctions::writeLogInfo($desc = "FavoriteAppController@store search favorite_apps with user id [ : " . $userId." ] and app id [".$request->AppID."].");

            $favoriteApp = FavoriteApp::where('UserID', $userId)->where('AppID', $request->AppID)->first();
            if (count($favoriteApp) > 0) {

                HelperFunctions::writeLogInfo($desc = "FavoriteAppController@store search result app with user id and app id found in fav list.Return back with error.");

                return back()->withErrors(['Error' => [CommonStrings::$errorFavApp]]);
            } else {

                $favoriteApp = FavoriteApp::create(['AppID' => $request->AppID, 'UserID' => $userId]);

                HelperFunctions::writeLogInfo($desc = "FavoriteAppController@store create  new favorite app [ ".$favoriteApp."].");

                if ($favoriteApp) {
                    session()->flash('success', CommonStrings::$successSave);
                    return redirect()->route('favorite.app.index');
                } else {
                    return back()->withErrors(['Error' => [CommonStrings::$failureSave]]);
                }

            }
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $favoriteApp = FavoriteApp::findOrFail($id);
            HelperFunctions::writeLogInfo($desc = "FavoriteAppController@destroy Remove user favoriteApp from list : " . $favoriteApp);

            if ($favoriteApp){
                $favoriteApp->delete();
                session()->flash('success', CommonStrings::$successDelete);
                return redirect()->route('favorite.app.index');
            }
            else{
                return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
            }
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }
}
