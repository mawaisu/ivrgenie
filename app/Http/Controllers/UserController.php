<?php

namespace App\Http\Controllers;

use App\CommonUtils\CommonStrings;
use App\CommonUtils\HelperFunctions;
use App\Company;
use App\Http\Requests\UserRequest;
use App\Role;
use App\Team;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission', ['only' => 'index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $users = User::all();
            $companies = Company::all();
            $roles = Role::all();

            HelperFunctions::writeLogInfo("UserController@index returning view with UserList : [" . $users . "]");

            return view('ivr.users.index', compact('users', 'companies', 'roles'));
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Errors' => CommonStrings::$commonError]);
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request)
    {
        try {
            $request->validated();
            HelperFunctions::writeLogInfo($desc = " UserController@store ");
            if ($request->userID == null or $request->userID == '') {
                $this->validate($request, ['username' => 'required|unique:user']);

                $user = User::create([
                    'username' => $request->username,
                    'firstname' => $request->firstName,
                    'lastname' => $request->lastName,
                    'role_id' => $request->role,
                    'CompanyID' => $request->company,
                    'TeamID' => $request->teams,
                    'password' => 1,
                    'auth_key' => 1
                ]);
                HelperFunctions::writeLogInfo($desc = " UserController@store  Create:New Record : " . $user);
                if ($user) {
                    session()->flash('success', CommonStrings::$successSave);
                } else {
                    return redirect()->route('user.index')->withErrors(['Error' => CommonStrings::$failureSave]);
                }

            } else {
                HelperFunctions::writeLogInfo($desc = " UserController@store");

                $user = User::where('username', $request->username)->where('id', $request->userID)->first();
                if ($user) {
                    $user->update([
                        'firstname' => $request->firstName,
                        'lastname' => $request->lastName,
                        'role_id' => $request->role,
                        'CompanyID' => $request->company,
                        'TeamID' => $request->teams,
                    ]);

                    session()->flash('error', CommonStrings::$successUpdate);
                    return redirect()->route('user.index');
                } else {
                    return redirect()->route('user.index')->withErrors(['Error' => CommonStrings::$failureUpdate]);
                }
            }
            return redirect()->route('user.index');
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }

    }

    public function getCompanyTeams(Request $request)
    {
        try {
            $id = $request->id;
            if ($id == null) {

                $data = Team::all();
                HelperFunctions::writeLogInfo("UserController@getCompanyTeams teamsList [" . $data . ']');
            } else {

                $data = Team::all()->where('Company_ID', $id);
                HelperFunctions::writeLogInfo("UserController@getCompanyTeams  Company ID : [" . $id . '] teamsList [' . $data . ']');
            }
            return json_encode($data);
        }catch (\Exception $exception){
            report($exception);
        }
    }

    public function getUserAjax(Request $request)
    {

        try{
        $data = User::findOrFail($request->id);
        HelperFunctions::writeLogInfo("UserController@getUserAjax  User ID : [" . $request->id . '] User : [' . $data . ']');
        return json_encode($data);
        }catch (\Exception $exception){
            report($exception);
        }

    }

    public function checkDuplicateAjax(Request $request)
    {
        try{
        $data = User::where('username', $request->name)->first();
        if ($data) {
            return json_encode(true);
        } else {
            return json_encode(false);
        }
        }catch (\Exception $exception){
            report($exception);
        }
    }

    public function destroy($id)
    {

    }
}
