<?php
/*Created By M Awais Ullah 25,Sep,2018*/

namespace App\Http\Controllers;

use App\Application;
use App\AppReport;
use App\CommonUtils\CommonStrings;
use App\CommonUtils\HelperFunctions;
use Illuminate\Http\Request;

class AppReportController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission', ['only' => 'index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            HelperFunctions::writeLogInfo($desc = "AppReportController@index! Returning report index page");
            $reports = AppReport::all();
            $apps = Application::all();
            return view('ivr.report.index', compact('reports', 'apps'));
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    public function store(Request $request)
    {
        try {
            HelperFunctions::writeLogInfo($desc = "AppReportController@store ! validate request");
            //custom message
            $customMessages = [
                'appID.required' => 'Application name is required.',
                'rptName.required' => 'Report Name is required.',
                'rptName.max' => 'Report Name cannot be greater than 75 Characters.',
                'rptSubject.required' => 'Report Subject is required.',
                'rptHour.required' => 'Hour field is required.',
                'rptFrq.required' => 'Report Frequency is required.',
                'rptStatus.required' => 'Report Status is required.',
            ];

            $this->validate($request, [
                'appID' => 'required',
                'rptName' => 'required|max:75',
                'rptSubject' => 'required|max:250',
                'rptHour' => 'required',
                'rptFrq' => 'required|in:1,7,6,5,9',
                'rptStatus' => 'required|in:D,L',
            ], $customMessages);
            HelperFunctions::writeLogInfo($desc = "AppReportController@store ! validation complete");

            if ($request["rptID"] != '0') {
                HelperFunctions::writeLogInfo($desc = "AppReportController@store ! Update request :Report Id:" . $request->rptID);

                $appReport = AppReport::find($request['rptID']);
                HelperFunctions::writeLogInfo($desc = "AppReportController@store ! Update request :Report:" . $appReport);
                if (isset($request["rptDay"])) {
                    $rptDay = $request["rptDay"];
                } else {
                    $rptDay = "0";
                }
                $appReport->update(['AppID' => $request['appID'], 'RptName' => $request['rptName'],
                    'RptSubject' => $request['rptSubject'], 'FromDateIndicator' => '1', 'RptHH' => $request['rptHour'],
                    'ReportEntryIndicator' => $request['rptFrq'], 'RptEntryDayIndicator' => $rptDay, 'Status' => $request['rptStatus']]);

                HelperFunctions::writeLogInfo($desc = "AppReportController@store ! Updated request :Report Id:" . $request->rptID);
                session()->flash('success', CommonStrings::$successUpdate);
            } else {
                HelperFunctions::writeLogInfo($desc = "AppReportController@store ! Create request");

                if (isset($request["rptDay"])) {
                    $rptDay = $request["rptDay"];
                } else {
                    $rptDay = "0";
                }
                $appReport = AppReport::create(['AppID' => $request['appID'], 'RptName' => $request['rptName'],
                    'RptSubject' => $request['rptSubject'], 'FromDateIndicator' => '1', 'RptHH' => $request['rptHour'],
                    'ReportEntryIndicator' => $request['rptFrq'], 'RptEntryDayIndicator' => $rptDay, 'Status' => $request['rptStatus']]);

                HelperFunctions::writeLogInfo($desc = "AppReportController@store ! Create request :Report Created successfully" . $appReport);
                session()->flash('success', CommonStrings::$successSave);
            }
            HelperFunctions::writeLogInfo($desc = "AppReportController@store ! Bye redirecting back to index page");
            return redirect()->back();
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    public function destroy($id)
    {
        try {
            HelperFunctions::writeLogInfo($desc = "AppReportController@destroy ! Hi i m in with report id :" . $id);
            $appReport = AppReport::find($id);

            if ($appReport) {
                HelperFunctions::writeLogInfo($desc = "AppReportController@destroy !Finding Report against Id :" . $appReport);
                $appReport->delete();
                HelperFunctions::writeLogInfo($desc = "AppReportController@destroy !Deleted Report Id :" . $id);
                session()->flash('success', CommonStrings::$successDelete);
            } else {
                HelperFunctions::writeLogInfo($desc = "AppReportController@destroy ! Failure record not deleted.");
                session()->flash('success', CommonStrings::$failureDelete);
            }
            HelperFunctions::writeLogInfo($desc = "AppReportController@destroy redirecting back to Report Index page");
            return redirect()->route('report.index');
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }
}
