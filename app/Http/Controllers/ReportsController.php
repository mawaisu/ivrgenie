<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $json = '{
	"Status": "100",
	"Description": "Success :: All went good",
	"Headings": [
	{
		"ANI": "ANI"
	},
	{
		"DNIS": "DNIS"
	},
	{
		"Call Date Time": "Call Date Time"
	},
	{
		"Type": "Type"
	},
	{
		"Detail": "Detail"
	},
	{
		"Emp Name": "Emp Name"
	},
	{
		"Employee ID": "Employee ID"
	},
	{
		"Emp Email ": "Emp Email"
	},
	{
		"Sup Name": "Sup Name"
	},
	{
		"Sup Email": "Sup Email"
	},
	{
		"Site": "Site"
	},
	{
		"Campaign": "Campaign"
	}
	
	],
	"Data": [{
		"ANI": "8766331688",
		"DNIS": "7/29/2018 6:34:28 AM",
		"Call Date Time": "Entire Day",
		"Type": "Entire Day",
		"Detail": "Entire Day",
		"Emp Name": "Chenelle Carter",
		"Employee ID": "157670",
		"Emp Email": "chenelle.carter@ibexglobal.local",
		"Sup Name": "Richard Clarke",
		"Sup Email": "rclarke@ibexglobal.local",
		"Site": "Waterfront",
		"Campaign": "Amazon Customer Services"
	},
	{
		"ANI": "8766331688",
		"DNIS": "7/29/2018 6:34:28 AM",
		"Call Date Time": "Entire Day",
		"Type": "Entire Day",
		"Detail": "Entire Day",
		"Emp Name": "Chenelle Carter",
		"Employee ID": "157670",
		"Emp Email": "chenelle.carter@ibexglobal.local",
		"Sup Name": "Richard Clarke",
		"Sup Email": "rclarke@ibexglobal.local",
		"Site": "Waterfront",
		"Campaign": "Amazon Customer Services"
	},
	{
		"ANI": "1766331688",
		"DNIS": "7/29/2018 6:34:28 AM",
		"Call Date Time": "Entire Day",
		"Type": "Entire Day",
		"Detail": "Entire Day",
		"Emp Name": "Chenelle Carter",
		"Employee ID": "157670",
		"Emp Email": "chenelle.carter@ibexglobal.local",
		"Sup Name": "Richard Clarke",
		"Sup Email": "rclarke@ibexglobal.local",
		"Site": "Waterfront",
		"Campaign": "Amazon Customer Services"
	},
	{
		"ANI": "8766331689",
		"DNIS": "7/29/2018 6:34:28 AM",
		"Call Date Time": "Entire Day",
		"Type": "Entire Day",
		"Detail": "Entire Day",
		"Emp Name": "Chenelle Carter",
		"Employee ID": "157670",
		"Emp Email": "chenelle.carter@ibexglobal.local",
		"Sup Name": "Richard Clarke",
		"Sup Email": "rclarke@ibexglobal.local",
		"Site": "Waterfront",
		"Campaign": "Amazon Customer Services"
	}
	]
}';

        $decode = json_decode($json, true);

        $headings = array();
        $tableData = array();


        foreach ($decode["Headings"] as $value) {
            foreach ($value as $key) {
                array_push($headings, $key);

            }
        }
        foreach ($decode["Data"] as $value) {

                array_push($tableData, $value);

        }

        $headCount = count($headings);

        $rowCount = count($tableData);

        $rows = $rowCount/$headCount;


        return view('ivr.reports-ivr.index',compact('headings','tableData','rows'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
