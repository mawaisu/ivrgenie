<?php

namespace App\Http\Controllers;

use App\CommonUtils\CommonStrings;
use App\CommonUtils\HelperFunctions;
use App\CommonUtils\RestCalls;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Exception;

class AuthController extends Controller
{
    public function __construct()
    {
        //apply middleware
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        // return login view

        HelperFunctions::writeLogInfo($desc = "Hi Awais! Returning Login page to user");
        return view('ivr.auth.login');
    }

    public function login(Request $request)
    {
        // Validate the form data
        $this->validate($request, [
            'id' => 'required',
            'pass' => 'required'
        ]);

        try {
            //find user from db
            $user = User::where('username', $request->id)->firstOrFail();
            HelperFunctions::writeLogInfo($desc = "Hi Awais!Searching User in Db");
            if (!empty($user)) {
                $host = CommonStrings::$ldapCorp;
                //  Auth::login($user);
                $userName = 'corp' . "\\" . $user->username;
                try {
                    $ldap = ldap_connect($host);
                    $bind = @ldap_bind($ldap, $userName, $request['pass']);
                    HelperFunctions::writeLogInfo($desc = "Hi! Searching User :" . $user . "password : [" . $request['pass'] . "]in LDAP service");
                    if ($bind === true) {
                        //service call for getting permissions for given role
                        $restResult = RestCalls::getRolePermissions($user->role_id);

                        //setting permission in session
                        $this->setPermissionRoleSession($restResult);

                        //setting user in Auth

                        Auth::login($user);
                        HelperFunctions::writeLogInfo($desc = "User{" . Auth::user() . " logged in successfully.");
                        //routing to dashboard
                        Session::put('UserID', $user->id);
                        Session::put('TeamID', $user->TeamID);
                        Session::put('CompanyID', $user->CompanyID);

                        session()->flash('success', 'User logged in successfully.');
                        return redirect()->intended(route('dashboard'));

                    } else {

                        return back()->withErrors(['Login Error' => [CommonStrings::$credentialError]]);
                    }
                } catch (Exception $e) {
                    report($e);
                    return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
                }
            } else {
                return back()->withErrors(['Login Error' => [CommonStrings::$loginError]]);
            }
        } catch (Exception $e) {
            // catch code
            report($e);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    public function logout()
    {
        HelperFunctions::writeLogInfo($desc = "Logout User:" . Auth::user());
        try {
            Auth::guard()->logout();
            Session::invalidate();
            session()->flash('success', 'User logged out successfully.');
            return redirect()->route('auth.login');
        }catch (Exception $e){
            report($e);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    public function setPermissionRoleSession($array)
    {

        $restResult = json_decode($array, true);
        HelperFunctions::writeLogInfo($desc = "Json Parse: Permissions :" . print_r($restResult));
        $permissions = array();
        $permissionsRoute = array();

        foreach ($restResult['selected'] as $selected) {
            array_push($permissions, $selected['label']);
            array_push($permissionsRoute, $selected['route']);
        }
        Session::put('permissionLabel', $permissions);
        Session::put('permissionRoute', $permissionsRoute);
        HelperFunctions::writeLogInfo($desc = "Put Session Permissions." . print_r($permissions, true) . "Put Session Permissions." . print_r($permissionsRoute, true));
        return;
    }

}
