<?php
/*Created By M Awais Ullah 25,Sep,2018*/

namespace App\Http\Controllers;

use App\Application;
use App\AppReport;
use App\CommonUtils\CommonStrings;
use App\CommonUtils\HelperFunctions;
use App\ReportDistro;
use Illuminate\Http\Request;

class ReportDistroController extends Controller
{


    public function __construct()
    {
        $this->middleware('permission', ['only' => 'index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $apps = Application::all();
            HelperFunctions::writeLogInfo($desc = "ReportDistroController@index! Returning View with Application List : " . ($apps) . ".");
            return view('ivr.report_distro.index', compact('apps'));
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    public function getReportListAjax(Request $request)
    {
        try {
            if($request->appid !='' or $request->appid !=null){
            HelperFunctions::writeLogInfo($desc = "ReportDistroController@getReportListAjax! I m in");
            $app = Application::findOrFail($request->appid);
            $reports = $app->appReports;
            HelperFunctions::writeLogInfo($desc = "ReportDistroController@getReportListAjax! Returns list of reports against appID : " . $reports);
            return json_encode($reports);
            }
            else{
                HelperFunctions::writeLogInfo($desc = "ReportDistroController@getReportListAjax! Returns empty params. ");
                return json_encode(array());
            }
        } catch (\Exception $exception) {
            report($exception);
            return json_encode(array());
        }
    }

    public function getEmailListAjax(Request $request)
    {
        try {
            HelperFunctions::writeLogInfo($desc = "ReportDistroController@getEmailListAjax! I m here.");
            $email = ReportDistro::where('RptID', $request->reportid)->first();
            if (count($email) > 0) {
                HelperFunctions::writeLogInfo($desc = "ReportDistroController@getEmailListAjax! Return Emails List :" . $email);
                return json_encode($email);
            } else {
                HelperFunctions::writeLogInfo($desc = "ReportDistroController@getEmailListAjax! No Email List :");
                return json_encode('{"status":0}');
            }
        } catch (\Exception $exception) {
            report($exception);
        }
    }


    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'reportName' => 'required',
                'emailList' => 'required'
            ]);
            HelperFunctions::writeLogInfo($desc = "ReportDistroController@store ! save Email list of Report Id : " . $request->reportName);
            $reportDistro = ReportDistro::where('RptID', $request->reportName)->first();

            if (count($reportDistro) > 0) {
                $reportDistro->Email = $request->emailList;

                $reportDistro->save();
                HelperFunctions::writeLogInfo($desc = "ReportDistroController@store ! saved Email list:" . $request->emailList . "of Report Id : " . $request->reportName);
            } else {
                $reportDistro = ReportDistro::create(['RptID' => $request->reportName, 'Email' => $request->emailList, 'Status' => 'L']);
                HelperFunctions::writeLogInfo($desc = "ReportDistroController@store ! saved Email list:" . $request->emailList . "of Report Id : " . $request->reportName);

            }
            session()->flash('success', 'Report Distro Saved Successfully.');
            return redirect()->intended(route('report.distro.index'));
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }
}
