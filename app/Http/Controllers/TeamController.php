<?php
/*Created By M Awais Ullah 25,Sep,2018*/

namespace App\Http\Controllers;

use App\CommonUtils\CommonStrings;
use App\CommonUtils\HelperFunctions;
use App\Http\Requests\TeamRequest;
use App\Team;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $teams = Team::all();
            HelperFunctions::writeLogInfo($dec = "TeamController@index return view with teams list : [" . $teams . "]");
            return view('ivr.team.index', compact('teams'));
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    public function store(TeamRequest $request)
    {
        try {
            $request->validated();
            if ($request['teamID'] == "" && $request['teamID'] == null) {
                $this->validate($request, ['Name' => 'unique:teams']);
                $team = Team::create(['Name' => $request['Name'], 'Description' => $request['teamDesc'], 'Company_ID' => $request['teamCompany']]);
                HelperFunctions::writeLogInfo($dec = "TeamController@store save team : [" . $team . "]");
                if ($team) {
                    session()->flash('success', CommonStrings::$successSave);
                    return redirect()->route('team.index');
                } else {
                    HelperFunctions::writeLogInfo($dec = "TeamController@store unable to save team ", 'error');
                    return redirect()->route('team.index')->withErrors(['Error' => CommonStrings::$failureSave]);
                }

            } else {

                $team = Team::find($request['teamID']);
                HelperFunctions::writeLogInfo($dec = "TeamController@store find team with id :" . $request['teamID'] . " => [" . $team . "]");
                if ($team) {
                    $team->update(['Name' => $request['Name'], 'Description' => $request['teamDesc'], 'Company_ID' => $request['teamCompany']]);
                    HelperFunctions::writeLogInfo($dec = "CompanyController@store updated team:" . $request['companyID'] . " => [" . $team . "]");

                    session()->flash('success', CommonStrings::$successUpdate);
                    return redirect()->route('team.index');
                } else {
                    HelperFunctions::writeLogInfo($dec = "CompanyController@store unable to update team id [" . $request['teamID'] . "]", 'error');

                    return redirect()->route('team.index')->withErrors(['Error' => CommonStrings::$failureUpdate]);
                }
            }
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    public function destroy($id)
    {
        try {
            $team = Team::findOrFail($id);
            HelperFunctions::writeLogInfo($desc = "TeamController@destroy Delete Team : " . $team);
            if ($team) {
                $team->delete();
                session()->flash('success', CommonStrings::$successDelete);
                return redirect()->route('team.index');
            } else {
                return redirect()->route('team.index')->withErrors(['Error' => CommonStrings::$failureDelete]);
            }
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    public function getTeamAjax(Request $request)
    {

        $data = Team::find($request->id);
        HelperFunctions::writeLogInfo("TeamController@getTeamAjax  find team id : [" . $request->id . "]");
        return json_encode($data);
    }
}
