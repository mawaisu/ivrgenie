<?php
/**
 * Created By M Awais Ullah 25,Sep,2018
 */

namespace App\Http\Controllers;

use App\CommonUtils\CommonStrings;
use App\CommonUtils\HelperFunctions;
use App\CommonUtils\RestCalls;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * @return Error Page View
     */
    public function errorPage()
    {
        return view('ivr.layouts.error.index');
    }

    /**
     * @param passes user id to RestService to get Dashboard Data
     * @return Dashboard view with RestService response
     */
    public function dashboard()
    {
        try {
            HelperFunctions::writeLogInfo($desc = "HomeController@dashboard calling rest service to get Dashboard Data");

        ($response =  RestCalls::getDashboardData('aaaaaaaaaaaaaaa'.Session::get('UserID')));

            return view('ivr.index',compact('response'));

        } catch (\Exception $exception) {
            report($exception);
        }
    }

    /**
     * @return 404ErrorPage
     */
    public function page404()
    {
        return view('errors.404');
    }
}
