<?php

namespace App\Http\Controllers;

use App\CommonUtils\CommonStrings;
use App\CommonUtils\HelperFunctions;
use App\Company;
use App\Http\Requests\CompanyRequest;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $companies = Company::all();
            HelperFunctions::writeLogInfo($dec = "CompanyController@index return view with companies list : [" . $companies . "]");
            return view('ivr.company.index', compact('companies'));
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    public function store(CompanyRequest $request)
    {
        try {
            if ($request['companyID'] == "" && $request['companyID'] == null) {

                $company = Company::create(['Name' => $request['companyName'], 'Description' => $request['companyDesc']]);
                HelperFunctions::writeLogInfo($dec = "CompanyController@store save company : [" . $company . "]");
                if ($company) {
                    session()->flash('success', CommonStrings::$successSave);
                    return redirect()->route('company.index');
                } else {
                    HelperFunctions::writeLogInfo($dec = "CompanyController@store unable to save company ", 'error');
                    return redirect()->route('company.index')->withErrors(['Error' => CommonStrings::$failureSave]);
                }

            } else {

                $company = Company::findOrFail($request['companyID']);
                HelperFunctions::writeLogInfo($dec = "CompanyController@store find company with id :" . $request['companyID'] . " => [" . $company . "]");
                if ($company) {
                    $company->update(['Name' => $request['companyName'], 'Description' => $request['companyDesc']]);
                    HelperFunctions::writeLogInfo($dec = "CompanyController@store updated company:" . $request['companyID'] . " => [" . $company . "]");

                    session()->flash('success', CommonStrings::$successUpdate);
                    return redirect()->route('company.index');
                } else {
                    HelperFunctions::writeLogInfo($dec = "CompanyController@store unable to update company id [" . $request['companyID'] . "]", 'error');

                    return redirect()->route('company.index')->withErrors(['Error' => CommonStrings::$failureUpdate]);
                }
            }
        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }


    public function destroy($id)
    {
        try {
            $company = Company::findOrFail($id);
            HelperFunctions::writeLogInfo($desc = "CompanyController@destroy Delete Company : " . $company);
            if ($company) {
                $company->delete();
                session()->flash('success', CommonStrings::$successDelete);
                return redirect()->route('company.index');
            } else {
                return redirect()->route('company.index')->withErrors(['Error' => CommonStrings::$failureDelete]);
            }

        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }

    public function getCompanyAjax(Request $request)
    {
        try {
            $data = Company::findOrFail($request->id);
            HelperFunctions::writeLogInfo($dec = "CompanyController@getCompanyAjax Company : [" . $data . "]");
            return json_encode($data);
        } catch (\Exception $exception) {
            report($exception);
        }
    }

    public function checkDuplicateAjax(Request $request)
    {
        try {
            $data = Company::where('Name', $request->name)->first();
            if ($data) {
                return json_encode($data = array("result" => 1));
            } else {
                return json_encode($data = array("result" => 0));
            }
        } catch (\Exception $exception) {
            report($exception);
        }
    }
}
