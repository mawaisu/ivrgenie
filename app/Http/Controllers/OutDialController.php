<?php

namespace App\Http\Controllers;

use App\CommonUtils\CommonStrings;
use App\CommonUtils\HelperFunctions;
use App\CommonUtils\RestCalls;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\DocBlock;

class OutDialController extends Controller
{
    public function getCwoDial()
    {
        HelperFunctions::writeLogInfo("OutDialController@getCwoDial : Return CWO-Out-dial page.");
        return view('ivr.outdial.index');
    }

    public function postCwoCall(Request $request)
    {
        try {
            HelperFunctions::writeLogInfo("OutDialController@postCwoCall : Post Call CWO-Out-dial.");
            $this->validate($request, [
                'name' => 'required',
                'number' => 'required|integer',
                'param1' => 'required',
                'param2' => 'required',
                'serverIp' => 'required|ip',
                'extension' => 'required|integer',
            ]);

            $data['name'] = $request->name;
            $data['number'] = $request->number;
            $data['policyNumber'] = $request->param1;
            $data['policyGroup'] = $request->param2;
            $data['serverIp'] = $request->serverIp;
            $data['extension'] = $request->extension;
            HelperFunctions::writeLogInfo("OutDialController@postCwoCall : Data [ " . print_r($data, true) . " ]");
            if ($data['number'][0] == 1) {
                return back()->withErrors(['Error' => CommonStrings::$outDialNumberError]);
            } elseif ($data['number'][0] == 8) {
                return back()->withErrors(['Error' => CommonStrings::$outDialNumberError]);
            } elseif ($data['number'][0] == 9) {
                return back()->withErrors(['Error' => CommonStrings::$outDialNumberError]);
            } elseif ($data['number'][0] == '+') {
                return back()->withErrors(['Error' => CommonStrings::$outDialNumberError]);
            }
            HelperFunctions::writeLogInfo("OutDialController@postCwoCall : Calling Rest Service with data [ " . print_r($data, true) . " ]");
            $output = RestCalls::cwoOutDial($data);

            if ($output == 100) {
                session()->flash('success', CommonStrings::$outDialSuccess);
                return redirect()->back();
            } elseif ($output == 400) {
                return back()->withErrors(['Error' => CommonStrings::$outDialFailure]);
            } else {
                return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
            }

        } catch (\Exception $exception) {
            report($exception);
            return back()->withErrors(['Error' => [CommonStrings::$commonError]]);
        }
    }
}
