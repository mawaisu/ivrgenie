<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tfn extends Model
{

    protected $table = 'appdnis';

    public  $timestamps = false;

    protected $fillable = [
        'ID','AppID','DNIS','Status','TFN','Remarks','Type','CompanyID','UserID','TeamID'
    ];

    protected $primaryKey = 'ID';

    public function application(){

        return $this->belongsTo('App\Application','AppID','AppID')->withDefault(['AppName'=>'Unknown App']);
    }

    public function team(){

        return $this->belongsTo('App\Team','TeamID','Team_ID')->withDefault(['Name'=>'Unknown Team']);
    }
}
