<?php
/**
 * Created by PhpStorm.
 * User: mawaisu
 * Date: 9/25/2018
 * Time: 4:22 PM
 */

namespace App;


use App\CommonUtils\RestCalls;

class Designer
{

    private $json;

    //
    public function getScript($appID) {

        $_json = $this->json;
        $params['IVR'] = $appID;
        $_json = RestCalls::call("GetIVRScript", $params);

       // dd($_json);

        $data = array("Status" => $_json["Status"],
            "IVR" => $_json["IVR"],
            "Name" => $_json["Name"]
        );
        $scriptData = array();
        $existing = array();
        $max = 0;
        $nodes=array();
        $childNodes=array();
        $existing=array();

        foreach ($_json['Script'] as $script)
        {  // outer loop

            if ($max < $script["PromptID"])
            {
                $max = $script["PromptID"];
            }
            if (in_array($script["PromptID"], $existing) == false)
            {
                $web=( isset($script["web"]) && $script["web"]!=null ?$script["web"] :[]);
                $node= array
                (
                    "id" => $script["PromptID"],
                    "parent" => "#",
                    "text" => $script["PromptID"] . ":" . $script["PromptText"],
                    "data" => array(
                        "Type" => strtolower($script["Type"]),
                        "PromptID" => $script["PromptID"],
                        "PromptText" => $script["PromptText"],
                        "ANSID" => $script["ANSID"],
                        "AnsTag" => $script["AnsTag"],
                        "PromptFileName" => $script["PromptFileName"],
                        "flow" =>null,
                        "web"=>$web,
                        "extra" => $script["extra"]
                    ),
                    "type" => strtolower($script["Type"])
                );
                $counter=0;
                if(is_array($script['flow']))
                {
                    foreach ($script['flow'] as $key => $item)
                    {
                        $nextNode =$this->lookupData($_json['Script'], $item['NextANSID']);

                        if ($nextNode != NULL || strtolower($script["Type"]) == "language")
                        {
                            $script['flow'][$key]['NextType'] = strtolower($nextNode['Type']);
                        }
                        else
                        {
                            $script['flow'][$key]['NextType'] = "hangup";

                            $script['flow'][$key]['NextANSID'] = "0";
                        }

                        $node['data']['flow'][]=$script['flow'][$key];

                        if (strtolower($script["Type"]) == "menu" || strtolower($script["Type"]) == "confirmation" || strtolower($script["Type"]) == "language")
                        {
                            if($script['flow'][$key]['Value']==-1)
                            {
                                continue;
                            }
                            $childNode = array("id" => $script["PromptID"].$counter,
                                "parent" => $script["PromptID"],
                                "text" =>  $script['flow'][$key]['Value'] . ": Keypress",
                                "data" => array(
                                    "Type" => "keypress",
                                    "flow" => array($script['flow'][$key])
                                ),
                                "type" => "keypress"
                            );
                            array_push($nodes,$childNode);
                            $counter++;
                        }

                    }
                }
                array_push($nodes,$node);

                array_push($existing, $script["PromptID"]);

            }
        }

        $tfns=Tfn::select('tfn')->where('AppID',$appID)->get();

        $data = array("Status" => $_json["Status"],
            "IVR" => $_json["IVR"],
            "Name" => $_json["Name"],
            "Start" => $_json["Start"],
            "Max" => $max,
            "Tfns"=>$tfns,
            "Script" => $nodes
        );

        return json_encode($data);
    }

    public function lookupData($data, $index) {

        foreach ($data as $row) {
            if ($row["PromptID"] == $index) {
                return $row;
            }
        }
        return null;
    }

    public function setScript($data) {
        $data = RestCalls::call("SaveIVRScript", $data);
        return $data;
    }
}