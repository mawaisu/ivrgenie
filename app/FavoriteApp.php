<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavoriteApp extends Model
{
    protected $table ='favorite_apps';

    protected $fillable = ['UserID','AppID'];

    protected $primaryKey = 'id';

    public $timestamps = false;

    public function application(){

        return $this->belongsTo('App\Application','AppID','AppID')->withDefault(['AppName'=>'Unknown App']);
    }

    public function user(){

        return $this->belongsTo('App\User','UserID','id')->withDefault(['username'=>'Unknown User']);;
    }
}
