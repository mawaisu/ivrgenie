<?php
/**
 * Created by PhpStorm.
 * User: mawaisu
 * Date: 8/30/2018
 * Time: 2:39 PM
 */

namespace App\CommonUtils;

class CommonStrings
{
    public static $getIvrRoleAssignment = "http://10.80.10.167/IVRLegacyServicesDev/ServiceRest.svc/URIIVRScript";
    public static $restUrlProduction = "http://10.80.10.167/IVRLegacyServices/ServiceRest.svc/URIIVRScript";
    public static $setIvrRoleAssignment = "http://10.80.10.167/IVRLegacyServicesDev/ServiceRest.svc/setRoleAssignment";
    public static $restUrl = "http://192.168.120.129/IVRLegacyServicesDev/ServiceRest.svc";
    public static $fileUploadServer = '10.80.10.82';
    public static $ldapCorp = 'ldap://corp.ibexglobal.com/';
    public static $accessError = 'Access Not Granted.Please Contact IVR Team.';
    public static $successSave = 'Record Saved Successfully.';
    public static $failureSave = 'Record Add Unsuccessful. Please contact IBEX IVR team. ';
    public static $successUpdate = 'Record Updated Successfully';
    public static $failureUpdate = 'Record Update Unsuccessful. Please contact IBEX IVR team.';
    public static $successDelete = 'Record Deleted Successfully.';
    public static $failureDelete = 'Record Deletion Unsuccessful. Please contact IBEX IVR team.';
    public static $reportFrequency = array('9' => 'Monthly', '5' => 'Mon-Fri', '6' => 'Mon-Sat', '7' => 'Mon-Sun', '1' => 'Single Day');
    public static $days = array('1' => 'Monday', '2' => 'Tuesday', '3' => 'Wednesday', '4' => 'Thursday', '5' => 'Friday', '6' => 'Saturday', '7' => 'Sunday');
    public static $appStatus = array('L' => 'Live', 'D' => 'Down');
    public static $hours = array(
        '0' => '00', '1' => '01', '2' => '02', '3' => '03', '4' => '04', '5' => '05', '6' => '06', '7' => '07', '8' => '08', '9' => '09', '10' => '10', '11' => '11', '12' => '12',
        '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '22', '23' => '23'
    );
    public static $appType = array('I' => 'Inbound', 'O' => 'Outbound', 'B' => 'Blended', 'N' => 'Inbound Plus', 'U' => 'Outbound Plus', 'L' => 'Blended Plus');
    public static $appEnv = array('S' => 'Staging', 'P' => 'Production');
    public static $appTime = array(
        '-12' => 'EST -12', '-11' => 'EST -11', '-10' => 'EST -10', '-9' => 'EST -9', '-8' => 'EST -8', '-7' => 'EST -7', '-6' => 'EST -6',
        '-5' => 'EST -5', '-4' => 'EST -4', '-3' => 'EST -3', '-2' => 'EST -2', '-1' => 'EST -1', '0' => 'EST',
        '1' => 'EST +1', '2' => 'EST +2', '3' => 'EST +3', '4' => 'EST +4', '5' => 'EST +5', '6' => 'EST +6',
        '7' => 'EST +7', '8' => 'EST +8', '9' => 'EST +9', '10' => 'EST +10', '11' => 'EST +11', '12' => 'EST +12',
    );
     public static $fromEmail = 'noreply@ibexglobal.com';
   // public static $fromEmail = 'itsupport@hunterpricekahn.co.uk';
    public static $toEmail = 'muhammad.awaisullah@ibex.co';
    public static $ccEmail = 'mwaqas@ibexglobal.com';
    public static $commonError = 'Something went wrong, Please contact IVR-Team!';
    public static $loginError = 'User not found, Please contact IVR-Team!';
    public static $credentialError = 'Wrong User name or Password, Please Enter Again!';
    public static $outDialSuccess = 'Call landed successfully.';
    public static $outDialFailure = 'Unable to make call, Please Check your parameters.';
    public static $outDialNumberError = 'The Out-Dial number cannot start from 1 or 8 or 9 or + ';

    //out dial strings
    public static $outDialServers = array('10.90.10.82', '10.80.10.80');
    public static $extensions = array('1244', '1245', '1246', '1247', '1209', '1210');
    public static $errorFavApp = 'This application already exist in favorite list.';
}