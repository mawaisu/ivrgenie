<?php
/**
 * Created by PhpStorm.
 * User: mawaisu
 * Date: 9/10/2018
 * Time: 8:59 PM
 */

namespace App\CommonUtils;
header('Content-Type: text/html; charset=utf-8');

class RestCalls
{
    public static function getRoleAssignmentAjax($roleId=0){


        $url=CommonStrings::$getIvrRoleAssignment;
        HelperFunctions::writeLogInfo($desc="Hi from RestCall! Calling service:".$url);

        $param=array("ParamList"=>$roleId,"RequestID"=>"900");
        $data=json_encode($param);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $output = curl_exec($ch);

        HelperFunctions::writeLogInfo($desc="Getting Response from service:".$output);

        curl_close($ch);
        //remove special chars,Invisible chars from response
        $array  = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $output);

        HelperFunctions::writeLogInfo($desc="Getting Response from service:".$output);

        return ($array);
    }


    // $service: the name of the service.
    // $data: Key value pair data for service.

    public static function call($service,$data)
    {
        if(empty($service)  || empty($data))
            return null;

        $url=CommonStrings::$restUrl.'/'.$service;
/*        if($service=="GetUserAuth")
        {
            $url=\Yii::$app->params['restActiveDirectoryURL'].'/'.$service;
        }
        if($service=="HavenHolidayReport")
        {
            $url=\Yii::$app->params['havenReportRestUrl'];
        }
        if($service=="aldb")
        {
            $url=\Yii::$app->params['aldbURL'];
        }
        if($service=="log")
        {
            $url=\Yii::$app->params['logsRestUrl'];
        }*/
        $data=json_encode($data);
        HelperFunctions::writeLogInfo("RestCalls: calling rest url :[ ".$url." ] with params data :[ ".$data." ]");
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $output = curl_exec($ch);
        curl_close($ch);
        $output=json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $output), true );
        return $output;
    }

    public static function getRolePermissions($roleId=0){


        $url=CommonStrings::$getIvrRoleAssignment;
        HelperFunctions::writeLogInfo($desc="Hi from RestCall! Calling service:".$url);

        $param=array("ParamList"=>$roleId,"RequestID"=>"930");
        $data=json_encode($param);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $output = curl_exec($ch);

        HelperFunctions::writeLogInfo($desc="Getting Response from service:".$output);

        curl_close($ch);
        //remove special chars,Invisible chars from response
        $array  = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $output);

        HelperFunctions::writeLogInfo($desc="Getting Response from service:".$output);

        return ($array);
    }

    public static function cwoOutDial($data){

        $url=CommonStrings::$restUrlProduction;
        HelperFunctions::writeLogInfo($desc="Hi from RestCall! Calling service:".$url);

        $params =$data['serverIp'].','.$data['name'].','.$data['policyNumber'].','.$data['policyGroup'].','.$data['number'].','.$data['extension'].',';

        $param=array("RequestID"=>"850","ParamList"=>$params);
        $dataReq=json_encode($param);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataReq);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $output = curl_exec($ch);
        $array  = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $output);

        HelperFunctions::writeLogInfo($desc="Getting Response from out dial service:".$output);

         $response =json_decode($array);

         try {
             if ($response->Status == 100) {

                 return  100;
             } else {

                 return 400;
             }
         }catch (\Exception $exception){
             report($exception);
             return 500;
         }
    }

    public static function getDashboardData($userId=14){


        $url=CommonStrings::$restUrlProduction;
        HelperFunctions::writeLogInfo($desc="Hi from RestCall! Calling service :".$url);

        $param=array("ParamList"=>$userId,"RequestID"=>"940");
        $data=json_encode($param);
        $ch = curl_init();

        HelperFunctions::writeLogInfo($desc="Calling service with param list :".$data);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $output = curl_exec($ch);

        HelperFunctions::writeLogInfo($desc="Getting Response from service [RequestID=940] : ".$output);

        curl_close($ch);
        //remove special chars,Invisible chars from response
        $array  = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $output);

        HelperFunctions::writeLogInfo($desc="Getting Response from service [RequestID = 940] Get Dashboard Data :".$output);
        $response =json_decode($array);
        try {
            if ($response->Status == 100) {

                return ($response);
            } else {

                return 400;
            }
        }catch (\Exception $exception){
            report($exception);
            return 500;
        }

    }


}