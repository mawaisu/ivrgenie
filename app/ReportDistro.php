<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportDistro extends Model
{
    protected $fillable = ['RptID', 'Email', 'Status'];

    protected $table = 'apprptemailsdistro';

    protected $primaryKey = 'ID';

    public $timestamps = false;

    public function appReport()
    {

        return $this->belongsTo('App\AppReport', 'RptID', 'RptID')->withDefault(['RptName'=>'Unknown Report']);;
    }

}
