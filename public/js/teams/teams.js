function addTeam()
{
	clearAppModalData();
	$("#teamTitle").html("Add Team");
	$("#addTeamPopup").modal('show');
}
function editTeam(id)
{
		var url='/ajax/get-team';
                loader("show");
                $("#teamID").val(id);
     	$("#teamTitle").html("Edit Team");           
        $.ajax({
            url: url,
			data:{id:id},
			dataType:"json",
            success: function(data)
			{
                                 
				if(data!=null)
				{
				    //clearAppModalData();
					$("#teamName").val(data.Name);
					$("#teamDesc").val(data.Description);
					$("#teamID").val(data.Team_ID);
					$("#teamCompany").val(data.Company_ID);
					$("#addTeamPopup").modal('show');
				}
                loader("hide");
            },
            error: function()
            {
                loader("hide");
                alert("Unable to get application data");
                
            }

        });
	
}

function clearAppModalData()
{

	$("#teamName").val("");
        $("#teamCompany").val("");
	$("#teamDesc").val("");
	
}

/*
$(document).ready(function (){

	 $('#addTeamForm').submit(function() {            
                var status;
                $("#addTeamPopup").modal('hide');
                loader("show");
                var name=$('#teamName').val();
                var teamCompany=$('#teamCompany').val();
                var url = baseUrl + '/ajax/check-teamduplicate';
                var id=$("#teamID").val();
        $.ajax({
            type: "POST",
            url: url,
            data: {name: name,
                   teamCompany: teamCompany,
                   id: id},            
            dataType: "json",
            async: false,
            success: function (data)
            {

                if (data != null)
                {  
                    status=data.result;  
                                    
                }
                
            },
            error: function ()
            {
                loader("hide");
                alert("Unable to get application data");

            }

        });
                
          if(status==1){
                       var obj = {title:"Duplicate Record", 
                                 'data-text':"Duplicate Record Exist Please check again",
                                 'data-type':"warning",
                                 'data-confirm-text':"OK",
                                  color:"white"
                                 };
                       messageBox(obj,clearAppModalData);          
                       loader("hide");
                       return false;
                       }
                       else{
                           return true;
                       }
  });

});
*/

