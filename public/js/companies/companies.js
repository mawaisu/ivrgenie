var companyVerifier=null;
function addCompany()
{
    clearAppModalData();
    $("#companyTitle").html("Add Company");
    $("#addCompanyPopup").modal('show');
}
function editCompany(id)
{
    $("#companyTitle").html("Edit Company");
        var url='/ajax/get-company';
                loader("show");
                $('#companyID').val(id);
        $.ajax({
            url: url,
            data:{id:id},
            dataType:"json",
            success: function(data)
            {
                        console.log(data);
                if(data!=null)
                {
                    //clearAppModalData();
                    $("#companyName").val(data.Name);
                    $("#companyDesc").val(data.Description);
                    $("#companyID").val(data.ID);
                    $("#addCompanyPopup").modal('show');
                }
                loader("hide");
            },
            error: function()
            {
                loader("hide");
                alert("Unable to get application data");
                
            }

        });
    
}

function clearAppModalData()
{

    $("#companyName").val("");
    $("#companyDesc").val("");
    
}

$(document).ready(function (){



    
     $('#addCompanyForm').submit(function() {
  // your code here
        if(!$("#addCompanyForm").valid())
        {
            return false;
        }
                var status;
                loader("show");
                var name=$('#companyName').val();
                var id=$('#companyID').val();
                var url = '/ajax/check-companyduplicate';
                 console.log(id+name);
        $.ajax({
            type: "get",
            url: url,
            data: {name: name,id:id},            
            dataType: "json",
            async: false,
            success: function (data)
            {

                console.log(data);
                if (data != null)
                {  
                    status=data.result;  
                                    
                }
                
            },
            error: function ()
            {
                loader("hide");
                alert("Unable to get application data");

            }

        });
                
          if(status==1){
                       var obj = {title:"Duplicate Record", 
                                 'data-text':"Duplicate Record Exist Please check again",
                                 'data-type':"warning",
                                 'data-confirm-text':"OK",
                                  color:"white"
                                 };
                       messageBox(obj,clearAppModalData);          
                       loader("hide");
                       return false;
                       }
                       else{
                          
                           return true;
                       }      
                
                
        
        
  });
 companyVerifier=$("#addCompanyForm").validate({
                        rules: 
                            {
                                companyName: 
                                            {
                                                required: true,
                                                alphanumeric:true,
                                                maxlength: 150,
                                                notBlank:true
                                            }
                            }
                    }); 
});