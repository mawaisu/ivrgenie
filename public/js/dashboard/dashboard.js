var socket = io.connect("http://localhost:4000");

/**  socket.on('chat',function (data) {

        alert(JSON.stringify(data));
		//$('#test').text(data.message);

        $('#header ul').append('<li>'+data.message+'</li>');
    });*/
socket.on('chat', function (data) {
    /** @var for data storing */
    var jsonData = data;
    var totalCalls = jsonData.totalCalls;
    var inboundCalls = jsonData.inbound;
    var outboundCalls = jsonData.outbound;
    var favApps = jsonData.favoriteApps;
    var applicationsData = jsonData.apps;
    var recentCalls = jsonData.recentCalls;

    console.log(recentCalls);

    $('#totalCalls1').text(totalCalls);
    $('#totalCalls2').text(totalCalls);

    $('#inBound1').text(inboundCalls);
    $('#inBound2').text(inboundCalls);

    $('#outBound1').text(outboundCalls);
    $('#outBound2').text(outboundCalls);

    $('#favApp1Count').text(favApps[0].count);
    $('#favApp2Count').text(favApps[1].count);
    $('#favApp3Count').text(favApps[2].count);
    $('#favApp4Count').text(favApps[3].count);

    $('#favApp1Name').text(favApps[0].name);
    $('#favApp2Name').text(favApps[1].name);
    $('#favApp3Name').text(favApps[2].name);
    $('#favApp4Name').text(favApps[3].name);

    $('#recentCallTable tbody').empty();


    for (i = 0; i < recentCalls.length; i++) {

        var row = "<tr>" +
            "<td>" + recentCalls[i].appName + "</td>" +
            "<td>" + recentCalls[i].dnis + "</td>" +
            "<td>" + recentCalls[i].callInTime + "</td>" +
            "<td>" + recentCalls[i].callEndTime + "</td>" +
            "<td>" + recentCalls[i].callDuration + "</td>" +
            "<td>" + recentCalls[i].hangupCode + "</td>" +
            "</tr>";
        $('#recentCallTable tbody').append(row);
    }

    $('.icon').show();


    $('#applications').highcharts({
        credits: {
            enabled: false
        },
        chart: {
            backgroundColor: '#ffffff',
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ' '
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                showInLegend: true,
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Application',
            colorByPoint: true,
            data: applicationsData
        }]
    });
    //$('#header ul').append('<li>'+data.message+'</li>');
});
/*

 $(function () {

	
	setTimeout(function(){location.reload(); }, 60000);
});*/
