function loadData()
{
	var location_id=$('#location_dd').val();
	var campaign_id=$('#campaign_dd').val();
	var type_id=$("#source").val();
	var from_date=$('#daterange-start').val();
	var to_date=$('#daterange-end').val();
	
	var url=baseUrl+'/ajax/al-dashboard-filter';
	loader("show");
	$.post(url,{data: { location_id: location_id, campaign_id: campaign_id, type_id: type_id, from_date: from_date, to_date: to_date}},
	function(response)
	{
		//$("#campaign_dd").html(data).attr('disabled',false).selectpicker('refresh'); 
		var data = $.parseJSON(response);
		totalCount = data.totalCount;
		successfulCount = data.successfulCount;
		warningCount = data.warningCount;
		exceptionCount = data.exceptionCount;
		warningsData = (data.warningsData);
		exceptionsData = (data.exceptionsData);
		successData = (data.successData);
		resultsData = (data.resultsData);
		$( "#resultCount" ).text(exceptionCount);
		$( "#totalCount" ).text(totalCount);
		$( "#successCount" ).text(successfulCount);
		$( "#warningCount" ).text(warningCount);
		$( "#exceptionCount" ).text(exceptionCount);
		initPie(exceptionsData);
		var SuccessDataModel = (data.SuccessDataModel);
		var table = $('#example').DataTable();
		table.clear().draw();
		SuccessDataModel.forEach( function (record)
		{
			table.row.add( [
            record.Status,
            record.AppType,
            record.CallDate,
            record.AgentEmail,
            record.SupervisorName,
            record.SupervisorEmail,
            record.Site,
            record.Campaign,
            record.AgentEmployeeID,
            record.AgentName,
            record.Time,
            record.Type,
            record.Detail,
            record.referencenumber,
            '<a class="btn bt-xs text-view" href="#." data-toggle="modal" data-target="#successModal"><i class="fa fa-eye"></i></a>'
			]).draw();
		});

		var WarningDataModel = (data.WarningDataModel);
		var table = $('#WarningModelTable').DataTable();
		table.clear().draw();
		WarningDataModel.forEach( function (record)
		{ 
			var row = table.row.add( [
			record.ID,
			record.ReferenceNo,
			record.AgentEmployeeID,
			record.AgentName,
			record.Status,
			record.Reason,
			record.Remarks,
			record.Assigned,
			record.CloseDateTime,
			record.ByWhom,
			record.Comments,
			'<a class="btn bt-xs text-view" href="#." data-toggle="modal" data-target="#updateModal"><i class="fa fa-eye"></i></a>'
			]).draw();
			table.row(row).column(0).nodes().to$().addClass('d_weID');
			table.row(row).column(1).nodes().to$().addClass('d_ReferenceNo');
			table.row(row).column(2).nodes().to$().addClass('d_AgentEmployeeID');
			table.row(row).column(3).nodes().to$().addClass('d_AgentName');
			table.row(row).column(4).nodes().to$().addClass('d_Status');
			table.row(row).column(5).nodes().to$().addClass('d_Reason');
			table.row(row).column(6).nodes().to$().addClass('d_Remarks');
			table.row(row).column(7).nodes().to$().addClass('d_Assigned');
			table.row(row).column(8).nodes().to$().addClass('d_CloseDateTime');
			table.row(row).column(9).nodes().to$().addClass('d_ByWhom');
			table.row(row).column(10).nodes().to$().addClass('d_Comments');
			
			});
		

		var ExceptionDataModel = (data.ExceptionDataModel);
		var table = $('#ExceptionModelTable').DataTable();
		table.clear().draw();
		ExceptionDataModel.forEach( function (record)
		{ 
			var row = table.row.add( [
			record.ID,
			record.ReferenceNo,
			record.AgentEmployeeID,
			record.AgentName,
			record.Status,
			record.Reason,
			record.Remarks,
			record.Assigned,
			record.CloseDateTime,
			record.ByWhom,
			record.Comments,
			'<a class="btn bt-xs text-view" href="#." data-toggle="modal" data-target="#updateModal"><i class="fa fa-eye"></i></a>'
			]).draw();
			table.row(row).column(0).nodes().to$().addClass('d_weID');
			table.row(row).column(1).nodes().to$().addClass('d_ReferenceNo');
			table.row(row).column(2).nodes().to$().addClass('d_AgentEmployeeID');
			table.row(row).column(3).nodes().to$().addClass('d_AgentName');
			table.row(row).column(4).nodes().to$().addClass('d_Status');
			table.row(row).column(5).nodes().to$().addClass('d_Reason');
			table.row(row).column(6).nodes().to$().addClass('d_Remarks');
			table.row(row).column(7).nodes().to$().addClass('d_Assigned');
			table.row(row).column(8).nodes().to$().addClass('d_CloseDateTime');
			table.row(row).column(9).nodes().to$().addClass('d_ByWhom');
			table.row(row).column(10).nodes().to$().addClass('d_Comments');
			});

		$('#modelData').hide();
		$('#WarningModelData').hide();
		$('#ExceptionModelData').show();
		loader("hide");

	});
}

function initPie(data)
{
	
	$('#results').highcharts({
		credits: {
            enabled: false
        },
        chart: {
            backgroundColor: '#ffffff',
            plotBorderWidth: null,
            plotShadow: false,
            height: 232,
            type: 'pie'
        },
        title: {
            text: ' '
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
				showInLegend: true,
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color:'black'
                    }
                }
            }
        },
        series: [{
            name: 'Results',
            colorByPoint: true,
            data: data
        }]
    });
}
function loadSuccessData(data)
{
	$('#successModal .modal-title').text("Details for "+data[9]);
	for (i = 0; i < 14; i++) {
		var tableRow = $("#successModal .modal-body tr").filter(function() {
			return $(this).attr("data-dt-column")==i;
		});
		tableRow.children().eq(1).text(data[i]);
	}
	
}

function loadUpdateData(data,row)
{
	$("#comments").attr('style','');
	$(".d_requiredAlert").addClass("hidden");
	$('#refno').val(data.find(".d_ReferenceNo").html());
	$('#reason').val(data.find(".d_Reason").html());
	$('#remarks').val(data.find(".d_Remarks").html());
	$('#weId').val(row[0]);
	$('#assigned').val('PS');
	if(data.find(".d_Status").html()=='Completed' || data.find(".d_Status").html()=='C')
	{
		$('#status').val('C');
	}
	else
	{
		$('#status').val('P');
	}
	$('#comments').val(data.find(".d_Comments").html());
	
}
$( document ).ready(function() {

	$('#content').jplist({				
      itemsBox: '.mylist' 
      ,itemPath: '.myitem' 
      ,panelPath: '.panel'	
   });
		$('#modelData').hide();
		$('#WarningModelData').hide();

	initPie(exceptionsData);
	$( "#resultCount" ).text(exceptionCount);
	$( "#resultLabel" ).text("Exceptions");
	$( "#dev-we-heading" ).text("Exceptions Detail");
	table = $('#example').DataTable( {
		retrieve: true,
	} );
	ExceptionTable = $('#ExceptionModelTable').DataTable();
	WarningTable = $('#WarningModelTable').DataTable();
	var column0 = table.column(0);
	var column1 = table.column(1);
	var column2 = table.column(2);
	var column3 = table.column(3);
	var column4 = table.column(4);
	var column5 = table.column(5);
	var column6 = table.column(6);
	var column7 = table.column(7);
	
	var EIdColumn = ExceptionTable.column(0);
	var WIdColumn = WarningTable.column(0);
	
 
    // Toggle the visibility
    column0.visible( ! column0.visible() );
    column1.visible( ! column1.visible() );
    column2.visible( ! column2.visible() );
    column3.visible( ! column3.visible() );
    column4.visible( ! column4.visible() );
    column5.visible( ! column5.visible() );
    column6.visible( ! column6.visible() );
    column7.visible( ! column7.visible() );
	
    EIdColumn.visible( ! EIdColumn.visible() );
    WIdColumn.visible( ! WIdColumn.visible() );
	
	// weTable = $('#weModelTable').DataTable();
	// weTable.columns(4).search('Exception', true, false).draw();
	$('#ExceptionModelData').show();

	
	//setTimeout(function(){location.reload(); }, 60000);
	
	 // $('#daterange').val(moment().format("MM/DD/YYYY")+" - "+moment().format("MM/DD/YYYY"));
	
	 // $("input[name=daterangepicker_start]").val(moment().format("YYYY-MM-DD"));
	 // $("input[name=daterangepicker_end]").val(moment().format("YYYY-MM-DD"));
	 
	
	$('#daterange').on('apply.daterangepicker', function(ev, picker) {
	  console.log(picker.startDate.format('YYYY-MM-DD'));
	  console.log(picker.endDate.format('YYYY-MM-DD'));
	  $('#daterange').val(picker.startDate.format('YYYY-MM-DD')+'-'+picker.endDate.format('YYYY-MM-DD'));
	  //loadData();

	});		
	
	$('#updateModal').on('hidden.bs.modal', function () {
		$('#ExceptionModelTable tbody tr').removeClass("d_active");
	})
	
	$(".BtnDashboard").click(function(e){
		$("#collapseExample").toggle();
		if($(".BtnDashboard").text().trim()=="Hide Graphs")
		{
			$(".BtnDashboard").text("Show Graphs")
		}
		else
		{
			$(".BtnDashboard").text("Hide Graphs")
		}
	});
	
	$("#submitBtn").click(function(e){
		e.preventDefault(); 
		if($('#daterange').val()==='')
		{
			alert("Please set date first");
			
		}
		else
		{
			$("#source").val($('#type_dd').val());
			//$(this).parents('form').submit();
			loadData();
		}
	});
	
	
	
});

$(document).ajaxComplete(function(){
    $('#example tbody tr').on('click', '.text-view', function () {
        //console.log(table.row(this.closest('tr')).data());
		if($(this).attr("data-target")=="#successModal")
		{
			loadSuccessData(table.row(this.closest('tr')).data());
		}
    });
	
	$('#WarningModelTable tbody tr').on('click', '.text-view', function () {
        //console.log(table.row(this.closest('tr')).data());
		$(this).closest('tr').addClass('d_active');
		$('#updateModal .modal-title').text("Update Warning");
		// WarningTable.row($(this).closest('tr')).invalidate();
		// WarningTable.draw();
		loadUpdateData($(this).closest('tr'),WarningTable.row(this.closest('tr')).data());
		
    });
	$('#ExceptionModelTable tbody tr').on('click', '.text-view', function () {
        //console.log(table.row(this.closest('tr')).data());
		$(this).closest('tr').addClass('d_active');
		
		$('#updateModal .modal-title').text("Update Exception");
		// ExceptionTable.row($(this).closest('tr')).invalidate();
		// ExceptionTable.draw();
		loadUpdateData($(this).closest('tr'),ExceptionTable.row(this.closest('tr')).data());
    });
})

$('#example tbody tr').on('click', '.text-view', function () {
        //console.log(table.row(this.closest('tr')).data());
		if($(this).attr("data-target")=="#successModal")
		{
			loadSuccessData(table.row(this.closest('tr')).data());
		}
    });
	
$('#WarningModelTable tbody tr').on('click', '.text-view', function () {
        //console.log(table.row(this.closest('tr')).data());
		$(this).closest('tr').addClass('d_active');

			$('#updateModal .modal-title').text("Update Warning");
			// WarningTable.row($(this).closest('tr')).invalidate();
			// WarningTable.draw();
			loadUpdateData($(this.closest('tr')),WarningTable.row(this.closest('tr')).data());
		
    });
$('#ExceptionModelTable tbody tr').on('click', '.text-view', function () {
        //console.log(table.row(this.closest('tr')).data());
		$(this).closest('tr').addClass('d_active');
		
			$('#updateModal .modal-title').text("Update Exception");
			// ExceptionTable.row($(this).closest('tr')).invalidate();
			// ExceptionTable.draw();
			loadUpdateData($(this.closest('tr')),ExceptionTable.row(this.closest('tr')).data());

    });

$('#location_dd').on('change', function(){
    var val=$(this).val();
	
	 if(val=='' || val==15)
     {
		  var option = $('<option></option>').attr("value", "").text("Select Location First");
		  $("#campaign_dd").empty().append(option);
		  $("#campaign_dd").html("<option val=''>Select Location First</option>");
		  $("#campaign_dd").prop("disabled", true);
		 
		 $('.selectpicker').selectpicker('refresh');
     }
	else
	{
		var url=baseUrl+'/ajax/campaigns';
		loader("show");
		$.post(url,{data: val},
		function(data){
			//console.log(data);
			$("#campaign_dd").html(data).attr('disabled',false).selectpicker('refresh');  
			loader("hide");
		});
		//loadData();
	}
});

$('#type_dd').on('change', function(){
    //loadData();
});

$('#campaign_dd').on('change', function(){
    //loadData();
});

$("#submitupdatemodal").click(function(e){
	   var comments = $("#comments").val();
	   if(comments == null || comments=="" )
	   {
			e.preventDefault();
			$("#comments").css("border-color","red");
			$(".d_requiredAlert").removeClass("hidden");
			$("#comments").show().focus();
			return false;
	   }
	   var assigned = $("#assigned").val();
	   var status = $("#status").val();
	   var comments = $("#comments").val();
	   var weId = $("#weId").val();
	   var tempExceptionTable=$("#ExceptionModelTable");
	   var tr=$(tempExceptionTable).find('tr.d_active');
	   if(tr.length)
	   {
			$(tr).removeClass("d_active");
	   }
	   else
	   {
			var tempWarningTable=$("#WarningModelTable");
			tr=$(tempWarningTable).find('tr.d_active');
			$(tr).removeClass("d_active");
	   }
	   //alert(str);
	   var url=baseUrl+'/ajax/exception-warning-update';
		loader("show");
		$.post(url,{data: { assigned: assigned, status: status, comments: comments, weId: weId}},
		function(response){
			var data = $.parseJSON(response);
			var status = data.status;
			if(status)
			{
				/*var row = ExceptionTable.row(0).search(data.record.ID).data();
				row[8] = data.record.CloseDateTime;
				row[9] = data.record.ByWhom;
				row[10] = data.record.Comments;
				ExceptionTable.row().search(data.record.ID).data(row); */
				$(tr).find('td.d_CloseDateTime').html(data.record.CloseDateTime);
				$(tr).find('td.d_ByWhom').html(data.record.ByWhom);
				$(tr).find('td.d_Comments').html(data.record.Comments);
				$(tr).find('td.d_Status').html(data.record.Status);
				loader("hide");
			}
			else
			{loader("hide");}
			
		});
});
$( "#exception" ).click(function() {
	$('#results').highcharts().destroy();
	initPie(exceptionsData);
	$( "#resultCount" ).text(exceptionCount);
	$( "#resultLabel" ).text("Exceptions");
	$( "#dev-we-heading" ).text("Exceptions Detail");
	
	$(this).addClass("active");
	$( "#success" ).removeClass("active");
	$( "#warning" ).removeClass("active");
	$( "#total" ).removeClass("active");
	// table = $('#example').DataTable( {
		// retrieve: true,
	// } );
	// table.columns(0).search('E', true, false).draw();
	$('#modelData').hide();
	$('#ExceptionModelData').show();
	$('#WarningModelData').hide();
});
$( "#success" ).click(function() {
	$('#results').highcharts().destroy();
	initPie(successData);
	$( "#resultCount" ).text(successfulCount);
	$( "#resultLabel" ).text("Successful");
	$( "#dev-we-heading" ).text("Successful Records");
	
	$(this).addClass("active");
	$( "#exception" ).removeClass("active");
	$( "#warning" ).removeClass("active");
	$( "#total" ).removeClass("active");
	// table = $('#example').DataTable( {
		// retrieve: true,
	// } );
	// table.columns(0).search('S', true, false).draw();
	$('#ExceptionModelData').hide();
	$('#WarningModelData').hide();
	$('#modelData').show();
});
$( "#warning" ).click(function() {
	$('#results').highcharts().destroy();
	initPie(warningsData);
	$( "#resultCount" ).text(warningCount);
	$( "#resultLabel" ).text("Warnings");
	$( "#dev-we-heading" ).text("Warnings Detail");
	$(this).addClass("active");
	$( "#exception" ).removeClass("active");
	$( "#success" ).removeClass("active");
	$( "#total" ).removeClass("active");
	// table = $('#example').DataTable( {
		// retrieve: true,
	// } );
	// table.columns(0).search('W', true, false).draw();
	$('#modelData').hide();
	$('#ExceptionModelData').hide();
	$('#WarningModelData').show();
});
$( "#total" ).click(function() {
	$('#results').highcharts().destroy();
	initPie(resultsData);
	$( "#resultCount" ).text(totalCount);
	$( "#resultLabel" ).text("Total");
	$(this).addClass("active");
	$( "#exception" ).removeClass("active");
	$( "#success" ).removeClass("active");
	$( "#warning" ).removeClass("active");
	$('#modelData').hide();
	$('#ExceptionModelData').hide();
	$('#WarningModelData').hide();
});