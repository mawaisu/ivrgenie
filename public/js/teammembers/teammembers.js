var teammembersVerifier=null;
function addMember()
{
    clearAppModalData();
    $("#addMemberPopup").modal('show');
}
function editCompany(id)
{

        console.log(id);   
        var url=baseUrl+'/ajax/get-member';
        loader("show");

        $.ajax({
            url: url,
            data:{id:id},
            dataType:"json",
            success: function(data)
            {
                                 
                if(data!=null)
                {
                    console.log(data);
                    clearAppModalData();
                    populateDropdowns(data.memberCompany);
                    $("#memberID").val(data.memberID);
                    console.log(data.memberID);
                    $("#memberCompany").val(data.memberCompany);
                    console.log(data.memberCompany);
                    $("#teams").val(data.teams);
                    console.log(data.teams);
                    $("#userName").val(data.users);
                    console.log(data.users);
                    $("#addMemberPopup").modal('show');
                }
                loader("hide");
            },
            error: function()
            {
                loader("hide");
                alert("Unable to get application data");
                
            }

        });
    
}

function clearAppModalData()
{

   $("#memberID").val("");
   $("#memberCompany").val("");
   $("#teams").empty();
   $("#userName").empty();
    
}


function populateDropdowns(companyID){
        var url=baseUrl+'/ajax/get-companyteams';
        var id=companyID;
        loader('show');
        $.ajax({
        url: url,
        type: 'POST',
        async: false,
        data:{id:id},
        dataType:"json",
            success: function(data)
            {
                                 
                if(data!=null)
                {
                    //clearAppModalData();
                                    for (var key in data[0]) {
                                       $('#teams').append($('<option>', {
                                                   value: key,
                                                   text: data[0][key]
                                            }));
                                         }
                                        $('#teams').prop('disabled', false); 
                                     for (var key in data[1]) {
                                       $('#userName').append($('<option>', {
                                                   value: key,
                                                   text: data[1][key]
                                            }));
                                         } 
                                         $('#userName').prop('disabled', false); 
                    
                }
                
            },
            error: function()
            {
                loader("hide");
                alert("Error Please contact IBEX-IVR team");
                
            }

        });
    
}


$(document).ready(function (){


// for automatic alert fades
$("#alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#alert").alert('close');
    });
    
     $('#addMemberForm').bind('submit', function () {
 		if(!$("#addMemberForm").valid())
		{
			return false;
		} 
	
	    loader("show");
        $('#submitButton').prop('disabled', true);
        $('#cancelmodal').prop('disabled', true);
   
        
  });

$('#memberCompany').on('change', function() {
     $("#teams").empty();
     $("#userName").empty();
     $('#teams').prop('disabled', true);
     $('#userName').prop('disabled', true);     
     var id=$("#memberCompany").val();
     loader("show");
     populateDropdowns(id);
     loader("hide");   
});

teammembersVerifier=$("#addMemberForm").validate({
						rules: 
							{
								memberCompany: 
											{
												required: true
											},
								teams: 
											{
												required: true
											},
								userName: 
											{
												required: true
												
											}											
							}
					});	

});