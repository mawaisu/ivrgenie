function CRUD(treeDesigner)
{
  
	var _this=this;
	_this.designer=treeDesigner;
	_this.fileUploadModal="#fileUploadModal";
	_this.promptModalID="#promptModal";
	_this.terminatorModalID="#terminatorModal";
	_this.ttsModalID="#ttsModal";
	_this.variableModalID="#variableModal";
	_this.functionModalID="#functionModal";
	_this.recordModalID="#recordModal";
	_this.transferModalID="#transferModal";
	_this.keypressModalID="#keypressModal";
	_this.confirmationKeypressModalID="#confirmationKeypressModal";
	_this.menuModalID="#menuModal";
	_this.audioModalID="#audioModal";
	_this.ifModalID="#ifModal";
	_this.uriModalID="#uriModal";
	_this.confirmationModalID="#confirmationModal";
	_this.setValueModalID="#setValueModal";
	_this.inputModalID="#inputModal";
	_this.invalidResponseTemplateID="#invalidResponseTemplate";
	_this.outputParamTemplateID="#outputParamTemplate";
	_this.inputParamTemplateID="#inputParamTemplate";
	_this.statusParamTemplateID="#statusParamTemplate";
	_this.confirmationInvalidResponseTemplateID="#confirmationInvalidResponseTemplate";
	_this.inputInvalidResponseTemplateID="#inputInvalidResponseTemplate";
	_this.legalInvalidResponseTemplateID="#legalInvalidResponseTemplate";
	_this.languageInvalidResponseTemplateID="#languageInvalidResponseTemplate";
	_this.noResponseTemplateID="#noResponseTemplate";
	_this.confirmationNoResponseTemplateID="#confirmationNoResponseTemplate";
	_this.inputNoResponseTemplateID="#inputNoResponseTemplate";
	
	_this.legalNoResponseTemplateID="#legalNoResponseTemplate";
	_this.languageNoResponseTemplateID="#languageNoResponseTemplate";
	
	_this.invalidTableID="#invalidResponseTable";
	_this.outputParamTableID="#outputParamTable";
	_this.inputParamTableID="#inputParamTable";
	_this.statusParamTableID="#statusParamTable";
	
	_this.confirmationInvalidTableID="#confirmationInvalidResponseTable";
	_this.languageInvalidTableID="#languageInvalidResponseTable";
	
	_this.inputInvalidTableID="#inputInvalidResponseTable";
	_this.legalInvalidTableID="#legalInvalidResponseTable";
	_this.noResponseTableID="#noResponseTable";
	_this.legalNoResponseTableID="#legalNoResponseTable";
	
	_this.languageNoResponseTableID="#languageNoResponseTable";
	
	_this.confirmationNoResponseTableID="#confirmationNoResponseTable";
	_this.inputNoResponseTableID="#inputNoResponseTable";
	_this.legalModalID="#legalModal";
	_this.languageModalID="#languageModal";
    
	_this.languageKeyPressModalID="#languageKeyPressModal";
	_this.langList=[{id:1,text:'English'},{id:2,text:'Spanish'},{id:3,text:'French'},{id:4,text:'German'},{id:7,text:'Spanish'}];
	_this.edit=function(node)
	{
		var type=_this.designer.tree._model.data[node.id].type;
               
        var parentType=_this.designer.tree._model.data[_this.designer.tree._model.data[node.id].parent].type;
                                
		switch(type)
		{
			case "prompt":
				_this.editPrompt(node);
			break;
			case "terminator":
				_this.editTerminator(node);
			break;
			case "tts":
				_this.editTts(node);
			break;			
			case "variable":
				_this.editVariable(node);
			break;			
			case "keypress":
				if(parentType=="menu"){
					_this.editKeypress(node);
				}
				else if(parentType=="language"){
					_this.editLanguageKeypress(node);
				}
				else{
					_this.editConfirmationKeypress(node);
				}
			break;			
			case "menu":
				_this.editMenu(node);
			break;
			case "record":
				_this.editRecord(node);
			break;
			case "transfer":
				_this.editTransfer(node);
			break;
            case "confirmation":
				_this.editConfirmation(node);
			break;
			
			case "input":
				_this.editInput(node);
			break;
			case "function":
				_this.editFunction(node);
			break;	
			case "if":
				_this.editIf(node);
			break;		
			case "uri":
				_this.editURI(node);
			break;		
			case "legal":
				_this.editLegal(node);
			break;
			case "language":
				_this.editLanguage(node);
			break;
			case "setvalue":
				_this.editSetValue(node);
			break;	
			case "audio":
				_this.editAudio(node);
			break;				
		}		
	}
	
	_this.uploadFile=function(node)
	{
		
		//var promptID=_this.designer.tree._model.data[node.id].data.PromptID;
		var promptID=node.id;
		var appID=_this.designer.builder.IVR;
		
		$('#fileAppId').val(appID);
		$('#filePromptId').val(promptID);

        $(_this.fileUploadModal).modal('show');
                                
				
	}
	_this.getGotoList=function()
	{
		var data=_this.designer.tree._model.data;
		var list=[];

		for (var node in data) 
		{
			
			if(node=='#' || data[node].type=="keypress")
					continue;
			var item={};
			item.id=data[node].id;
			item.text=data[node].data.PromptText;
			list.push(item);
		}	

		return list;		
	}
/*	_this.getOperandList=function()
	{
		var data=_this.designer.tree._model.data;
		var list=[];

		for (var node in data) 
		{
			
			if(node=='#' || data[node].type=="keypress" || 
			 data[node].type=="if" ||
			data[node].type=="prompt" || data[node].type=="confirmation" ||
			data[node].type=="record" || data[node].type=="transfer" || 
			data[node].type=="function" ||  data[node].type=="uri" || 
			data[node].type=="tts"
			)
			{
					continue;
			}
			
			var item={};
			item.id=data[node].id;
			item.text=data[node].data.PromptText;
			list.push(item);
		}	
		return list;

	} */		
	_this.populateGotoList=function(id,node)
	{
		    var id="#"+id;
			var options = $(id);			
			var list=_this.getGotoList();
			options.html('<option value="0">Hangup</option>');
			
			$.each(list, function() {
				
				var text=this.id+':'+this.text;
				if(node.id==this.id)
				{
				 // options.append($("<option />").val(this.id).text(text).attr('disabled',true));
				}
				else 
				{
					options.append($("<option />").val(this.id).text(text));
				}
			});	
					
	}
	
	
	_this.populateLanguageKeypress=function(node,current)
	{

		
		var data=_this.designer.tree._model.data[node.id];
		var parent=_this.designer.tree._model.data[data.parent];
		var childrens=parent.children;
		var existingLanguage=[];
		if(childrens.length>0)
		{
			for(var i=0; i<childrens.length; i++)
			{
				var nodeID=childrens[i];
				var keypressData=_this.designer.tree._model.data[nodeID].data;
				var flow=keypressData.flow[0];
				existingLanguage.push(flow.NextANSID);
			}
		}
		id="#languageKeypressList";
		var options = $(id);			
		options.html('');
		var key="";
		

		
		for(var v=0; v<_this.langList.length; v++)
		{
			
			key=_this.langList[v].id;

			
			if(existingLanguage.indexOf(key)==-1 || key==current)
			{
			 var text=_this.langList[v].text;
			 options.append($("<option />").val(key).text(text));
			}
		}
					
	}	
	_this.populateOperands=function(id)
	{
		
		    var id="#"+id;
			var options = $(id);			
			//var list=_this.getOperandList();
			var list=getOperandList();
			
			var html='<option value="">Select Operand</option>';
			if(id!="#cmdType"){
			html+='<option value="variableInteger">Variable Integer</option>';
            html+='<option value="variableString">Variable String</option>';
			html+='<option value="getCurrentYear">Current year</option>';
			html+='<option value="getCurrentMonth">Current month</option>';
			html+='<option value="getCurrentDay">Current day</option>';
			html+='<option value="getCurrentHour">Current hour</option>';
			html+='<option value="getCurrentMinute">Current Minute(s)</option>';
			html+='<option value="getCurrentSeconds">Current Second(s)</option>';
			html+='<option value="getCurrentDayOfWeek">Current Day of Week</option>';
			html+='<option value="getCurrentANI">Current ANI</option>';
			html+='<option value="getCurrentDNIS">Current DNIS</option>';
			html+='<option value="isContain">Contains</option>';
			}				
			options.html(html);
			
			$.each(list, function() {
				
				var text=this.id+':'+this.text;

					options.append($("<option />").val(this.id).text(text));
				
			});			
		
	}
	_this.editURI=function(node)
	{
		   

		    $("#uriID").val('');
		    $("#uriText").val('');

			$(".input-param-rows").remove();
			$(".output-param-rows").remove();
			$(".status-param-rows").remove();			
			var data=_this.designer.tree._model.data;
			_this.populateGotoList("uriGoto",node);
			
			 if(data!=null)
			 {
				data=data[node.id];

				$("#uriID").val(data.id);
				$("#uriText").val(data.data.PromptText);

				if(data.data.flow[0].Value=="-2"){
				  $('#continueHangup').prop('checked', true); 	
				}

				//var list=_this.getOperandList();
				var list=getOperandList();
				
				var option="<option value='0'>Create New</option>";
		
				$("#inputParamList").html(option);
				$("#outputParamList").html(option);
				$("#statusParamList").html(option);

 			$.each(list, function() {

				var text=this.id+':'+this.text;

					$("#inputParamList").append($("<option />").val(this.id).text(text));
					$("#outputParamList").append($("<option />").val(this.id).text(text));
					$("#statusParamList").append($("<option />").val(this.id).text(text));


				});					
			
				var  params=data.data.web;
				console.log(params);
				for(i=0; i<params.length; i++)
				{
					var param={};
					param.paramName=params[i].Name;
					param.paramType=params[i].Type;
					param.paramAction=params[i].Action;
					param.paramValue=params[i].Value;
					if(params[i].Type=="IN")
					{
					
						_this.populateInputParamRow(param);
					}
					else if(params[i].Type=="OUT")
					{

						_this.populateOutputParamRow(param);						
					}
					else
					{

						_this.populateStatusParamRow(param);						
					}
				}
				if(data.data.flow.length>0)
				{				
					$("#uriGoto").val(data.data.flow[0].NextANSID);
				}
				else 
				{
					$("#uriGoto").val(0);
				}				
				$(_this.uriModalID).modal('show');
			}	
			
	}				
	_this.editIf=function(node)
	{
		   

		    $("#ifID").val('');
		    $("#ifText").val('');

			
			var data=_this.designer.tree._model.data;
			 _this.populateGotoList("ifGotoSuccess",node);
			 _this.populateGotoList("ifGotoFailure",node);
			
			 _this.populateOperands("ifLeftOperand");
			 _this.populateOperands("ifRightOperand");
			
			 if(data!=null)
			 {
				data=data[node.id];

				$("#ifID").val(data.id);
				$("#ifText").val(data.data.PromptText);

					
				
				identifiers=[/^CON/];
				extra=_this.getExtraList(identifiers,node.id);	
				if(extra.length>0)
				{
					
					$("#ifOperator").val(extra[0].id);
				}
				else 
				{
					$("#ifOperator").val(2);
				}				

				identifiers=[/^VLI/];
				extra=_this.getExtraList(identifiers,node.id);	
				if(extra.length>0)
				{
					
					$("#ifLeftOperand").val("variableInteger");
					$("#ifVLI").val(extra[0].id);
					$("#ifVLI").removeClass('hidden');
				}
				identifiers=[/^VLS/];
				extra=_this.getExtraList(identifiers,node.id);	
				if(extra.length>0)
				{
					
					$("#ifLeftOperand").val("variableString");
					$("#ifVLS").val(extra[0].id);
					$("#ifVLS").removeClass('hidden');
					
				}
				identifiers=[/^CLS/];
				extra=_this.getExtraList(identifiers,node.id);	
				if(extra.length>0)
				{
					
					$("#ifLeftOperand").val("isContain");
					$("#ifVLS").val(extra[0].id);
					$("#ifVLS").removeClass('hidden');
					
				}
				identifiers=[/^FLI/];
				extra=_this.getExtraList(identifiers,node.id);					
				if(extra.length>0)
				{
					$("#ifLeftOperand").val(extra[0].id);

				}

				identifiers=[/^FLS/];
				extra=_this.getExtraList(identifiers,node.id);					
				if(extra.length>0)
				{
					$("#ifLeftOperand").val("isContain");

				}

				$("#ifLeftOperand").change();
				
				identifiers=[/^VRI/];
				extra=_this.getExtraList(identifiers,node.id);	
				if(extra.length>0)
				{
					
					$("#ifRightOperand").val("variableInteger");
					$("#ifVRI").val(extra[0].id);
					$("#ifVRI").removeClass('hidden');


					
				}
				identifiers=[/^VRS/];
				extra=_this.getExtraList(identifiers,node.id);	
				if(extra.length>0)
				{

					$("#ifRightOperand").val("variableString");
					$("#ifOperator").val("2"); 
                    $("#ifOperator").attr("disabled",true); 
					$("#ifVRS").val(extra[0].id);
					$("#ifVRS").removeClass('hidden');
					
				}
				
				identifiers=[/^CRI/];
				extra=_this.getExtraList(identifiers,node.id);	
				if(extra.length>0)
				{
					
					$("#ifRightOperand").val(extra[0].id);


					
				}				

				identifiers=[/^CLI/];
				extra=_this.getExtraList(identifiers,node.id);	
				if(extra.length>0)
				{
					
					$("#ifLeftOperand").val(extra[0].id);
					
				}				
				identifiers=[/^FRI/];
				extra=_this.getExtraList(identifiers,node.id);					
				if(extra.length>0)
				{
					$("#ifRightOperand").val(extra[0].id);
					
				}

			/*	identifiers=[/^FRS/];
				extra=_this.getExtraList(identifiers,node.id);					
				if(extra.length>0)
				{
					$("#ifRightOperand").val("isContain");

				} */

				if(data.data.flow.length>0)
				{				
					$("#ifGotoSuccess").val(data.data.flow[0].NextANSID);
					$("#ifGotoFailure").val(data.data.flow[1].NextANSID);
				}
				else 
				{
					$("#ifGotoSuccess").val(0);
					$("#ifGotoFailure").val(0);
				}
				
				
		     	$("#ifRightOperand").change();
			
				
				$(_this.ifModalID).modal('show');
			}	
				
	}	

			_this.editAudio=function(node)
	{
		  
		  
               
		    $("#audioID").val('');
		    $("#audioText").val('');
			
			var data=_this.designer.tree._model.data;
			
			 _this.populateGotoList("audioGoto",node);
			 if(data!=null)
			 {
				data=data[node.id];

				$("#audioID").val(data.id);
				$("#audioText").val(data.data.PromptText);
                 
                if(data.data.flow.length>0)
				{				
					$("#audioGoto").val(data.data.flow[0].NextANSID);
				}
				else 
				{
					$("#audioGoto").val(0);
				} 
					
				
				identifiers=[/^HANGUPKEY/];
				extra=_this.getExtraList(identifiers,node.id);	
				if(extra.length>0)
				{
					
					$("#audioHangupKey").val(extra[0].id);
				}
						
				identifiers=[/^PAUSEKEY/];
				extra=_this.getExtraList(identifiers,node.id);	
				if(extra.length>0)
				{
					
					$("#audioPauseKey").val(extra[0].id);
				}
                identifiers=[/^SARTOVERKEY/];
				extra=_this.getExtraList(identifiers,node.id);	
				if(extra.length>0)
				{
					
					$("#audioStartOverKey").val(extra[0].id);
				}

			}		
				
				$(_this.audioModalID).modal('show');
	
				
	}	

    _this.saveAudio=function (node){
      if(!$("#audioForm").valid())
		{
			return false;
		}
		var id=$("#audioID").val();
         _this.set_id(id);
         if(!_this.designer.nodeArray.includes(id))
        {
        	_this.designer.nodeArray.push(id);		
        }  		
		var data=_this.designer.tree._model.data[id];
		data.text=id+":"+$("#audioText").val();

		data.data.PromptText=$("#audioText").val();

		var flow=new Flow();
		flow.NextANSID=$("#audioGoto").val();
		flow.Value=-1;
		
		data.data.flow=[];
		data.data.flow.push(flow);

		data.data.extra=[];
		
		

		
		var h=new Extra();
		h.IDENTIFIER="HANGUPKEY";
		h.VALUE=$("#audioHangupKey").val();		



		var p=new Extra();
		p.IDENTIFIER="PAUSEKEY";
		p.VALUE=$("#audioPauseKey").val();


		var s=new Extra();
		s.IDENTIFIER="SARTOVERKEY";
		s.VALUE=$("#audioStartOverKey").val();	
		
				
		data.data.extra.push(h);
		data.data.extra.push(p);
		data.data.extra.push(s);


		_this.designer.tree._model.data[id]=data;

		var nodeID="#"+id;
	
	
		_this.designer.tree.redraw_node(nodeID);
			
	
		$(_this.audioModalID).modal('hide');
     
    }	


		_this.editSetValue=function(node)
	{
		  
		  
               
		    $("#setValueID").val('');
		    $("#setValueText").val('');
            $("#setValueVAL").val('');
			
			var data=_this.designer.tree._model.data;
			
			 _this.populateOperands("cmdType");
			 _this.populateGotoList("setValueGoto",node);
			 if(data!=null)
			 {
				data=data[node.id];

				$("#setValueID").val(data.id);
				$("#setValueText").val(data.data.PromptText);
                 
                if(data.data.flow.length>0)
				{				
					$("#setValueGoto").val(data.data.flow[0].NextANSID);
				}
				else 
				{
					$("#setValueGoto").val(0);
				} 
					
				
				identifiers=[/^PROMPTID/];
				extra=_this.getExtraList(identifiers,node.id);	
				if(extra.length>0)
				{
					
					$("#cmdType").val(extra[0].id);
				}
						
				identifiers=[/^VAL/];
				extra=_this.getExtraList(identifiers,node.id);	
				if(extra.length>0)
				{
					var v=extra[0].id;
					//var res=v.split("|");
					if(v=="PROMPTID")
					{
					  if( /^\d+$/.test(extra[0].text) ){ 	
						$("#valType").val("cmd");
						$("#valType").change();
						$("#setValueVAL").val(extra[0].text);
					  }
					  else {
					  		$("#valType").val(extra[0].text);
					  		$("#setValueVAL").remove();
					  	}	
					}
					else
					{	
						$("#valType").val("text");
                        $("#setValueVAL").val(v);
                    }    
				}
			}		
				
				$(_this.setValueModalID).modal('show');
	
				
	}


	_this.saveSetValue=function (node){
      if(!$("#setValueForm").valid())
		{
			return false;
		}
		var id=$("#setValueID").val();
         _this.set_id(id);
         if(!_this.designer.nodeArray.includes(id))
        {
        	_this.designer.nodeArray.push(id);		
        }  		
		var data=_this.designer.tree._model.data[id];
		data.text=id+":"+$("#setValueText").val();

		data.data.PromptText=$("#setValueText").val();

		var flow=new Flow();
		flow.NextANSID=$("#setValueGoto").val();
		flow.Value=-1;
		
		data.data.flow=[];
		data.data.flow.push(flow);

		data.data.extra=[];
		
		

		
		var pid=new Extra();
		pid.IDENTIFIER="PROMPTID";
		pid.VALUE=$("#cmdType").val();		



		var v=new Extra();
		v.IDENTIFIER="VAL";
		
		if($("#valType").val()=="cmd")
		{	
			v.VALUE="PROMPTID|"
			v.VALUE=v.VALUE+$("#setValueVAL").val();	
		}
		else if($("#valType").val()=="text")
		{
			v.VALUE=$("#setValueVAL").val();
		}
		else 
		{
		   v.VALUE="PROMPTID|"	
		   v.VALUE=v.VALUE+$("#valType").val();		
		}	
		
				
		data.data.extra.push(pid);
		data.data.extra.push(v);			

		_this.designer.tree._model.data[id]=data;

		var nodeID="#"+id;
	
	
		_this.designer.tree.redraw_node(nodeID);
			
	
		$(_this.setValueModalID).modal('hide');
     
    }


	_this.editFunction=function(node)
	{
		   

		    $("#functionText").rules("remove");
		    $("#functionID").val('');
		    $("#functionText").val('');
			
			var data=_this.designer.tree._model.data;
			_this.populateGotoList("functionGotoSuccess",node);
			_this.populateGotoList("functionGotoFailure",node);
			
				
			
			if(data!=null)
			{
				 data=data[node.id];

				$("#functionID").val(data.id);
				$("#functionText").val(data.data.PromptText);

					
				
				identifiers=[/^FRM/];
				extra=_this.getExtraList(identifiers,node.id);	
				if(extra.length>0)
				{
					
					$("#functionFromHour").val(extra[0].id);
				}
				else 
				{
					$("#functionFromHour").val(0);
				}				

				identifiers=[/^FTO/];
				extra=_this.getExtraList(identifiers,node.id);	
				if(extra.length>0)
				{
					
					$("#functionToHour").val(extra[0].id);
				}
				else 
				{
					$("#functionToHour").val(0);
				}

				identifiers=[/^FRM_MM/];
				extra=_this.getExtraList(identifiers,node.id);	
				if(extra.length>0)
				{
					
					$("#functionFromMin").val(extra[0].id);
				}
				else 
				{
					$("#functionFromMin").val(0);
				}

				identifiers=[/^FTO_MM/];
				extra=_this.getExtraList(identifiers,node.id);	
				if(extra.length>0)
				{
					
					$("#functionToMin").val(extra[0].id);
				}
				else 
				{
					$("#functionToMin").val(0);
				}
				

				var identifiers=[/^TYP/];
				extra=_this.getExtraList(identifiers,node.id);					
				if(extra.length>0)
				{
					if($("#functionToMin").val()==0 && $("#functionFromMin").val()==0)
					{
						var typeValue=extra[0].id;
						typeValue++;
						$("#functionType").val(typeValue);
					}
					else 
					{
						$("#functionType").val(extra[0].id);
					}
				}
				else 
				{
					$("#functionType").val("1");
				}							
				
				
				if(data.data.flow.length>0)
				{				
					$("#functionGotoSuccess").val(data.data.flow[0].NextANSID);
					$("#functionGotoFailure").val(data.data.flow[1].NextANSID);
				}
				else 
				{
					$("#functionGotoSuccess").val(0);
					$("#functionGotoFailure").val(0);
				}
				
				
				
				$(_this.functionModalID).modal('show');
			}	
				
	}		
	_this.editRecord=function(node)
	{
		   

		   $("#recordID").val('');
		    $("#recordText").val('');
		    $("#recordBargin").val('0');
			
			var data=_this.designer.tree._model.data;
			_this.populateGotoList("recordGoto",node);
			
				
			
			if(data!=null)
			{
				 data=data[node.id];

				$("#recordID").val(data.id);
				$("#recordText").val(data.data.PromptText);
	

				if(data.data.flow.length>0)
				{				
					$("#recordGoto").val(data.data.flow[0].NextANSID);
				}
				else 
				{
					$("#recordGoto").val(0);
				}
				
				var identifiers=[/^BR/];
				var extra=_this.getExtraList(identifiers,node.id);	
				if(extra.length>0)
				{
					
					$("#recordBargin").val(extra[0].id);
				}
				else 
				{
					$("#recordBargin").val("FALSE");
				}	

				identifiers=[/^ML/];
				extra=_this.getExtraList(identifiers,node.id);					
				if(extra.length>0)
				{
					
					$("#recordML").val(extra[0].id);
				}
				else 
				{
					$("#recordML").val("0");
				}

				identifiers=[/^IS/];
				extra=_this.getExtraList(identifiers,node.id);					
				if(extra.length>0)
				{
					
					$("#recordIS").val(extra[0].id);
				}
				else 
				{
					$("#recordIS").val("0");
				}

				identifiers=[/^BP/];
				extra=_this.getExtraList(identifiers,node.id);					
				if(extra.length>0)
				{
					
					$("#recordBP").val(extra[0].id);
				}
				else 
				{
					$("#recordBP").val("0");
				}

				identifiers=[/^PB/];
				extra=_this.getExtraList(identifiers,node.id);					
				if(extra.length>0)
				{
					
					$("#recordPB").val(extra[0].id);
				}
				else 
				{
					$("#recordPB").val("0");
				}
                identifiers=[/^FILENAME/];
				extra=_this.getExtraList(identifiers,node.id);					
				if(extra.length>0)
				{
					
					$("#recordFileName").val(extra[0].id);
				}
				else 
				{
					$("#recordFileName").val("N/A");
				}


				
				$(_this.recordModalID).modal('show');
			}	
				
	}
	_this.editTransfer=function(node)
	{
		   

		   $("#transferID").val('');
		    $("#transferText").val('');
		    $("#transferBargin").val('0');
			
			var data=_this.designer.tree._model.data;
			
			_this.populateGotoList("transferA",node);
			_this.populateGotoList("transferB",node);
			_this.populateGotoList("transferN",node);
			_this.populateGotoList("transferC",node);
			
				
			
			if(data!=null)
			{
				 data=data[node.id];

				$("#transferID").val(data.id);
				$("#transferText").val(data.data.PromptText);
	
				var identifiers=[/^A/];
				var flow=_this.getFlowList(identifiers,node.id);	
				if(flow.length>0)
				{
					
					$("#transferA").val(flow[0].NextANSID);
				}
				else 
				{
					$("#transferA").val("0");
				}
				
				identifiers=[/^B/];
				flow=_this.getFlowList(identifiers,node.id);	
				if(flow.length>0)
				{
					
					$("#transferB").val(flow[0].NextANSID);
				}
				else 
				{
					$("#transferB").val("0");
				}
				
				identifiers=[/^\*/];
				flow=_this.getFlowList(identifiers,node.id);	
				if(flow.length>0)
				{
					
					$("#transferC").val(flow[0].NextANSID);
				}
				else 
				{
					$("#transferC").val("0");
				}

				identifiers=[/^N/];
				flow=_this.getFlowList(identifiers,node.id);	
				if(flow.length>0)
				{
					
					$("#transferN").val(flow[0].NextANSID);
				}
				else 
				{
					$("#transferN").val("0");
				}
				
				identifiers=[/^XT/];
				var extra=_this.getExtraList(identifiers,node.id);	

				if(extra.length>0)
				{
					
					$("#transferXT").val(extra[0].id);
				}
				else 
				{
					$("#transferXT").val("TFN");
				}	

				identifiers=[/^XD/];
				extra=_this.getExtraList(identifiers,node.id);					
				if(extra.length>0)
				{
					
					
					var l=extra[0].id.length;
					if(l==4)
					{
						$("#transferXT").change();
						$("#transferXT").val("CMD");
						
					}	
					$("#transferXD").val(extra[0].id);

				}
				else 
				{
					$("#transferXD").val("");
				}


				identifiers=[/^BR/];
				extra=_this.getExtraList(identifiers,node.id);					
				if(extra.length>0)
				{
					
					$("#transferBargin").val(extra[0].id);
				}
				else 
				{
					$("#transferBargin").val("FALSE");
				}

				//$("#transferXT").trigger("change");
				
				$(_this.transferModalID).modal('show');
			}	
				
	}
	_this.editVariable=function(node)
	{
		   

		    $("#variableID").val('');
		    $("#variableText").val('');

			
			var data=_this.designer.tree._model.data;
		
			
				
			
			if(data!=null)
			{
				 data=data[node.id];

				$("#variableID").val(data.id);
				$("#variableText").val(data.data.PromptText);
	
				if(data.data.extra.length>0)
				{

					$("#variableValue").val(data.data.extra[0].VALUE);
				}
				else 
				{
					$("#variableValue").val(0);
				}
		
				$(_this.variableModalID).modal('show');
			}	
				
	}		
	_this.editPrompt=function(node)
	{
		   

		    $("#promptID").val('');
		    $("#promptText").val('');
		    $("#promptBargin").val('0');
			
			var data=_this.designer.tree._model.data;
			_this.populateGotoList("promptGoto",node);
			
				
			
			if(data!=null)
			{
				 data=data[node.id];

				$("#promptID").val(data.id);
				$("#promptText").val(data.data.PromptText);

				if(data.data.extra.length>0)
				{
					
					$("#promptBargin").val(data.data.extra[0].VALUE);
				}
				else 
				{
					$("#promptBargin").val("FALSE");
				}
				if(data.data.flow.length>0)
				{				
					$("#promptGoto").val(data.data.flow[0].NextANSID);
				}
				else 
				{
					$("#promptGoto").val(0);
				}
				$(_this.promptModalID).modal('show');
			}	
				
	}
	
	_this.editTerminator=function(node)
	{
		   

		    $("#terminatorpromptID").val('');
		    $("#terminatorpromptText").val('');
		    $("#terminatorpromptBargin").val('0');
			
			var data=_this.designer.tree._model.data;
			_this.populateGotoList("terminatorpromptGoto",node);
			
				
			
			if(data!=null)
			{
				 data=data[node.id];

				$("#terminatorpromptID").val(data.id);
				$("#terminatorpromptText").val(data.data.PromptText);

				if(data.data.extra.length>0)
				{
					
					$("#terminatorpromptBargin").val(data.data.extra[0].VALUE);
				}
				else 
				{
					$("#terminatorpromptBargin").val("FALSE");
				}
				if(data.data.flow.length>0)
				{				
					$("#terminatorpromptGoto").val(data.data.flow[0].NextANSID);
				}
				else 
				{
					$("#terminatorpromptGoto").val(0);
				}
				$(_this.terminatorModalID).modal('show');
			}	
				
	}
	_this.editTts=function(node)
	{
		   

		    $("#ttsID").val('');
		    $("#ttsText").val('');

			
			var data=_this.designer.tree._model.data;
			_this.populateGotoList("ttsGoto",node);
			
			//var list=_this.getOperandList();
			var list=getOperandList();

			var option="<option value='0'>Select Variable/Command</option>";

			$("#ttsVariable").html(option);

			$.each(list, function() {

				var text=this.id+':'+this.text;

				$("#ttsVariable").append($("<option />").val(this.id).text(text));


			});									
			
			if(data!=null)
			{
				 data=data[node.id];
				
				$("#ttsID").val(data.id);
				$("#ttsText").val(data.data.PromptText);

				var identifiers=[/^VR/];
				var extra=_this.getExtraList(identifiers,node.id);	

				if(extra.length>0)
				{
					
					$("#ttsVariable").val(extra[0].id);
				}
				else 
				{
					$("#ttsVariable").val('');
				}
				
				var identifiers=[/^GRR/];
				var extra=_this.getExtraList(identifiers,node.id);	

				if(extra.length>0)
				{
					
					$("#ttsType").val(extra[0].id);
				}
				else 
				{
					$("#ttsType").val('DIGITS');
				}
				
				if(data.data.flow.length>0)
				{				
					$("#ttsGoto").val(data.data.flow[0].NextANSID);
				}
				else 
				{
					$("#ttsGoto").val(0);
				}
				$(_this.ttsModalID).modal('show');
			}	
				
	}	
	_this.editLanguageKeypress=function(node)
	{
		   $("#languageKeypressID").val('');
		   $("#languageKeypress").val('');

			
			var data=_this.designer.tree._model.data;
			
		
			
			if(data!=null)
			{
				 data=data[node.id];
			
				
				if(data.data.flow.length>0)
				{		
					
					_this.populateLanguageKeypress(node,data.data.flow[0].NextANSID);
					$("#languageKeypress").val(data.data.flow[0].Value);										
					$("#languageKeypressID").val(data.id);	
					$("#languageKeypressList").val(data.data.flow[0].NextANSID);
				}
				else 
				{
					$("#languageKeypressList").val('1');
				}
	
				$(_this.languageKeyPressModalID).modal('show');
			}

			
	}	
	_this.editKeypress=function(node)
	{
		   $("#keypressID").val('');
		   $("#keypress").val('');

			
			var data=_this.designer.tree._model.data;
			
			_this.populateGotoList("keypressGoto",node);
			if(data!=null)
			{
				 data=data[node.id];
			
				
				if(data.data.flow.length>0)
				{		
					
					_this.populateKeypress(node,data.data.flow[0].Value);
					$("#keypress").val(data.data.flow[0].Value);										
					$("#keypressID").val(data.id);	
					$("#keypressGoto").val(data.data.flow[0].NextANSID);
				}
				else 
				{
					$("#keypressGoto").val(0);
				}
	
				$(_this.keypressModalID).modal('show');
			}

			
	}
     _this.editConfirmationKeypress=function(node)
	{
		   $("#confirmationKeypressID").val('');
		   $("#confirmationKeypress").val('');

			
			var data=_this.designer.tree._model.data;

			_this.populateGotoList("confirmationKeypressGoto",node);
			if(data!=null)
			{
				data=data[node.id];				
				if(data.data.flow.length>0)
				{						
					$("#confirmationKeypress").val(data.data.flow[0].Value);										
					$("#confirmationKeypressID").val(data.id);	
					$("#confirmationKeypressGoto").val(data.data.flow[0].NextANSID);
				}	
				$(_this.confirmationKeypressModalID).modal('show');
			}

			
	}
	_this.editMenu=function(node)
	{
		    $("#myUploadExtraForm").hide();
		    $("#menuID").val('');
		    $("#menuText").val('');
		    $("#menuTimeOut").val('3000');
			
			var data=_this.designer.tree._model.data;
			_this.populateGotoList("invalidGotoList",node);
			_this.populateGotoList("noResponseGotoList",node);
			_this.populateResponseList('invalidPromptList');
			_this.populateResponseList('noResponsePromptList');
				
			$(".invalid-response-rows").remove();
			$(".no-response-rows").remove();
			if(data!=null)
			{
				 data=data[node.id];

				$("#menuID").val(data.id);
				$("#menuText").val(data.data.PromptText);
				var identifiers=[/^INV/];
				var invalidList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< invalidList.length; i++)
				{
						_this.populateInvalidResponseRow(invalidList[i]);
				}
				identifiers=[/^NR/];
				var noResponseList=_this.getExtraList(identifiers,node.id);
			
				for(var i=0; i< noResponseList.length; i++)
				{
						_this.populateNoResponseRow(noResponseList[i]);
				}
				identifiers=[/^TO/];
				var toList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< toList.length; i++)
				{
						$("#menuTimeOut").val(toList[i].id);
				}
				identifiers=[/^ASR/];
				var asrList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< asrList.length; i++)
				{
						$("#menuASR").val(asrList[i].id);
				}

				identifiers=[/^GR/];
				var grList=_this.getExtraList(identifiers,node.id);			
				for(var i=0; i< grList.length; i++)
				{
						$("#menuGrammer").val(grList[i].id);
				}

                identifiers=[/^TRACKINV/];
				var tiList=_this.getExtraList(identifiers,node.id);			
				for(var i=0; i< tiList.length; i++)
				{
						if(tiList[i].id=="TRUE")
						{	
							$("#trackINV").prop('checked', true);
						}
						else 
						{
							$("#trackINV").prop('checked', false);	
						}	
				}

               
                identifiers=[/^TRACKNR/];
				var tnList=_this.getExtraList(identifiers,node.id);			
				for(var i=0; i< tnList.length; i++)
				{
						if(tnList[i].id=="TRUE")
						{	
							$("#trackNR").prop('checked', true);
						}
						else
						{
     						$("#trackNR").prop('checked', false);
						}	
				}   


				$(_this.menuModalID).modal('show');
			}
			
						
		
	}
	_this.editInput=function(node)
	{
		    $("#myInputUploadExtraForm").hide();   
		    $("#inputID").val('');
		    $("#inputText").val('');
		    $("#inputTimeOut").val('3000');
			
			var data=_this.designer.tree._model.data;
			_this.populateGotoList("inputKeypressGoto",node);
			//_this.populateGotoList("inputInvalidGotoList",node);
			_this.populateGotoList("inputNoResponseGotoList",node);
			//_this.populateResponseList('inputInvalidPromptList');
			_this.populateResponseList('inputNoResponsePromptList');
				
			//$(".input-invalid-response-rows").remove();
			$(".input-no-response-rows").remove();
			if(data!=null)
			{
				 data=data[node.id];

				$("#inputID").val(data.id);
				$("#inputText").val(data.data.PromptText);
				//var identifiers=[/^INV/];
				//var invalidList=_this.getExtraList(identifiers,node.id);
				
				//for(var i=0; i< invalidList.length; i++)
				//{
			//			_this.populateInputInvalidResponseRow(invalidList[i]);
			//	}
				identifiers=[/^NR/];
				var noResponseList=_this.getExtraList(identifiers,node.id);
			
				for(var i=0; i< noResponseList.length; i++)
				{
						_this.populateInputNoResponseRow(noResponseList[i]);
				}
				identifiers=[/^TO/];
				var toList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< toList.length; i++)
				{
						$("#inputTimeOut").val(toList[i].id);

				}
                 
                identifiers=[/^LN/];
				var toList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< toList.length; i++)
				{
						$("#inputLength").val(toList[i].id);
				}

				identifiers=[/^ASR/];
				var asrList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< asrList.length; i++)
				{
						$("#inputASR").val(asrList[i].id);
				}

				identifiers=[/^GR/];
				var grList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< grList.length; i++)
				{
						$("#inputGrammer").val(grList[i].id);
				}
				if(data.data.flow.length>0)
				{				
					$("#inputKeypressGoto").val(data.data.flow[0].NextANSID);
				}
				else 
				{
					$("#inputKeypressGoto").val(0);
				}				

				$(_this.inputModalID).modal('show');
			}
			
						
		
	}
	_this.editLegal=function(node)
	{
		
		    $("#legalID").val('');
		    $("#legalText").val('');
		    $("#legalTimeOut").val('3000');
		    $("#legalLength").val('5');
			
			var data=_this.designer.tree._model.data;
			_this.populateGotoList("legalKeypressGoto",node);
			_this.populateGotoList("legalInvalidGotoList",node);
			_this.populateGotoList("legalNoResponseGotoList",node);
			_this.populateResponseList('legalInvalidPromptList');
			_this.populateResponseList('legalNoResponsePromptList');
				
			$(".legal-invalid-response-rows").remove();
			$(".legal-no-response-rows").remove();
		
			if(data!=null)
			{
				 data=data[node.id];

				$("#legalID").val(data.id);
				$("#legalText").val(data.data.PromptText);
				var identifiers=[/^INV/];
		
				var invalidList=_this.getExtraList(identifiers,node.id);
	
				for(var i=0; i< invalidList.length; i++)
				{
						_this.populateLegalInvalidResponseRow(invalidList[i]);
				}
				identifiers=[/^NR/];
				var noResponseList=_this.getExtraList(identifiers,node.id);
			
				for(var i=0; i< noResponseList.length; i++)
				{
						_this.populateLegalNoResponseRow(noResponseList[i]);
				}
				identifiers=[/^TO/];
				var toList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< toList.length; i++)
				{
						$("#legalTimeOut").val(toList[i].id);

				}
                 
                identifiers=[/^LN/];
				var toList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< toList.length; i++)
				{
						$("#legalLength").val(toList[i].id);
				}

				identifiers=[/^ASR/];
				var asrList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< asrList.length; i++)
				{
						$("#legalASR").val(asrList[i].id);
				}

				identifiers=[/^GR/];
				var grList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< grList.length; i++)
				{
						$("#legalGrammer").val(grList[i].id);
				}

				identifiers=[/^APPTYPE/];
				var appType=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< appType.length; i++)
				{
					   if(appType[i].id=="Custom"){
							$("#appType").val("C");
					   }
					   else
					   {
					   	    $("#appType").val("S");
					   }	
				}
                 
                identifiers=[/^APPMODE/];
				var appMode=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< appMode.length; i++)
				{
					   if(appMode[i].id=="State"){
							$("#appMode").val("S");
					   }
					   else if(appMode[i].id=="Governor")
					   {
					   	    $("#appMode").val("G");
					   }
					   else{
					   	    $("#appMode").val("F"); 
					   }	
				} 

				identifiers=[/^APPTEL/];
				var appTEL=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< appTEL.length; i++)
				{
					   if(appTEL[i].id=="Capital"){
							$("#appTel").val("C");
					   }
					   else{
					   	    $("#appTel").val("D"); 
					   }	
				} 

				identifiers=[/^MODE/];
				var mode=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< mode.length; i++)
				{
					   if(mode[i].id=="Senator"){
							$("#mode").val("S");
					   }
					   else if(mode[i].id=="Representative"){
					   	    $("#mode").val("R"); 
					   }
					   else{
					   	   $("#mode").val("B");
					   }	
				} 

				identifiers=[/^LISTORDER/];
				var lo=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< lo.length; i++)
				{
					   if(lo[i].id=="Senator"){
							$("#listOrder").val("S");
					   }
					   else{
					   	   $("#listOrder").val("R");
					   }	
				} 

				identifiers=[/^APPSTATE/];
				var appState=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< appState.length; i++)
				{
					   if(appState[i].id=="N/A"){
							$("#appState").val("FD");
					   }
					   	
				} 


				identifiers=[/^CUSTOMSOURCE/];
				var cs=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< cs.length; i++)
				{

					   if(cs[i].id=="N/A"){
							$("#customText").val("");
							$("#customText").attr('disabled', true);
					   }
					   else{
					   	    $("#customText").val(cs[i].id);
					   }
					   	
				} 
                 
                identifiers=[/^PLAYBACK/];
				var pb=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< pb.length; i++)
				{

					   if(pb[i].id=="YES"){
							 $('#zipPlayBack').prop('checked', true); 
					   }
					   else{
					   	     $('#zipPlayBack').prop('checked', false); 
					   }
					   	
				} 

				identifiers=[/^PLAYPRIORLIST/];
				var ppl=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< ppl.length; i++)
				{
					   if(ppl[i].id=="filename"){
							$("#PLAYPRIORLIST").val("filename");
							$("#legalPlayPriorListBtn").removeClass();
            				$("#legalPlayPriorListBtn").addClass("ivr-upload btn btn-xs actionbuttons");
					   }
					   else
					   {
					   	    $("#PLAYPRIORLIST").val(ppl[i].id);
					   	    $("#legalPlayPriorListBtn").removeClass();
            				$("#legalPlayPriorListBtn").addClass("ivr-uploaded btn btn-xs actionbuttons");
					   }	
					   	
				} 
				identifiers=[/^PLAYPRIORTRANSFER/];
				var ppt=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< ppt.length; i++)
				{
					   if(ppt[i].id=="filename"){
							$("#PLAYPRIORTRANSFER").val("filename");
							$("#legalPlayPriorTransferBtn").removeClass();
            				$("#legalPlayPriorTransferBtn").addClass("ivr-upload btn btn-xs actionbuttons"); 
					   }
					   else
					   {
					   	    $("#PLAYPRIORTRANSFER").val(ppt[i].id);
					   	    $("#legalPlayPriorTransferBtn").removeClass();
            				$("#legalPlayPriorTransferBtn").addClass("ivr-uploaded btn btn-xs actionbuttons"); 
					   }	
					   	
				}
                appTypeChange();
                appModeChange();
                modeChange();
				
                
                
                 
			
				$(_this.legalModalID).modal('show');
			}
			
						
		
	}	
     _this.editConfirmation=function(node)
	{
		    $("#myConfirmationUploadExtraForm").hide();
		    $("#confirmationID").val('');
		    $("#confirmationText").val('');
		    $("#confirmationTimeOut").val('');
			
			var data=_this.designer.tree._model.data;
			_this.populateGotoList("confirmationInvalidGotoList",node);
			_this.populateGotoList("confirmationNoResponseGotoList",node);
			_this.populateResponseList('confirmationInvalidPromptList');
			_this.populateResponseList('confirmationNoResponsePromptList');
				 
			$(".confirmation-invalid-response-rows").remove();
			$(".confirmation-no-response-rows").remove(); 
			if(data!=null)
			{
				data=data[node.id];

				$("#confirmationID").val(data.id);
                               
				$("#confirmationText").val(data.data.PromptText);
                               
				var identifiers=[/^INV/];
				var invalidList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< invalidList.length; i++)
				{
	                _this.populateConfirmationInvalidResponseRow(invalidList[i]);
				}
				identifiers=[/^NR/];
				var noResponseList=_this.getExtraList(identifiers,node.id);
			
				for(var i=0; i< noResponseList.length; i++)
				{
						_this.populateConfirmationNoResponseRow(noResponseList[i]);
				}
				identifiers=[/^TO/];
				var toList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< toList.length; i++)
				{
				   $("#confirmationTimeOut").val(toList[i].id);
				
			    }	
				
				identifiers=[/^ASR/];
				var asrList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< asrList.length; i++)
				{
						$("#confirmationASR").val(asrList[i].id);
				}

				identifiers=[/^GR/];
				var grList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< grList.length; i++)
				{
						$("#confirmationGrammer").val(grList[i].id);
				}					
            }				
                                  
				$(_this.confirmationModalID).modal('show');
	}

     _this.editLanguage=function(node)
	{
		    $("#myLanguageUploadExtraForm").hide(); 
		    $("#languageID").val('');
		    $("#languageText").val('');
		    $("#languageTimeOut").val('3000');
			
			var data=_this.designer.tree._model.data;
			_this.populateGotoList("languageGoto",node);
			_this.populateGotoList("languageInvalidGotoList",node);
			_this.populateGotoList("languageNoResponseGotoList",node);
			_this.populateResponseList('languageInvalidPromptList');
			_this.populateResponseList('languageNoResponsePromptList');
				 
			$(".language-invalid-response-rows").remove();
			$(".language-no-response-rows").remove(); 
			if(data!=null)
			{
				data=data[node.id];

				$("#languageID").val(data.id);
                               
				$("#languageText").val(data.data.PromptText);
                               
				var identifiers=[/^INV/];
				var invalidList=_this.getExtraList(identifiers,node.id);

				for(var i=0; i< invalidList.length; i++)
				{
	                _this.populateLanguageInvalidResponseRow(invalidList[i]);
				}
				identifiers=[/^NR/];
				var noResponseList=_this.getExtraList(identifiers,node.id);
			
				for(var i=0; i< noResponseList.length; i++)
				{
						_this.populateLanguageNoResponseRow(noResponseList[i]);
				}
				identifiers=[/^TO/];
				var toList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< toList.length; i++)
				{
				$("#languageTimeOut").val(toList[i].id);
				
			    }	
				
				identifiers=[/^ASR/];
				var asrList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< asrList.length; i++)
				{
						$("#languageASR").val(asrList[i].id);
				}

				identifiers=[/^GR/];
				var grList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< grList.length; i++)
				{
						$("#languageGrammer").val(grList[i].id);
				}

				if(data.data.flow.length>0)
				{				
					$("#languageGoto").val(data.data.flow[0].NextANSID);
				}
				else 
				{
					$("#languageGoto").val(0);
				}					
            }				
                                  
				$(_this.languageModalID).modal('show');
	}
			        
					
	_this.getFlowList=function(identifiers,key)
	{
		var data=_this.designer.tree._model.data;	

		if(key!=null)
		{
			data={key:data[key]};
		}
		var list=[];
		var split='';
		for( var nodeID in data)
		{
			if(node=="#")
				continue;
			
			var node=data[nodeID];
			if(node.type=="menu" || node.type=="record" || node.type=="transfer" )
			{
				var flow=node.data.flow;
				
				for(var i=0; i<flow.length; i++)
				{
					split=flow[i].Value.split('|');
					if(split.length<=2)
					{
						split.push(0);
					}
					if(identifiers==null)
					{
							
							list.push({NextANSID:flow[i].NextANSID,Value:split[0]});
					}
					
					else if(identifiers!=null)
					{
							identifiers.some(
										function(regx)
											{ 
												if(regx.test(flow[i].Value))
												{
													list.push({NextANSID:flow[i].NextANSID,Value:split[0]});
												}
											}
										);
							
							
					}
				}
		
			}
		}
		return list;
	}		
	_this.getExtraList=function(identifiers,key)
	{
		var data=_this.designer.tree._model.data;	

		if(key!=null)
		{
			var obj={}
			obj[key]=data[key];
			data=obj;
		
			
		}
		var list=[];
		var split='';
		for( var nodeID in data)
		{
			if(nodeID=="#")
				continue;
			
			var node=data[nodeID];
			if(node.type=="menu" || node.type=="record" || node.type=="transfer" || node.type=="confirmation" || node.type=="input" || node.type=="function" || node.type=="if" || node.type=="legal" || node.type=="language" || node.type=="tts" || node.type=="setvalue")
			{
				var extra=node.data.extra;
				
				for(var i=0; i<extra.length; i++)
				{
					split=extra[i].VALUE.split('|');
			        
					for(var j=split.length; j<5; j++)
					{
						split.push(0);
					}
				    if(split[4]== 0){
						split[4]="TRUE";
						
				    }    
					
					if(identifiers==null)
					{
							
						list.push({identifier:extra[i].IDENTIFIER,id:split[0],text:split[1],action:split[2],file:split[3],playPrompt:split[4]});
					}
					
					else if(identifiers!=null)
					{
							identifiers.some(
										function(regx)
											{ 
												if(regx.test(extra[i].IDENTIFIER))
												{
							                        
							                        	
													list.push({identifier:extra[i].IDENTIFIER,id:split[0],text:split[1],action:split[2],file:split[3],playPrompt:split[4]});
							                        
												    
												}
											}
										);
							
							
					}
				}
		
			}
		}

		return list;
	}	
	_this.getNextResponseID=function()
	{
		
			var identifiers=[/^INV/,/^NR/];
			var list=_this.getExtraList(identifiers,null);	
			var max=_this.designer.responseID;
			$.each(list, function() {
				
				if( parseInt(this.id) > max)
				{
					max=this.id
				}
			});	
			max++;
			_this.designer.responseID=max;
			return max; 
	}
	_this.populateResponseList=function(id)
	{
		    var id="#"+id;
			var options = $(id);	
			var identifiers=[/^INV/,/^NR/];
			var list=_this.getExtraList(identifiers,null);
			var existing=[];
			options.html('<option value="0">Create New</option>');
			
			$.each(list, function() {
				if(existing.indexOf(this.id)==-1)
				{	
				var text=this.id+':'+this.text;
					existing.push(id);
					options.append($("<option />").val(this.id).text(text));
				}
			});	
					
	}

	_this.populateKeypress=function(node,current)
	{
		var data=_this.designer.tree._model.data[node.id];
		var parent=_this.designer.tree._model.data[data.parent];
		var childrens=parent.children;
		var existingKeypress=[];
		if(childrens.length>0)
		{
			for(var i=0; i<childrens.length; i++)
			{
				var nodeID=childrens[i];
				var keypressData=_this.designer.tree._model.data[nodeID].data;
				var flow=keypressData.flow[0];
				existingKeypress.push(flow.Value);
			}
		}
		var id="#keypress";
		var options = $(id);			
		options.html('');
		var key="";
		for(var v=0; v<=11; v++)
		{
			key=v;
			if(key==10)
			{	
				key="*";
			}
			if(key==11)
			{
                key="#";
			}	
			if(existingKeypress.indexOf(key.toString())==-1 || key==current)
			{
			 var text=key+": Keypress";
			 options.append($("<option />").val(key).text(text));
			}
		}


	}
    _this.saveLanguageKeypress=function()
	{
		
				
		var id=$("#languageKeypressID").val();
		if(!_this.designer.nodeArray.includes(id))
        {
        	_this.designer.nodeArray.push(id);		
        }
		if(id=='')
			return;
		var data=_this.designer.tree._model.data[id];
	
		var flow=new Flow();
		flow.NextANSID=$("#languageKeypressList").val();
		flow.Value=$("#languageKeypress").val();

		data.text=flow.Value+" :Keypress";
		data.data.flow=[];
		data.data.flow.push(flow);
		
		_this.designer.tree._model.data[id]=data;

		var nodeID="#"+id;
	
	
		_this.designer.tree.redraw_node(nodeID);
	
		$(_this.languageKeyPressModalID).modal('hide');	
	}	
    _this.saveKeypress=function()
	{
		
				
		var id=$("#keypressID").val();
		if(!_this.designer.nodeArray.includes(id))
        {
        	_this.designer.nodeArray.push(id);		
        }
		if(id=='')
			return;
		var data=_this.designer.tree._model.data[id];
	
		var flow=new Flow();
		flow.NextANSID=$("#keypressGoto").val();
		flow.Value=$("#keypress").val();
		data.text=$("#keypress").find('option:selected').text();
		data.data.flow=[];
		data.data.flow.push(flow);
		
		_this.designer.tree._model.data[id]=data;

		var nodeID="#"+id;
	
	
		_this.designer.tree.redraw_node(nodeID);
	
		$(_this.keypressModalID).modal('hide');	
	}
     _this.saveConfirmationKeypress=function()
	{
		
				
		var id=$("#confirmationKeypressID").val();
		if(id=='')
			return;
		if(!_this.designer.nodeArray.includes(id))
        {
        	_this.designer.nodeArray.push(id);		
        }	
		var data=_this.designer.tree._model.data[id];
	
		var flow=new Flow();
		flow.NextANSID=$("#confirmationKeypressGoto").val();
		flow.Value=$("#confirmationKeypress").val();
		data.text=flow.Value+" :Keypress";
		data.data.flow=[];
		data.data.flow.push(flow);
		
		_this.designer.tree._model.data[id]=data;

		var nodeID="#"+id;
	
	
		_this.designer.tree.redraw_node(nodeID);
		$(_this.confirmationKeypressModalID).modal('hide');	
	}
	_this.saveURI=function()
	{
       if(!$("#uriForm").valid())
		{
			return false;
		}
            
		// if($(".no-response-rows").length==0 || $(".invalid-response-rows").length==0)
		// {
			// alert('Please define atleast one invalid and one no response');
			// return;
		// }
		
		var id=$("#uriID").val();
         _this.set_id(id);
		if(!_this.designer.nodeArray.includes(id))
        {
        	_this.designer.nodeArray.push(id);		
        }		
		var data=_this.designer.tree._model.data[id];
		data.text=id+":"+$("#uriText").val();		
		data.data.PromptText=$("#uriText").val();
		
		data.data.extra=[];
		data.data.flow=[];
		data.data.web=[];

		var flow=new Flow();
		flow.NextANSID=$("#uriGoto").val();

		if($("#continueHangup").is(':checked')){                               
          flow.Value="-2";

        }

		data.data.flow.push(flow);
		
        

		var counter=1;
		var rowIndex=3;
		
		$(".output-param-rows").each(
							function()
							{
						
								var row=$(this);
								var param=new Parameter();
								
								param.Name=row.find("#outputParamName").val();
								param.Type="OUT";
								param.Action=row.attr("data-param-type");
								param.Value=row.find("#outputParamValue").val();
								data.data.web.push(param);

							}	);	


		$(".input-param-rows").each(
					function()
					{
				
						var row=$(this);
						var param=new Parameter();
						
						param.Name=row.find("#inputParamName").val();
						param.Type="IN";
						param.Action=row.attr("data-param-type");
						param.Value=row.find("#inputParamValue").val();
						data.data.web.push(param);

					});
					
                    
				    var param=new Parameter();
						
					param.Name=$(".status-param-rows").find("#statusParamName").val();
					param.Type="Status";
					param.Action=$(".status-param-rows").attr("data-param-type");
					param.Value=$(".status-param-rows").find("#statusParamValue").val();
					data.data.web.push(param); 


		_this.designer.tree._model.data[id]=data;

		var nodeID="#"+id;
	
	
		_this.designer.tree.redraw_node(nodeID);
	
		$(_this.uriModalID).modal('hide');	
		
	}

	_this.saveMenu=function()
	{
       if(!$("#menuForm").valid())
		{
			return false;
		}
  
		if($(".no-response-rows").length==0 || $(".invalid-response-rows").length==0)
		{
			alert('Please define atleast one invalid and one no response');
			return;
		}
		
		var id=$("#menuID").val();
         _this.set_id(id); 
        if(!_this.designer.nodeArray.includes(id))
        {
        	_this.designer.nodeArray.push(id);		
        }  		
		var data=_this.designer.tree._model.data[id];
		data.text=id+":"+$("#menuText").val();		
		data.data.PromptText=$("#menuText").val();

		var extra=new Extra();		
		data.data.extra=[];
		extra.IDENTIFIER="TO";
		extra.VALUE=$("#menuTimeOut").val();
        data.data.extra.push(extra);

		var asr=new Extra();	
		asr.IDENTIFIER="ASR";
		asr.VALUE=$("#menuASR").val();	
		data.data.extra.push(asr);

		var gr=new Extra();	
		gr.IDENTIFIER="GR";
		gr.VALUE=$("#menuGrammer").val();			
		data.data.extra.push(gr);
		
		if( $("#menuGrammer").val() == "Cust")
		{		
			var cust=new Extra();	
			cust.IDENTIFIER="GT";
			cust.VALUE=$("#grammerText").val();			
			data.data.extra.push(cust);
		} 		

		var trackINV=new Extra();
		trackINV.IDENTIFIER="TRACKINV";
		if($("#trackINV").is(':checked')){                               
          trackINV.VALUE="TRUE";
        }
        else
        {
          trackINV.VALUE="FALSE";	
        }	
		data.data.extra.push(trackINV);

		var trackNR=new Extra();
		trackNR.IDENTIFIER="TRACKNR";
		if($("#trackNR").is(':checked')){                               
          trackNR.VALUE="TRUE";
        }
        else
        {
          trackNR.VALUE="FALSE";	
        }	
		data.data.extra.push(trackNR);

		
		var counter=1;
		var rowIndex=3;
		$(".no-response-rows").each(
									function()
									{

										var row=$(this);
								        extra=new Extra();
										extra.IDENTIFIER="NR"+counter;
										var playPrompt="FALSE";
										if(row.find("#playPrompt").prop("checked")){
                  							playPrompt="TRUE";
										}
										extra.VALUE=row.find("#noResponsePromptID").val()+"|"+row.find("#noResponsePromptText").val()+"|"+row.find('input[name="noResponseAction['+rowIndex+']"]:checked').val()+"|"+row.find("#extraFilename").val()+"|"+playPrompt;								
										data.data.extra.push(extra);
										counter++;
										rowIndex++;
									}
								  );

		counter=1;
		rowIndex=3;
		$(".invalid-response-rows").each(
									function()
									{
										var row=$(this);
								        extra=new Extra();
										extra.IDENTIFIER="INV"+counter;
										var playPrompt="FALSE";
										if(row.find("#playPrompt").prop("checked")){
                  							playPrompt="TRUE";
										}
										extra.VALUE=row.find("#invalidPromptID").val()+"|"+row.find("#invalidPromptText").val()+"|"+row.find('input[name="invalidAction['+rowIndex+']"]:checked').val()+"|"+row.find("#extraFilename").val()+"|"+playPrompt;								
										data.data.extra.push(extra);
										counter++;
										rowIndex++
									}
								  );
								  
		_this.designer.tree._model.data[id]=data;

		var nodeID="#"+id;
	
	
		_this.designer.tree.redraw_node(nodeID);
	
		$(_this.menuModalID).modal('hide');		
	}

	_this.saveConfirmation=function()
	{
        if(!$("#confirmationForm").valid())
		{
			return false;
		}
     
		if($(".confirmation-no-response-rows").length==0 || $(".confirmation-invalid-response-rows").length==0)
		{
			alert('Please define atleast one invalid and one no response');
			return;
		}
		
		var id=$("#confirmationID").val();
         _this.set_id(id); 
        if(!_this.designer.nodeArray.includes(id))
        {
        	_this.designer.nodeArray.push(id);		
        }		 
		var data=_this.designer.tree._model.data[id];
		data.text=id+":"+$("#confirmationText").val();		
		data.data.PromptText=$("#confirmationText").val();		
		var extra=new Extra();		
		data.data.extra=[];
		extra.IDENTIFIER="TO";
		extra.VALUE=$("#confirmationTimeOut").val();
		
		var asr=new Extra();	
		asr.IDENTIFIER="ASR";
		asr.VALUE=$("#confirmationASR").val();	
		data.data.extra.push(asr);

		var gr=new Extra();	
		gr.IDENTIFIER="GR";
		gr.VALUE=$("#confirmationGrammer").val();			
		data.data.extra.push(gr);
		
		data.data.extra.push(extra);
		var counter=1;
		var rowIndex=3;
		$(".confirmation-no-response-rows").each(
									function()
									{

										var row=$(this);
								        extra=new Extra();
										extra.IDENTIFIER="NR"+counter;
										extra.VALUE=row.find("#confirmationNoResponsePromptID").val()+"|"+row.find("#confirmationNoResponsePromptText").val()+"|"+row.find('input[name="confirmationNoResponseAction['+rowIndex+']"]:checked').val()+"|"+row.find("#extraFilename").val();
																	
										data.data.extra.push(extra);
										counter++;
										rowIndex++;
									}
								  );
       
		counter=1;
		rowIndex=3;
		$(".confirmation-invalid-response-rows").each(
									function()
									{
										var row=$(this);
								        extra=new Extra();
										extra.IDENTIFIER="INV"+counter;
										extra.VALUE=row.find("#confirmationInvalidPromptID").val()+"|"+row.find("#confirmationInvalidPromptText").val()+"|"+row.find('input[name="confirmationInvalidAction['+rowIndex+']"]:checked').val()+"|"+row.find("#extraFilename").val();								
										
										data.data.extra.push(extra);
										counter++;
										rowIndex++
									}
								  );
								  
		_this.designer.tree._model.data[id]=data;

		var nodeID="#"+id;
	
	
		_this.designer.tree.redraw_node(nodeID);
	
		$(_this.confirmationModalID).modal('hide');		
	}

	_this.saveLanguage=function()
	{
        if(!$("#languageForm").valid())
		{
			return false;
		}
        
    

		if($(".language-no-response-rows").length==0 || $(".language-invalid-response-rows").length==0)
		{
			alert('Please define atleast one invalid and one no response');
			return;
		}
		
		var id=$("#languageID").val();
         _this.set_id(id);
		if(!_this.designer.nodeArray.includes(id))
        {
        	_this.designer.nodeArray.push(id);		
        }	
		var data=_this.designer.tree._model.data[id];
		data.text=id+":"+$("#languageText").val();		
		data.data.PromptText=$("#languageText").val();	

		var flow=new Flow();
		flow.NextANSID=$("#languageGoto").val();
		data.data.flow=[];
		data.data.flow.push(flow);	
		
		var extra=new Extra();		
		data.data.extra=[];
		extra.IDENTIFIER="TO";
		extra.VALUE=$("#languageTimeOut").val();
		
		var asr=new Extra();	
		asr.IDENTIFIER="ASR";
		asr.VALUE=$("#languageASR").val();	
		data.data.extra.push(asr);

		var gr=new Extra();	
		gr.IDENTIFIER="GR";
		gr.VALUE=$("#languageGrammer").val();			
		data.data.extra.push(gr);
		
		data.data.extra.push(extra);
		var counter=1;
		var rowIndex=3;
		$(".language-no-response-rows").each(
									function()
									{

										var row=$(this);
								        extra=new Extra();
										extra.IDENTIFIER="NR"+counter;
										extra.VALUE=row.find("#languageNoResponsePromptID").val()+"|"+row.find("#languageNoResponsePromptText").val()+"|"+row.find('input[name="languageNoResponseAction['+rowIndex+']"]:checked').val()+"|"+row.find("#extraFilename").val();
																	
										data.data.extra.push(extra);
										counter++;
										rowIndex++;
									}
								  );
       
		counter=1;
		rowIndex=3;
		$(".language-invalid-response-rows").each(
									function()
									{
										var row=$(this);
								        extra=new Extra();
										extra.IDENTIFIER="INV"+counter;
										extra.VALUE=row.find("#languageInvalidPromptID").val()+"|"+row.find("#languageInvalidPromptText").val()+"|"+row.find('input[name="languageInvalidAction['+rowIndex+']"]:checked').val()+"|"+row.find("#extraFilename").val();								
										
										data.data.extra.push(extra);
										counter++;
										rowIndex++
									}
								  );
								  
		_this.designer.tree._model.data[id]=data;

		var nodeID="#"+id;
	
	
		_this.designer.tree.redraw_node(nodeID);
	
		$(_this.languageModalID).modal('hide');		
	}
	
	_this.saveLegal=function()
	{
	
        if(!$("#legalForm").valid())
		{
			
			return false;
		}
   
		if($(".legal-no-response-rows").length==0 || $(".legal-invalid-response-rows").length==0)
		{
			alert('Please define atleast one invalid and one no response');
			return;
		}
		
		var id=$("#legalID").val();
         _this.set_id(id);
		if(!_this.designer.nodeArray.includes(id))
        {
        	_this.designer.nodeArray.push(id);		
        }	
		var data=_this.designer.tree._model.data[id];
		data.text=id+":"+$("#legalText").val();		
		data.data.PromptText=$("#legalText").val();

		 var flow=new Flow();
		flow.NextANSID=$("#legalKeypressGoto").val();
		data.data.flow=[];
		data.data.flow.push(flow);	

			
		data.data.extra=[];
		
		var extra=new Extra();	
		extra.IDENTIFIER="TO";
		extra.VALUE=$("#legalTimeOut").val();
		data.data.extra.push(extra);


        var extraLength=new Extra();
		extraLength.IDENTIFIER="LN";
		extraLength.VALUE=$("#legalLength").val();
		data.data.extra.push(extraLength);
		
		var asr=new Extra();	
		asr.IDENTIFIER="ASR";
		asr.VALUE=$("#legalASR").val();	
		data.data.extra.push(asr);

		var gr=new Extra();	
		gr.IDENTIFIER="GR";
		gr.VALUE=$("#legalGrammer").val();			
		data.data.extra.push(gr);
        
        var at=new Extra();	
		at.IDENTIFIER="APPTYPE";
		at.VALUE=$("#appType").val();
		if(at.VALUE=="C"){
			at.VALUE="Custom";
		}
		else
		{
            at.VALUE="Standard"; 
		}			
		data.data.extra.push(at);


		var m=new Extra();	
		m.IDENTIFIER="APPMODE";
		m.VALUE=$("#appMode").val();
		if(m.VALUE=="S"){
			m.VALUE="State";
		}
		else if(m.VALUE=="G")
		{
            m.VALUE="Governor"; 
		}
		else {
		    m.VALUE="Federal";				
		}			
		data.data.extra.push(m);

		var tel=new Extra();	
		tel.IDENTIFIER="APPTEL";
		tel.VALUE=$("#appTel").val();
		if(tel.VALUE=="C"){
			tel.VALUE="Capital";
		}
		else
		{
            tel.VALUE="District"; 
		}			
		data.data.extra.push(tel);


		var mode=new Extra();	
		mode.IDENTIFIER="MODE";
		mode.VALUE=$("#mode").val();
		if(mode.VALUE=="S"){
			mode.VALUE="Senator";
		}
		else if(mode.VALUE=="R")
		{
            mode.VALUE="Representative"; 
		}
		else
		{
		   	mode.VALUE="Both";	
		}			
		data.data.extra.push(mode);
        
        var lo=new Extra();	
		lo.IDENTIFIER="LISTORDER";
		lo.VALUE=$("#listOrder").val();
		if(lo.VALUE=="S"){
			lo.VALUE="Senator";
		}
		else 
		{
            lo.VALUE="Representative"; 
		}		
		data.data.extra.push(lo);

		var appState=new Extra();	
		appState.IDENTIFIER="APPSTATE";
		appState.VALUE=$("#appState").val();
		if(appState.VALUE=="FD"){
			appState.VALUE="N/A";
		}		
		data.data.extra.push(appState);

		var cs=new Extra();	
		cs.IDENTIFIER="CUSTOMSOURCE";
		cs.VALUE=$("#customText").val();
		if(cs.VALUE==""){
			cs.VALUE="N/A";
		}		
		data.data.extra.push(cs);
		

		var pb=new Extra();	
		pb.IDENTIFIER="PLAYBACK";
		pb.VALUE="NO";		
		if($("#zipPlayBack").is(':checked')){                               
          pb.VALUE="YES";

        }
        data.data.extra.push(pb);


        var pb=new Extra();	
		pb.IDENTIFIER="PLAYBACK";
		pb.VALUE="NO";		
		if($("#zipPlayBack").is(':checked')){                               
          pb.VALUE="YES";

        }
        data.data.extra.push(pb);


        var ppl=new Extra();	
		ppl.IDENTIFIER="PLAYPRIORLIST";
		ppl.VALUE=$("#PLAYPRIORLIST").val();		
        data.data.extra.push(ppl);

        var ppt=new Extra();	
		ppt.IDENTIFIER="PLAYPRIORTRANSFER";
		ppt.VALUE=$("#PLAYPRIORTRANSFER").val();		
        data.data.extra.push(ppt);

		var counter=1;
		var rowIndex=3;
		$(".legal-no-response-rows").each(
									function()
									{
										
										var row=$(this);
								        extra=new Extra();
										extra.IDENTIFIER="NR"+counter;
										extra.VALUE=row.find("#legalNoResponsePromptID").val()+"|"+row.find("#legalNoResponsePromptText").val()+"|"+row.find('input[name="legalNoResponseAction['+rowIndex+']"]:checked').val()+"|"+row.find("#extraFilename").val();																
						
										data.data.extra.push(extra);
										counter++;
										rowIndex++;
									}
								  );

		counter=1;
		rowIndex=3;
		$(".legal-invalid-response-rows").each(
									function()
									{
										var row=$(this);
								        extra=new Extra();
										extra.IDENTIFIER="INV"+counter;
										extra.VALUE=row.find("#legalInvalidPromptID").val()+"|"+row.find("#legalInvalidPromptText").val()+"|"+row.find('input[name="legalInvalidAction['+rowIndex+']"]:checked').val()+"|"+row.find("#extraFilename").val();								
								
										data.data.extra.push(extra);								
										counter++;
										rowIndex++
									}
								  );
								  
		_this.designer.tree._model.data[id]=data;

		var nodeID="#"+id;
	
	
		_this.designer.tree.redraw_node(nodeID);
	
		$(_this.legalModalID).modal('hide');		
	}	
	
	_this.saveInput=function()
	{
	
        if(!$("#inputForm").valid())
		{
			
			return false;
		}
		       
		if($(".input-no-response-rows").length==0 )
		{
			alert('Please define atleast one invalid and one no response');
			return;
		}
		
		var id=$("#inputID").val();
		_this.set_id(id);
		if(!_this.designer.nodeArray.includes(id))
        {
        	_this.designer.nodeArray.push(id);		
        }	
		var data=_this.designer.tree._model.data[id];
		data.text=id+":"+$("#inputText").val();		
		data.data.PromptText=$("#inputText").val();

		var flow=new Flow();
		flow.NextANSID=$("#inputKeypressGoto").val();
		data.data.flow=[];
		data.data.flow.push(flow);	

		var extra=new Extra();		
		data.data.extra=[];
		extra.IDENTIFIER="TO";
		extra.VALUE=$("#inputTimeOut").val();
		data.data.extra.push(extra);


        data.data.extra.push(extra);
        var extraLength=new Extra();
		extra.IDENTIFIER="LN";
		extra.VALUE=$("#inputLength").val();
		data.data.extra.push(extraLength);
		
		var asr=new Extra();	
		asr.IDENTIFIER="ASR";
		asr.VALUE=$("#inputASR").val();	
		data.data.extra.push(asr);

		var gr=new Extra();	
		gr.IDENTIFIER="GR";
		gr.VALUE=$("#inputGrammer").val();			
		data.data.extra.push(gr);		
		
		

		var counter=1;
		var rowIndex=3;
		$(".input-no-response-rows").each(
									function()
									{
										
										var row=$(this);
								        extra=new Extra();
										extra.IDENTIFIER="NR"+counter;
										extra.VALUE=row.find("#inputNoResponsePromptID").val()+"|"+row.find("#inputNoResponsePromptText").val()+"|"+row.find('input[name="inputNoResponseAction['+rowIndex+']"]:checked').val()+"|"+row.find("#extraFilename").val();																
									
										data.data.extra.push(extra);
										counter++;
										rowIndex++;
									}
								  );
								  
		_this.designer.tree._model.data[id]=data;

		var nodeID="#"+id;
	
	
		_this.designer.tree.redraw_node(nodeID);
	
		$(_this.inputModalID).modal('hide');		
	}
	_this.saveRecord=function()
	{
	
		if(!$("#recordForm").valid())
		{
			return false;
		}
		
		var id=$("#recordID").val();
		 _this.set_id(id);
		if(!_this.designer.nodeArray.includes(id))
        {
        	_this.designer.nodeArray.push(id);		
        } 
		var data=_this.designer.tree._model.data[id];
		data.text=id+":"+$("#recordText").val();
		
		data.data.PromptText=$("#recordText").val();
		
		var flow=new Flow();
		flow.NextANSID=$("#recordGoto").val();
		data.data.flow=[];
		data.data.flow.push(flow);
		
		var BR=new Extra();
		BR.IDENTIFIER="BR";
		BR.VALUE=$("#recordBargin").val();
		
		data.data.extra=[];
		data.data.extra.push(BR);
		
		var ML=new Extra();
		ML.IDENTIFIER="ML";
		ML.VALUE=$("#recordML").val();
		data.data.extra.push(ML);
		
		var IS=new Extra();		
		IS.IDENTIFIER="IS";
		IS.VALUE=$("#recordIS").val();
		data.data.extra.push(IS);
		
		
		var BP=new Extra();		
		BP.IDENTIFIER="BP";
		BP.VALUE=$("#recordBP").val();
		data.data.extra.push(BP);

		var PB=new Extra();		
		PB.IDENTIFIER="PB";
		PB.VALUE=$("#recordPB").val();
		data.data.extra.push(PB);

		var RFL=new Extra();		
		RFL.IDENTIFIER="FILENAME";
		RFL.VALUE=$("#recordFileName").val();
		if(RFL.VALUE==""){
			RFL.VALUE="N/A";
		}
		data.data.extra.push(RFL);


		
		_this.designer.tree._model.data[id]=data;

		var nodeID="#"+id;
	
	
		_this.designer.tree.redraw_node(nodeID);
	
		$(_this.recordModalID).modal('hide');


	}
	_this.saveTransfer=function()
	{
	
		if(!$("#transferForm").valid())
		{
			return false;
		}
		var id=$("#transferID").val();
         _this.set_id(id);
		if(!_this.designer.nodeArray.includes(id))
        {
        	_this.designer.nodeArray.push(id);		
        }	
		var data=_this.designer.tree._model.data[id];
		data.text=id+":"+$("#transferText").val();
		
		data.data.PromptText=$("#transferText").val();
		data.data.flow=[];
		
		var A=new Flow();
		A.NextANSID=$("#transferA").val();
		A.Value="A";
		data.data.flow.push(A);

		var B=new Flow();
		B.NextANSID=$("#transferB").val();
		B.Value="B";
		data.data.flow.push(B);

		var C=new Flow();
		C.NextANSID=$("#transferC").val();
		C.Value="*";
		data.data.flow.push(C);


		var N=new Flow();
		N.NextANSID=$("#transferN").val();
		N.Value="N";
		data.data.flow.push(N);
		
		data.data.extra=[];
		
		var BR=new Extra();
		BR.IDENTIFIER="BR";
		BR.VALUE=$("#recordBargin").val();
		data.data.extra.push(BR);
		
		var XD=new Extra();
		XD.IDENTIFIER="XD";
		XD.VALUE=$("#transferXD").val();
		data.data.extra.push(XD);
		
		var XT=new Extra();		
		XT.IDENTIFIER="XT";
		XT.VALUE=$("#transferXT").val();
		data.data.extra.push(XT);
		
		
		

		
		_this.designer.tree._model.data[id]=data;

		var nodeID="#"+id;
	
	
		_this.designer.tree.redraw_node(nodeID);
	
		$(_this.transferModalID).modal('hide');


	}
	_this.saveIF=function()
	{
	
		if(!$("#ifForm").valid())
		{
			return false;
		}
		var id=$("#ifID").val();
         _this.set_id(id);
		if(!_this.designer.nodeArray.includes(id))
        {
        	_this.designer.nodeArray.push(id);		
        }	
		var data=_this.designer.tree._model.data[id];
		data.text=id+":"+$("#ifText").val();

		data.data.PromptText=$("#ifText").val();

		var success=new Flow();
		success.NextANSID=$("#ifGotoSuccess").val();
		success.Value=1;

		var failure=new Flow();
		failure.NextANSID=$("#ifGotoFailure").val();
		failure.Value=0;
		
		data.data.flow=[];
		data.data.flow.push(success);
		data.data.flow.push(failure);

		data.data.extra=[];
		
		

		
		var con=new Extra();
		con.IDENTIFIER="CON";
		con.VALUE=$("#ifOperator").val();		



		var left=new Extra();

		if($("#ifLeftOperand").val()=="variableInteger")
		{
			left.IDENTIFIER="VLI";
			left.VALUE=$("#ifVLI").val();	
						
		}
		else if($("#ifLeftOperand").val()=="variableString")
		{     
			left.IDENTIFIER="VLS";
			left.VALUE=$("#ifVLS").val();

		}
		else if($("#ifLeftOperand").val().search('getCurrent')==0)
		{
			left.IDENTIFIER="FLI";
			left.VALUE=$("#ifLeftOperand").val();		
		}
		else if($("#ifLeftOperand").val()=="isContain")
		{
			var temp=new Extra();
			temp.IDENTIFIER="CLS";
			temp.VALUE=$("#ifVLS").val();
			data.data.extra.push(temp);
			left.IDENTIFIER="FLS";
			left.VALUE=$("#ifLeftOperand").val();

		}
		else 
		{
			left.IDENTIFIER="CLI";
			left.VALUE=$("#ifLeftOperand").val();		

		}
		
		//left.VALUE=$("#ifLeftOperand").val();	
		
		var right=new Extra();
	
		if($("#ifRightOperand").val()=="variableInteger")
		{
			right.IDENTIFIER="VRI";
			right.VALUE=$("#ifVRI").val();	
						
		}
		else if($("#ifRightOperand").val()=="variableString")
		{
			right.IDENTIFIER="VRS";
			right.VALUE=$("#ifVRS").val();
		}	
		else if($("#ifRightOperand").val().search('getCurrent')==0)
		{
			right.IDENTIFIER="FRI";
			right.VALUE=$("#ifRightOperand").val();	
		}
	/*	else if($("#ifRightOperand").val()=="isContain")
		{
			var temp=new Extra();
			temp.IDENTIFIER="VRS";
			temp.VALUE=$("#ifVRS").val();
			data.data.extra.push(temp);
			right.IDENTIFIER="FRS";
			right.VALUE=$("#ifRightOperand").val();

		} */
		else 
		{
			right.IDENTIFIER="CRI";
			right.VALUE=$("#ifRightOperand").val();		

		}
		
				
		data.data.extra.push(con);
		data.data.extra.push(left);		
		data.data.extra.push(right);		

		_this.designer.tree._model.data[id]=data;

		var nodeID="#"+id;
	
	
		_this.designer.tree.redraw_node(nodeID);
			
	
		$(_this.ifModalID).modal('hide');


	}		
	_this.saveFunction=function()
	{
	
	   
	    $("#functionType").change();
		if(!$("#functionForm").valid())
		{
			return false;
		}	

		var id=$("#functionID").val();
         _this.set_id(id);
		if(!_this.designer.nodeArray.includes(id))
        {
        	_this.designer.nodeArray.push(id);		
        }	
		var data=_this.designer.tree._model.data[id];
		data.text=id+":"+$("#functionText").val();

		data.data.PromptText=$("#functionText").val();

		var success=new Flow();
		success.NextANSID=$("#functionGotoSuccess").val();
		success.Value=1;

		var failure=new Flow();
		failure.NextANSID=$("#functionGotoFailure").val();
		failure.Value=0;
		
		data.data.flow=[];
		data.data.flow.push(success);
		data.data.flow.push(failure);

		data.data.extra=[];
		
		

		
		var fhour=new Extra();
		fhour.IDENTIFIER="FRM";
		fhour.VALUE=$("#functionFromHour").val();		

		data.data.extra.push(fhour);
		
		var thour=new Extra();
		thour.IDENTIFIER="FTO";
		thour.VALUE=$("#functionToHour").val();
		
		data.data.extra.push(thour);
		


		var type=new Extra();
		type.IDENTIFIER="TYP";
		
		var typeValue=$("#functionType").val();
		type.VALUE=typeValue;
		
		if($("#functionFromMin").val()==0 && $("#functionToMin").val()==0)
		{
			typeValue--;
			type.VALUE=typeValue.toString();
		}
		
		if(type.VALUE==1 || type.VALUE==3 || type.VALUE==5)
		{
			var fmin=new Extra();
			fmin.IDENTIFIER="FRM_MM";
			fmin.VALUE=$("#functionFromMin").val();		
			data.data.extra.push(fmin);		

			var tmin=new Extra();
			tmin.IDENTIFIER="FTO_MM";
			tmin.VALUE=$("#functionToMin").val();	
			data.data.extra.push(tmin);			
		}		
		
		data.data.extra.push(type);		

		_this.designer.tree._model.data[id]=data;

		var nodeID="#"+id;
	
	
		_this.designer.tree.redraw_node(nodeID);
			
	
		$(_this.functionModalID).modal('hide');


	}	
	_this.saveVariable=function()
	{
	
		if(!$("#variableForm").valid())
		{
			return false;
		}
		var id=$("#variableID").val();
         _this.set_id(id);
		if(!_this.designer.nodeArray.includes(id))
        {
        	_this.designer.nodeArray.push(id);		
        }	
		var data=_this.designer.tree._model.data[id];
		data.text=id+":"+$("#variableText").val();
		
		data.data.PromptText=$("#variableText").val();
		data.data.flow=[];
		
		data.data.extra=[];
	
		_this.designer.tree._model.data[id]=data;
		
		var extra=new Extra();
		extra.IDENTIFIER="VAL";
		extra.VALUE=$("#variableValue").val();
		
		if(extra.VALUE=="")
		{
			extra.VALUE=0;
		}
		
		data.data.extra.push(extra);


		
		var nodeID="#"+id;
		_this.designer.tree.redraw_node(nodeID);
	
		$(_this.variableModalID).modal('hide');


	}
	
	_this.set_id=function(id)
	{
		var selected=$(_this.designer.treeID).jstree("get_selected");
		var inst = $.jstree.reference(selected[0]);
		inst.set_id(selected,id);		
	}
	_this.saveTts=function()
	{
	
		if(!$("#ttsForm").valid())
		{
			return false;
		}
		var id=$("#ttsID").val();
        _this.set_id(id);
		if(!_this.designer.nodeArray.includes(id))
        {
        	_this.designer.nodeArray.push(id);		
        }

		

		//$(_this.designer.treeID).jstree('get_selected').attr('id')
		//console.log($().jstree('get_selected').attr('id'));
		var data=_this.designer.tree._model.data[id];
		data.text=id+":"+$("#ttsText").val();
		
		data.data.PromptText=$("#ttsText").val();
		
		var flow=new Flow();
		flow.NextANSID=$("#ttsGoto").val();
		data.data.flow=[];
		data.data.flow.push(flow);
		
		var ttsVariable=new Extra();
		ttsVariable.IDENTIFIER="VR";
		ttsVariable.VALUE=$("#ttsVariable").val();

		var ttsType=new Extra();
		ttsType.IDENTIFIER="GRR";
		ttsType.VALUE=$("#ttsType").val();
		
		data.data.extra=[];
		data.data.extra.push(ttsVariable);
		data.data.extra.push(ttsType);
		
		_this.designer.tree._model.data[id]=data;

		var nodeID="#"+id;
	

		_this.designer.tree.redraw_node(nodeID);
	
		$(_this.ttsModalID).modal('hide');


	}	
	_this.savePrompt=function()
	{
	
		if(!$("#promptForm").valid())
		{
			return false;
		}
		var id=$("#promptID").val();
        _this.set_id(id);
		
        if(!_this.designer.nodeArray.includes(id))
        {
        	_this.designer.nodeArray.push(id);		
        }

		

		//$(_this.designer.treeID).jstree('get_selected').attr('id')
		//console.log($().jstree('get_selected').attr('id'));
		var data=_this.designer.tree._model.data[id];
		data.text=id+":"+$("#promptText").val();
		
		data.data.PromptText=$("#promptText").val();
		
		var flow=new Flow();
		flow.NextANSID=$("#promptGoto").val();
		data.data.flow=[];
		data.data.flow.push(flow);
		
		var extra=new Extra();
		extra.IDENTIFIER="BR";
		extra.VALUE=$("#promptBargin").val();
		
		data.data.extra=[];
		data.data.extra.push(extra);
		
		_this.designer.tree._model.data[id]=data;

		var nodeID="#"+id;
	
	
		_this.designer.tree.redraw_node(nodeID);
	
		$(_this.promptModalID).modal('hide');


	}
	_this.noResponseGoto=function(obj)
	{
		var row=$(obj.parentNode.parentNode.parentNode);
		row.find("#noResponseGoto").val(row.find("#noResponseGotoList").val());
			
	}
	_this.inputNoResponseGoto=function(obj)
	{
		//var row=$(obj.target.parentNode.parentNode);
		var row=$(obj.parentNode.parentNode.parentNode);
		row.find("#inputNoResponseGoto").val(row.find("#inputNoResponseGotoList").val());
			
	}
	_this.legalNoResponseGoto=function(obj)
	{
		//var row=$(obj.target.parentNode.parentNode);
		var row=$(obj.parentNode.parentNode.parentNode);
		row.find("#legalNoResponseGoto").val(row.find("#legalNoResponseGotoList").val());
			
	}	
	_this.languageNoResponseGoto=function(obj)
	{
		//var row=$(obj.target.parentNode.parentNode);
		var row=$(obj.parentNode.parentNode.parentNode);
		row.find("#languageNoResponseGoto").val(row.find("#languageNoResponseGotoList").val());
			
	}	
	_this.removeNoResponseRow=function(obj)
	{
		var row=$(obj.parentNode.parentNode);
		row.remove();
	}

	_this.addNoResponseRow=function()
	{
            if(!$("#noResponseForm").valid())
		{
			return false;
		}
               
		var data={};
		var promptID=$("#noResponseRow").find("#noResponsePromptList").val();
		var promptText=$("#noResponseRow").find("#noResponsePromptText").val();
		var action=$("#noResponseRow").find('input[name="noResponseAction"]:checked').val();
		var playPrompt=$("#noResponseRow").find('#playPrompt').prop("checked");
		if(promptID==0)
		{
			promptID=_this.getNextResponseID();
			

			$("#noResponseRow").find("#noResponsePromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
			$("#invalidRow").find("#invalidPromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
			

		}
		data.id=promptID;
		data.text=promptText;
		data.action=action;
        data.playPrompt="FALSE";
		if(playPrompt)
		{
            data.playPrompt="TRUE";
		}	
		_this.populateNoResponseRow(data);

			$("#noResponseRow").find("#noResponsePromptList").val(0);
			$("#noResponseRow").find("#noResponsePromptText").val('');		

	}
	_this.addInputNoResponseRow=function()
	{
            if(!$("#inputNoResponseForm").valid())
		{
			return false;
		}
             
		var data={};
		var promptID=$("#inputNoResponseRow").find("#inputNoResponsePromptList").val();
		var promptText=$("#inputNoResponseRow").find("#inputNoResponsePromptText").val();
		var action=$("#inputNoResponseRow").find('input[name="inputNoResponseAction"]:checked').val();
		if(promptID==0)
		{
			promptID=_this.getNextResponseID();
			

			$("#inputNoResponseRow").find("#inputNoResponsePromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
		//	$("#inputInvalidRow").find("#inputInvalidPromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
			

		}
		data.id=promptID;
		data.text=promptText;
		data.action=action;
		_this.populateInputNoResponseRow(data);

			$("#inputNoResponseRow").find("#inputNoResponsePromptList").val(0);
			$("#inputNoResponseRow").find("#inputNoResponsePromptText").val('');		

	}
	_this.addLegalNoResponseRow=function()
	{
        if(!$("#legalNoResponseForm").valid())
		{
			return false;
		}
             
		var data={};
		var promptID=$("#legalNoResponseRow").find("#legalNoResponsePromptList").val();
		var promptText=$("#legalNoResponseRow").find("#legalNoResponsePromptText").val();
		var action=$("#legalNoResponseRow").find('input[name="legalNoResponseAction"]:checked').val();
		if(promptID==0)
		{
			promptID=_this.getNextResponseID();
			

			$("#legalNoResponseRow").find("#legalNoResponsePromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
			$("#legalInvalidRow").find("#legalInvalidPromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
			

		}
		data.id=promptID;
		data.text=promptText;
		data.action=action;
		_this.populateLegalNoResponseRow(data);

			$("#legalNoResponseRow").find("#legalNoResponsePromptList").val(0);
			$("#legalNoResponseRow").find("#legalNoResponsePromptText").val('');		

	}
	_this.addLanguageNoResponseRow=function()
	{
        if(!$("#languageNoResponseForm").valid())
		{
			return false;
		}
             
		var data={};
		var promptID=$("#languageNoResponseRow").find("#languageNoResponsePromptList").val();
		var promptText=$("#languageNoResponseRow").find("#languageNoResponsePromptText").val();
		var action=$("#languageNoResponseRow").find('input[name="languageNoResponseAction"]:checked').val();
		if(promptID==0)
		{
			promptID=_this.getNextResponseID();
			

			$("#languageNoResponseRow").find("#languageNoResponsePromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
			$("#languageInvalidRow").find("#languageInvalidPromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
			

		}
		data.id=promptID;
		data.text=promptText;
		data.action=action;
		_this.populateLanguageNoResponseRow(data);

			$("#languageNoResponseRow").find("#languageNoResponsePromptList").val(0);
			$("#languageNoResponseRow").find("#languageNoResponsePromptText").val('');		

	}		
        _this.addConfirmationNoResponseRow=function()
	{
            if(!$("#confirmationNoResponseForm").valid())
		{
			return false;
		}
               
		var data={};
		var promptID=$("#confirmationNoResponseRow").find("#confirmationNoResponsePromptList").val();
		var promptText=$("#confirmationNoResponseRow").find("#confirmationNoResponsePromptText").val();
		var action=$("#confirmationNoResponseRow").find('input[name="confirmationNoResponseAction"]:checked').val();
		if(promptID==0)
		{
			promptID=_this.getNextResponseID();
			

			$("#confirmationNoResponseRow").find("#confirmationNoResponsePromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
			$("#confirmationInvalidRow").find("#confirmationInvalidPromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
			

		}
		data.id=promptID;
		data.text=promptText;
		data.action=action;
		_this.populateConfirmationNoResponseRow(data);

			$("#confirmationNoResponseRow").find("#confirmationNoResponsePromptList").val(0);
			$("#confirmationNoResponseRow").find("#confirmationNoResponsePromptText").val('');		

	}
	_this.setNoResponsePromptText=function(event)
	{
		var row=$(event.target.parentNode.parentNode);
		var val=row.find("#noResponsePromptList").val();
		if(val=="0")
		{
			row.find("#noResponsePromptText").val('');
			row.find("#noResponsePromptText").attr('readonly',false);
			row.find("#noResponsePromptText").focus();
		
			
		}
		else 
		{
				val=row.find("#noResponsePromptList").find('option:selected').text().split(":");
				row.find("#noResponsePromptText").attr('readonly',true);
				row.find("#noResponsePromptText").val(val[1]);
				row.find("#noResponsePromptText").focus();
		}
		
		
	}	
	_this.setInputNoResponsePromptText=function(event)
	{
		var row=$(event.target.parentNode.parentNode);
		var val=row.find("#inputNoResponsePromptList").val();
		if(val=="0")
		{
			row.find("#inputNoResponsePromptText").val('');
			row.find("#inputNoResponsePromptText").attr('readonly',false);
			row.find("#inputNoResponsePromptText").focus();
		
			
		}
		else 
		{
				val=row.find("#inputNoResponsePromptList").find('option:selected').text().split(":");
				row.find("#inputNoResponsePromptText").attr('readonly',true);
				row.find("#inputNoResponsePromptText").val(val[1]);
				row.find("#inputNoResponsePromptText").focus();
		}
		
		
	}	
        _this.setConfirmationNoResponsePromptText=function(event)
	{
		var row=$(event.target.parentNode.parentNode.parentNode);
		var val=row.find("#confirmationNoResponsePromptList").val();
		if(val=="0")
		{
			row.find("#confirmationNoResponsePromptText").val('');
			row.find("#confirmationNoResponsePromptText").attr('readonly',false);
			row.find("#confirmationNoResponsePromptText").focus();
		
			
		}
		else 
		{
				val=row.find("#confirmationNoResponsePromptList").find('option:selected').text().split(":");
				row.find("#confirmationNoResponsePromptText").attr('readonly',true);
				row.find("#confirmationNoResponsePromptText").val(val[1]);
				row.find("#confirmationNoResponsePromptText").focus();
		}
		
		
	}
	_this.populateNoResponseRow=function(data)
	{
		var cloned=$(_this.noResponseTemplateID).clone(true);
		var count=$(_this.noResponseTableID).find('tr').length;
		

		var prompt=cloned.find('#noResponsePromptID').val(data.id);
		var text=cloned.find('#noResponsePromptText').val(data.text);
		var actions=cloned.find('input[name="noResponseAction"]');
		var gotoList=cloned.find('#noResponseGotoList');
		
		gotoList.html($("#noResponseRow").find("#noResponseGotoList").html());
		
		prompt.attr('name','noResponsePromptID['+count+']');
		text.attr('name','noResponsePromptText['+count+']');
		gotoList.attr('name','noResponseGotoList['+count+']');
		actions.each(function(){ $(this).attr('name','noResponseAction['+count+']');});
         
        if(data.file && data.file!=="filename" && data.file!==0 && typeof data.file!== 'undefined' && data.file!="undefined"){
       //  cloned.find(".input-group").replaceWith('<div class="input-group" style="display:inline-block"><span class="btn btn-xs actionbuttonsmedium btn-file lastaction"><i class="fa fa-upload file-uploaded"></i><input name="extraFileupload" id="extraFileupload" type="file" class="form-control"/></span></div> ');       
         cloned.find("#extraFilename").val(data.file);	
        }  

		//cloned.find('#invalidPromptText').val(data.action);
		if(data.action=="RP")
		{
			cloned.find('#noResponseRepeat').prop('checked',true);
		}
		else 
		{
			cloned.find('#noResponseGoto').prop('checked',true);
			cloned.find('#noResponseGoto').prop('value',data.action);
			gotoList.val(data.action);
		}
		if(data.playPrompt=="TRUE")
		{
			cloned.find('#playPrompt').prop('checked',true);
		}
		else
		{
		    cloned.find('#playPrompt').prop('checked',false);
		}			
		cloned.removeClass('hidden');
		cloned.addClass('no-response-rows');

		count=count-2;
		var rowID='NR'+count;
		cloned.attr('id',rowID);
		$(_this.noResponseTableID).append(cloned);
	}
/*	_this.populateInputNoResponseRow=function(data)
	{
		var cloned=$(_this.inputNoResponseTemplateID).clone(true);
		var count=$(_this.inputNoResponseTableID).find('tr').length;
		

		var prompt=cloned.find('#inputNoResponsePromptID').val(data.id);
		var text=cloned.find('#inputNoResponsePromptText').val(data.text);
		var actions=cloned.find('input[name="inputNoResponseAction"]');
		var gotoList=cloned.find('#inputNoResponseGotoList');
		
		gotoList.html($("#inputNoResponseRow").find("#inputNoResponseGotoList").html());
		
		prompt.attr('name','inputNoResponsePromptID['+count+']');
		text.attr('name','inputNoResponsePromptText['+count+']');
		gotoList.attr('name','inputNoResponseGotoList['+count+']');
		actions.each(function(){ $(this).attr('name','inputNoResponseAction['+count+']');});

	
		//cloned.find('#invalidPromptText').val(data.action);
		if(data.action=="RP")
		{
			cloned.find('#inputNoResponseRepeat').prop('checked',true);
		}
		else 
		{
			cloned.find('#inputNoResponseGoto').prop('checked',true);
			cloned.find('#inputNoResponseGoto').prop('value',data.action);
			gotoList.val(data.action);
		}			
		cloned.removeClass('hidden');
		cloned.addClass('input-no-response-rows');

		count=count-2;
		var rowID='NR'+count;
		cloned.attr('id',rowID);
		$(_this.inputNoResponseTableID).append(cloned);
	} */
	_this.populateLegalNoResponseRow=function(data)
	{
		var cloned=$(_this.legalNoResponseTemplateID).clone(true);
		var count=$(_this.legalNoResponseTableID).find('tr').length;
		

		var prompt=cloned.find('#legalNoResponsePromptID').val(data.id);
		var text=cloned.find('#legalNoResponsePromptText').val(data.text);
		var actions=cloned.find('input[name="legalNoResponseAction"]');
		var gotoList=cloned.find('#legalNoResponseGotoList');
		
		gotoList.html($("#legalNoResponseRow").find("#legalNoResponseGotoList").html());
		
		prompt.attr('name','legalNoResponsePromptID['+count+']');
		text.attr('name','legalNoResponsePromptText['+count+']');
		gotoList.attr('name','legalNoResponseGotoList['+count+']');
		actions.each(function(){ $(this).attr('name','legalNoResponseAction['+count+']');});
		//cloned.find('#invalidPromptText').val(data.action);
        if(data.file && data.file!=="filename" && data.file!==0 && typeof data.file!== 'undefined' && data.file!="undefined"){
         cloned.find(".input-group").replaceWith('<div class="input-group" style="display:inline-block"><span class="btn btn-xs actionbuttonsmedium btn-file lastaction"><i class="fa fa-upload file-uploaded"></i><input name="extraFileupload" id="extraFileupload" type="file" class="form-control"/></span></div> ');       
         cloned.find("#extraFilename").val(data.file);	
        }


		if(data.action=="RP")
		{
			cloned.find('#legalNoResponseRepeat').prop('checked',true);
		}
		else 
		{
			cloned.find('#legalNoResponseGoto').prop('checked',true);
			cloned.find('#legalNoResponseGoto').prop('value',data.action);
			gotoList.val(data.action);
		}			
		cloned.removeClass('hidden');
		cloned.addClass('legal-no-response-rows');

		count=count-2;
		var rowID='NR'+count;
		cloned.attr('id',rowID);
		$(_this.legalNoResponseTableID).append(cloned);
	}
    
	_this.populateInputNoResponseRow=function(data)
	{
		var cloned=$(_this.inputNoResponseTemplateID).clone(true);
		var count=$(_this.inputNoResponseTableID).find('tr').length;
		

		var prompt=cloned.find('#inputNoResponsePromptID').val(data.id);
		var text=cloned.find('#inputNoResponsePromptText').val(data.text);
		var actions=cloned.find('input[name="inputNoResponseAction"]');
		var gotoList=cloned.find('#inputNoResponseGotoList');
		
		gotoList.html($("#inputNoResponseRow").find("#inputNoResponseGotoList").html());
		
		prompt.attr('name','inputNoResponsePromptID['+count+']');
		text.attr('name','inputNoResponsePromptText['+count+']');
		gotoList.attr('name','inputNoResponseGotoList['+count+']');
		actions.each(function(){ $(this).attr('name','inputNoResponseAction['+count+']');});
		//cloned.find('#invalidPromptText').val(data.action);
        if(data.file && data.file!=="filename" && data.file!==0 && typeof data.file!== 'undefined' && data.file!="undefined"){
     //    cloned.find(".input-group").replaceWith('<div class="input-group" style="display:inline-block"><span class="btn btn-xs actionbuttonsmedium btn-file lastaction"><i class="fa fa-upload file-uploaded"></i><input name="extraFileupload" id="extraFileupload" type="file" class="form-control"/></span></div> ');       
         cloned.find("#extraFilename").val(data.file);	
        }


		if(data.action=="RP")
		{
			cloned.find('#inputNoResponseRepeat').prop('checked',true);
		}
		else 
		{
			cloned.find('#inputNoResponseGoto').prop('checked',true);
			cloned.find('#inputNoResponseGoto').prop('value',data.action);
			gotoList.val(data.action);
		}			
		cloned.removeClass('hidden');
		cloned.addClass('input-no-response-rows');

		count=count-2;
		var rowID='NR'+count;
		cloned.attr('id',rowID);
		$(_this.inputNoResponseTableID).append(cloned);
	}
 	_this.populateLanguageNoResponseRow=function(data)
	{
		var cloned=$(_this.languageNoResponseTemplateID).clone(true);
		var count=$(_this.languageNoResponseTableID).find('tr').length;
		

		var prompt=cloned.find('#languageNoResponsePromptID').val(data.id);
		var text=cloned.find('#languageNoResponsePromptText').val(data.text);
		var actions=cloned.find('input[name="languageNoResponseAction"]');
		var gotoList=cloned.find('#languageNoResponseGotoList');
		
		gotoList.html($("#languageNoResponseRow").find("#languageNoResponseGotoList").html());
		
		prompt.attr('name','languageNoResponsePromptID['+count+']');
		text.attr('name','languageNoResponsePromptText['+count+']');
		gotoList.attr('name','languageNoResponseGotoList['+count+']');
		actions.each(function(){ $(this).attr('name','languageNoResponseAction['+count+']');});


        if(data.file && data.file!=="filename" && data.file!==0 && typeof data.file!== 'undefined' && data.file!="undefined"){
         cloned.find(".language-group").replaceWith('<div class="language-group" style="display:inline-block"><span class="btn btn-xs actionbuttonsmedium btn-file lastaction"><i class="fa fa-upload file-uploaded"></i><input name="extraFileupload" id="extraFileupload" type="file" class="form-control"/></span></div> ');       
         cloned.find("#extraFilename").val(data.file);	
        }


		//cloned.find('#invalidPromptText').val(data.action);
		if(data.action=="RP")
		{
			cloned.find('#languageNoResponseRepeat').prop('checked',true);
		}
		else 
		{
			cloned.find('#languageNoResponseGoto').prop('checked',true);
			cloned.find('#languageNoResponseGoto').prop('value',data.action);
			gotoList.val(data.action);
		}			
		cloned.removeClass('hidden');
		cloned.addClass('language-no-response-rows');

		count=count-2;
		var rowID='NR'+count;
		cloned.attr('id',rowID);
		$(_this.languageNoResponseTableID).append(cloned);
	}   
	_this.populateConfirmationNoResponseRow=function(data)
	{
		var cloned=$(_this.confirmationNoResponseTemplateID).clone(true);
		var count=$(_this.confirmationNoResponseTableID).find('tr').length;
		

		var prompt=cloned.find('#confirmationNoResponsePromptID').val(data.id);
		var text=cloned.find('#confirmationNoResponsePromptText').val(data.text);
		var actions=cloned.find('input[name="confirmationNoResponseAction"]');
		var gotoList=cloned.find('#confirmationNoResponseGotoList');
		
		gotoList.html($("#confirmationNoResponseRow").find("#confirmationNoResponseGotoList").html());
		
		prompt.attr('name','confirmationNoResponsePromptID['+count+']');
		text.attr('name','confirmationNoResponsePromptText['+count+']');
		gotoList.attr('name','confirmationNoResponseGotoList['+count+']');
		actions.each(function(){ $(this).attr('name','confirmationNoResponseAction['+count+']');});


        if(data.file && data.file!=="filename" && data.file!==0 && typeof data.file!== 'undefined' && data.file!="undefined"){
       //  cloned.find(".input-group").replaceWith('<div class="input-group" style="display:inline-block"><span class="btn btn-xs actionbuttonsmedium btn-file lastaction"><i class="fa fa-upload file-uploaded"></i><input name="extraFileupload" id="extraFileupload" type="file" class="form-control"/></span></div> ');       
         cloned.find("#extraFilename").val(data.file);	
        }


		//cloned.find('#invalidPromptText').val(data.action);
		if(data.action=="RP")
		{
			cloned.find('#confirmationNoResponseRepeat').prop('checked',true);
		}
		else 
		{
			cloned.find('#confirmationNoResponseGoto').prop('checked',true);
			cloned.find('#confirmationNoResponseGoto').prop('value',data.action);
			gotoList.val(data.action);
		}			
		cloned.removeClass('hidden');
		cloned.addClass('confirmation-no-response-rows');

		count=count-2;
		var rowID='NR'+count;
		cloned.attr('id',rowID);
		$(_this.confirmationNoResponseTableID).append(cloned);
	}

	_this.invalidResponseGoto=function(obj)
	{
              if(!$("#noResponseForm").valid())
		{
			return false;
		}
		var row=$(obj.parentNode.parentNode.parentNode);
		row.find("#invalidGoto").val(row.find("#invalidGotoList").val());
			
	}
	_this.inputInvalidResponseGoto=function(obj)
	{
		if(!$("#inputNoResponseForm").valid())
		{
			return false;
		}
		var row=$(obj.parentNode.parentNode.parentNode);
		row.find("#inputInvalidGoto").val(row.find("#inputInvalidGotoList").val());
			
	}
	_this.legalInvalidResponseGoto=function(obj)
	{
		if(!$("#legalNoResponseForm").valid())
		{
			return false;
		}
		var row=$(obj.parentNode.parentNode.parentNode);
		row.find("#legalInvalidGoto").val(row.find("#legalInvalidGotoList").val());
			
	}
	_this.languageInvalidResponseGoto=function(obj)
	{
		if(!$("#languageNoResponseForm").valid())
		{
			return false;
		}
		var row=$(obj.parentNode.parentNode.parentNode);
		row.find("#languageInvalidGoto").val(row.find("#languageInvalidGotoList").val());
			
	}	
	_this.confirmationInvalidResponseGoto=function(obj)
	{
		
        if(!$("#confirmationInvalidResponseForm").valid())
		{
			return false;
		}
		var row=$(obj.parentNode.parentNode.parentNode);
		row.find("#confirmationInvalidGoto").val(row.find("#confirmationInvalidGotoList").val());
			
	}
	_this.confirmationNoResponseGoto=function(obj)
	{
        if(!$("#confirmationNoResponseForm").valid())
		{
			return false;
		}
		var row=$(obj.parentNode.parentNode.parentNode);
		row.find("#confirmationNoResponseGoto").val(row.find("#confirmationNoResponseGotoList").val());
			
	}
	_this.removeInvalidResponseRow=function(obj)
	{

		var row=$(obj.parentNode.parentNode);
		row.remove();
	}	

	_this.removeOutputParamRow=function(obj)
	{

		var row=$(obj.parentNode.parentNode);
		row.remove();
	}	
	_this.removeInputParamRow=function(obj)
	{

		var row=$(obj.parentNode.parentNode);
		row.remove();
	}	
	_this.removeStatusParamRow=function(obj)
	{

		var row=$(obj.parentNode.parentNode);
		row.remove();
	}		
	_this.addConfirmationInvalidResponseRow=function()
	{ 
            
       if(!$("#confirmationInvalidResponseForm").valid())
		{
            
			return false;
		}
		var data={};
		var promptID=$("#confirmationInvalidRow").find("#confirmationInvalidPromptList").val();
		var promptText=$("#confirmationInvalidRow").find("#confirmationInvalidPromptText").val();
		var action=$("#confirmationInvalidRow").find('input[name="confirmationInvalidAction"]:checked').val();
		if(promptID==0)
		{
		
			promptID=_this.getNextResponseID();
			

			$("#confirmationInvalidRow").find("#confirmationInvalidPromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
			$("#confirmationNoResponseRow").find("#confirmationNoResponsePromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
	
			
			
		}
		data.id=promptID;
		data.text=promptText;
		data.action=action;
		_this.populateConfirmationInvalidResponseRow(data);
		
			$("#confirmationInvalidRow").find("#confirmationInvalidPromptList").val(0);
			$("#confirmationInvalidRow").find("#confirmationInvalidPromptText").val('');		

	}
	_this.addOutputParamRow=function()
	{ 
       if(!$("#outputParamForm").valid())
		{
			return false;
		}
		var data={};
		var paramName=$("#outputParamRow").find("#outputParamNameHolder").val();
        var paramValue=$("#outputParamRow").find("#outputParamValueHolder").val();

        data.paramName=paramName;
		data.paramValue=paramValue;
		if( $("#outputParamList").val()=="0")
		{
			data.paramAction="VAR";
		}
		else 
		{
			data.paramAction="CMD";
		}


		_this.populateOutputParamRow(data);
		
		$("#outputParamRow").find("#outputParamNameHolder").val('');
	    $("#outputParamRow").find("#outputParamValueHolder").val('');

	}	
	_this.addInputParamRow=function()
	{ 
       if(!$("#inputParamForm").valid())
		{
			return false;
		}
		var data={};
		var paramName=$("#inputParamRow").find("#inputParamNameHolder").val();
		var paramValue=$("#inputParamRow").find("#inputParamValueHolder").val();


		data.paramName=paramName;
		data.paramValue=paramValue;
		if( $("#inputParamList").val()=="0")
		{
			data.paramAction="VAR";
		}
		else 
		{
			data.paramAction="CMD";
		}		
		_this.populateInputParamRow(data);
		
			$("#inputParamRow").find("#inputParamNameHolder").val('');
			$("#inputParamRow").find("#inputParamValueHolder").val('');
	

	}
		_this.addStatusParamRow=function()
	{ 
       if(!$("#statusParamForm").valid())
		{
			return false;
		}
		var c=$(_this.statusParamTableID).find('tr').length;
		if(c==4){
			alert("There can be only one status Parameter");
			return false;
		}

		var data={};
		var paramName=$("#statusParamRow").find("#statusParamNameHolder").val();
		var paramValue=$("#statusParamRow").find("#statusParamValueHolder").val();


		data.paramName=paramName;
		data.paramValue=paramValue;
		if( $("#statusParamList").val()=="0")
		{
			data.paramAction="VAR";
		}
		else 
		{
			data.paramAction="CMD";
		}		
		_this.populateStatusParamRow(data);
		
			$("#statusParamRow").find("#statusParamNameHolder").val('');
			$("#statusParamRow").find("#statusParamValueHolder").val('');
	

	}	
	_this.addInvalidResponseRow=function()
	{ 
       if(!$("#invalidResponseForm").valid())
		{
			return false;
		}
		var data={};
		var promptID=$("#invalidRow").find("#invalidPromptList").val();
		var promptText=$("#invalidRow").find("#invalidPromptText").val();
		var action=$("#invalidRow").find('input[name="invalidAction"]:checked').val();
		var playPrompt=$("#invalidRow").find('#playPrompt').prop("checked");
		if(promptID==0)
		{
		
			promptID=_this.getNextResponseID();
			

			$("#invalidRow").find("#invalidPromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
			$("#noResponseRow").find("#noResponsePromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
	
			
			
		}
		data.id=promptID;
		data.text=promptText;
		data.action=action;
		data.playPrompt="FALSE";
		if(playPrompt)
		{
            data.playPrompt="TRUE";
		}	

		_this.populateInvalidResponseRow(data);
		
			$("#invalidRow").find("#invalidPromptList").val(0);
			$("#invalidRow").find("#invalidPromptText").val('');		

	}
	_this.addInputInvalidResponseRow=function()
	{ 
       if(!$("#inputInvalidResponseForm").valid())
		{
			return false;
		}
		var data={};
		var promptID=$("#inputInvalidRow").find("#inputInvalidPromptList").val();
		var promptText=$("#inputInvalidRow").find("#inputInvalidPromptText").val();
		var action=$("#inputInvalidRow").find('input[name="inputInvalidAction"]:checked').val();
		if(promptID==0)
		{
		
			promptID=_this.getNextResponseID();
			

			$("#inputInvalidRow").find("#inputInvalidPromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
			$("#inputNoResponseRow").find("#inputNoResponsePromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
	
			
			
		}
		data.id=promptID;
		data.text=promptText;
		data.action=action;
		_this.populateInputInvalidResponseRow(data);
		
			$("#inputInvalidRow").find("#inputInvalidPromptList").val(0);
			$("#inputInvalidRow").find("#inputInvalidPromptText").val('');		

	}
	_this.addLanguageInvalidResponseRow=function()
	{ 
       if(!$("#languageInvalidResponseForm").valid())
		{
			return false;
		}

		var data={};
		var promptID=$("#languageInvalidRow").find("#languageInvalidPromptList").val();
		var promptText=$("#languageInvalidRow").find("#languageInvalidPromptText").val();
		var action=$("#languageInvalidRow").find('input[name="languageInvalidAction"]:checked').val();
		if(promptID==0)
		{
		
			promptID=_this.getNextResponseID();
			

			$("#languageInvalidRow").find("#languageInvalidPromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
			$("#languageNoResponseRow").find("#languageNoResponsePromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
	
			
			
		}
		data.id=promptID;
		data.text=promptText;
		data.action=action;
		_this.populateLanguageInvalidResponseRow(data);
		
			$("#languageInvalidRow").find("#languageInvalidPromptList").val(0);
			$("#languageInvalidRow").find("#languageInvalidPromptText").val('');		

	}	
	_this.addLegalInvalidResponseRow=function()
	{ 
       if(!$("#legalInvalidResponseForm").valid())
		{
			return false;
		}

		var data={};
		var promptID=$("#legalInvalidRow").find("#legalInvalidPromptList").val();
		var promptText=$("#legalInvalidRow").find("#legalInvalidPromptText").val();
		var action=$("#legalInvalidRow").find('input[name="legalInvalidAction"]:checked').val();
		if(promptID==0)
		{
		
			promptID=_this.getNextResponseID();
			

			$("#legalInvalidRow").find("#legalInvalidPromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
			$("#legalNoResponseRow").find("#legalNoResponsePromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
	
			
			
		}
		data.id=promptID;
		data.text=promptText;
		data.action=action;
		_this.populateLegalInvalidResponseRow(data);
		
			$("#legalInvalidRow").find("#legalInvalidPromptList").val(0);
			$("#legalInvalidRow").find("#legalInvalidPromptText").val('');		

	}	
	_this.setInvalidPromptText=function(event)
	{
		var row=$(event.target.parentNode.parentNode);
		var val=row.find("#invalidPromptList").val();
		if(val=="0")
		{
			row.find("#invalidPromptText").val('');
			row.find("#invalidPromptText").attr('readonly',false);
			row.find("#invalidPromptText").focus();
		
			
		}
		else 
		{
				val=row.find("#invalidPromptList").find('option:selected').text().split(":");
				row.find("#invalidPromptText").attr('readonly',true);
				row.find("#invalidPromptText").val(val[1]);
				row.find("#invalidRepeat").focus();
		}
		
		
	}	
	_this.setInputParam=function(event)
	{
		var row=$(event.target.parentNode.parentNode.parentNode);
		var val=row.find("#inputParamList").val();
		if(val=="0")
		{
			$("#inputParamNameHolder").rules("remove");
			$("#inputParamValueHolder").rules("remove");			
			$("#inputParamNameHolder").rules("add",{required:true,checkDuplicateInputParamter:true,variableDeclaration:true,maxlength: 50});
			$("#inputParamValueHolder").rules("add",{required:true,maxlength: 50});
	
			row.find("#inputParamNameHolder").val('');
			row.find("#inputParamNameHolder").attr('readonly',false);
			row.find("#inputParamNameHolder").focus();			
			row.find("#inputParamValueHolder").val('');
			row.find("#inputParamValueHolder").attr('readonly',false);
			row.find("#inputParamValueHolder").focus();
		
			
		}
		else 
		{
				$("#inputParamNameHolder").rules("remove");
				$("#inputParamValueHolder").rules("remove");	
			$("#inputParamNameHolder").rules("add",{required:true,checkDuplicateInputParamter:true,maxlength: 50});				
				val=row.find("#inputParamList").find('option:selected').text().split(":");
				row.find("#inputParamNameHolder").attr('readonly',false);
				row.find("#inputParamValueHolder").attr('readonly',false);
				row.find("#inputParamNameHolder").val(val[0]);
				row.find("#inputParamValueHolder").val(val[1]);
			
		}
		
		
	}
	_this.setStatusParam=function(event)
	{
		var row=$(event.target.parentNode.parentNode.parentNode);
		var val=row.find("#statusParamList").val();
		if(val=="0")
		{
		//	$("#statusParamNameHolder").rules("remove");
		//	$("#statusParamValueHolder").rules("remove");			
		//	$("#statusParamNameHolder").rules("add",{required:true,checkDuplicateInputParamter:true,variableDeclaration:true,maxlength: 50});
		//	$("#statusParamValueHolder").rules("add",{required:true,maxlength: 50});
	
			row.find("#statusParamNameHolder").val('');
			row.find("#statusParamNameHolder").attr('readonly',false);
			row.find("#statusParamNameHolder").focus();			
			row.find("#statusParamValueHolder").val('');
			row.find("#statusParamValueHolder").attr('readonly',false);
			row.find("#statusParamValueHolder").focus();
		
			
		}
		else 
		{
			//	$("#inputParamNameHolder").rules("remove");
			//	$("#inputParamValueHolder").rules("remove");	
			//  $("#inputParamNameHolder").rules("add",{required:true,checkDuplicateInputParamter:true,maxlength: 50});				
				val=row.find("#statusParamList").find('option:selected').text().split(":");
				row.find("#statusParamNameHolder").attr('readonly',false);
				row.find("#statusParamValueHolder").attr('readonly',false);
				row.find("#statusParamNameHolder").val(val[0]);
				row.find("#statusParamValueHolder").val(val[1]);
			
		}
		
		
	}
	_this.setOutputParam=function(event)
	{
		var row=$(event.target.parentNode.parentNode.parentNode);
		var val=row.find("#outputParamList").val();
		if(val=="0")
		{
			$("#outputParamNameHolder").rules("remove");
			$("#outputParamValueHolder").rules("remove");			
			$("#outputParamNameHolder").rules("add",{required:true,checkDuplicateOutputParamter:true,variableDeclaration:true,maxlength: 50});
			$("#outputParamValueHolder").rules("add",{required:true,maxlength: 50});
	
			row.find("#outputParamNameHolder").val('');
			row.find("#outputParamNameHolder").attr('readonly',false);
			row.find("#outputParamNameHolder").focus();			
			row.find("#outputParamValueHolder").val('');
			row.find("#outputParamValueHolder").attr('readonly',false);
			row.find("#outputParamValueHolder").focus();
		
			
		}
		else 
		{
				$("#outputParamNameHolder").rules("remove");
				$("#outputParamValueHolder").rules("remove");	
			    $("#outputParamNameHolder").rules("add",{required:true,checkDuplicateOutputParamter:true,maxlength: 50});				
				val=row.find("#outputParamList").find('option:selected').text().split(":");
				row.find("#outputParamNameHolder").attr('readonly',false);
				row.find("#outputParamValueHolder").attr('readonly',false);
				row.find("#outputParamNameHolder").val(val[0]);
				row.find("#outputParamValueHolder").val(val[1]);
			
		}
		
		
	}
	_this.setInputInvalidPromptText=function(event)
	{
		var row=$(event.target.parentNode.parentNode.parentNode);
		var val=row.find("#inputInvalidPromptList").val();
		if(val=="0")
		{
			row.find("#inputInvalidPromptText").val('');
			row.find("#inputInvalidPromptText").attr('readonly',false);
			row.find("#inputInvalidPromptText").focus();
		
			
		}
		else 
		{
				val=row.find("#inputInvalidPromptList").find('option:selected').text().split(":");
				row.find("#inputInvalidPromptText").attr('readonly',true);
				row.find("#inputInvalidPromptText").val(val[1]);
				row.find("#inputInvalidRepeat").focus();
		}
		
		
	}	
        _this.setConfirmationInvalidPromptText=function(event)
	{
		var row=$(event.target.parentNode.parentNode.parentNode);
		var val=row.find("#confirmationInvalidPromptList").val();
		if(val=="0")
		{
			row.find("#confirmationInvalidPromptText").val('');
			row.find("#confirmationInvalidPromptText").attr('readonly',false);
			row.find("#confirmationInvalidPromptText").focus();
		
			
		}
		else 
		{
				val=row.find("#confirmationInvalidPromptList").find('option:selected').text().split(":");
				row.find("#confirmationInvalidPromptText").attr('readonly',true);
				row.find("#confirmationInvalidPromptText").val(val[1]);
				row.find("#confirmationInvalidRepeat").focus();
		}
		
		
	}
	
	
	_this.populateInvalidResponseRow=function(data)
	{
		var cloned=$(_this.invalidResponseTemplateID).clone(true);
		var count=$(_this.invalidTableID).find('tr').length;
		

		var prompt=cloned.find('#invalidPromptID').val(data.id);
		var text=cloned.find('#invalidPromptText').val(data.text);
		var actions=cloned.find('input[name="invalidAction"]');
		var gotoList=cloned.find('#invalidGotoList');
		
		gotoList.html($("#invalidRow").find("#invalidGotoList").html());
		



		prompt.attr('name','invalidPromptID['+count+']');
		text.attr('name','invalidPromptText['+count+']');
		gotoList.attr('name','invalidGotoList['+count+']');
		actions.each(function(){ $(this).attr('name','invalidAction['+count+']');});
		//cloned.find('#invalidPromptText').val(data.action);
        if(data.file && data.file!=="filename" && data.file!==0 && typeof data.file!== 'undefined'){
        // cloned.find(".input-group").replaceWith('<div class="input-group" style="display:inline-block"><span class="btn btn-xs actionbuttonsmedium btn-file lastaction"><i class="fa fa-upload file-uploaded"></i><input name="extraFileupload" id="extraFileupload" type="file" class="form-control"/></span></div> ');       
         cloned.find("#extraFilename").val(data.file);	
        }  



		if(data.action=="RP")
		{
			cloned.find('#invalidRepeat').prop('checked',true);
		}
		else 
		{
			cloned.find('#invalidGoto').prop('checked',true);
			cloned.find('#invalidGoto').prop('value',data.action);
			gotoList.val(data.action);
		}	
		if(data.playPrompt=="TRUE")
		{
			cloned.find('#playPrompt').prop('checked',true);
		}
		else
		{
		    cloned.find('#playPrompt').prop('checked',false);
		}		
		cloned.removeClass('hidden');
		cloned.addClass('invalid-response-rows');

		count=count-2;
		var rowID='INV'+count;
		cloned.attr('id',rowID);
		$(_this.invalidTableID).append(cloned);
	}
	_this.populateOutputParamRow=function(data)
	{
		var cloned=$(_this.outputParamTemplateID).clone(true);
		var count=$(_this.outputParamTableID).find('tr').length;
		

		var paramName=cloned.find('#outputParamName').val(data.paramName);
        var paramValue=cloned.find('#outputParamValue').val(data.paramValue); 
		
		paramName.attr('name','outputParamName['+count+']');
		paramValue.attr('name','outputParamValue['+count+']');
		
		cloned.removeClass('hidden');
		cloned.addClass('output-param-rows');
		cloned.attr('data-param-type',data.paramAction);
		count=count-2;
		var rowID='param'+count;
		cloned.attr('id',rowID);
		$(_this.outputParamTableID).append(cloned);
	}
	_this.populateInputParamRow=function(data)
	{
		var cloned=$(_this.inputParamTemplateID).clone(true);
		var count=$(_this.inputParamTableID).find('tr').length;
		

		var paramName=cloned.find('#inputParamName').val(data.paramName);
		var paramValue=cloned.find('#inputParamValue').val(data.paramValue);

		
		paramName.attr('name','inputParamName['+count+']');
		paramValue.attr('name','inputParamValue['+count+']');
		
		cloned.removeClass('hidden');
		cloned.addClass('input-param-rows');

		cloned.attr("data-param-type",data.paramAction);
		
		count=count-2;
		var rowID='param'+count;
		cloned.attr('id',rowID);
		$(_this.inputParamTableID).append(cloned);
	}
	_this.populateStatusParamRow=function(data)
	{
		var cloned=$(_this.statusParamTemplateID).clone(true);
		//var count=$(_this.inputParamTableID).find('tr').length;
		

		var paramName=cloned.find('#statusParamName').val(data.paramName);
		var paramValue=cloned.find('#statusParamValue').val(data.paramValue);

		
		paramName.attr('name','statusParamName[1]');
		paramValue.attr('name','statusParamValue[1]');
		
		cloned.removeClass('hidden');
		cloned.addClass('status-param-rows');

		cloned.attr("data-param-type",data.paramAction);
		
		//count=count-2;
		var rowID='param'+ '1';
		cloned.attr('id',rowID);
		$(_this.statusParamTableID).append(cloned);
	}		
	_this.populateInputInvalidResponseRow=function(data)
	{
		var cloned=$(_this.inputInvalidResponseTemplateID).clone(true);
		var count=$(_this.inputInvalidTableID).find('tr').length;
		

		var prompt=cloned.find('#inputInvalidPromptID').val(data.id);
		var text=cloned.find('#inputInvalidPromptText').val(data.text);
		var actions=cloned.find('input[name="inputInvalidAction"]');
		var gotoList=cloned.find('#inputInvalidGotoList');
         
      

		gotoList.html($("#inputInvalidRow").find("#inputInvalidGotoList").html());
		prompt.attr('name','inputInvalidPromptID['+count+']');
		text.attr('name','inputInvalidPromptText['+count+']');
		gotoList.attr('name','inputInvalidGotoList['+count+']');
		actions.each(function(){ $(this).attr('name','inputInvalidAction['+count+']');});
		//cloned.find('#invalidPromptText').val(data.action);
        if(data.file && data.file!=="filename" && data.file!==0 && typeof data.file!== 'undefined' && data.file!="undefined"){
         cloned.find(".input-group").replaceWith('<div class="input-group" style="display:inline-block"><span class="btn btn-xs actionbuttonsmedium btn-file lastaction"><i class="fa fa-upload file-uploaded"></i><input name="extraFileupload" id="extraFileupload" type="file" class="form-control"/></span></div> ');       
         cloned.find("#extraFilename").val(data.file);	
        }

		if(data.action=="RP")
		{
			cloned.find('#inputInvalidRepeat').prop('checked',true);
		}
		else 
		{
			cloned.find('#inputInvalidGoto').prop('checked',true);
			cloned.find('#inputInvalidGoto').prop('value',data.action);
			gotoList.val(data.action);
		}			
		cloned.removeClass('hidden');
		cloned.addClass('input-invalid-response-rows');

		count=count-2;
		var rowID='INV'+count;
		cloned.attr('id',rowID);
		$(_this.inputInvalidTableID).append(cloned);
	}

	_this.populateLegalInvalidResponseRow=function(data)
	{
		var cloned=$(_this.legalInvalidResponseTemplateID).clone(true);
		var count=$(_this.legalInvalidTableID).find('tr').length;
		

		var prompt=cloned.find('#legalInvalidPromptID').val(data.id);
		var text=cloned.find('#legalInvalidPromptText').val(data.text);
		var actions=cloned.find('input[name="legalInvalidAction"]');
		var gotoList=cloned.find('#legalInvalidGotoList');
         
      

		gotoList.html($("#legalInvalidRow").find("#legalInvalidGotoList").html());
		prompt.attr('name','legalInvalidPromptID['+count+']');
		text.attr('name','legalInvalidPromptText['+count+']');
		gotoList.attr('name','legalInvalidGotoList['+count+']');
		actions.each(function(){ $(this).attr('name','legalInvalidAction['+count+']');});
		//cloned.find('#invalidPromptText').val(data.action);
        if(data.file && data.file!=="filename" && data.file!==0 && typeof data.file!== 'undefined' && data.file!="undefined"){
         cloned.find(".input-group").replaceWith('<div class="input-group" style="display:inline-block"><span class="btn btn-xs actionbuttonsmedium btn-file lastaction"><i class="fa fa-upload file-uploaded"></i><input name="extraFileupload" id="extraFileupload" type="file" class="form-control"/></span></div> ');       
         cloned.find("#extraFilename").val(data.file);	
        }

		if(data.action=="RP")
		{
			cloned.find('#legalInvalidRepeat').prop('checked',true);
		}
		else 
		{
			cloned.find('#legalInvalidGoto').prop('checked',true);
			cloned.find('#legalInvalidGoto').prop('value',data.action);
			gotoList.val(data.action);
		}			
		cloned.removeClass('hidden');
		cloned.addClass('legal-invalid-response-rows');

		count=count-2;
		var rowID='INV'+count;
		cloned.attr('id',rowID);
		$(_this.legalInvalidTableID).append(cloned);
	}	

    _this.populateLanguageInvalidResponseRow=function(data)
	{
		//languageInvalidResponseTemplate
		var cloned=$(_this.languageInvalidResponseTemplateID).clone(true);
		var count=$(_this.languageInvalidTableID).find('tr').length;
		

		var prompt=cloned.find('#languageInvalidPromptID').val(data.id);
		var text=cloned.find('#languageInvalidPromptText').val(data.text);
		var actions=cloned.find('input[name="languageInvalidAction"]');
		var gotoList=cloned.find('#languageInvalidGotoList');
		
		gotoList.html($("#languageInvalidRow").find("#languageInvalidGotoList").html());
		
		prompt.attr('name','languageInvalidPromptID['+count+']');
		text.attr('name','languageInvalidPromptText['+count+']');
		gotoList.attr('name','languageInvalidGotoList['+count+']');
		actions.each(function(){ $(this).attr('name','languageInvalidAction['+count+']');});
        
        if(data.file && data.file!=="filename" && data.file!==0 && typeof data.file!== 'undefined' && data.file!="undefined"){
         cloned.find(".input-group").replaceWith('<div class="input-group" style="display:inline-block"><span class="btn btn-xs actionbuttonsmedium btn-file lastaction"><i class="fa fa-upload file-uploaded"></i><input name="extraFileupload" id="extraFileupload" type="file" class="form-control"/></span></div> ');       
         cloned.find("#extraFilename").val(data.file);	
        } 


		//cloned.find('#invalidPromptText').val(data.action);
		if(data.action=="RP")
		{
			cloned.find('#languageInvalidRepeat').prop('checked',true);
		}
		else 
		{
			cloned.find('#languageInvalidGoto').prop('checked',true);
			cloned.find('#languageInvalidGoto').prop('value',data.action);
			
			gotoList.val(data.action);
		}			
		cloned.removeClass('hidden');
		cloned.addClass('language-invalid-response-rows');

		count=count-2;
		var rowID='INV'+count;
		cloned.attr('id',rowID);
		$(_this.languageInvalidTableID).append(cloned);
	}	
    _this.populateConfirmationInvalidResponseRow=function(data)
	{
		var cloned=$(_this.confirmationInvalidResponseTemplateID).clone(true);
		var count=$(_this.confirmationInvalidTableID).find('tr').length;
		

		var prompt=cloned.find('#confirmationInvalidPromptID').val(data.id);
		var text=cloned.find('#confirmationInvalidPromptText').val(data.text);
		var actions=cloned.find('input[name="confirmationInvalidAction"]');
		var gotoList=cloned.find('#confirmationInvalidGotoList');
		
		gotoList.html($("#confirmationInvalidRow").find("#confirmationInvalidGotoList").html());
		
		prompt.attr('name','confirmationInvalidPromptID['+count+']');
		text.attr('name','confirmationInvalidPromptText['+count+']');
		gotoList.attr('name','confirmationInvalidGotoList['+count+']');
		actions.each(function(){ $(this).attr('name','confirmationInvalidAction['+count+']');});
        
        if(data.file && data.file!=="filename" && data.file!==0 && typeof data.file!== 'undefined' && data.file!="undefined"){
       //  cloned.find(".input-group").replaceWith('<div class="input-group" style="display:inline-block"><span class="btn btn-xs actionbuttonsmedium btn-file lastaction"><i class="fa fa-upload file-uploaded"></i><input name="extraFileupload" id="extraFileupload" type="file" class="form-control"/></span></div> ');       
         cloned.find("#extraFilename").val(data.file);	
        } 


		//cloned.find('#invalidPromptText').val(data.action);
		if(data.action=="RP")
		{
			cloned.find('#confirmationInvalidRepeat').prop('checked',true);
		}
		else 
		{
			cloned.find('#confirmationInvalidGoto').prop('checked',true);
			cloned.find('#confirmationInvalidGoto').prop('value',data.action);
			
			gotoList.val(data.action);
		}			
		cloned.removeClass('hidden');
		cloned.addClass('confirmation-invalid-response-rows');

		count=count-2;
		var rowID='INV'+count;
		cloned.attr('id',rowID);
		$(_this.confirmationInvalidTableID).append(cloned);
	}
	_this.delete=function(node)
	{

			_this.designer.tree.delete_node (node);
			if(Object.keys(_this.designer.tree._model.data).length<=1)
			{
				_this.designer.builder.Start="";
			}
			
			 if(node.id==_this.designer.builder.Start)
			{
				_this.designer.builder.Start="";
			}
			if(Object.keys(_this.designer.tree._model.data).length==2)
			{
				var n=_this.designer.tree._model.data["#"].children[0];
				_this.designer.builder.Start=n;
				n="#"+n;
				$(n).find(".ivr-start .fa").removeClass("fa-toggle-off");
				$(n).find(".ivr-start .fa").addClass("fa-toggle-on");
				
			}			

	}
	_this.add=function(node)
	{

		 var target="#"+node.id;
		
		var data=_this.designer.tree._model.data[node.id];
		var childrens=data.children;

		if(data.type=="menu" && childrens.length<=11)
		{
			
			_this.designer.create("keypress",target);
		}
		else if (data.type=="language" && childrens.length<5)
		{
			_this.designer.create("keypress",target);			
		}
	}
	
	$("#menuGrammer").on("change", function(){
		//alert("awais coding"); 
		var val=$(this).val();
		if(val == "Cust")
		{
			if($('#grammerText').length){
				//alert("Div2 exists");
				$('#grammerText').remove();
				//$(this).closest('div').find('input').remove();
				$(this).closest('div').append('<input type="text" placeholder="Grammer Text" class="form-control input-sm prompt_ID" name="grammerText" maxlength="5"  id="grammerText">');

			}else{
				//alert("Div2 does not exists");
				$(this).closest('div').append('<input type="text" placeholder="Grammer Text" class="form-control input-sm prompt_ID" name="grammerText" maxlength="5"  id="grammerText">');
			
			}
		}
		else
		{
			$(this).closest('div').find('input').remove();
		}	
	});
	
	$("#inputGrammer").on("change", function(){
		//alert("awais coding"); 
		var val=$(this).val();
		if(val == "Cust")
		{
			if($('#grammerText').length){
				//alert("Div2 exists");
				$('#grammerText').remove();
				//$(this).closest('div').find('input').remove();
				$(this).closest('div').append('<input type="text" placeholder="Grammer Text" class="form-control input-sm prompt_ID" name="grammerText" maxlength="5"  id="grammerText">');

			}else{
				//alert("Div2 does not exists");
				$(this).closest('div').append('<input type="text" placeholder="Grammer Text" class="form-control input-sm prompt_ID" name="grammerText" maxlength="5"  id="grammerText">');
			
			}
			//$(this).closest('div').append('<input type="text" placeholder="Grammer Text" class="form-control input-sm prompt_ID" name="grammerText" maxlength="5"  id="grammerText">');
			
		}
		else
		{
			$(this).closest('div').find('input').remove();
		}	
	});
}


