
function ScriptBuilder()
{
	var _this=this;
	this.Status="";
	this.IVR="";
	this.Name="";	
	this.Start="";
	this.errors=[];
	this.validate=function(node)
	{
		
		if(node.type=="transfer")
		{
			var identifiers=[/^XD/];
			var extra=treeDesigner.crud.getExtraList(identifiers,node.id);	
			if(extra.length>0 && (extra[0].id=='' || extra[0].id=='undefined'))
			{	
				this.errors.push({node:node.id,message:"Extension can not be null."});
			}
		}
	}
	this.build=function(model)
	{
		this.errors=[];
		var data=model.data;
		var ivr=new IVR();
		
		ivr.Status=this.Status;
		ivr.IVR=this.IVR;
		ivr.Name=this.Name;
		ivr.Start=this.Start;
		var validCommands=['prompt','menu','record','terminator','transfer','confirmation','if','input','function','uri','variable','legal','language','tts','setvalue','audio'];

		var node='';
		console.log(data);
		for (var nodeID in data) 
		{
			if(nodeID=="#")
				continue;
			node=data[nodeID];
			if(validCommands.indexOf(node.type)!=-1)
			{
				this.validate(node);
				var script=new Script();
				script.Type=node.type;
				script.ANSID=node.data.ANSID;
				script.PromptID=node.id;
				script.PromptText=node.data.PromptText;
				script.AnsTag=node.data.AnsTag;
				script.PromptFileName=node.data.PromptFileName;
				script.extra=node.data.extra;
				script.web=node.data.web;
			//	script.flow=node.data.flow;

				if(node.type=="menu" || node.type=="confirmation" || node.type=="language")
				{
					
					if( node.type=="language")
					{
						script.flow.push(node.data.flow[0]);
					}
					var childrens=node.children;
					for(var i=0; i<childrens.length; i++)
					{
						script.flow.push(data[childrens[i]].data.flow[0]);
					}
					
				}
				else 
				{
					script.flow=node.data.flow;
				}
				if(this.Start==node.id)
				{
					ivr.Script.splice(0,0,script);
				}
				else 
				{
					ivr.Script.push(script);
				}
			}

		}
    	
		return ivr;

	}
}    
