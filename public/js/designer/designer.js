
var treeDesigner=new TreeDesigner();

var tree=null;

var promptVerifier=null;
var ttsVerifier=null;
var menuVerifier=null;
var invalidVerifier=null;
var noresponseVerifier=null;
var functionVerifier=null;
var ifVerifier=null;
var inputParamVerifier=null;
var outputParamVerifier=null;
var variableVerifier=null;
var recordVerifier=null;
var setValueVerifier=null;
var audioVerifier=null;

var inputVerifier=null;
var transferVerifier=null;
var confirmationVerifier=null;
var uriVerifier=null;

var legalVerifier=null;
var legalNoResponseVerifier=null;
var legalInvalidVerifier=null;

var languageVerifier=null;
var languageNoResponseVerifier=null;
var languageInvalidVerifier=null;


  
	 
     $( "#command li" ).draggable({
	  revert: false,
	
      helper: "clone"
    });  
	
    $( "#ivrContainer" ).droppable({
	  revert: "invalid",

      drop: function( event, ui ) {
			var command=ui.draggable.attr("command");
			var target=$("#ivrContainer").attr("aria-activedescendant");

			
			if($(".jstree-container-ul li").length==0){
				treeDesigner.create(command,"#");
				
			}
			else 
			{
				var id="#"+target;
			
				treeDesigner.create(command,target);
			
			}
			scrollToBottom("ivrContainer");
      
      }
    });


	 enableDisableMenu("hide");
	 treeDesigner.load(false);
	
	 $( "#ivrContainer" ).bind("mouseover",function(){ 

	 $("#ivrContainer").attr("aria-activedescendant","#"); });


	
	

	function enableDisableMenu(action)
	{
	
		if(action=="show")
		{
			$("#command").removeClass("menuDisable");
			$("#command li").draggable( "enable" );
		}
		else
		{			
			$("#command").addClass("menuDisable");
			$( "#command li" ).draggable( "disable" );

		}
	}
	
	function closeModal(id)
	{

			alert("hi");
			console.log("hello");

                     //  $('label[class="error"]').remove();
			 promptVerifier.resetForm();
			 ttsVerifier.resetForm();
			 menuVerifier.resetForm();
			 invalidVerifier.resetForm();
			 noresponseVerifier.resetForm();
			 functionVerifier.resetForm();
			 ifVerifier.resetForm();
			 inputParamVerifier.resetForm();
			 outputParamVerifier.resetForm();
			 variableVerifier.resetForm();
			 recordVerifier.resetForm();
			 inputVerifier.resetForm();
			 legalVerifier.resetForm();
			 languageVerifier.resetForm();
			 transferVerifier.resetForm();
			 confirmationVerifier.resetForm();
			 uriVerifier.resetForm();
			 languageNoResponseVerifier.resetForm();
			 languageInvalidVerifier.resetForm();
			id="#"+id;
			$(id).modal("hide");
	}

	$("#ivrList").change(function ()
	{
		$("#tfnNumbers").html('');
		var appID=this.selectedOptions[0].value;

		console.log("appID : "+appID);
		loader("show");
		if(appID==0)
		{
				enableDisableMenu("hide");
				treeDesigner.load(null);
				loader("hide");
				return;
		}
		
		var url='/designer/getIvrScriptAjax';
		var request = $.ajax({
		  url: url,
		  data: { appID : appID },
		  dataType: "json",
		  success:function(data)
		  {
				if(data!=null)
				{
					treeDesigner.builder.Status=data.Status;
					treeDesigner.builder.IVR=data.IVR;
					treeDesigner.builder.Start=data.Start;
					treeDesigner.builder.Name=data.Name;
					if(data.Max!="" &&  data.Max!=0)
					{
						treeDesigner.nodeID= parseInt(data.Max);
						treeDesigner.nodeArray.push(treeDesigner.nodeID);
						treeDesigner.nodeID++;
					}
					var tfns=data.Tfns;
					if(tfns.length>0)
					{
							var numbers="";
							for(var i=0; i<tfns.length; i++)
							{
								numbers=numbers+" "+tfns[i].tfn+",";
							}
							$("#tfnNumbers").html(numbers.replace(/,+$/,''));
					}
					treeDesigner.load(data.Script);
				}
				loader("hide");
				enableDisableMenu("show");
				
		  },
		  error:function()
		  {
			loader("hide");
			alert('failed');
		  }
		});
		 
			
	});
	function moveRowUp(row)
	{

		row=$(row).parent().parent();
		var prev=row.prev();
		if(prev.length==1 && prev.is(":visible")==true)
		{
			row.prev().before(row);
		}		
	}
	function moveRowDown(row)
	{
		row=$(row).parent().parent();
		var next=row.next();
		if(next.length==1)
		{
			row.next().after(row);
		}
		
	}
	function scrollToBottom(ctl)
	{
		ctl="#"+ctl;
		$(ctl).scrollTop($(ctl)[0].scrollHeight);
	}
	//// validations

	function validateOperandSelection(left,right)
	{
		var functions=['getCurrentYear','getCurrentMonth','getCurrentDay','getCurrentHour','getCurrentMinute','getCurrentSeconds','getCurrentDayOfWeek'];
		if(functions.indexOf(left)!=-1 && functions.indexOf(right)!=-1)
		{
			return false;
		}else if(left=="variable" && right=="variable")
		{
			return false;
		}
		else if(left==right)
		{
			return false;
		}
		else 
		{
			return true;
		}
	}
	$("#ifLeftOperand").change(
				function()
				{
					if(this.value==""){
						$("#ifRightOperand").attr("disabled",true);
					}
					$("#ifVLI").addClass("hidden");
					$("#ifVLS").addClass("hidden");
					$("#ifOperator").attr("disabled",false);
					$("#ifRightOperand").attr("disabled",false);
				/*	if(!validateOperandSelection($("#ifLeftOperand").val(),$("#ifRightOperand").val()))
					{
							this.value="";
							$("#ifVLI").addClass("hidden");
							$("#ifVLS").addClass("hidden");
							return;
					} */
					if(this.value=="variableInteger")
					{
						// if($("#ifRightOperand").val()=="variable")
						// {
							// this.value="";
							// return;
						// }
						var options = $("#ifRightOperand");
                        var html='<option value="">Select Operand</option>';
					    html+='<option value="variableInteger">Variable Integer</option>';
					    html+='<option value="getCurrentANI">Current ANI</option>';
			            html+='<option value="getCurrentDNIS">Current DNIS</option>';			
						options.html(html);		
						$("#ifVLS").addClass("hidden");				
						$("#ifVLI").rules("remove");
						$("#ifVLI").removeClass("hidden");
						$("#ifVLI").rules("add",{required:true,digits: true});
						list=getOperandList();
			                $.each(list, function() {
				
								var text=this.id+':'+this.text;

								options.append($("<option />").val(this.id).text(text));
				
							});	
						addValidationOnVariable("ifRightOperand","ifVLI");						
					}
					else if(this.value=="variableString"){
						//$("#ifVLI").attr('name', "ifVLS");
						//$("#ifVLI").attr('id', "ifVLS");
						 $('#ifRightOperand option').remove();

                         var options = $("#ifRightOperand");
                         var html='<option value="">Select Operand</option>';
					     html+='<option value="variableString">Variable String</option>';			
						 options.html(html);
			             list=getOperandList();
			             $.each(list, function() {
				
								var text=this.id+':'+this.text;

								options.append($("<option />").val(this.id).text(text));
				
						});	
						$("#ifOperator").val("2");
						$("#ifOperator").attr("disabled",true);
						$("#ifVLI").addClass("hidden");
                        $("#ifVLS").rules("remove");
						$("#ifVLS").removeClass("hidden");
						$("#ifVLS").rules("add",{required:true,digits: true});
						addValidationOnVariable("ifRightOperand","ifVLS"); 
					}
					else if(this.value=="getCurrentYear" || this.value=="getCurrentMonth" || this.value=="getCurrentDay" || this.value=="getCurrentHour" || this.value=="getCurrentMinute" || this.value=="getCurrentSeconds" || this.value=="getCurrentDayOfWeek" || this.value=="getCurrentANI" || this.value=="getCurrentDNIS" || this.value=="isContain"){
                            $('#ifRightOperand option').remove();
                 
                            var options = $("#ifRightOperand");
                         	var html='<option value="">Select Operand</option>';
                         	 if(this.value=="isContain")
                            {  
                            	html+='<option value="variableString">Variable String</option>';
				                $("#ifVLI").addClass("hidden");
	                        	$("#ifVLS").rules("remove");
								$("#ifVLS").removeClass("hidden");
								$("#ifVLS").rules("add",{required:true});
			                }
			                else
			                {
			                	html+='<option value="variableInteger">Variable Integer</option>';
			                }
										
							options.html(html);
							if(this.value=="getCurrentANI" || this.value=="getCurrentDNIS" || this.value=="isContain")
							{	
								$("#ifOperator").val("2");
								$("#ifOperator").attr("disabled",true);
			                }
		

			                list=getOperandList();
			                $.each(list, function() {
				
								var text=this.id+':'+this.text;

								options.append($("<option />").val(this.id).text(text));
				
							});		
                          
                          
                          
					}
					else 
					{
						/*if($("#ifRightOperand").val()=="variable")
						{
							addValidationOnVariable("ifLeftOperand","ifVRI");
						}						
						$("#ifVLI").addClass("hidden");*/

			 $('#ifRightOperand option').remove();

            var options = $("#ifRightOperand");		
			//var list=_this.getOperandList();
			var list=getOperandList();
			
			var html='<option value="">Select Operand</option>';
			html+='<option value="variableInteger">Variable Integer</option>';
            html+='<option value="variableString">Variable String</option>';
			html+='<option value="getCurrentYear">Current year</option>';
			html+='<option value="getCurrentMonth">Current month</option>';
			html+='<option value="getCurrentDay">Current day</option>';
			html+='<option value="getCurrentHour">Current hour</option>';
			html+='<option value="getCurrentMinute">Current Minute(s)</option>';
			html+='<option value="getCurrentSeconds">Current Second(s)</option>';
			html+='<option value="getCurrentDayOfWeek">Current Day of Week</option>';
			html+='<option value="getCurrentANI">Current ANI</option>';
			html+='<option value="getCurrentDNIS">Current DNIS</option>';	
			options.html(html);
			
			$.each(list, function() {
				
				var text=this.id+':'+this.text;

					options.append($("<option />").val(this.id).text(text));
				
			});			
						
					}						
					

				}
			);
			
	function addValidationOnVariable(control,variable)
	{
		variable="#"+variable;
		control="#"+control;
		
		$(variable).rules("remove");
		if( $(control).val()=="getCurrentYear")
		{
			$(variable).rules("add",{required:true,digits:true,min:2000, maxlength:4});
		}
		else if ( $(control).val()=="getCurrentMonth")
		{
			$(variable).rules("add",{required:true,digits: true,min:1,maxlength:2});
		}
		else if ( $(control).val()=="getCurrentDay")
		{
			$(variable).rules("add",{required:true,digits: true,min:1,max:7,maxlength:1});
		}							
		else if ( $(control).val()=="getCurrentMonth")
		{
			$(variable).rules("add",{required:true,digits: true,min:1,max:12,maxlength:2});
		}
		else if ( $(control).val()=="getCurrentMinute")
		{
			$(variable).rules("add",{required:true,digits: true,min:1,max:30,maxlength:2});
		}	
		else if ( $(control).val()=="getCurrentSeconds")
		{
			$(variable).rules("add",{required:true,digits: true,min:1,max:60,maxlength:2});
		}
		else if ( $(control).val()=="getCurrentDayOfWeek")
		{
			$(variable).rules("add",{required:true,digits: true,min:1,max:7,maxlength:1});
		}	
		else if ( $(control).val()=="variableInteger")
		{
			$(variable).rules("add",{required:true,digits: true});	
		}
		else{
			$(variable).rules("add",{required:true});
		}
	}
	
	$("#ifRightOperand").change(
				function()
				{

					$("#ifVRI").addClass("hidden");
					$("#ifVRS").addClass("hidden");
					$("#ifOperator").attr("disabled",false);

			/*		if(!validateOperandSelection($("#ifLeftOperand").val(),$("#ifRightOperand").val()))
					{
							this.value="";
							$("#ifVRI").addClass("hidden");
							$("#ifVRS").addClass("hidden");
							return;
					} */					
					
					if(this.value=="variableInteger")
					{
						// if($("#ifLeftOperand").val()=="variable")
						// {
							// this.value="";
							// return;
						// }
						$("#ifVRS").addClass("hidden");
						$("#ifVRI").rules("remove");
						$("#ifVRI").removeClass("hidden");
						$("#ifVRI").rules("add",{required:true,digits: true});	
						addValidationOnVariable("ifLeftOperand","ifVRI");
					}
					else if(this.value=="variableString"){
					//	$("#ifVRI").attr('name', "ifVRS");
					//	$("#ifVRI").attr('id', "ifVRS");
					    $("#ifOperator").val("2");
						$("#ifOperator").attr("disabled",true);
					    $("#ifVRI").addClass("hidden");
                        $("#ifVRS").rules("remove");
						$("#ifVRS").removeClass("hidden");
						$("#ifVRS").rules("add",{required:true,digits: true});
						addValidationOnVariable("ifLeftOperand","ifVRS"); 
					}
					else if(this.value=="getCurrentANI" || this.value=="getCurrentDNIS" ){
					//	$("#ifVRI").attr('name', "ifVRS");
					//	$("#ifVRI").attr('id', "ifVRS");
					    $("#ifOperator").val("2");
						$("#ifOperator").attr("disabled",true);
					     
					}
					else 
					{
						if($("#ifLeftOperand").val()=="variable")
						{
							addValidationOnVariable("ifRightOperand","ifVLI");
						}
						$("#ifVRI").addClass("hidden");
					}
				
				}

					
	
				
			);				
	$("#transferXT").change(
				function()
				{
					$("#transferXD").rules("remove");
					$("#transferXD").remove();
				    

			    if(this.value=="CMD")
			        {
			            $("#transferOutDiv").append('<select class="form-control transferXT"  id="transferXD" name="transferXD" </select>');
			            options=$("#transferXD");
			            list=getOperandListTransfer();
			                 
						$.each(list, function() {							
						    var text=this.id+':'+this.text;
                            options.append($("<option />").val(this.id).text(text));
							
										});
			                 
			        }
        		else if(this.value=="TFN")
					{
						var r={required: true};
						$("#transferOutDiv").append('<input type="text" class="form-control transferXD" name="transferXD" maxlength="50" id="transferXD" placeholder="">');
						$("#transferXD").rules("add",r);	
					}
				else 
					{
						var r={required: true,telephoneextension:true};
						$("#transferOutDiv").append('<input type="text" class="form-control transferXD" name="transferXD" maxlength="50" id="transferXD" placeholder="">');
						$("#transferXD").rules("add",r);	
					}
				}
			);
    
    	$("#valType").change(
				function()
				{
					
					$("#setValueVAL").remove();
				    

			    if(this.value=="cmd")
			        {
			            $("#setValueDiv").append('<select class="form-control transferXT"  id="setValueVAL" name="setValueVAL" </select>');
			            options=$("#setValueVAL");
			            list=getOperandList();
			                 
						$.each(list, function() {							
						    var text=this.id+':'+this.text;
                            options.append($("<option />").val(this.id).text(text));
							
										});
			                 
			        }
				else if(this.value=="text")
					{
						var r={required: true};
						$("#setValueDiv").append('<input type="text" class="form-control transferXD" name="setValueVAL"  id="setValueVAL" placeholder="">');
						$("#setValueVAL").rules("add",r);	
					}


				}
			);



	$("#functionType").change(
				function()
				{
					
					$("#functionFromHour").rules("remove");
					$("#functionToHour").rules("remove");
					$("#functionFromMin").rules("remove");
					$("#functionToMin").rules("remove");
					
					$("#functionFromHour").rules("add",{required:true,min:0,max:23,digits: true,le:'#functionToHour'});
					$("#functionToHour").rules("add",{required:true,min:0,max:23,digits: true});		
					
					if(this.value==1 || this.value==3 || this.value==5)
					{
						$("#functionFromMin").rules("add",{required:true,min:0,max:59,digits: true,le:'#functionToMin'});
						$("#functionToMin").rules("add",{required:true,min:0,max:59,digits: true});	
				
					}

				}
			);	

function getOperandList()
	{
		var data=treeDesigner.tree._model.data;
		var list=[];

		for (var node in data) 
		{
			
			if(node=='#' || data[node].type=="keypress" || 
			 data[node].type=="if" ||
			//data[node].type=="record" || data[node].type=="confirmation" ||
			data[node].type=="prompt" || data[node].type=="transfer" || 
			data[node].type=="function" ||  data[node].type=="uri" || 
			data[node].type=="tts" || data[node].type=="setvalue" || data[node].type=="audio"
			)
			{
					continue;
			}
			
			var item={};
			item.id=data[node].id;
			item.text=data[node].data.PromptText;
			list.push(item);
		}	
		return list;

	}

function getOperandListTransfer()
	{
		var data=treeDesigner.tree._model.data;
		var list=[];

		for (var node in data) 
		{
		
			if(data[node].type=="input" || data[node].type=="variable")
			{	
				var item={};
				item.id=data[node].id;
				item.text=data[node].data.PromptText;
				list.push(item);
			}	
		}	
		return list;

	}



function appTypeChange(){
	var value=$("#appType").val();
	if(value=="C"){
		$("#appMode").prop('disabled', true);
		$("#appTel").prop('disabled', true);
		$("#mode").prop('disabled', true);
		$("#appState").prop('disabled', true);
		$("#listOrder").prop('disabled', true);
		$("#customText").prop('disabled', false);
	}
	else{    
		$("#appMode").prop('disabled', false);
		$("#appTel").prop('disabled', false);
		$("#mode").prop('disabled', false);
		$("#appState").prop('disabled', false);
		$("#listOrder").prop('disabled', true);
		$("#customText").prop('disabled', true);

	}
}

function appModeChange(){
	var value=$("#appMode").val();
	var valueAppType=$("#appType").val();
	if(value=="F" && valueAppType!="C"){
		$("#appMode").prop('disabled', false);
		$("#appTel").prop('disabled', false);
		$("#mode").prop('disabled', false);
		$("#listOrder").prop('disabled', true);
		$("#appState").prop('disabled', true);
		$("#customText").prop('disabled', true);
	}
	else if(value=="G" && valueAppType!="C"){    
		$("#appMode").prop('disabled', false);
		$("#appTel").prop('disabled', true);
		$("#mode").prop('disabled', true);
		$("#listOrder").prop('disabled', true);
		$("#appState").prop('disabled', false);	
		$("#customText").prop('disabled', true);

	}
	else if(value=="F" && valueAppType!="C"){
        $("#appMode").prop('disabled', false);
		$("#appTel").prop('disabled', false);
		$("#mode").prop('disabled', false);
		$("#appState").prop('disabled', false);
		$("#listOrder").prop('disabled', true);
		$("#customText").prop('disabled', true);

	}
}

function modeChange(){
	var value=$("#mode").val();
	var valueAppType=$("#appType").val();
	if(value=="B" && valueAppType!="C"){
		$("#appMode").prop('disabled', false);
		$("#appTel").prop('disabled', false);
		$("#mode").prop('disabled', false);
		$("#listOrder").prop('disabled', false);
		$("#appState").prop('disabled', true);
		$("#customText").prop('disabled', true);
	}
	else if((value=="S" || value=="R") && valueAppType!="C"){
        $("#appMode").prop('disabled', false);
		$("#appTel").prop('disabled', false);
		$("#mode").prop('disabled', false);
		$("#appState").prop('disabled', false);
		$("#listOrder").prop('disabled', true);
		$("#customText").prop('disabled', true);

	}
}



	$( document ).ready(function() {
		$(".toggle-btn").click(function(){
			$("#xmlEditor").toggle();
			if($('#xmlEditor').css('display') == 'block')
			{
			$('.toggle-btn').attr('value', 'Show IVR Designer');
			}else{
			$('.toggle-btn').attr('value', 'Show XML Editor');
			}
		});		
		 $(".toggleHeader").hide();
			$(".displayHeader").click(function(){
				$(".toggleHeader").toggle("fast");
			});		
			promptVerifier=$("#promptForm").validate({
												rules: 
													{
														promptText: 
																	{
																		required: true,
																		alphanumeric:true,
																		maxlength: 150
                                                                                                                                               
																	},
														promptID: 
																	{
																		required: true,
																		promptID:true,
																		checkDuplicatedPromptID:true,
																		maxlength: 5
                                                                                                                                               
																	}																	
													}
											});
			ttsVerifier=$("#ttsForm").validate({
												rules: 
													{
														ttsText: 
																	{
																		required: true,
																		alphanumeric:true,
																		maxlength: 150
                                                                                                                                               
																	},
														ttsVariable: 
																	{
																		required: true,
                                                                                                                                               
																	},
														ttsType: 
																	{
																		required: true,
                                                                                                                                               
																	},																	
																	
														ttsID: 
																	{
																		required: true,
																		promptID:true,
																		checkDuplicatedPromptID:true,
																		maxlength: 5
                                                                                                                                               
																	}																	
													}
											});											
			ifVerifier=$("#ifForm").validate({
												rules: 
													{
														ifText: 
																	{
																		required: true,
																		alphanumeric:true,
																		maxlength: 150
                                                                                                                                               
																	},
														ifID: 
																	{
																		required: true,
																		promptID:true,
																		checkDuplicatedPromptID:true,
																		maxlength: 5
                                                                                                                                               
																	},
														ifLeftOperand:
																	{
																		required:true
																	},	
														ifRightOperand:
																	{
																		required:true
																	}
																	
																	
													}
											});											
			functionVerifier=$("#functionForm").validate({
												rules: 
													{
														functionID: 
																	{
																		required: true,
																		promptID:true,
																		checkDuplicatedPromptID:true,
																		maxlength: 5
                                                                                                                                               
																	},
														 																				
													}
											}); 											
			recordVerifier=$("#recordForm").validate({
												rules: 
													{
														recordText: 
																	{
																		required: true,
																		alphanumeric:true,
																		maxlength: 150

																	},
														recordID: 
																	{
																		required: true,
																		promptID:true,
																		checkDuplicatedPromptID:true,
																		maxlength: 5
                                                                                                                                               
																	},
													}
											});	
			transferVerifier=$("#transferForm").validate({
												rules: 
													{
														transferXD:
														{
															required: true
														},
																	
														transferText: 
																	{
																		required: true,
																		alphanumeric:true,
																		maxlength: 150

																	},
														transferID: 
																	{
																		required: true,
																		promptID:true,
																		checkDuplicatedPromptID:true,
																		maxlength: 5
                                                                                                                                               
																	}
													}
											});													
                       menuVerifier=$("#menuForm").validate({
												rules: 
													{
														menuText: 
																	{
																		required: true,
                                                                                                                                                alphanumeric:true,
                                                                                                                                                maxlength: 150
																	},
														menuID: 
																	{
																		required: true,
																		promptID:true,
																		checkDuplicatedPromptID:true,
																		maxlength: 5
                                                                                                                                               
																	}
													}
											}); 
                       invalidVerifier= $("#invalidResponseForm").validate({
												rules: 
													{
														invalidPromptText: 
																	{
																		
                                                                        alphanumeric:true,                                                                        alphanumeric:true,
                                                                                                                                                maxlength: 150
																	}
													}
											});
                        noresponseVerifier=$("#noResponseForm").validate({
												rules: 
													{
														noResponsePromptText: 
																	{
																		alphanumeric:true,  
                                                                                                                                                alphanumeric:true,
                                                                                                                                                maxlength: 150
																	}
													}
											});  
						inputInvalidVerifier= $("#inputInvalidResponseForm").validate({
												rules: 
													{
														inputInvalidPromptText: 
																	{
																		alphanumeric:true , 
                                                                                                                                                alphanumeric:true,
                                                                                                                                                maxlength: 150
																	}
													}
											});
                        inputNoresponseVerifier=$("#inputNoResponseForm").validate({
												rules: 
													{
														inputNoResponsePromptText: 
																	{
																		alphanumeric:true , 
                                                                                                                                                alphanumeric:true,
                                                                                                                                                maxlength: 150
																	}
													}
											});    
						confirmationInvalidVerifier= $("#confirmationInvalidResponseForm").validate({
												rules: 
													{
														confirmationInvalidPromptText: 
																	{
																		alphanumeric:true , 
                                                                                                                                                alphanumeric:true,
                                                                                                                                                maxlength: 150
																	}
													}
											});
                        confirmationNoresponseVerifier=$("#confirmationNoResponseForm").validate({
												rules: 
													{
														confirmationNoResponsePromptText: 
																	{
																		alphanumeric:true,  
                                                                                                                                                alphanumeric:true,
                                                                                                                                                maxlength: 150
																	}
													}
											});
                        legalNoResponseVerifier=$("#legalNoResponseForm").validate({
												rules: 
													{
														legalNoResponsePromptText: 
																	{
																		alphanumeric:true , 
                                                                                                                                                alphanumeric:true,
                                                                                                                                                maxlength: 150
																	}
													}
											});	
                        languageNoResponseVerifier=$("#languageNoResponseForm").validate({
												rules: 
													{
														languageNoResponsePromptText: 
																	{
																		alphanumeric:true,  
                                                                                                                                                alphanumeric:true,
                                                                                                                                                maxlength: 150
																	}
													}
											});											
						legalInvalidVerifier= $("#legalInvalidResponseForm").validate({
												rules: 
													{
														legalInvalidPromptText: 
																	{
																		alphanumeric:true,  
                                                                                                                                                alphanumeric:true,
                                                                                                                                                maxlength: 150
																	}
													}
											});	
						languageInvalidVerifier= $("#languageInvalidResponseForm").validate({
												rules: 
													{
														languageInvalidPromptText: 
																	{
																		alphanumeric:true , 
                                                                                                                                                alphanumeric:true,
                                                                                                                                                maxlength: 150
																	}
													}
											});												
                        inputParamVerifier=$("#inputParamForm").validate({
												rules: 
													{
														inputParamNameHolder: 
																	{
																		required: true,
																		variableDeclaration: true  ,
																		checkDuplicateInputParamter: true,
																		maxlength: 50
																	},												
														inputParamValueHolder: 
														{
															required: true,

															maxlength: 50
														}
																	
													}
											});											
                         outputParamVerifier=$("#outputParamForm").validate({
												rules: 
													{
														outputParamHolder: 
																	{
																		required: true,
                                                                        variableDeclaration: true  ,
                                                                        checkDuplicateOutputParamter: true  ,
																		maxlength: 50
																	}
													}
											});
                         inputVerifier=$("#inputForm").validate({
												rules: 
													{
														inputText: 
																	{
																		required: true,
																		alphanumeric:true,
																		maxlength: 150
																	},
														inputID: 
																	{
																		required: true,
																		promptID:true,
																		checkDuplicatedPromptID:true,
																		maxlength: 5
                                                                                                                                               
																	}
													}
											});
                         legalVerifier=$("#legalForm").validate({
												rules: 
													{
														legalText: 
																	{
																		required: true,
																		alphanumeric:true,
																		maxlength: 150
																	},
														legalID: 
																	{
																		required: true,
																		promptID:true,
																		checkDuplicatedPromptID:true,
																		maxlength: 5
                                                                                                                                               
																	}
													}
											});	
                         languageVerifier=$("#languageForm").validate({
												rules: 
													{
														languageText: 
																	{
																		required: true,
																		alphanumeric:true,
																		maxlength: 150
																	},
														languageID: 
																	{
																		required: true,
																		promptID:true,
																		checkDuplicatedPromptID:true,
																		maxlength: 5
                                                                                                                                               
																	}
													}
											});												
                         confirmationVerifier=$("#confirmationForm").validate({
												rules: 
													{
														confirmationText: 
																	{
																		required: true,
																		alphanumeric:true,
																		maxlength: 150
																	},
														confirmationID: 
																	{
																		required: true,
																		promptID:true,
																		checkDuplicatedPromptID:true,
																		maxlength: 5
                                                                                                                                               
																	}
													}
											});	
                         uriVerifier=$("#uriForm").validate({
												rules: 
													{
														uriText: 
																	{
																		required: true,
																		
																		maxlength: 150
																	},
														uriID: 
																	{
																		required: true,
																		promptID:true,
																		checkDuplicatedPromptID:true,
																		maxlength: 5
                                                                                                                                               
																	}
													}
											});												
                         variableVerifier=$("#variableForm").validate({
												rules: 
													{
														variableText: 
																	{
																		required: true,
																		maxlength: 150
																	},
															variableValue: 
																	{
																		required: true,
																		maxlength: 50
																	},
														variableID: 
																	{
																		required: true,
																		promptID:true,
																		checkDuplicatedPromptID:true,
																		maxlength: 5
                                                                                                                                               
																	}																
													}
											});	

                             
                             setValueVerifier=$("#setValueForm").validate({
												rules: 
													{
														setValueText: 
																	{
																		required: true,
                                                                        alphanumeric: true  ,

																		maxlength: 150
																	},
														setValueVAL: 
																	{
																		required: true,

																		maxlength: 50
																	},
														setValueID: 
																	{
																		required: true,
																		promptID:true,
																		checkDuplicatedPromptID:true,
																		maxlength: 5
                                                                                                                                               
																	}																
													}
											});	
                             audioVerifier=$("#audioForm").validate({
												rules: 
													{
														audioText: 
																	{
																		required: true,
                                                                        alphanumeric: true  ,

																		maxlength: 150
																	},
														audioID: 
																	{
																		required: true,
																		promptID:true,
																		checkDuplicatedPromptID:true,
																		maxlength: 5
                                                                                                                                               
																	}																
													}
											});

							$("#confirmationASR").change(function(){
								if($(this).val()=="FALSE")
								{
									$("#confirmationGrammer").attr('disabled',true);
								}
								else 
								{
									$("#confirmationGrammer").attr('disabled',false);
								}
									
							});
							
							$("#inputASR").change(function(){
								if($(this).val()=="FALSE")
								{
									$("#inputGrammer").attr('disabled',true);
								}
								else 
								{
									$("#inputGrammer").attr('disabled',false);
								}
									
							});	

							$("#menuASR").change(function(){
								if($(this).val()=="FALSE")
								{
									$("#menuGrammer").attr('disabled',true);
								}
								else 
								{
									$("#menuGrammer").attr('disabled',false);
								}
									
							});								
  
				$(".trigger").click(function(){
				     $("div.menu").toggleClass("showHideMenu");
				});  
	});  


  
            // (function($){
                // $(window).load(function(){
                    // $(".content").mCustomScrollbar(); 
                // });
                // new UISearch( document.getElementById( 'sb-search' ) );
            // })(jQuery);
      

// Created By M Awais Ullah 24-07-2018

function copyIVR()
{
	 var appID=$("#ivrList option:selected").val();
    console.log(appID);
		//loader("show");
		if(appID==0)
		{
				return;
		}
	console.log("here");
    clearCopyIVRModal();
    $("#ivrTitle").html("Copy IVR");
    $("#copyIvrId").val(appID);
    $("#copyIVRPopUp").modal('show');


   
}


	
	function clearCopyIVRModal()
{
	$("#copyIVRPopupName").val("");
	$("#copyIvrId").val("");
	
	//$('#appnameloader').html('');
	//$("#applicationModalLabel").html("Add Application");
}


$(document).ready(function(){
        $("#inputModal").on('hide.bs.modal', function () {
            alert('The input is about to be hidden.');
            $('#inputGrammer').prop('disabled', true);
			$('#grammerText').remove();
    });

      $("#menuModal").on('hide.bs.modal', function () {
            alert('The menu is about to be hidden.');
            $('#menuGrammer').prop('disabled', true);
            $('#grammerText').remove();
    });
});