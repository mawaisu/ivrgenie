var extraRow, form;
var legalList;
var legalTransfer;
var options = { 
    beforeSubmit: beforeSubmit, 
    success: afterSuccess,
    resetForm: true 
};

$('#MyUploadForm').submit(function() {

    $(this).ajaxSubmit(options);
    return false;
});



function afterSuccess(output) {

    $('#loading-img1').hide();
    $('#submit-btn1').show();
    var promptID = $('#filePromptId').val();
    var data = treeDesigner.tree._model.data[promptID];
    data.data.PromptFileName = output;
    promptTreeID = "#" + promptID;
    $(promptTreeID).find(".ivr-upload").addClass("ivr-uploaded btn btn-xs actionbuttons");
     swal({
            title: 'Successfull',
            text: 'File uploaded successfully',
            type: 'success',
            showCancelButton: false,
            confirmButtonText: 'OK'
        });
     $("#fileUploadModal").modal("hide");
     $("#fileUploadModal").find(".file-input-label").text(" ");
     
}


function beforeSubmit() {


    if (window.File && window.FileReader && window.FileList && window.Blob) {

        if (!$('#file_upload').val()) {
             swal({
            title: 'No File',
            text: 'Please upload a gsm file',
            type: 'warning',
            showCancelButton: false
        });
            return false;
        }

        var fsize = $('#file_upload')[0].files[0].size;
        var ftype = $('#file_upload')[0].files[0].type;
        var filename=$('#file_upload')[0].files[0].name;
        var fext=filename.split('.').pop(); 
        
        if(fext!="gsm"){
             swal({
            title: 'Unsupported File Type',
            text: 'Please upload gsm file only',
            type: 'warning',
            showCancelButton: false
        });
            return false;
        }


        $('#submit-btn1').hide();
        $('#loading-img1').show();

    }

}




var extraOptions = {
   
    beforeSubmit: extraBeforeSubmit, 
    success: extraAfterSuccess, // post-submit callback 
    resetForm: true // reset the form after successful submit 
};

$('#myUploadExtraForm').submit(function() {

    $(this).ajaxSubmit(extraOptions);
    return false;
});
$('#myConfirmationUploadExtraForm').submit(function() {

    $(this).ajaxSubmit(extraOptions);
    return false;
});
$('#myInputUploadExtraForm').submit(function() {

    $(this).ajaxSubmit(extraOptions);
    return false;
});
$('#myLanguageUploadExtraForm').submit(function() {

    $(this).ajaxSubmit(extraOptions);
    return false;
});
$('#myLegalUploadExtraForm').submit(function() {

    $(this).ajaxSubmit(extraOptions);
    return false;
});

$('#myLegalUploadPriorForm').submit(function() {

    $(this).ajaxSubmit(extraOptions);
    return false;
});

function extraBeforeSubmit(obj) {

    if (window.File && window.FileReader && window.FileList && window.Blob) {
        if (!$(form).find('#extraFile_upload').val()) {
             swal({
            title: 'No File',
            text: 'Please upload a file first',
            type: 'warning',
            showCancelButton: false
        });
            return false;
        }

        $(form).find('#submit-btn').hide();
        $(form).find('#loading-img').show();

    }

}

function extraAfterSuccess(output) {
    $(form).find('#loading-img').hide();
    $(form).find('#submit-btn').show();
    //hide submit button
    if(legalList || legalTransfer)
    {
         if(legalList)
         {
            $("#PLAYPRIORLIST").val(output);
            $("#legalPlayPriorListBtn").removeClass();
            $("#legalPlayPriorListBtn").addClass("ivr-uploaded btn btn-xs actionbuttons");
         }
         else {
            $("#PLAYPRIORTRANSFER").val(output);
            $("#legalPlayPriorTransferBtn").removeClass();
            $("#legalPlayPriorTransferBtn").addClass("ivr-uploaded btn btn-xs actionbuttons");    
          } 
    }
    else
    {
        $(extraRow).find("#extraFilename").val(output);
    }    
             swal({
            title: 'File Uploaded',
            text: 'Congtulations!!!File uploaded successfully',
            type: 'success',
            showCancelButton: false
        });
    $(form).hide();         
}

function extraFileUploadFormOpener(obj) {
    var promptID;
    var mainObj = $(obj).closest(".modal.fade");
    var modalID = $(mainObj).attr("id");
    var row = $(obj).closest("tr");
    extraRow = row;

    if (modalID == "menuModal") {
        form = $(mainObj).find('#myUploadExtraForm');
        promptID = $("#menuID").val();

    } else if (modalID == "confirmationModal") {
        form = $(mainObj).find('#myConfirmationUploadExtraForm');
        promptID = $("#confirmationID").val();
    } else if (modalID == "inputModal") {
        form = $(mainObj).find('#myInputUploadExtraForm');
        promptID = $("#inputID").val();
    }
    else if(modalID=="languageModal"){
        form = $(mainObj).find('#myLanguageUploadExtraForm');
        promptID = $("#languageID").val();
    }
    else if(modalID=="legalModal"){
        form = $(mainObj).find('#myLegalUploadExtraForm');
        promptID = $("#legalID").val();
    }
    $(form).toggle();
    var filePromptId = $(row).find(".form-control").val();
    var fileAppId = treeDesigner.builder.IVR;

    $(form).find("#extraFileAppId").val(fileAppId);
    $(form).find("#extraFilePromptId").val(filePromptId);
    $(form).find("#extraOwnerPromptId").val(promptID);

}


function extraLegalFileUploadFormOpener(obj) {
    legalList=false;
    legalTransfer=false;
    var promptID;
    var filePromptId;
    var mainObj = $(obj).closest(".modal.fade");
    var modalID = $(mainObj).attr("id");
    form = $(mainObj).find('#myLegalUploadPriorForm');
    promptID = $("#legalID").val();
    
    $(form).toggle();
    if(obj.id=="legalPlayPriorListBtn")
    {    
       filePromptId = "PLAYPRIORLIST";
       legalList=true;
       legalTransfer=false;
    }
    else
    {
       filePromptId = "PLAYPRIORTRANSFER";
       legalList=false;
       legalTransfer=true;   
    }    
    var fileAppId = treeDesigner.builder.IVR;
     
    $(form).find("#extraFileAppId").val(fileAppId);
    $(form).find("#extraFilePromptId").val(filePromptId);
    $(form).find("#extraOwnerPromptId").val(promptID);

}

