

function TreeDesigner()
{
	
	var _this=this;
	_this.treeID="#ivrContainer";

	_this.tree=null;
	_this.nodeID=1000;
	_this.nodeArray=[_this.nodeID];
	_this.responseID=8000;
	_this.builder=new ScriptBuilder();
	_this.crud=new CRUD(_this);
	
	_this.create=function(command,target)
	{
		var node=_this.getNode(command);
		if(node==null)
			return;
		var inst = $.jstree.reference(target);
		var obj = inst.get_node(target);


		var output=inst.create_node(obj,node, "last", function (newNode) {

		if(newNode.type=="menu")
		{

			_this.create("keypress",newNode.id);
			
		}
		else if(newNode.type=="confirmation"  || newNode.type=="language" )
		{
			_this.create("keypress",newNode.id);
			_this.create("keypress",newNode.id);
		}
         
			$(_this.treeID).jstree(true).open_node(obj);
		});

		
		
	}
	
	_this.getNextKeyPress=function(node)
	{
		var data=_this.tree._model.data[node.parent];
		var childrens=data.children;
		var existingKeypress=[];
		var list=[];
		if(childrens.length>0)
		{
			for(var i=0; i<childrens.length; i++)
			{
				var nodeID=childrens[i];
				var keypressData=_this.tree._model.data[nodeID].data;
				var flow=keypressData.flow[0];
				existingKeypress.push(flow.Value);
			}
		}
		var key="";
		for(var v=0; v<=11; v++)
		{
			key=v;
			if(key==10)
			{	
				key="*";
			}
			if(key==11)
			{
			    key="#";	
			}	
			if(existingKeypress.indexOf(key.toString())==-1 )
			{
				list.push(key);
			}
		}
		
		list.sort(function(a,b){return a-b;});
		list.reverse();
		key=list.pop();
		if(key=="#" && list.length==0)
		{
			return key;
		}
		else if (key=="#")
		{
			    key=list.pop();
				if(key=="*" && list.length==0)
				{
					return key;
				}
				else if (key=="*")
				{
					return list.pop();
				}
				return key;
		}
		

			
		return list.pop();
		
	}
	_this.getNextLanguageKeyPress=function(node)
	{
		var data=_this.tree._model.data[node.parent];
		var childrens=data.children;
		var existingKeypress=[];
		var list=[];
		if(childrens.length>0)
		{
			for(var i=0; i<childrens.length; i++)
			{
				var nodeID=childrens[i];
				var keypressData=_this.tree._model.data[nodeID].data;
				var flow=keypressData.flow[0];
				existingKeypress.push(flow.Value);
			}
		}
		var key="";
		for(var v=0; v<=5; v++)
		{
			key=v;
			if(key==10)
				key="*";

			if(key==5)
				key=7; 
			
			if(existingKeypress.indexOf(key.toString())==-1 )
			{
				list.push(key);
			}
		}
		
		list.sort(function(a,b){return a-b;});
		list.reverse();
		key=list.pop();
		if(key=="*" && list.length==0)
		{
			return key;
		}else if (key=="*")
		{
			return list.pop();
		}
			
		return list.pop();
		
	}	
	_this.getNode=function(command)
	{
		var node=null;
		var script=new Script();
		script.flow.push(new Flow());
		script.extra.push(new Extra());
		switch(command) 
		{
			
			case "prompt":
				script.Type="prompt";
				script.PromptText="Play";
				node={type:"prompt",text:"Play",data:script};
			break;
			case "tts":
				script.Type="tts";
				script.PromptText="TTS";
				node={type:"tts",text:"TTS",data:script};
			break;			
			case "variable":
				script.Type="variable";
				script.PromptText="Variable";
				node={type:"variable",text:"Variable",data:script};
			break;			
			case "function":
				script.Type="function";
				script.PromptText="Is Bussiness Hour";
				var success=new Flow();
				success.Value="1";
				script.flow.push(success);				
				node={type:"function",text:"Is Bussiness Hour",data:script};
			break;
			case "setvalue":
			    script.Type="setvalue";
				script.PromptText="setvalue";			
				node={type:"setvalue",text:"Set value to var/command",data:script}  
            break; 
            case "audio":
			    script.Type="audio";
				script.PromptText="audio";			
				node={type:"audio",text:"audio",data:script}  
            break; 
			case "if":
				script.Type="if";
				script.PromptText="IF";
				
				script.flow[0].Value=0;
				
				var success=new Flow();
				success.Value="1";
				script.flow.push(success);		
				
				var con=new Extra();
				con.IDENTIFIER="CON";
				con.VALUE="2";
				
				var left=new Extra();
				left.IDENTIFIER="FLI";
				left.VALUE="getCurrentHour";

				var right=new Extra();
				right.IDENTIFIER="VRI";
				right.VALUE="10";	
				script.extra.pop();
				script.extra.push(con);	
				script.extra.push(left);	
				script.extra.push(right);	
				
				node={type:"if",text:"IF",data:script};
			break;				
			case "keypress":
				script.Type="keypress";
				
				node={type:"keypress",text:"Keypress",data:script};
			break;
			case "uri":
				script.Type="uri";
				script.PromptText="www.webservice.com";
				

				
				var timeout=new Extra();
				timeout.IDENTIFIER="TO";
				timeout.VALUE="3000";
				script.extra.push(timeout);
				
				var invalid=new Extra();
				invalid.IDENTIFIER="INV1";
				invalid.VALUE=_this.responseID + "|That was an invalid selection please try again|RP";
				script.extra.push(invalid);				
				_this.responseID++;
				var noresponse=new Extra();
				noresponse.IDENTIFIER="NR1";
				noresponse.VALUE=_this.responseID + "|I am sorry you are having difficulties please try call your later|RP";
				script.extra.push(noresponse);	
				_this.responseID++;
				node={type:"uri",text:"www.webservice.com",data:script};
			break;			
			case "menu":
				script.Type="menu";
				script.PromptText="Menu";
				

				
				var timeout=new Extra();
				timeout.IDENTIFIER="TO";
				timeout.VALUE="3000";
				script.extra.push(timeout);
				
				var invalid=new Extra();
				invalid.IDENTIFIER="INV1";
				invalid.VALUE=_this.responseID + "|That was an invalid selection please try again|RP";
				script.extra.push(invalid);				
				_this.responseID++;
				var noresponse=new Extra();
				noresponse.IDENTIFIER="NR1";
				noresponse.VALUE=_this.responseID + "|I am sorry you are having difficulties please try call your later|RP";
				script.extra.push(noresponse);	
				_this.responseID++;
				node={type:"menu",text:"Menu",data:script};
			break;
			case "transfer":
				script.Type="transfer";
				script.PromptText="Transfer";
				
				var A=new Flow();
				A.Value="A";
				script.flow.push(A);
				
				var B=new Flow();
				B.Value="B"
				script.flow.push(B);
				
				var C=new Flow();
				C.Value="*";
				script.flow.push(C);
				
				var N=new Flow();
				N.Value="N";
				script.flow.push(N);
				
				var XT=new Extra();
				XT.IDENTIFIER="XT";
				XT.VALUE="TFN";
				script.extra.push(XT);				
				
				var BR=new Extra();
				BR.IDENTIFIER="BR";
				BR.VALUE="TRUE";
				script.extra.push(BR);	

				var XD=new Extra();
				XD.IDENTIFIER="XD";
				XD.VALUE="";
				script.extra.push(XD);	

			
				node={type:"transfer",text:"Transfer",data:script};
			break;
			case "record":
				script.Type="record";
				script.PromptText="Record";
				
				var ML=new Extra();
				ML.IDENTIFIER="ML";
				ML.VALUE="30000";
				script.extra.push(ML);				
				
				var BR=new Extra();
				BR.IDENTIFIER="BR";
				BR.VALUE="TRUE";
				script.extra.push(ML);	

				var IS=new Extra();
				IS.IDENTIFIER="IS";
				IS.VALUE="5";
				script.extra.push(IS);	

				var BP=new Extra();
				BP.IDENTIFIER="BP";
				BP.VALUE="1";
				script.extra.push(BP);	

				var PB=new Extra();
				PB.IDENTIFIER="PB";
				PB.VALUE="0";
				script.extra.push(PB);	
				
				node={type:"record",text:"Record",data:script};
			break;
			
			case "terminator":
				script.Type="terminator";
				script.PromptText="terminator";
				
				var terminatorFlow=new Flow();
				script.flow.push(terminatorFlow);
				
				node={type:"terminator",text:"terminator",data:script};
			break;
			
			case "language":
				script.Type="language";
				script.PromptText="Language";
				

				
				var timeout=new Extra();
				timeout.IDENTIFIER="TO";
				timeout.VALUE="3000";
				script.extra.push(timeout);
				
				var invalid=new Extra();
				invalid.IDENTIFIER="INV1";
				invalid.VALUE=_this.responseID + "|That was an invalid selection please try again|RP";
				script.extra.push(invalid);				
				_this.responseID++;
				var noresponse=new Extra();
				noresponse.IDENTIFIER="NR1";
				noresponse.VALUE=_this.responseID + "|I am sorry you are having difficulties please try call your later|RP";
				script.extra.push(noresponse);	
				_this.responseID++;
				node={type:"language",text:"Language",data:script};
			break;			
			case "confirmation":
			    script.PromptText="Confirmation";
				script.Type="confirmation";
                
				script.flow[0].Value="1";
				

                var k2=new Flow();
				k2.Value="2";
				script.flow.push(k2);
                                
                var timeout=new Extra();
				timeout.IDENTIFIER="TO";
				timeout.VALUE="3000";
				script.extra.push(timeout);
				
				var invalid=new Extra();
				invalid.IDENTIFIER="INV1";
				invalid.VALUE=_this.responseID + "|That was an invalid selection please try again|RP";
				script.extra.push(invalid);				
				_this.responseID++;
				var noresponse=new Extra();
				noresponse.IDENTIFIER="NR1";
				noresponse.VALUE=_this.responseID + "|I am sorry you are having difficulties please try call your later|RP";
				script.extra.push(noresponse);	
				_this.responseID++;
				node={type:"confirmation",text:"Confirmation",data:script};
			break;
				
			case "input":
			    script.PromptText="Input";
				script.Type="input";
                
				var timeout=new Extra();
				timeout.IDENTIFIER="TO";
				timeout.VALUE="3000";
				script.extra.push(timeout);
				
				var invalid=new Extra();
				invalid.IDENTIFIER="INV1";
				invalid.VALUE=_this.responseID + "|That was an invalid selection please try again|RP";
				script.extra.push(invalid);				
				_this.responseID++;
				var noresponse=new Extra();
				noresponse.IDENTIFIER="NR1";
				noresponse.VALUE=_this.responseID + "|I am sorry you are having difficulties please try call your later|RP";
				script.extra.push(noresponse);	
				_this.responseID++;
				node={type:"input",text:"Input",data:script};
			break;
			case "legal":
			    script.PromptText="Legal";
				script.Type="legal";
                
				var timeout=new Extra();
				timeout.IDENTIFIER="TO";
				timeout.VALUE="3000";
				script.extra.push(timeout);

				var ppl=new Extra();
				ppl.IDENTIFIER="PLAYPRIORLIST";
				ppl.VALUE="filename";
				script.extra.push(ppl);

				var ppt=new Extra();
				ppt.IDENTIFIER="PLAYPRIORTRANSFER";
				ppt.VALUE="filename";
				script.extra.push(ppt);
				
				var invalid=new Extra();
				invalid.IDENTIFIER="INV1";
				invalid.VALUE=_this.responseID + "|That was an invalid selection please try again|RP";
				script.extra.push(invalid);				
				_this.responseID++;
				var noresponse=new Extra();
				noresponse.IDENTIFIER="NR1";
				noresponse.VALUE=_this.responseID + "|I am sorry you are having difficulties please try call your later|RP";
				script.extra.push(noresponse);	
				_this.responseID++;
				node={type:"legal",text:"Legal",data:script};
			break;			
			default:
						
			
		} 		
		return node;
	}
	_this.rules=function(from,to)
	{
		
		
         
			if(from=="keypress" && (to!="menu" && to!="confirmation" && to!="language") )
			{
				return false;
			}
			else if((from=="variable" || from=="prompt" || from=="menu" || from=="transfer" || from=="record"|| from=="terminator" || from=="confirmation" || from=="input" || from=="function" || from=="if" || from=="uri" || from=="legal" || from=="language" || from=="tts" || from=="setvalue" || from=="audio") && to=="#")
			{
				return true;
			}else  if(from=="keypress" && (to=="menu" || to=="confirmation" || to=="input" || to=="legal" || to=="language"))
			{
                                
				return true;
                               
			}
			
			return false;
	}
	_this.moveOnNode=function(event,node)
	{
		var id=event.target.getAttribute('data');		
		if(id!='' && id!=0)
		{
			event.stopPropagation();
			$(".jstree-anchor").removeClass("selected");
			_this.tree.deselect_all();
			_this.tree.select_node(id);
			id="#"+id;
			$(id).find(".jstree-anchor").addClass("selected");
		
		}
		else 
		{
			$(".jstree-anchor").removeClass("selected");
		}
		
	}

	_this.addButtons=function()
	{
		
		
	
		$.jstree.plugins.buttons = function (options, parent) {

			this.redraw_node = function(obj, deep, callback) {
			
				obj = parent.redraw_node.call(this, obj, deep, callback);
				if(obj) {

					// 1 - Delete button
					var deleteBtn = document.createElement('button');
					deleteBtn.className = "ivr-delete btn btn-xs actionbuttons";
					deleteBtn.setAttribute('data-type','warning');		
					deleteBtn.setAttribute('title','Delete!');		
					deleteBtn.setAttribute('data-text','Are you sure, you want to delete this record');		
					deleteBtn.setAttribute('data-confirm-text','Yes');		
					deleteBtn.setAttribute('data-success-text','Your record has been deleted!');		
					deleteBtn.innerHTML = "<i class='fa fa-times'></i>";	
					
					// 2 - Edit button
					var editBtn=document.createElement('button');
					editBtn.className = "ivr-edit btn btn-xs actionbuttons";
					editBtn.setAttribute('title','Edit');	
					editBtn.innerHTML = "<i class='fa fa-pencil'></i>";	
					
					// 3 - Add button
					var addBtn=document.createElement('button');
					addBtn.className = "ivr-add btn btn-xs actionbuttons";
					addBtn.setAttribute('title','Add');
					addBtn.innerHTML = "<i class='fa fa-plus'></i>";
					
					// 4 - Start button
					var startBtn = document.createElement('button');
					startBtn.className = "ivr-start btn btn-xs actionbuttons";
					startBtn.setAttribute('title','Start');	
					
						if(_this.builder.Start==obj.id)
						{
							startBtn.innerHTML = "<i class='fa fa-toggle-on'></i>";
						}
						else
						{
							startBtn.innerHTML = "<i class='fa fa-toggle-off'></i>";
						}
					
					

					// 5 - Upload button
					var data=_this.tree._model.data[obj.id];
                    var uploadBtn=document.createElement('button'); 
					
					if(data.data.PromptFileName=="0"){
						uploadBtn.className = "ivr-upload btn btn-xs actionbuttons";							
				    }
				    else{
				    	uploadBtn.className = "ivr-uploaded btn btn-xs actionbuttons";
				    }
				    uploadBtn.innerHTML = "<i class='fa fa-upload'></i>";
					
					if(data.data && data.data.flow && data.data.flow[0] && data.type!="menu" && data.type!="confirmation" && data.type!="language")
					{
						var link = document.createElement('a');
						link.className = "node-sub-link";	
						link.setAttribute('data',data.data.flow[0].NextANSID);
						var linkText="";
						if(data.data.flow[0].NextType=='hangup' || data.data.flow[0].NextANSID==0)
						{
							link.className=link.className+ " ivr-sub-hangup";
						}
						else
						{
							link.className=link.className+ " ivr-sub-goto";
							var parentNode=_this.tree._model.data[obj.id].parent;
							var pData=_this.tree._model.data[parentNode];							
							if(pData.type=="language" )
							{
							  if(data.data.flow[0].NextANSID==7)
							  {	
								linkText=_this.crud.langList[4].text;
							  } 
							  else
							  {
							  	linkText=_this.crud.langList[data.data.flow[0].NextANSID-1].text;
							  }	
							}
							else 
							{
								linkText=data.data.flow[0].NextANSID;
							}
						}
						link.innerHTML=linkText;
						obj.childNodes[2].appendChild(link);
						link.onclick=function(event){_this.moveOnNode(event,obj);};
					}
               
                                                        
							
					if(_this.tree._model.data[_this.tree._model.data[obj.id].parent].type!="confirmation")
					{
						obj.childNodes[2].appendChild(deleteBtn);
					}

                    if(data.type!="keypress" && data.type!="variable")
					{
						obj.childNodes[2].appendChild(uploadBtn);
						uploadBtn.onclick=function(event)
				       	{
						_this.crud.uploadFile(obj);
						//_this.edit(obj);
					   };
						obj.childNodes[2].appendChild(startBtn);
						startBtn.onclick=function(event)
						{
							_this.builder.Start=obj.id;
							$(".ivr-start .fa").each(function(){ $(this).removeClass("fa-toggle-on"); $(this).addClass("fa-toggle-off"); });
							$(this).find(".fa").addClass("fa-toggle-on");
						}						
					}
					obj.childNodes[2].appendChild(editBtn);
			        
					
					
					//obj.insertBefore(tmp, obj.childNodes[2]);

					if(data.data && (data.type=="menu" || data.type=="language"))
					{
						obj.childNodes[2].appendChild(addBtn);
						addBtn.onclick=function(event){_this.crud.add(obj);};
					}
					deleteBtn.onclick=function(event){	
							var parent=_this.tree._model.data[obj.id].parent;
							var data=_this.tree._model.data[parent];
							if(data.type=="menu" || data.type=="language")
							{
								if(data.children.length==1)
								{
									
									swal(
											{
												title: "Delete",
												text: capitalizeFirstLetter(data.type)+" must have atleast one keypress",
												type: 'error',
												confirmButtonClass: "btn-danger",
												confirmButtonColor: "#DD6B55",
												closeOnConfirm: true
											},
											function()
											{
												
												swal.disableButtons();
											}
											);									
								}
								else 
								{
									messageBox(deleteBtn,function(){_this.crud.delete(obj);});
								}
							}
							else 
							{
								messageBox(deleteBtn,function(){_this.crud.delete(obj);});
							}					
						
					};
					editBtn.onclick=function(event)
					{
						_this.crud.edit(obj);
						//_this.edit(obj);
					};
					


					
					
				}
				return obj;
			
			};
				

		}
	

		
	}

	_this.load=function(data)
	{

		if(_this.tree!=null)
		{
			_this.tree.settings.core.data =data;
			_this.tree.refresh();
		}
		else 
		{

			_this.addButtons();
			$(_this.treeID).jstree({
							"plugins" : [ 'types','wholerow' ,'dnd','buttons'],
							"state" : { "key" : "demo2" },
							"types":{
										"menu":{"icon" : "../images/ico-subnav-menu.svg"},
										"prompt":{"icon" : "../images/ico-subnav-play.svg"},
										"keypress":{"icon" : "../images/ico-subnav-keypress.svg"},
										"record":{"icon" : "../images/ico-record.svg"},
										"terminator":{"icon" : "../images/ico-record.svg"},
                                        "confirmation":{"icon" : "../images/ico-subnav-confirmation.svg"},
										"transfer":{"icon" : "../images/ico-subnav-transfer.svg"},
										"input":{"icon" : "../images/ico-subnav-input.svg"},
										"if":{"icon" : "../images/ico-subnav-if.svg"},
										"uri":{"icon" : "../images/ico-subnav-uri.svg"},
										"variable":{"icon" : "../images/ico-subnav-variable.svg"},
										"function":{"icon" : "../images/ico-subnav-function.svg"},
										"language":{"icon" : "../images/ico-subnav-language.svg"},
										"tts":{"icon" : "../images/ico-subnav-tts.svg"},
										"legal":{"icon" : "../images/ico-subnav-legal.svg"},
										"setvalue":{"icon" : "../images/ico-subnav-setValue.svg"},
										"audio":{"icon" : "../images/ico-subnav-audio.svg"}



									},										
							'core' : {
								//'data':[{"id":1,"type":"menu","text":"Root node","children":[{"id":2,"type":"prompt","text":"Child node 1"},{"id":3,"type":"prompt","text":"Child node 2"}]}],
								'multiple' : false,
								'check_callback' : function(o, n, p, i, m) {
					                var allowed=["keypress","menu","prompt","record","terminator","transfer","confirmation","input","function","if","uri","variable","legal","language","tts","setvalue","audio"];
									if(allowed.indexOf(n.type)==-1)
										return false;
									if(o==="create_node")
									{
								
										if(!_this.rules(n.type,p.type))
											
											return false;
											
										
									}



									
									if(m && m.dnd && m.pos !== 'i') { return false; }
									if(o === "move_node" || o === "copy_node") {
										if(this.get_node(n).parent === this.get_node(p).id || (!_this.rules(n.type,p.type))) 
										{ 
											return false; 
										}
										
										
									}
									
									return true;
								},


					}}).on('create_node.jstree', function (e, data) {
					    var max=Math.max.apply(Math, _this.nodeArray);
					    _this.nodeID=max+1;
					    _this.nodeArray.push(_this.nodeID);
						data.instance.set_id(data.node, _this.nodeID);
						if(Object.keys(_this.tree._model.data).length==2)
						{
							
							_this.builder.Start=_this.nodeID;
							var node="#"+_this.nodeID;
					
						}							
						var text="";
						if(data.node.type=="keypress")
						{
                                                        var keypress=1;
                                                        if(_this.tree._model.data[data.node.parent].type=="menu"  )
                                                        {
                                                            keypress=_this.getNextKeyPress(data.node);
                                                            data.node.data.flow[0].Value=keypress.toString();
                                                        }
														else if (_this.tree._model.data[data.node.parent].type=="language")
														{
                                                            keypress=_this.getNextLanguageKeyPress(data.node);
														
                                                            data.node.data.flow[0].Value=keypress.toString();
                          									if(keypress==7)
                          									{				
                                                                    data.node.data.flow[0].NextANSID=_this.crud.langList[4].id;
                                                              	
                                                            }
                                                            else {
                                                              		data.node.data.flow[0].NextANSID=_this.crud.langList[keypress-1].id;															
                                                            }  																
														}
                                                        else{
                                                           
                                                            if(_this.tree._model.data[data.node.parent].children.length==2){
                                                                keypress=2;
                                                                      
                                                            }
                                                      
                                                        }
                                                        data.node.data.flow[0].Value=keypress.toString();
							text=keypress+" :Keypress";
							
							data.instance.set_text(data.node, text);	

						}
						else 
						{

							text=_this.nodeID+":"+data.node.text;
							data.instance.set_text(data.node, text);
						
						}
						 _this.nodeID++;
					    
				}).on("loaded.jstree", function (event, data) {
            // you get two params - event & data - check the core docs for a detailed description
            $(this).jstree("open_all");
        }) ;
					
				
			_this.tree=$(_this.treeID).jstree(true);
		
		}
	}
	_this.clearErrors=function()
	{
		$(".jstree-anchor").removeClass("error");
	}
	_this.handleErrors=function(errors)
	{
		for(var i=0; i<errors.length; i++)
		{
			error=errors[i];
			var nodeId="#"+error.node;
			$(nodeId).find(".jstree-anchor").addClass("error");
			
		}
	}
	_this.saveIVR=function()
	{
			_this.clearErrors();
			if(Object.keys(_this.tree._model.data).length<=1)
			{
				alert("There must be atleast one command.");
				//message("Sorry!","There must be atleast one command.","error");
				return;
			}
			if (_this.builder.Start=="")
			{
				alert("Please select any command as starting command.");
				//message("Sorry!","Please select any command as starting command","error");
				return;
			}
			_this.builder.UserID=1;
			var script=_this.builder.build(_this.tree._model);
			if(_this.builder.errors.length>0)
			{
				_this.handleErrors(_this.builder.errors);
				return;
			}
			script=JSON.stringify(script);
			loader("show");
			var url='/designer/setIvrScriptAjax';
        var CSRF_TOKEN = $('input[name=_token]').val();

        console.log(CSRF_TOKEN);
			var request = $.ajax({
			  url: url,
			  type: "post",
              data: {_token: CSRF_TOKEN,data:script},
			  dataType: "json",
			  success:function(data)
			  {
				   console.log(data);
					loader("hide");
					if(data.Status==100)
					{
						successMessage("IVR has been saved");
					}
					else if (data.Status==400)
					{
						message("Error","There is an error.IVR has not been saved.");
					}
					else if (data.Status==600)
					{
						message("Security","This user does not have permission to save IVR.");
					}
				
			  },
			  error:function(data)
			  {
                  console.log(data);
				loader("hide");
			  }
			});		
			
	}
}