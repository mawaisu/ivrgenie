function addTfn()
{
	clearTfnModalData();
	$("#addTfnPopup").modal('show');
	
}

function editTfn(id)
{
		var url='/tfn/getAjax';
        loader("show");
        $.ajax({
            url: url,
			data:{id:id},
			dataType:"json",
            success: function(data)
			{
				if(data!=null)
				{
					clearTfnModalData();					
					customerAppID = data[0].AppID;
					loader("hide");
					if($("#role").val()=="sysadmin"){
						$("#customerList").val(data[0].TeamID);
					}
					else if($("#role").val()=="user")
					{
						
						$("#customerList").val($("#userID").val()).change();
					}	
					else
					{
						$("#customerList").val(data[0].userID).change();
					}	

					if(data[0].appID!=0)
					{
						//$("#customerAppList").val(data.appID).change();
					}

					$("#dnis").val(data[0].DNIS);
					$("#dnistfn").val(data[0].TFN);
					if($("#tfnstatus").val())
					{
						$("#tfnstatus").val(data[0].Status).change();
					}
					else 
					{
                        $("#tfnstatus").val(data[0].Status).change();
						//$("#tfnstatus").val("");
					}
                    $('#customerAppList').html('<option value="">Select Application</option>');
					console.log(data);
                    for(var i=0; i<data[1].length; i++)
                    {
                        console.log(data[1][i].AppID);
                        $('#customerAppList').append('<option value="'+data[1][i].AppID+'">'+data[1][i].AppName+'</option>');
                    }
                    var id=data[0].AppID;
                    $('#customerAppList').prop('disabled',false);
                    $("#customerAppList").val(data[0].AppID).change();
                    $("#tfntype").val(data[0].Type).change();
					$("#tfnremarks").val(data[0].Remarks);
					$("#tfnRecordID").val(data[0].ID);
					$('#dnis').prop('disabled', true);
					$("#tfnModalLabel").html("Edit TFN");
					$("#addTfnPopup").modal('show');
				}
                                
            },
            error: function()
            {
                loader("hide");                
                alert("Unable to get tfn data");
            }

        });
	
}

function clearTfnModalData()
{
	$("#tfnRecordID").val("");
	$("#customerList").val("");
	$('#customerAppList').html('<option value="">Select Application</option>');
	$('#customerAppList').prop('disabled', true);
	$("#dnis").val("");
	$('#dnis').prop('disabled', false);
	$("#dnistfn").val("");
	$("#tfnstatus").val("");
	$("#customerList").val("");
	$("#tfntype").val("S");
	$("#tfnremarks").val("");
	$('#inlineloader').html('');
	$("#tfnavailability").attr("style","display:none");
	$("#availability").attr("style","display:none");
	$("#tfnModalLabel").html("Add TFN");

}

function getApplications(appID)
{
		$('#customerAppList').prop('disabled', true);
   		//$('#tfnstatus').attr('disabled',true);

		
        var customerID = $('#customerList').val();
		if(customerID=="")
        {
            alert("Please Select a Valid Customer");
			$('#inlineloader').html('');
        }

		else
        {
			if(customerID=="0")
			{
				
				$('#customerAppList').html("<option value='0' selected>Free</option>");
                $('#tfnstatus').html('<option value="F">Free</option>');
                $('#inlineloader').html('');
				
			}
			else
			{
                $('#tfnstatus').prop('disabled',false);
              //  $("#tfnstatus option[value='F']").remove();
                $('#tfnstatus').html('<option value="L">Live</option>'+'<option value="D">Down</option>');


				var url='/tfn/getTeamApps';
				loader("show");
				$.ajax({
					url: url,
					data:{customerID:customerID},
					dataType:"json",
					success: function(data)
					{
						if(data!=null)
						{
							console.log(data);
							$('#customerAppList').html('<option value="">Select Application</option>');
							for(var i=0; i<data.length; i++)
							{
                                console.log(data[i].AppID);
								$('#customerAppList').append('<option value="'+data[i].AppID+'">'+data[i].AppName+'</option>');
							}
							
							/*if(customerAppID!=-1)
							{
								$("#customerAppList").val(customerAppID).change();
							}*/
							$('#inlineloader').html('');
							$('#customerAppList').prop('disabled', false);
							
						}
						else
						{
							$('#customerAppList').html('<option value="">No Application Found</option>');
						}
											loader("hide");
											$('#inlineloader').html('');
											
											
					},
					
					error: function()
					{
											loader("hide");
											$('#inlineloader').html('');											
						alert("Unable to get application data for this customer");
					}
				});
			}
        }
		
}

$(document).ready(function (){


// to check the value of tfn
	/*	$("#dnistfn").blur(function() {
		$("#dnistfn").attr("disabled", true);	
		$('#tfnloader').html('<img src="../images/inlineLoader.gif" />');
		$("#submitButton").attr("disabled", true);	
		   var dnisval = $('#dnistfn').val();
			if (dnisval == '') 
			{
				$("#tfnavailability").attr("style","display:none");
				$('#tfnloader').html('');
				$("#dnistfn").attr("disabled", false);
			}
			else
			{
					var url = '/tfn/checkTfn';
					var tfnRecId = $('#tfnRecordID').val();
					
				$.ajax({
					url: url,
					data: { tfnNumber: dnisval, tfnRecId: tfnRecId},
					dataType: "json",
					success: function(data)
					{
						
						if(data== true)
						{
							$("#tfnavailability").attr("style","display:none");
							$("#tfnavailability").removeClass("nt-login-failure");
							$("#submitButton").attr("disabled", false);
						}
						else
						{
							$("#tfnavailability").attr("style","display:block");
							$("#tfnavailability").addClass("nt-login-failure");
							$("#submitButton").attr("disabled", true);	
							
						}
													
													$('#tfnloader').html('');
													$("#dnistfn").attr("disabled", false);
						
					},
					error: function()
					{
													
						alert("Tfn check Call failed");
						$('#tfnloader').html('');
						$("#submitButton").attr("disabled", true);
						$("#dnistfn").attr("disabled", false);						
													
					}
					 
				});
			}

			
			
		});*/

// to assign dnis value to tfn and  check dnis value
		$("#dnis").blur(function() {
		$("#dnis").attr("disabled", true);
		$("#dnistfn").attr("disabled", true);			
		$('#dnisloader').html('<img src="../images/inlineLoader.gif" />');
		$("#submitButton").attr("disabled", true);	
		   var dnisval = $('#dnis').val();
			if (dnisval == '') 
			{
				$("#availability").attr("style","display:none");
				$('#dnisloader').html('');
				$("#dnis").attr("disabled", false);
				$("#dnistfn").attr("disabled", false);
			}
			else
			{
					var url = '/tfn/checkDnis';
					var dnisRecId = $('#tfnRecordID').val();
					
				$.ajax({
					url: url,
					data: { dnisNumber: dnisval, dnisRecId: dnisRecId},
					dataType: "json",
					success: function(data)
					{
						
						if(data== true)
						{
							$("#availability").attr("style","display:none");
							$("#availability").removeClass("nt-login-failure");
							$("#submitButton").attr("disabled", false);
							if($("#dnistfn").val()=='')
							{
								$("#dnistfn").val(dnisval);
							}
							
						}
						else
						{
							$("#availability").attr("style","display:block");
							$("#availability").addClass("nt-login-failure");
							$("#submitButton").attr("disabled", true);	
							
						}
													
													$('#dnisloader').html('');
													$("#dnis").attr("disabled", false);
													$("#dnistfn").attr("disabled", false);
						
					},
					error: function()
					{
													
						alert("Dnis check Call failed");
						$('#dnisloader').html('');
						$("#submitButton").attr("disabled", true);
						$("#dnis").attr("disabled", false);	
						$("#dnistfn").attr("disabled", false);
													
					}
					 
				});
			}
			
			
			
		});
// to populate application list based on customer selected
	    $('#customerList').on('change', function() {
            var customerID = $('#customerList').val();
			console.log('team '+customerID);
		   getApplications(customerID);
    });
			
// to enable applist dropdown as form is submitted.  It executes in case if free/dummy customer is selected.
         $('#addEditTfnForm').bind('submit', function () {
             $('#customerAppList').prop('disabled', false);
             $('#submitButton').prop('disabled', true);
             $('#cancelmodal').prop('disabled', true);
             $("#addTfnPopup").modal('hide');
             loader("show");

         });

});

