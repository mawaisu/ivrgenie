var userVerifier = null;


function check() {
    alert("i am here");

}

function addUser() {
    clearUserModal();
    $("#userName").attr("readonly", false);
    $("#userName").attr("disable", false);
    $("#userTitle").html("Add User");
    //populateTeams();
    $("#editUserPopup").modal('show');
}

function populateTeams() {
    var url = '/ajax/get-companyteams';
    var companyID = $("#company").val();
    loader("show");
    $.ajax({
        url: url,
        type: 'get',
        data: {id: companyID},
        dataType: "json",
        success: function (data) {

            console.log(data);

            $("#teams").empty();
            $("#teams").append('<option value="">Select your team</option>');

            if (data != null) {
                for (var key in data) {
                    $('#teams').append($('<option>', {
                        value: data[key].Team_ID,
                        text: data[key].Name
                    }));

                }
                $('#teams').prop('disabled', false);
            }
            loader("hide");
        },
        error: function () {
            loader("hide");
            alert("Error Please contact IBEX-IVR team");
        }
    });
}

function clearUserModal() {
    $("#firstName").val("");
    $("#lastName").val("");
    $("#userName").val("");
    $("#teams").empty();
    $("#teams").append('<option value="">Select your team</option>');
}

function editUser(id) {

    var url = '/ajax/get-user';
    var userID = id;
    loader("show");
    $.ajax({
        url: url,
        type: 'get',
        data: {id: userID},
        dataType: "json",
        success: function (data) {

            console.log(data);
            $("#userName").attr("readonly", true);
            $("#userName").attr("disable", true);
            $("#userTitle").html("Edit User");

            //  populateDropdown(id);
            $("#userName").val(data.username);
            $("#firstName").val(data.firstname);
            $("#company").val(data.CompanyID);
            $("#userID").val(data.id);
            $("#lastName").val(data.lastname);
            $("#role").val(data.role_id);


            populateTeams();
            $("#company").change();
            $("#role").change();
            $("#teams").val(data.TeamID);
            $("#teams").change();
            loader("hide");
            $("#editUserPopup").modal('show');

        },
        error: function () {
            loader("hide");
            alert("Error Please contact IBEX-IVR team");
        }
    });


}

function populateDropdown(userID) {
    var url = baseUrl + '/ajax/get-userteams';
    var id = userID;

    loader("show");
    $.ajax({
        url: url,
        type: 'POST',
        async: false,
        data: {id: id},
        dataType: "json",
        success: function (data) {
            if (data != null) {
                for (var key in data) {
                    $('#teams').append($('<option>', {
                        value: key,
                        text: data[key]
                    }));
                }
                $('#teams').prop('disabled', false);
            }
            loader("hide");
        },
        error: function () {
            loader("hide");
            alert("Error Please contact IBEX-IVR team");
        }
    });
}
/*
$(document).ready(function () {


        alert($("#userID").val());
        $('#userName').blur(function () {
            // your code here
            var status;
            loader("show");
            var name = $('#userName').val();
            var id = $('#userID').val();
            var url = '/ajax/check-user';

            if ($("#userTitle").val()==='Edit User') {
                $.ajax({
                    type: "get",
                    url: url,
                    data: {name: name},
                    dataType: "json",
                    async: false,
                    success: function (data) {
                        console.log(data);
                        if (data != null) {
                            status = data;
                        }
                    },
                    error: function () {
                        loader("hide");
                        alert("Unable to get user data");
                    }
                });

                if (status == 1) {
                    var obj = {
                        title: "Duplicate Record",
                        'data-text': "Duplicate Record Exist Please check again",
                        'data-type': "warning",
                        'data-confirm-text': "OK",
                        color: "white"
                    };
                    messageBox(obj, clearUserModal);
                    loader("hide");
                    return false;
                }
                else {
                    alert("in else")
                    return true;
                }
            }
        });


    userVerifier = $("#editUser").validate({
        rules:
            {
                userName:
                    {
                        required: true,
                        maxlength: 150,
                        notBlank: true,
                        username: true
                    },
                firstName:
                    {
                        required: true,
                        alphanumeric: true,
                        maxlength: 150,
                        notBlank: true
                    },
                lastName:
                    {
                        required: true,
                        alphanumeric: true,
                        maxlength: 150,
                        notBlank: true
                    }
            }
    });
});*/
