$('#example').DataTable({
	"order": [[0, 'desc']]
});


function addReport()
{
	clearAppModalData();
	$("#addRptPopup").modal('show');
	
}


function editReport(rptID,AppID,rptName,rptSubject,rptHour,rptFrq,rptDay,rptStatus)
{
        clearAppModalData();	
		$("#appID").val(AppID);
		$("#rptID").val(rptID);
		$("#rptName").val(rptName);
		$("#rptSubject").val(rptSubject);
		$("#rptHour").val(rptHour);
		$("#rptFrq").val(rptFrq);
		$("#rptStatus").val(rptStatus);
		$("#rptFrq").change();
		$("#rptDay").val(rptDay);
		$("#addRptPopup").modal('show');
	
}



function clearAppModalData()
{
	$("#appID").val("");
	$("#rptID").val("0");
	$("#rptName").val("");
	$("#rptSubject").val("");
	$("#rptHour").val("");
	$("#rptFrq").val("");
	$("#rptDay").val("0");
	$("#rptStatus").val("L");
}


$("#rptFrq").change(function () {
        if($(this).val()==1){
            $("#rptDay").val("0");	
    	   $("#rptDay").prop("disabled",false);
        }
        else
        {
          $("#rptDay").prop("disabled",true);	
        }	
});

$( "#addRptForm" ).submit(function( event ) {
	  $("#addRptPopup").modal('hide');
      loader("show");
});	


/*
$(document).ready(function (){	
	 $('#addAppForm').bind('submit', function () {
		$('#submitButton').prop('disabled', true);
		$('#cancelmodal').prop('disabled', true);
		$("#addAppPopup").modal('hide');
		loader("show");
		});
		
		// to check the value of application name
		$("#appName").blur(function() {
		$("#appName").attr("disabled", true);
		$("#appAbrv").attr("disabled", true);
		$('#appnameloader').html('<img src="../images/inlineLoader.gif" />');
		$("#submitButton").attr("disabled", true);	
		   var appnameval = $('#appName').val();
			if (appnameval == '') 
			{
				$("#appnameavailability").attr("style","display:none");
				$('#appnameloader').html('');
				$("#appName").attr("disabled", false);
				$("#appAbrv").attr("disabled", false);
			}
			else
			{
					var url = baseUrl+'/ajax/check-application';
					var appRecId = $('#appID').val();
					
				$.ajax({
					url: url,
					data: { name: appnameval, appRecId:appRecId },
					dataType: "json",
					success: function(data)
					{
						
						if(data== true)
						{
							$("#appnameavailability").attr("style","display:none");
							$("#appnameavailability").removeClass("nt-login-failure");
							$("#submitButton").attr("disabled", false);
							if($("#appAbrv").val()=='')
							{
								$("#appAbrv").val(appnameval);
							}
							
							
							
						}
						else
						{
							$("#appnameavailability").attr("style","display:block");
							$("#appnameavailability").addClass("nt-login-failure");
							$("#submitButton").attr("disabled", true);	
							
						}
													
													$('#appnameloader').html('');
													$("#appName").attr("disabled", false);
													$("#appAbrv").attr("disabled", false);
						
					},
					error: function()
					{
													
						alert("application name check Call failed");
						$('#appnameloader').html('');
						$("#submitButton").attr("disabled", true);
						$("#appName").attr("disabled", false);
						$("#appAbrv").attr("disabled", false);						
													
					}
					 
				});
			}
			
			
			
		});
		
		
  

}); */