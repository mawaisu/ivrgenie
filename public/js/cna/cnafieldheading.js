function addCnaFieldHeading()
{
	clearAppModalData();
	$("#addCnaFieldHeadingPopup").modal('show');
	
}


function editCnaFieldHeading(fieldType,srNO,label)
{
        clearAppModalData();        	
		$("#type").val(fieldType);
		$("#sr").val(srNO);
		$("#label").val(label);
		$("#new").val("1");
		$("#type").prop("disabled", true);
        $("#sr").prop("disabled", true);
		$("#addCnaFieldHeadingPopup").modal('show');
	
}



function clearAppModalData()
{
	$("#type").prop("disabled", false);
	$("#sr").prop("disabled", false);
	$("#new").val("0");
	$("#type").val("");
	$("#sr").val("0");
	$("#label").val("");
}

$( "#addCnaFieldHeadingForm" ).submit(function( event ) {
	  $("#type").prop("disabled", false);
      $("#sr").prop("disabled", false);
	  $("#addCnaFieldHeadingPopup").modal('hide');
      loader("show");
});	


/*
$(document).ready(function (){	
	 $('#addAppForm').bind('submit', function () {
		$('#submitButton').prop('disabled', true);
		$('#cancelmodal').prop('disabled', true);
		$("#addAppPopup").modal('hide');
		loader("show");
		});
		
		// to check the value of application name
		$("#appName").blur(function() {
		$("#appName").attr("disabled", true);
		$("#appAbrv").attr("disabled", true);
		$('#appnameloader').html('<img src="../images/inlineLoader.gif" />');
		$("#submitButton").attr("disabled", true);	
		   var appnameval = $('#appName').val();
			if (appnameval == '') 
			{
				$("#appnameavailability").attr("style","display:none");
				$('#appnameloader').html('');
				$("#appName").attr("disabled", false);
				$("#appAbrv").attr("disabled", false);
			}
			else
			{
					var url = baseUrl+'/ajax/check-application';
					var appRecId = $('#appID').val();
					
				$.ajax({
					url: url,
					data: { name: appnameval, appRecId:appRecId },
					dataType: "json",
					success: function(data)
					{
						
						if(data== true)
						{
							$("#appnameavailability").attr("style","display:none");
							$("#appnameavailability").removeClass("nt-login-failure");
							$("#submitButton").attr("disabled", false);
							if($("#appAbrv").val()=='')
							{
								$("#appAbrv").val(appnameval);
							}
							
							
							
						}
						else
						{
							$("#appnameavailability").attr("style","display:block");
							$("#appnameavailability").addClass("nt-login-failure");
							$("#submitButton").attr("disabled", true);	
							
						}
													
													$('#appnameloader').html('');
													$("#appName").attr("disabled", false);
													$("#appAbrv").attr("disabled", false);
						
					},
					error: function()
					{
													
						alert("application name check Call failed");
						$('#appnameloader').html('');
						$("#submitButton").attr("disabled", true);
						$("#appName").attr("disabled", false);
						$("#appAbrv").attr("disabled", false);						
													
					}
					 
				});
			}
			
			
			
		});
		
		
  

}); */