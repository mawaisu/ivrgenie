function addRole()
{
    clearAppModalData();
    $("#addRolePopup").modal('show');

    var js = "{\n" +
        "\t\"Status\": \"100\",\n" +
        "\t\"Description\": \"Successful\",\n" +
        "\t\"RoleID\": \"1\",\n" +
        "\t\"Role Description\": \"Admin\",\n" +
        "\t\"Routes\": [{\n" +
        "\t\t\"RouteID\": \"1\",\n" +
        "\t\t\"Descrtion\": \"Dashboard\",\n" +
        "\t\t\"Rute\": \"\\\\Dashboard.index\",\n" +
        "\t\t\"Edit\": \"true\",\n" +
        "\t\t\"Delete\": \"false\"\n" +
        "\t},\n" +
        "\t{\n" +
        "\t\t\"RouteID\": \"2\",\n" +
        "\t\t\"Description\": \"Favourite\",\n" +
        "\t\t\"Rute\": \"Favourite.index\",\n" +
        "\t\t\"Edit\": \"true\",\n" +
        "\t\t\"Delete\": \"false\"\n" +
        "\t}]\n" +
        "}";

    console.log(JSON.parse(js));

}
function editRole(id)
{
    var url='/role/get';
    loader("show");
    $.ajax({
        url: url,
        data:{id:id},
        dataType:"json",
        success: function(data)
        {
            console.log(data);
            if(data!=null)
            {
                clearAppModalData();
                $("#roleName").val(data.role.name);
                $("#roleStatus").val(data.role.status);
                $("#roleId").val(data.role.id);
                $("#roleModalLabel").html("Edit Role");
                $("#addRolePopup").modal('show');

            }
            loader("hide");
        },
        error: function()
        {
            loader("hide");
            alert("Unable to get role data");

        }

    });

}

function clearAppModalData()
{
    $("#roleId").val("");
    $("#roleName").val("");
    $("#roleStatus").val("");
}
