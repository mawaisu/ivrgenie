

function SurveyTreeDesigner()
{
	
	var _this=this;
	_this.treeID="#ivrContainer";

	_this.tree=null;
	_this.nodeID=9000;
	_this.responseID=8000;
	_this.builder=new ScriptBuilder();
	_this.crud=new CRUD(_this);
	_this.keypress=2;
	_this.create=function(command,target)
	{
		var node=_this.getNode(command);
		if(node==null)
			return;
		var inst = $.jstree.reference(target);
		var obj = inst.get_node(target);
	
		var output=inst.create_node(obj,node, "last", function (newNode) {
			if(newNode.type=="menu")
			{
					for(var i=0; i<_this.keypress; i++)
					{
						_this.create("keypress",newNode.id);
					}
				
			}

         
			$(_this.treeID).jstree(true).open_node(obj);
		});	
		_this.assignKeypressNextANSID();	
	}
	
	_this.getNextKeyPress=function(node)
	{
		var data=_this.tree._model.data[node.parent];
		var childrens=data.children;
		var existingKeypress=[];
		var list=[];
		if(childrens.length>0)
		{
			for(var i=0; i<childrens.length; i++)
			{
				var nodeID=childrens[i];
				var keypressData=_this.tree._model.data[nodeID].data;
				var flow=keypressData.flow[0];
				existingKeypress.push(flow.Value);
			}
		}
		var key="";
		for(var v=0; v<10; v++)
		{
			key=v;

			
			if(existingKeypress.indexOf(key.toString())==-1 )
			{
				list.push(key);
			}
		}
		
		list.sort(function(a,b){return a-b;});
		list.reverse();
		key=list.pop();

			
		return list.pop();
		
	}
	_this.getNode=function(command)
	{
		var node=null;
		var script=new Script();
		script.flow.push(new Flow());
		script.extra.push(new Extra());
		switch(command) 
		{
			

	
			case "keypress":
				script.Type="keypress";
				
				node={type:"keypress",text:"Keypress",data:script};
			break;			
			case "menu":
				script.Type="menu";
				script.PromptText="Menu";
				

				
				var timeout=new Extra();
				timeout.IDENTIFIER="TO";
				timeout.VALUE="3000";
				script.extra.push(timeout);
				
				var invalid=new Extra();
				invalid.IDENTIFIER="INV1";
				invalid.VALUE=_this.responseID + "|That was an invalid selection please try again|RP";
				script.extra.push(invalid);				
				_this.responseID++;
				var noresponse=new Extra();
				noresponse.IDENTIFIER="NR1";
				noresponse.VALUE=_this.responseID + "|I am sorry you are having difficulties please try call your later|RP";
				script.extra.push(noresponse);	
				_this.responseID++;
				node={type:"menu",text:"Menu",data:script};
			break;
			default:
						
			
		} 		
		return node;
	}
	_this.rules=function(from,to)
	{
                  
			if(from=="keypress" && (to!="menu" && to!="confirmation") )
			{
				return false;
			}
			else if((from=="prompt" || from=="menu" || from=="transfer" || from=="record" || from=="confirmation" || from=="input" || from=="function" || from=="if") && to=="#")
			{
				return true;
			}else  if(from=="keypress" && (to=="menu" || to=="confirmation" || to=="input"))
			{
                                
				return true;
                               
			}
			
			return false;
	}


	_this.assignKeypressNextANSID=function()
	{
		var data=_this.tree._model.data["#"];	
		var parents=data.children;
		if(parents.length>1)
		{
			for(var i=0; i< parents.length-1; i++)
			{
				var childrens=_this.tree._model.data[parents[i]].children;
				for(var j=0; j<childrens.length; j++)
				{
						var node=_this.tree._model.data[childrens[j]].data;
						node.flow[0].NextANSID=parents[i+1];
				}
			}
		}
		_this.tree.redraw(true);
		
	}


	_this.moveOnNode=function(event,node)
	{

		var id=event.target.getAttribute('data');
		
		if(id!='' && id!=0)
		{
			event.stopPropagation();
			$(".jstree-anchor").removeClass("selected");
			_this.tree.deselect_all();
			_this.tree.select_node(id);
			id="#"+id;
			$(id).find(".jstree-anchor").addClass("selected");
		
		}
		else 
		{
			$(".jstree-anchor").removeClass("selected");
		}
		
	}
	_this.addButtons=function()
	{
		
		
	
		$.jstree.plugins.buttons = function (options, parent) {

			this.redraw_node = function(obj, deep, callback) {
			
				obj = parent.redraw_node.call(this, obj, deep, callback);
				if(obj) {

					var deleteBtn = document.createElement('button');
					deleteBtn.className = "ivr-delete btn btn-xs actionbuttons";
					deleteBtn.setAttribute('data-type','warning');		
					deleteBtn.setAttribute('title','Delete!');		
					deleteBtn.setAttribute('data-text','Are you sure, you want to delete this record');		
					deleteBtn.setAttribute('data-confirm-text','Yes');		
					deleteBtn.setAttribute('data-success-text','Your record has been deleted!');		
					deleteBtn.innerHTML = "<i class='fa fa-times'></i>";	
					
					var editBtn=document.createElement('button');
					editBtn.className = "ivr-edit btn btn-xs actionbuttons";	
					editBtn.innerHTML = "<i class='fa fa-pencil'></i>";	

					var uploadBtn=document.createElement('button');
					uploadBtn.className = "ivr-upload btn btn-xs actionbuttons";	
					uploadBtn.innerHTML = "<i class='fa fa-upload'></i>";	
					
					var addBtn=document.createElement('button');
					addBtn.className = "ivr-add btn btn-xs actionbuttons";
					addBtn.innerHTML = "<i class='fa fa-plus'></i>";
					
					var startBtn = document.createElement('button');
					
					startBtn.className = "ivr-start btn btn-xs actionbuttons";
					startBtn.setAttribute('title','Start');	

					if(_this.builder.Start==obj.id)
					{

						startBtn.innerHTML = "<i class='fa fa-toggle-on'></i>";
					}
					else
					{
						startBtn.innerHTML = "<i class='fa fa-toggle-off'></i>";
					}
					
					


					var data=_this.tree._model.data[obj.id];
				
					if(data.data && data.data.flow && data.data.flow[0] && data.type!="menu" && data.type!="confirmation")
					{
						var link = document.createElement('a');
						link.className = "node-sub-link";	
						link.setAttribute('data',data.data.flow[0].NextANSID);
						var linkText="";
						if(data.data.flow[0].NextType=='hangup' || data.data.flow[0].NextANSID==0)
						{
							link.className=link.className+ " ivr-sub-hangup";
						}
						else
						{
							link.className=link.className+ " ivr-sub-goto";
							linkText=data.data.flow[0].NextANSID;
						}
						link.innerHTML=linkText;
						obj.childNodes[2].appendChild(link);
						link.onclick=function(event){_this.moveOnNode(event,obj);};
					}
               
                                                        
							
							if(_this.tree._model.data[_this.tree._model.data[obj.id].parent].type!="confirmation")
							{
                                                            obj.childNodes[2].appendChild(deleteBtn);
                                                        }
                                if(data.type!="keypress")
					{
						obj.childNodes[2].appendChild(uploadBtn);
						uploadBtn.onclick=function(event)
				       	{
						_this.crud.uploadFile(obj);
						//_this.edit(obj);
					   };
						obj.childNodes[2].appendChild(startBtn);
						startBtn.onclick=function(event)
						{
							_this.builder.Start=obj.id;
							$(".ivr-start .fa").each(function(){ $(this).removeClass("fa-toggle-on"); $(this).addClass("fa-toggle-off"); });
							$(this).find(".fa").addClass("fa-toggle-on");
						}						
					}
					if(data.data && data.type!="keypress")
					{
						obj.childNodes[2].appendChild(editBtn);
					}					
					
					
			        
					
					
					//obj.insertBefore(tmp, obj.childNodes[2]);

					if(data.data && data.type=="menu")
					{
						obj.childNodes[2].appendChild(addBtn);
						addBtn.onclick=function(event){_this.crud.add(obj);};
					}
					deleteBtn.onclick=function(event){	
							var parent=_this.tree._model.data[obj.id].parent;
							var data=_this.tree._model.data[parent];
							if(data.type=="menu")
							{
								if(data.children.length==2)
								{
									
									swal(
											{
												title: "Delete",
												text: "Menu must have atleast two keypress",
												type: 'error',
												confirmButtonClass: "btn-danger",
												confirmButtonColor: "#DD6B55",
												closeOnConfirm: true
											},
											function()
											{
												
												swal.disableButtons();
											}
											);									
								}
								else 
								{
									messageBox(deleteBtn,function(){_this.crud.delete(obj);});
								}
							}
							else 
							{
								messageBox(deleteBtn,function(){_this.crud.delete(obj);});
							}					
						
					};
					editBtn.onclick=function(event)
					{
						_this.crud.edit(obj);
						//_this.edit(obj);
					};
					


					
					
				}
				return obj;
			
			};
				

		}
	

		
	}

	_this.load=function(data)
	{

		if(_this.tree!=null)
		{
			_this.tree.settings.core.data =data;
			_this.tree.refresh();
			
		}
		else 
		{

			_this.addButtons();
			$(_this.treeID).jstree({
							"plugins" : [ 'types','wholerow' ,'dnd','buttons'],
							"state" : { "key" : "demo2" },
							"types":{
										"menu":{"icon" : "../images/ico-subnav-menu.png"},
										
										"keypress":{"icon" : "../images/ico-subnav-keypress.png"}
									

									},										
							'core' : {
								//'data':[{"id":1,"type":"menu","text":"Root node","children":[{"id":2,"type":"prompt","text":"Child node 1"},{"id":3,"type":"prompt","text":"Child node 2"}]}],
								'multiple' : false,
								'check_callback' : function(o, n, p, i, m) {
					                var allowed=["keypress","menu"];
									if(allowed.indexOf(n.type)==-1)
										return false;
									if(o==="create_node")
									{
								
										if(!_this.rules(n.type,p.type))
											
											return false;
											
										
									}



									
									if(m && m.dnd && m.pos !== 'i') { return false; }
									if(o === "move_node" || o === "copy_node") {
										if(this.get_node(n).parent === this.get_node(p).id || (!_this.rules(n.type,p.type))) 
										{ 
											return false; 
										}
										
										
									}
									
									return true;
								},


					}}).on('create_node.jstree', function (e, data) {
						data.instance.set_id(data.node, _this.nodeID);
						if(Object.keys(_this.tree._model.data).length==2)
						{
							
							_this.builder.Start=_this.nodeID;
							var node="#"+_this.nodeID;
					
						}							
						var text="";
						if(data.node.type=="keypress")
						{
                                                        var keypress=1;
                                                        if(_this.tree._model.data[data.node.parent].type=="menu")
                                                        {
                                                            keypress=_this.getNextKeyPress(data.node);
                                                            data.node.data.flow[0].Value=keypress.toString();
                                                        }
                                                        else{
                                                           
                                                            if(_this.tree._model.data[data.node.parent].children.length==2){
                                                                keypress=2;
                                                                      
                                                            }
                                                      
                                                        }
                                                        data.node.data.flow[0].Value=keypress.toString();
							text=keypress+" :Keypress";
							
							data.instance.set_text(data.node, text);	

						}
						else 
						{

							text=_this.nodeID+":"+data.node.text;
							data.instance.set_text(data.node, text);
						
						}
						 _this.nodeID++;
					    
				}).on("loaded.jstree", function (event, data) {
            // you get two params - event & data - check the core docs for a detailed description
            $(this).jstree("open_all");
        }) ;
					
				
			_this.tree=$(_this.treeID).jstree(true);
			
		}
	}
	_this.clearErrors=function()
	{
		$(".jstree-anchor").removeClass("error");
	}
	_this.handleErrors=function(errors)
	{
		for(var i=0; i<errors.length; i++)
		{
			error=errors[i];
			var nodeId="#"+error.node;
			$(nodeId).find(".jstree-anchor").addClass("error");
			
		}
	}
	_this.saveSurvey=function()
	{
			_this.clearErrors();
			if(Object.keys(_this.tree._model.data).length<=1)
			{
				//alert("There must be atleast one command.");
				message("Sorry!","There must be atleast one question.","error");
				return;
			}
			else if (_this.builder.Start=="")
			{
				//alert("Please select any command as starting command.");
				message("Sorry!","Please select any question as starting command","error");
				return;
			}
			
			var script=_this.builder.build(_this.tree._model);
			if(_this.builder.errors.length>0)
			{
				_this.handleErrors(_this.builder.errors);
				return;
			}
			script=JSON.stringify(script);
			loader("show");
			var url=baseUrl+"/ajax/set-survey-script";
			
			var request = $.ajax({
			  url: url,
			  type: "post",
              data: {data:script},			  
			  dataType: "json",

			  success:function(data)
			  {
					loader("hide");
					if(data.Status==100)
					{
						successMessage("Survey has been saved");
					}
					else if (data.Status==400)
					{
						message("Error","There is an error.Survey has not been saved.");
					}
					else if (data.Status==600)
					{
						message("Security","This user does not have permission to save survey.");
					}
	
			  },
			  error:function()
			  {
				loader("hide");
			  }
			});		
			
	}
}