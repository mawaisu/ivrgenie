function CRUD(treeDesigner)
{

	var _this=this;
	_this.designer=treeDesigner;
	_this.fileUploadModal="#fileUploadModal";

	_this.keypressModalID="#keypressModal";

	_this.menuModalID="#menuModal";

	_this.invalidResponseTemplateID="#invalidResponseTemplate";

	_this.noResponseTemplateID="#noResponseTemplate";

	_this.invalidTableID="#invalidResponseTable";

	_this.noResponseTableID="#noResponseTable";

 //////
// Survey Code starts here 
//// 
	_this.create=function()
	{
		
	}
///////	
	_this.edit=function(node)
	{
		var type=_this.designer.tree._model.data[node.id].type;
               
        var parentType=_this.designer.tree._model.data[_this.designer.tree._model.data[node.id].parent].type;
                                
		switch(type)
		{

			case "keypress":
                            if(parentType=="menu"){
				_this.editKeypress(node);
                            }
                            else{
                                _this.editConfirmationKeypress(node);
                            }
			break;			
			case "menu":
				_this.editMenu(node);
			break;
		
		}		
	}
	
	_this.uploadFile=function(node)
	{
		
		var promptID=_this.designer.tree._model.data[node.id].data.PromptID;
		var appID=_this.designer.builder.IVR;
		
		$('#fileAppId').val(appID);
		$('#filePromptId').val(promptID);

        $(_this.fileUploadModal).modal('show');
                                
				
	}
	_this.getGotoList=function()
	{
		var data=_this.designer.tree._model.data;
		var list=[];

		for (var node in data) 
		{
			
			if(node=='#' || data[node].type=="keypress")
					continue;
			var item={};
			item.id=data[node].id;
			item.text=data[node].data.PromptText;
			list.push(item);
		}	

		return list;		
	}


	_this.populateGotoList=function(id,node)
	{
		    var id="#"+id;
			var options = $(id);			
			var list=_this.getGotoList();
			options.html('<option value="0">Hangup</option>');
			
			$.each(list, function() {
				
				var text=this.id+':'+this.text;
				if(node.id==this.id)
				{
				 // options.append($("<option />").val(this.id).text(text).attr('disabled',true));
				}
				else 
				{
					options.append($("<option />").val(this.id).text(text));
				}
			});	
					
	}

	_this.editKeypress=function(node)
	{
		   $("#keypressID").val('');
		   $("#keypress").val('');

			
			var data=_this.designer.tree._model.data;
			
			_this.populateGotoList("keypressGoto",node);
			if(data!=null)
			{
				 data=data[node.id];
			
				
				if(data.data.flow.length>0)
				{		
					
					_this.populateKeypress(node,data.data.flow[0].Value);
					$("#keypress").val(data.data.flow[0].Value);										
					$("#keypressID").val(data.id);	
					$("#keypressGoto").val(data.data.flow[0].NextANSID);
				}
				else 
				{
					$("#keypressGoto").val(0);
				}
	
				$(_this.keypressModalID).modal('show');
			}

			
	}

	_this.editMenu=function(node)
	{
		
		    $("#menuID").val('');
		    $("#menuText").val('');
		    $("#menuTimeOut").val('3000');
			
			var data=_this.designer.tree._model.data;
			_this.populateGotoList("invalidGotoList",node);
			_this.populateGotoList("noResponseGotoList",node);
			_this.populateResponseList('invalidPromptList');
			_this.populateResponseList('noResponsePromptList');
				
			$(".invalid-response-rows").remove();
			$(".no-response-rows").remove();
			if(data!=null)
			{
				 data=data[node.id];

				$("#menuID").val(data.id);
				$("#menuText").val(data.data.PromptText);
				var identifiers=[/^INV/];
				var invalidList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< invalidList.length; i++)
				{
						_this.populateInvalidResponseRow(invalidList[i]);
				}
				identifiers=[/^NR/];
				var noResponseList=_this.getExtraList(identifiers,node.id);
			
				for(var i=0; i< noResponseList.length; i++)
				{
						_this.populateNoResponseRow(noResponseList[i]);
				}
				identifiers=[/^TO/];
				var toList=_this.getExtraList(identifiers,node.id);
				
				for(var i=0; i< toList.length; i++)
				{
						$("#menuTimeOut").val(toList[i].id);
				}				
				$(_this.menuModalID).modal('show');
			}
			
						
		
	}

		
	_this.getFlowList=function(identifiers,key)
	{
		var data=_this.designer.tree._model.data;	

		if(key!=null)
		{
			data={key:data[key]};
		}
		var list=[];
		var split='';
		for( var nodeID in data)
		{
			if(node=="#")
				continue;
			
			var node=data[nodeID];
			if(node.type=="menu" || node.type=="record" || node.type=="transfer")
			{
				var flow=node.data.flow;
				
				for(var i=0; i<flow.length; i++)
				{
					split=flow[i].Value.split('|');
					if(split.length<=2)
					{
						split.push(0);
					}
					if(identifiers==null)
					{
							
							list.push({NextANSID:flow[i].NextANSID,Value:split[0]});
					}
					
					else if(identifiers!=null)
					{
							identifiers.some(
										function(regx)
											{ 
												if(regx.test(flow[i].Value))
												{
													list.push({NextANSID:flow[i].NextANSID,Value:split[0]});
												}
											}
										);
							
							
					}
				}
		
			}
		}
		return list;
	}		
	_this.getExtraList=function(identifiers,key)
	{
		var data=_this.designer.tree._model.data;	

		if(key!=null)
		{
			var obj={}
			obj[key]=data[key];
			data=obj;
		
			
		}
		var list=[];
		var split='';
		for( var nodeID in data)
		{
			if(nodeID=="#")
				continue;
			
			var node=data[nodeID];
			if(node.type=="menu" || node.type=="record" || node.type=="transfer" || node.type=="confirmation" || node.type=="input" || node.type=="function" || node.type=="if")
			{
				var extra=node.data.extra;
				
				for(var i=0; i<extra.length; i++)
				{
					split=extra[i].VALUE.split('|');
					if(split.length<=2)
					{
						split.push(0);
					}
					if(identifiers==null)
					{
							
							list.push({identifier:extra[i].IDENTIFIER,id:split[0],text:split[1],action:split[2]});
					}
					
					else if(identifiers!=null)
					{
							identifiers.some(
										function(regx)
											{ 
												if(regx.test(extra[i].IDENTIFIER))
												{
													list.push({identifier:extra[i].IDENTIFIER,id:split[0],text:split[1],action:split[2]});
												}
											}
										);
							
							
					}
				}
		
			}
		}

		return list;
	}	
	_this.getNextResponseID=function()
	{
		
			var identifiers=[/^INV/,/^NR/];
			var list=_this.getExtraList(identifiers,null);	
			var max=_this.designer.responseID;
			$.each(list, function() {
				
				if( parseInt(this.id) > max)
				{
					max=this.id
				}
			});	
			max++;
			_this.designer.responseID=max;
			return max; 
	}
	_this.populateResponseList=function(id)
	{
		    var id="#"+id;
			var options = $(id);	
			var identifiers=[/^INV/,/^NR/];
			var list=_this.getExtraList(identifiers,null);
			var existing=[];
			options.html('<option value="0">Create New</option>');
			
			$.each(list, function() {
				if(existing.indexOf(this.id)==-1)
				{	
				var text=this.id+':'+this.text;
					existing.push(id);
					options.append($("<option />").val(this.id).text(text));
				}
			});	
					
	}

	_this.populateKeypress=function(node,current)
	{
		var data=_this.designer.tree._model.data[node.id];
		var parent=_this.designer.tree._model.data[data.parent];
		var childrens=parent.children;
		var existingKeypress=[];
		if(childrens.length>0)
		{
			for(var i=0; i<childrens.length; i++)
			{
				var nodeID=childrens[i];
				var keypressData=_this.designer.tree._model.data[nodeID].data;
				var flow=keypressData.flow[0];
				existingKeypress.push(flow.Value);
			}
		}
		var id="#keypress";
		var options = $(id);			
		options.html('');
		var key="";
		for(var v=1; v<10; v++)
		{
			key=v;

			
			if(existingKeypress.indexOf(key.toString())==-1 || key==current)
			{
			 var text=key+": Keypress";
			 options.append($("<option />").val(key).text(text));
			}
		}


	}
    _this.saveKeypress=function()
	{
		
				
		var id=$("#keypressID").val();
		if(id=='')
			return;
		var data=_this.designer.tree._model.data[id];
	
		var flow=new Flow();
		flow.NextANSID=$("#keypressGoto").val();
		flow.Value=$("#keypress").val();
		data.text=$("#keypress").find('option:selected').text();
		data.data.flow=[];
		data.data.flow.push(flow);
		
		_this.designer.tree._model.data[id]=data;

		var nodeID="#"+id;
	
	
		_this.designer.tree.redraw_node(nodeID);
	
		$(_this.keypressModalID).modal('hide');	
	}


	_this.saveMenu=function()
	{
                  if(!$("#menuForm").valid())
		{
			return false;
		}
            
		if($(".no-response-rows").length==0 || $(".invalid-response-rows").length==0)
		{
			alert('Please define atleast one invalid and one no response');
			return;
		}
		
		var id=$("#menuID").val();
		var data=_this.designer.tree._model.data[id];
		data.text=id+":"+$("#menuText").val();		
		data.data.PromptText=$("#menuText").val();		
		var extra=new Extra();		
		data.data.extra=[];
		extra.IDENTIFIER="TO";
		extra.VALUE=$("#menuTimeOut").val();
		data.data.extra.push(extra);
		var counter=1;
		var rowIndex=3;
		$(".no-response-rows").each(
									function()
									{

										var row=$(this);
								        extra=new Extra();
										extra.IDENTIFIER="NR"+counter;
										extra.VALUE=row.find("#noResponsePromptID").val()+"|"+row.find("#noResponsePromptText").val()+"|"+row.find('input[name="noResponseAction['+rowIndex+']"]:checked').val();								
										data.data.extra.push(extra);
										counter++;
										rowIndex++;
									}
								  );

		counter=1;
		rowIndex=3;
		$(".invalid-response-rows").each(
									function()
									{
										var row=$(this);
								        extra=new Extra();
										extra.IDENTIFIER="INV"+counter;
										extra.VALUE=row.find("#invalidPromptID").val()+"|"+row.find("#invalidPromptText").val()+"|"+row.find('input[name="invalidAction['+rowIndex+']"]:checked').val();								
										data.data.extra.push(extra);
										counter++;
										rowIndex++
									}
								  );
								  
		_this.designer.tree._model.data[id]=data;

		var nodeID="#"+id;
	
	
		_this.designer.tree.redraw_node(nodeID);
	
		$(_this.menuModalID).modal('hide');		
	}



	_this.noResponseGoto=function(event)
	{
		var row=$(event.target.parentNode.parentNode);
		row.find("#noResponseGoto").val(row.find("#noResponseGotoList").val());
			
	}

	_this.removeNoResponseRow=function(obj)
	{
		var row=$(obj.parentNode.parentNode);
		row.remove();
	}

	_this.addNoResponseRow=function()
	{
            if(!$("#noResponseForm").valid())
		{
			return false;
		}
               
		var data={};
		var promptID=$("#noResponseRow").find("#noResponsePromptList").val();
		var promptText=$("#noResponseRow").find("#noResponsePromptText").val();
		var action=$("#noResponseRow").find('input[name="noResponseAction"]:checked').val();
		if(promptID==0)
		{
			promptID=_this.getNextResponseID();
			

			$("#noResponseRow").find("#noResponsePromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
			$("#invalidRow").find("#invalidPromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
			

		}
		data.id=promptID;
		data.text=promptText;
		data.action=action;
		_this.populateNoResponseRow(data);

			$("#noResponseRow").find("#noResponsePromptList").val(0);
			$("#noResponseRow").find("#noResponsePromptText").val('');		

	}

	_this.setNoResponsePromptText=function(event)
	{
		var row=$(event.target.parentNode.parentNode);
		var val=row.find("#noResponsePromptList").val();
		if(val=="0")
		{
			row.find("#noResponsePromptText").val('');
			row.find("#noResponsePromptText").attr('readonly',false);
			row.find("#noResponsePromptText").focus();
		
			
		}
		else 
		{
				val=row.find("#noResponsePromptList").find('option:selected').text().split(":");
				row.find("#noResponsePromptText").attr('readonly',true);
				row.find("#noResponsePromptText").val(val[1]);
				row.find("#noResponsePromptText").focus();
		}
		
		
	}	

	_this.populateNoResponseRow=function(data)
	{
		var cloned=$(_this.noResponseTemplateID).clone(true);
		var count=$(_this.noResponseTableID).find('tr').length;
		

		var prompt=cloned.find('#noResponsePromptID').val(data.id);
		var text=cloned.find('#noResponsePromptText').val(data.text);
		var actions=cloned.find('input[name="noResponseAction"]');
		var gotoList=cloned.find('#noResponseGotoList');
		
		gotoList.html($("#noResponseRow").find("#noResponseGotoList").html());
		
		prompt.attr('name','noResponsePromptID['+count+']');
		text.attr('name','noResponsePromptText['+count+']');
		gotoList.attr('name','noResponseGotoList['+count+']');
		actions.each(function(){ $(this).attr('name','noResponseAction['+count+']');});
		//cloned.find('#invalidPromptText').val(data.action);
		if(data.action=="RP")
		{
			cloned.find('#noResponseRepeat').prop('checked',true);
		}
		else 
		{
			cloned.find('#noResponseGoto').prop('checked',true);
			cloned.find('#noResponseGoto').prop('value',data.action);
			gotoList.val(data.action);
		}			
		cloned.removeClass('hidden');
		cloned.addClass('no-response-rows');

		count=count-2;
		var rowID='NR'+count;
		cloned.attr('id',rowID);
		$(_this.noResponseTableID).append(cloned);
	}



	_this.removeInvalidResponseRow=function(obj)
	{

		var row=$(obj.parentNode.parentNode);
		row.remove();
	}
	

	_this.addInvalidResponseRow=function()
	{ 
       if(!$("#invalidResponseForm").valid())
		{
			return false;
		}
		var data={};
		var promptID=$("#invalidRow").find("#invalidPromptList").val();
		var promptText=$("#invalidRow").find("#invalidPromptText").val();
		var action=$("#invalidRow").find('input[name="invalidAction"]:checked').val();
		if(promptID==0)
		{
		
			promptID=_this.getNextResponseID();
			

			$("#invalidRow").find("#invalidPromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
			$("#noResponseRow").find("#noResponsePromptList").append( $('<option />').val(promptID).text(promptID+":"+promptText));
	
			
			
		}
		data.id=promptID;
		data.text=promptText;
		data.action=action;
		_this.populateInvalidResponseRow(data);
		
			$("#invalidRow").find("#invalidPromptList").val(0);
			$("#invalidRow").find("#invalidPromptText").val('');		

	}

	_this.setInvalidPromptText=function(event)
	{
		var row=$(event.target.parentNode.parentNode);
		var val=row.find("#invalidPromptList").val();
		if(val=="0")
		{
			row.find("#invalidPromptText").val('');
			row.find("#invalidPromptText").attr('readonly',false);
			row.find("#invalidPromptText").focus();
		
			
		}
		else 
		{
				val=row.find("#invalidPromptList").find('option:selected').text().split(":");
				row.find("#invalidPromptText").attr('readonly',true);
				row.find("#invalidPromptText").val(val[1]);
				row.find("#invalidRepeat").focus();
		}
		
		
	}	


	_this.populateInvalidResponseRow=function(data)
	{
		var cloned=$(_this.invalidResponseTemplateID).clone(true);
		var count=$(_this.invalidTableID).find('tr').length;
		

		var prompt=cloned.find('#invalidPromptID').val(data.id);
		var text=cloned.find('#invalidPromptText').val(data.text);
		var actions=cloned.find('input[name="invalidAction"]');
		var gotoList=cloned.find('#invalidGotoList');
		
		gotoList.html($("#invalidRow").find("#invalidGotoList").html());
		
		prompt.attr('name','invalidPromptID['+count+']');
		text.attr('name','invalidPromptText['+count+']');
		gotoList.attr('name','invalidGotoList['+count+']');
		actions.each(function(){ $(this).attr('name','invalidAction['+count+']');});
		//cloned.find('#invalidPromptText').val(data.action);
		if(data.action=="RP")
		{
			cloned.find('#invalidRepeat').prop('checked',true);
		}
		else 
		{
			cloned.find('#invalidGoto').prop('checked',true);
			cloned.find('#invalidGoto').prop('value',data.action);
			gotoList.val(data.action);
		}			
		cloned.removeClass('hidden');
		cloned.addClass('invalid-response-rows');

		count=count-2;
		var rowID='INV'+count;
		cloned.attr('id',rowID);
		$(_this.invalidTableID).append(cloned);
	}
	

	_this.delete=function(node)
	{

			_this.designer.tree.delete_node (node);
			if(Object.keys(_this.designer.tree._model.data).length<=1)
			{
				_this.designer.builder.Start="";
				$("#wizard").removeClass("hidden");
				$("#discard").removeClass("hidden");
			}
			if(Object.keys(_this.designer.tree._model.data).length==2)
			{
				var n=_this.designer.tree._model.data["#"].children[0];
				_this.designer.builder.Start=n;
				n="#"+n;
				$(n).find(".ivr-start .fa").removeClass("fa-toggle-off");
				$(n).find(".ivr-start .fa").addClass("fa-toggle-on");
				
			}
			_this.designer.assignKeypressNextANSID();			

	}
	_this.add=function(node)
	{

		 var target="#"+node.id;
		
		var data=_this.designer.tree._model.data[node.id];
		var childrens=data.children;
		
		if(childrens.length<9)
		{
			_this.designer.create("keypress",target);
		}
	}
	

}

