
var treeDesigner=new SurveyTreeDesigner();

var tree=null;

var editVerifier=null;
var menuVerifier=null;
var noresponseVerifier=null;
var invalidVerifier=null;
var menuVerifier=null;

  
	 
     $( "#command li" ).draggable({
	  revert: false,
	
      helper: "clone"
    });  
	
    $( "#ivrContainer" ).droppable({
	  revert: "invalid",

      drop: function( event, ui ) {
			var command=ui.draggable.attr("command");
			var target=$("#ivrContainer").attr("aria-activedescendant");

			
			if($(".jstree-container-ul li").length==0){
				treeDesigner.create(command,"#");
				
			}
			else 
			{
				var id="#"+target;
			
				treeDesigner.create(command,target);
			
			}
			scrollToBottom("ivrContainer");
      
      }
    });


	 enableDisableMenu("hide");
	 treeDesigner.load(false);
	
	 $( "#ivrContainer" ).bind("mouseover",function(){ 

	 $("#ivrContainer").attr("aria-activedescendant","#"); });

	function checkDuplicateSurvey()
	{
		
		var exist=false
		$('#ivrList  option').each(function(){
	
			if (this.text.toLowerCase() == $("#editName").val().toLowerCase()) {
				exist=true;
			}
		});	
		return exist;
	}
	function create()
	{
	    if(!$("#editForm").valid())
		{
			return false;
		}	
			treeDesigner.load(null);
			var number=parseInt($("#editNumber").val());
			var level=parseInt($("#editLevel").val());

		

		
			treeDesigner.keypress=level;
			for(i=0; i<number; i++)
			{
				treeDesigner.create("menu","#");
			}
			treeDesigner.keypress=2;
			closeModal('editModal');
			enableDisableMenu("show");
			treeDesigner.assignKeypressNextANSID();
			
		
			$("#discard").removeClass("hidden");
			$("#wizard").removeClass("hidden");
	}
	

	function enableDisableMenu(action)
	{
	
		if(action=="show")
		{
			$("#command").removeClass("menuDisable");
			$("#command li").draggable( "enable" );
		}
		else
		{			
			$("#command").addClass("menuDisable");
			$( "#command li" ).draggable( "disable" );

		}
	}
	
	function closeModal(id)
	{

	//	editVerifier.resetForm(); 
		id="#"+id;
		$(id).modal("hide");
	}
	$("#wizard").click(function()
	{
		
		$("#editModal").modal();
	});
	
	$("#discard").click(
		function()
		{
			
		    messageBox($("#discard"),function(){
				treeDesigner.builder.Name='';
			
			
				$("#ivrList").val("0");
				$("#ivrList").change();				
				
			});

			
		}
	);
	$("#ivrList").change(function ()
	{
		$("#tfnNumbers").html('');
		var appID=this.selectedOptions[0].value;
		loader("show");
		if(appID==0)
		{
				enableDisableMenu("hide");
				treeDesigner.load(null);
				loader("hide");
				$("#discard").addClass("hidden");
				$("#wizard").addClass("hidden");
				$("#save").addClass("hidden");
				return;
		}

		
		var url=baseUrl+'/ajax/get-script';
		var request = $.ajax({
		  url: url,
		  data: { appID : appID },
		  dataType: "json",
		  success:function(data)
		  {
				if(data!=null)
				{
					treeDesigner.builder.Status=data.Status;
					treeDesigner.builder.IVR=data.IVR;
					treeDesigner.builder.Start=data.Start;
					treeDesigner.builder.Name=data.Name;
					if(data.Max!="" &&  data.Max!=0)
					{
						treeDesigner.nodeID= parseInt(data.Max);
						treeDesigner.nodeID++;
					}
					var tfns=data.Tfns;
					if(tfns.length>0)
					{
							var numbers="";
							for(var i=0; i<tfns.length; i++)
							{
								numbers=numbers+" "+tfns[i].tfn+",";
							}
							$("#tfnNumbers").html(numbers.replace(/,+$/,''));
					}
					treeDesigner.load(data.Script);
				}
				loader("hide");
				enableDisableMenu("show");
				
				if(Object.keys(treeDesigner.tree._model.data).length<1)
				{
					
					$("#wizard").removeClass("hidden");
					$("#discard").removeClass("hidden");
				}				
				else
				{
					$("#discard").addClass("hidden");
					$("#wizard").addClass("hidden");
				}
				$("#save").removeClass("hidden");			
		  },
		  error:function()
		  {
			loader("hide");
			alert('failed');
		  }
		});
		 
			
	});
	function moveRowUp(row)
	{

		row=$(row).parent().parent();
		var prev=row.prev();
		if(prev.length==1 && prev.is(":visible")==true)
		{
			row.prev().before(row);
		}		
	}
	function moveRowDown(row)
	{
		row=$(row).parent().parent();
		var next=row.next();
		if(next.length==1)
		{
			row.next().after(row);
		}
		
	}
	function scrollToBottom(ctl)
	{
		ctl="#"+ctl;
		$(ctl).scrollTop($(ctl)[0].scrollHeight);
	}
	//// validations

		
	$( document ).ready(function() {
		
		 $(".toggleHeader").hide();
			$(".displayHeader").click(function(){
				$(".toggleHeader").toggle("fast");
			});		
			
                      menuVerifier=$("#menuForm").validate({
												rules: 
													{
														menuText: 
																	{
																		required: true,
                                                                                                                                                alphanumeric:true,
                                                                                                                                                maxlength: 150
																	}
													}
											}); 
                       invalidVerifier= $("#invalidResponseForm").validate({
												rules: 
													{
														invalidPromptText: 
																	{
																		required: true,
                                                                                                                                                alphanumeric:true,
                                                                                                                                                maxlength: 150
																	}
													}
											});
                        noresponseVerifier=$("#noResponseForm").validate({
												rules: 
													{
														noResponsePromptText: 
																	{
																		required: true,
                                                                                                                                                alphanumeric:true,
                                                                                                                                                maxlength: 150
																	}
													}
											});  			
			editVerifier=$("#editForm").validate({
												rules: 
													{

																	
														editName:
														{
															required:true,
															checkDuplicateSurvey:true
														}
													}
											});
	jQuery.validator.addMethod("checkDuplicateSurvey", function(value, element, params) 
				{
					return this.optional(element) || !checkDuplicateSurvey();
				}, jQuery.validator.format("The survey with this name already exist, please try another name."));
		
                                                                                        
	});             
	
	
