function addApplication()
{
	clearAppModalData();
	$("#addAppPopup").modal('show');
	
}
function editApplication(id)
{
		var url='/application/get';
                loader("show");
        $.ajax({
            url: url,
			data:{id:id},
			dataType:"json",
            success: function(data)
			{
				console.log(data);
				if(data!=null)
				{
				    clearAppModalData();
					$("#appName").val(data.AppName);
					$("#customerID").val(data.UserID);
					$("#appAbrv").val(data.AppAbrv);
					$("#appType").val(data.AppType).change();
					$("#appStatus").val(data.Status).change();
					$("#timeZone").val(data.TimeZone).change();
					$("#environment").val(data.Environment).change();
					$("#appID").val(data.AppID);
					$("#applicationModalLabel").html("Edit Application");
					$("#addAppPopup").modal('show');
					
				}
                loader("hide");
            },
            error: function()
            {
                loader("hide");
                alert("Unable to get application data");
                
            }

        });
	
}

function clearAppModalData()
{
	$("#appID").val("");
	$("#appName").val("");
	$("#appAbrv").val("");
	$("#appType").val("I");
	$("#appStatus").val("L");
	$("#timeZone").val("0");
	$("#environment").val("S");
	$("#appnameavailability").attr("style","display:none");
	$('#appnameloader').html('');
	$("#applicationModalLabel").html("Add Application");
}

$(document).ready(function (){
	
	 $('#addAppForm').bind('submit', function () {
		$('#submitButton').prop('disabled', true);
		$('#cancelmodal').prop('disabled', true);
		$("#addAppPopup").modal('hide');
		loader("show");
		});
		
		// to check the value of application name
		$("#appName").blur(function() {
		$("#appName").attr("disabled", true);
		$("#appAbrv").attr("disabled", true);
		$('#appnameloader').html('<img src="../images/inlineLoader.gif" />');
		$("#submitButton").attr("disabled", true);	
		   var appnameval = $('#appName').val();
			if (appnameval == '') 
			{
				$("#appnameavailability").attr("style","display:none");
				$('#appnameloader').html('');
				$("#appName").attr("disabled", false);
				$("#appAbrv").attr("disabled", false);
			}
			else
			{
					var url ='/application/check';
					var appRecId = $('#appID').val();
					
				$.ajax({
					url: url,
					data: { name: appnameval, appRecId:appRecId },
					dataType: "json",
					success: function(data)
					{
						console.log(data);
						if(data== true)
						{
							$("#appnameavailability").attr("style","display:none");
							$("#appnameavailability").removeClass("nt-login-failure");
							$("#submitButton").attr("disabled", false);
							if($("#appAbrv").val()=='')
							{
								$("#appAbrv").val(appnameval);
							}
						}
						else
						{
							$("#appnameavailability").attr("style","display:block");
							$("#appnameavailability").addClass("nt-login-failure");
							$("#submitButton").attr("disabled", true);	
							
						}
													
													$('#appnameloader').html('');
													$("#appName").attr("disabled", false);
													$("#appAbrv").attr("disabled", false);
						
					},
					error: function()
					{
													
						alert("application name check Call failed");
						$('#appnameloader').html('');
						$("#submitButton").attr("disabled", true);
						$("#appName").attr("disabled", false);
						$("#appAbrv").attr("disabled", false);						
													
					}
					 
				});
			}

		});
});