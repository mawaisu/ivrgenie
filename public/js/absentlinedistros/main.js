$( document ).ready(function() {
    $("#newRecord").css("display", "none");
    $("#absentlinedistros-wfmdistro").val('');
    $("#absentlinedistros-opsdistro").val('');
    $("#absentlinedistros-hrdistro").val('');
    $("#update").val("1");
    $("#campaignCode").html("<option val=''>Select Location First</option>");
    $("#campaignCode").prop("disabled", true);
    $("#locationDropDown").val('');

});


setTimeout(function() {
        $('#updateRecord').fadeOut('fast');
}, 5000);




function validateEmail(email) {
    email=email.trim();
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

$('#locationDropDown').on('change', function(){
    var val=$(this).val();
    $("#absentlinedistros-locationname").val($(this).find('option:selected').text());
    $("#loc").val(val);
    $("#camp").val("0");
    $("#newRecord").css("display", "none");
    if(val=='')
    {
      $("#campaignCode").html("<option val=''>Select Location First</option>");
      $("#campaignCode").prop("disabled", true);
      return false;
    }
    
    $("#update").val("1");
    $("#absentlinedistros-wfmdistro").val('');
    $("#absentlinedistros-opsdistro").val('');
    $("#absentlinedistros-hrdistro").val('');

    $("#locationName").val($("#locationDropDown option:selected").text());
    var url=baseUrl+'/absentlinedistros/campaigns';
    loader("show");
    $.post(url,{data: val},
    function(data){
        $("#campaignCode").html(data).attr('disabled',false).selectpicker('refresh');        
        loader("hide");
    });


});


$('#manage').on('submit',function(e){
  $("#absentlinedistros-wfmdistro").parent().find("div.help-block").empty();
  $("#absentlinedistros-opsdistro").parent().find("div.help-block").empty();
  $("#absentlinedistros-hrdistro").parent().find("div.help-block").empty();
  if($("#camp").val()=='' || $("#camp").val()=='0')
  {
    e.preventDefault();
    swal({
              title: 'Error',
              text: "No campaign Selected",
              type: 'warning',
              showCancelButton: false,
              confirmButtonText: 'OK'
    });
  }
  var wfm=$("#absentlinedistros-wfmdistro").val();
  var wfmArray = wfm.split(";");
  for(i=0;i<wfmArray.length;i++)   
  {

    if(!validateEmail(wfmArray[i]))
    {
      e.preventDefault();
      $("#absentlinedistros-wfmdistro").parent().addClass("has-error");
     // $("#absentlinedistros-wfmdistro").addClass("has-error");
      $("#absentlinedistros-wfmdistro").parent().find("div.help-block").append("Not a valid email addrress");
       break;
    }

  }
  var ops=$("#absentlinedistros-opsdistro").val();
  var opsArray = ops.split(";");
  for(i=0;i<opsArray.length;i++)   
  {

    if(!validateEmail(opsArray[i]))
    {
      e.preventDefault();
      $("#absentlinedistros-opsdistro").parent().addClass("has-error");
     // $("#absentlinedistros-wfmdistro").addClass("has-error");
      $("#absentlinedistros-opsdistro").parent().find("div.help-block").append("Not a valid email addrress");
       break;
    }

  }
  var hr=$("#absentlinedistros-hrdistro").val();
  var hrArray = hr.split(";");
  for(i=0;i<hrArray.length;i++)   
  {

    if(!validateEmail(hrArray[i]))
    {
      e.preventDefault();
      $("#absentlinedistros-hrdistro").parent().addClass("has-error");
     // $("#absentlinedistros-wfmdistro").addClass("has-error");
      $("#absentlinedistros-hrdistro").parent().find("div.help-block").append("Not a valid email addrress");
       break;
    }

  }  


});

$('#campaignCode').on('change', function(){
    var val=$(this).val();
    $("#camp").val(val);
    $("#newRecord").css("display", "none");
    $("#absentlinedistros-wfmdistro").val('');
    $("#absentlinedistros-opsdistro").val('');
    $("#absentlinedistros-hrdistro").val(''); 
   if(val=="")
   {
    return false;
   }
  /*  if(val=='')
    {
      $("#campaignCode").html("<option val=''>Select Location First</option>");
      $("#campaignCode").prop("disabled", true);
      return false;
    }

    $("#locationName").val($("#locationDropDown option:selected").text()); */
    var location=$("#locationDropDown").val();
    var url=baseUrl+'/absentlinedistros/getdistro';
    console.log(location);
    loader("show");
    $.post(url,
    {
        data: val,
        location: location

    },
    function(data){      
        var obj=jQuery.parseJSON(data);
          if( typeof(obj[0]) != "undefined"){

             $("#absentlinedistros-wfmdistro").val(obj[0].WFMDistro);
             $("#absentlinedistros-opsdistro").val(obj[0].OPSDistro);
             $("#absentlinedistros-hrdistro").val(obj[0].HRDistro);
             $("#update").val("1"); 
             
            }
            else
            {
                $("#update").val("0");
                $("#newRecord").css("display", "block");
                $('#newRecord').fadeIn('fast');
                setTimeout(function() {
                    $('#newRecord').fadeOut('fast');
                }, 5000);
            }    
        loader("hide");
    });

});  






