function getReportList() {
    var val = $("#appName").val();
    loader("show");
    var url = '/report/list/ajax';
    $("#emailList").val("");
    $.ajax({
        type: 'GET',
        url: url,
        data: {appid: val},
        success: function (data) {
            $('#reportName').find('option').remove().end().append('<option value="">Select a value</option>');
            var obj = jQuery.parseJSON(data);

            console.log(obj);
            if(obj.length>0){
            for (i = 0; i<obj.length; i++) {
                console.log(obj[i]);
                $("#reportName").append($('<option></option>').val(obj[i].RptID).html(obj[i].RptName));
            }}
            else {

            }
         /*   jQuery.each(obj, function (i, val) {
                $("#reportName").append($('<option></option>').val(i).html(val));

            });*/
            $("#reportName").prop("disabled", false);
            loader("hide");


        },
        error: function () {
            loader("hide");
        }


    });


}

function getEmailList() {
    var val = $("#reportName").val();
    loader("show");
    var url ='/email/list/ajax';
    $.ajax({
        type: 'GET',
        url: url,
        data: {reportid: val},
        success: function (data) {
            var obj = jQuery.parseJSON(data);
            console.log(obj);
            if (obj.length != 0) {
             $("#emailList").val(obj.Email);
            }
            else {
                $("#emailList").val("");
            }
            loader("hide");

        },
        error: function () {
            loader("hide");
        }


    });


}



