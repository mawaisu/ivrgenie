$("#verifyCdrid").validate({
                        rules: 
                            {
                                cdrid: 
                                            {
                                                digits: true,
                                                required:true,
                                               
                                            }         


                            }
                    }); 

$('#havenDtmf').click(function(){
    var cdrid=$("#cdrid").val();
    var url=baseUrl+'/ajax/havendtmf';
    loader("show");
    $.ajax({  
        type: 'POST',  
        url: url, 
        data: { cdrid:cdrid },
        success: function(data){
            data=JSON.parse(data);
            for(var i = 0; i < data.length; i++) {
              var obj = data[i];
              $("#dtmfModalDiv").append('<div class="row"><div class="col-xs-8 col-sm-6">' + obj.Prompt + '</div><div class="col-xs-4 col-sm-6">' + obj.ANS + '</div></div>');     
              
        } 
        loader("hide");
        $("#dtmfModal").modal("show");
        },
        error:  function() {
            alert("Error");
            loader("hide");
          }  
    }); 
});


$( "#verifyCdrid" ).submit(function() {
  $("#verifyCdrid").validate();
  var value=$("#cdrid").val();
  $("#cdridholder").val(value);
  loader("show"); 
});




$("#transcriptionForm").submit(function() {
  var cdrid=$("#mycdrid").val();
  if(cdrid=="cdrid"){
  	alert("Please verify cdrid first then transcribe the record");
  	return false;
  }
  loader("show");
});

$("#lookUp").click(function(){
    if($("#postCode").val()==""){
    	alert("Please enter a post code to look for");
    }
    else{
    	loader("show");
    	$("#address").empty();
    	var postCode=$("#postCode").val();
    	var url=baseUrl+'/ajax/hvnlookup';
    	$.ajax({  
   			type: 'POST',  
    		url: url, 
    		data: { postCode:postCode },
    		success: function(data){
    			
    			$("#address").prop("disabled", false);

        		var obj = JSON.parse(data);
               
        		$.each(obj.Level, function(index, element) {
                       if(element.address1.length==0 || jQuery.isEmptyObject(element.address1) || /\r|\n/.exec(element.address1[0]) ){
                       	  element.address1="";
                       }
                       if(jQuery.isEmptyObject(element.address2) || /\r|\n/.exec(element.address2[0]) ){
                          element.address2="";
                       }
                       
                       if(element.Town.length==0 || jQuery.isEmptyObject(element.Town) || /\r|\n/.exec(element.Town[0]) ){
                       	  element.Town="";  
                       }
                       if(element.County.length==0 || jQuery.isEmptyObject(element.County) || /\r|\n/.exec(element.County[0]) ){
                          element.County=""; 
                       }
        			   var temp=element.address1+'|'+ element.address2+'|'+element.Town+'|'+element.County+'|'+element.Postcode; 
    					$('#address').append($('<option>', { 
        						value: temp,
        						text : temp 
    					})); 
				});
                loader("hide");



    		},
    		error: function() {

    			loader("hide");
    			swal({
             		 title: 'Error',
              		text: "No data Found against this Post Code",
              		type: 'warning',
                    showCancelButton: false,
                    confirmButtonText: 'OK'
            	});	
    			
    		}
		});



    }   

   
});

$('#address').on('change', function() {
  		 var temp=this.value;
  		 var arr = temp.split('|');
  		 $("#address1").val(arr[0]);
  		 $("#town").val(arr[1]);
  		 $("#city").val(arr[2]);
  		 $("#country").val(arr[3]);
  		 $("#postCode").val(arr[4]);
});

