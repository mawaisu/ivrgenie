$("#verify").click(function() {
    var cdrid=$("#cdrid").val();
    var appID=$("#appID").val();
    var url=baseUrl+'/ajax/verify-cdrid';
    if(cdrid=="")
    {
      swal({
              title: 'Error',
              text: "Please enter the valid cdrid id",
              type: 'warning',
              showCancelButton: false,
              confirmButtonText: 'OK'
            });  
      return false;
    }
    else
    {  
      loader("show");
      $.ajax({
      method: "POST",
      url: url,
      data:{cdrid:cdrid,appID:appID},
      dataType:"json",
      success: function(data)
      {
          if(data=="success")
          {

            $("#appVerified").val(appID);
            swal({
                  title: 'Success',
                  text: 'Verification Successfull!',
                  type: 'success',
                  showCancelButton: false,
                  confirmButtonText: 'OK'
            });

          }
          else
          {
            swal({
              title: 'Error',
              text: data,
              type: 'warning',
              showCancelButton: false,
              confirmButtonText: 'OK'
            });  

          }  
          loader("hide");
                      
      }
    });
    } 
    
});


$("#saveUsTranscription").submit(function(e){
  e.preventDefault();
  
  if($("#appVerified").val()=="0")
  {
     swal({
              title: 'Error',
              text: "cdrid is not verified",
              type: 'warning',
              showCancelButton: false,
              confirmButtonText: 'OK'
    });
     
     return false;  
  }
  
  $("#saveUsTranscription").unbind().submit();
  


});
