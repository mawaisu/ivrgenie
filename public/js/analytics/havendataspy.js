$("#toDate").datepicker();
$("#fromDate").datepicker();
//$('#dataSpyRecords').DataTable();
$('#dataSpyRecords').DataTable( {
    responsive: true
} );

/*function editHavenRecord(cdrid, callDate, calltime, calloutcome, fname, lname, Addr1, Addr2, town, postcode, batchid, ver_code, ver_date, transcriber) {

    $("#transcriber").val(transcriber);
    $("#myCdrid").val(cdrid);
    $("#appName").val("Haven Holidays (Service) 2009");
    $("#callDate").val(callDate);
    $("#calltime").val(calltime);
    $("#outcome").val(calloutcome);
    $("#fname").val(fname);
    $("#lname").val(lname);
    $("#addr1").val(Addr1);
    $("#addr2").val(Addr2);
    $("#town").val(town);
    $("#postCode").val(postcode);
    $("#batchid").val(batchid);
    $("#ver_code").val(ver_code);
    $("#ver_date").val(ver_date);


    $("#editHavenRecord").modal("show");


} */
function verifyHavenCdrids(){
    $("#verifyHavenCdrids").modal("show");
}


$("#verifyHavenRecords").validate({
                        rules: 
                            {
                                verifyFromCdrid: 
                                            {
                                                digits: true,
                                                required:true,
                                               
                                            },
                                verifyToCdrid: 
                                            {
                                                digits: true,
                                                required:true,
                                               
                                            }            


                            }
                    }); 





$("#searchHavenData").validate({
                        rules: 
                            {
                                fromBatch: 
                                            {
                                                digits: true,
                                            
                                               
                                            },
                                toBatch: 
                                            {
                                                digits: true,
                                            
                                               
                                            }            


                            }
                    }); 
  

function editHavenRecord(cdrid)
{

    loader("show");
    var url=baseUrl+'/ajax/get-havendata';
    $.ajax({
        url: url,
        method: "POST",
        data: { cdrid : cdrid },
        dataType: "json",
        success: function(data)
        {
           
           $("#transcriber").val(data[0].TranscribedBy);
           $("#appName").val(data[0].appname);
           $("#callDate").val(data[0].CallDate);
           $("#callTime").val(data[0].CallInTime);
           $("#addr1").val(data[0].Addr1);
           $("#addr2").val(data[0].Addr2);
           $("#addr3").val(data[0].Addr3);
           $("#town").val(data[0].town);
           $("#country").val(data[0].Country);
           $("#postCode").val(data[0].postcode);
           $("#title").val(data[0].title);
           $("#fname").val(data[0].fname);
           $("#lname").val(data[0].lname);
           $("#advert").val(data[0].HeardAdvert);
           $("#outcome").val(data[0].calloutcome);
           $("#havenCdrid").val(data[0].cdrid);
           loader("hide");
           $("#editHavenRecord").modal("show");                     
           
     
        },
        error: function()
        {
            loader("hide");
   
           alert("Unable to get haven data");
                
        }
    });
     
     
    
}

function generateDataSpyReport()
{

    loader("show");
    var url=baseUrl+'/ajax/havenrpt-generation';
    $.ajax({
        url: url,
        method: "POST",
        dataType: "json",
        success: function(data)
        {                    
          
           swal({
            title: 'Success',
            text: 'Report Generated Successfully',
            type: 'success',
            showCancelButton: false,
            confirmButtonText: 'OK'
          });
           loader("hide");    
          
        },
        error: function()
        {
            loader("hide");
   
            swal({
            title: 'Error',
            text: 'Unable to generate Haven Data Spy Report',
            type: 'warning',
            showCancelButton: false,
            confirmButtonText: 'OK'
            });
                
        }
    });
     
     
    
}

function generateDataSpyReReport()
{
    var val=$("#reReportBatchID").val();  
    loader("show");
    var url=baseUrl+'/ajax/havenrerpt-generation';
    $.ajax({
        url: url,
        method: "POST",
        dataType: "json",
        data: { batchID:val },
        success: function(data)
        {                    
          
           swal({
            title: 'Success',
            text: 'Report Generated Successfully',
            type: 'success',
            showCancelButton: false,
            confirmButtonText: 'OK'
          });
           loader("hide");

          
        },
        error: function()
        {
            loader("hide");
   
           swal({
            title: 'Error',
            text: 'Unable to generate Haven Data Spy Report',
            type: 'warning',
            showCancelButton: false,
            confirmButtonText: 'OK'
            });
                
        }
    });

    $("#reReportModal").modal("hide");
     
     
    
}

function reReportModal()
{
     $("#reReportModal").modal("show");
}




//$("#searchHavenData").submit(function() {
$('#searchHavenData').on('submit', function (e) { 
    $("#searchHavenData").validate();   
    var searchField = $("#searchField").val();
    var searchPhrase = $("#searchPhrase").val();
    var fromBatch = $("#fromBatch").val();
    var toBatch = $("#toBatch").val();
    var fromDate = $("#fromDate").val();
    var toDate = $("#toDate").val();
    if (searchField != "" && searchPhrase == "") {
        swal({
            title: 'Search Phrase Missing',
            text: 'Please enter the value for search Phrase as well',
            type: 'warning',
            showCancelButton: false,
            confirmButtonText: 'OK'
        });

        return false;
    }
    if (searchField == "" && searchPhrase != "") {
        swal({
            title: 'Search field Missing',
            text: 'Please enter the value for search field as well',
            type: 'warning',
            showCancelButton: false,
            confirmButtonText: 'OK'
        });
        return false;
    }

    if (fromBatch != "" && toBatch == "") {
        swal({
            title: 'Missing BatchID',
            text: 'Please enter a value for toBatch as well',
            type: 'warning',
            showCancelButton: false,
            confirmButtonText: 'OK'
        });
        return false;
    }
    if (fromBatch == "" && toBatch != "") {
        swal({
            title: 'Missing BatchID',
            text: 'Please enter a value for fromBatch as well',
            type: 'warning',
            showCancelButton: false,
            confirmButtonText: 'OK'
        });
        return false;
    }
    if (fromDate != "" && toDate == "") {
        swal({
            title: 'Missing Date',
            text: 'Please enter a value for toDate as well',
            type: 'warning',
            showCancelButton: false,
            confirmButtonText: 'OK'
        });
        return false;
    }
    if (fromDate == "" && toDate != "") {
        swal({
            title: 'Missing BatchID',
            text: 'Please enter a value for fromDate as well',
            type: 'warning',
            showCancelButton: false,
            confirmButtonText: 'OK'
        });
        return false;
    }


});



$('#editHavenRecord').on('submit', function (e) {

          e.preventDefault();
          loader("show");
          var url=baseUrl+'/analytics/update-haven';
          $("#editHavenRecord").modal('hide');
          

          $.post(url,$("#editHavenRecordForm").serialize()
                ).done(function(result){
     //               $("#searchHavenData").submit();
                    if(result=="Success"){
                        $("#searchHavenData").submit();
                    }
                    else{
                        alert("Error updating the record");
                    }

                    loader("hide"); 
                });
      

}); 








