function getReportList(){
	var val=$("#appName").val();
    loader("show");
    	var url=baseUrl+'/ajax/report-list';
        $("#emailList").val("");
    	$.ajax({  
   			type: 'POST',  
    		url: url, 
    		data: { appid:val },
    		success: function(data){
    			$('#reportName').find('option').remove().end().append('<option value="">Select a value</option>');
    			var obj = jQuery.parseJSON(data);
    			     jQuery.each(obj, function(i, val) {
    			     	      $("#reportName").append($('<option></option>').val(i).html(val));

					 });
					 $("#reportName").prop("disabled", false);
					 loader("hide");
    			     

    		},
            error: function() {
            	loader("hide");
            }
				
                
            }); 


} 

function getEmailList(){
	var val=$("#reportName").val();
    loader("show");
    	var url=baseUrl+'/ajax/report-email';
    	$.ajax({  
   			type: 'POST',  
    		url: url, 
    		data: { reportid:val },
    		success: function(data){
                     var obj = jQuery.parseJSON(data);
                     if(obj.length!=0){
					       $("#emailList").val(obj[0].Email);
                     }
                     else{
                           $("#emailList").val("");
                     }      
					 loader("hide");

    		},
            error: function() {
            	loader("hide");
            }
				
                
            }); 


}



