function addPermission()
{
    clearAppModalData();
    $("#addPermissionPopup").modal('show');

    var js = "{\n" +
        "\t\"Status\": \"100\",\n" +
        "\t\"Description\": \"Successful\",\n" +
        "\t\"RoleID\": \"1\",\n" +
        "\t\"Role Description\": \"Admin\",\n" +
        "\t\"Routes\": [{\n" +
        "\t\t\"RouteID\": \"1\",\n" +
        "\t\t\"Descrtion\": \"Dashboard\",\n" +
        "\t\t\"Rute\": \"\\\\Dashboard.index\",\n" +
        "\t\t\"Edit\": \"true\",\n" +
        "\t\t\"Delete\": \"false\"\n" +
        "\t},\n" +
        "\t{\n" +
        "\t\t\"RouteID\": \"2\",\n" +
        "\t\t\"Description\": \"Favourite\",\n" +
        "\t\t\"Rute\": \"Favourite.index\",\n" +
        "\t\t\"Edit\": \"true\",\n" +
        "\t\t\"Delete\": \"false\"\n" +
        "\t}]\n" +
        "}";

    console.log(JSON.parse(js));

}
function editPermission(id)
{
    var url='/permission/get';
    loader("show");
    $.ajax({
        url: url,
        data:{id:id},
        dataType:"json",
        success: function(data)
        {
            console.log(data);
            if(data!=null)
            {
                clearAppModalData();
                $("#permissionActualName").val(data.permission.actual_name);
                $("#permissionDisplayName").val(data.permission.display_name);
                $("#permissionStatus").val(data.permission.status);
                $("#permissionId").val(data.permission.id);
                $("#permissionModalLabel").html("Edit Permission");
                $("#addPermissionPopup").modal('show');

            }
            loader("hide");
        },
        error: function()
        {
            loader("hide");
            alert("Unable to get permission data");

        }

    });

}

function clearAppModalData()
{
    $("#permissionId").val("");
    $("#permissionActualName").val("");
    $("#permissionDisplayName").val("");
    $("#permissionStatus").val("");
}
