function loader(action)
{
		if(action=="show")
		{
			$("#loader").show();
		}
		else 
		{
			$("#loader").hide();
		}
}
function successMessage(successText)
{
	
	swal("Done!", successText, "success");
				
}
function message(title,text,type)
{
	swal.setDefaults({ closeOnConfirm:false});
	swal({title:title,text:text, type:type,closeOnConfirm: true});
				
}
function checkDuplicateInputParamter()
{
	var duplicate=false;
		$(".input-param-rows").each(
									function()
									{
								
										var row=$(this);
										if($("#inputParamNameHolder").val()==row.find("#inputParamName").val())
										{
											duplicate=true;
										}

									}
								  );
	return duplicate;
}
function checkPromptIdDuplication(value)
{
	var duplicate=false;
	$.each( treeDesigner.tree._model.data, function( key, obj ) {
												
											    if(key==value)
												{
													var selected=$(treeDesigner.treeID).jstree("get_selected");
													if(selected[0]!=key)
													{
														duplicate=true;
													}
												}
											});
	return duplicate;
}
function checkDuplicateOutputParamter()
{
	var duplicate=false;
		$(".output-param-rows").each(
									function()
									{
								
										var row=$(this);
										if($("#outputParamHolder").val()==row.find("#outputParamName").val())
										{
											duplicate=true;
										}

									}
								  );
	return duplicate;
}
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
function messageBox(obj,callback)
{
	var href=$(obj).attr('data-href');
	var title=$(obj).attr('title');
	var type=$(obj).attr('data-type');
	var text=$(obj).attr('data-text');
	var confirmText=$(obj).attr('data-confirm-text');
	var closeOnConfirm=$(obj).attr('data-closeOnConfirm');
	
	if(closeOnConfirm=="")
	{
		closeOnConfirm=true;
	}else if (closeOnConfirm=="false")
	{
		closeOnConfirm=false;
	}else if (closeOnConfirm=="true")
	{
		closeOnConfirm=true;
	}

	
	

	swal(
			{
				title: title,
				text: text,
				type: type,
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonColor: "#DD6B55",
				confirmButtonText: confirmText,
				closeOnConfirm: closeOnConfirm
			},
			function()
			{
				callback();
				//swal.disableButtons();

			}
		);
		return false;
		

}
	$( document ).ready(function() {
		// adding custom function
		jQuery.validator.addMethod("variableDeclaration", 
									function(value, element) {
									
										return this.optional(element) || /^[a-zA-Z_$][a-zA-Z_$0-9]*$/.test(value);
									}, 
									"invalid variable name"
								 );	
		jQuery.validator.addMethod("checkDuplicateInputParamter", 
									function(value, element) {
									
										return this.optional(element) || !checkDuplicateInputParamter();
									}, 
									"Parameter with this name already exist."
								 );	
		jQuery.validator.addMethod("checkDuplicateOutputParamter", 
									function(value, element) {
									
										return this.optional(element) || !checkDuplicateOutputParamter();
									}, 
									"Parameter with this name already exist."
								 );									 
		jQuery.validator.addMethod("alphanumeric", 
									function(value, element) {
									
										return this.optional(element) || /^[a-zA-Z0-9\s]+$/i.test(value);
									}, 
									"Letters, numbers or space only please"
								 );
		jQuery.validator.addMethod("username", 
									function(value, element) {
									
										return this.optional(element) || /^[a-zA-Z0-9\.]+$/i.test(value);
									}, 
									"Letters, numbers or . only please"
								 );								 
		jQuery.validator.addMethod("promptID", 
									function(value, element) {
									
										return this.optional(element) || /^[a-zA-Z0-9]+$/i.test(value);
									}, 
									"Letters or numbers only please"
								 );								 
		jQuery.validator.addMethod("notBlank",
			 function(value, element) 
			 { 
	
			  return value.indexOf(" ")!=0 && value != ""; 
			}, 
		"Space at the beginning is not allowed."
		);	
		jQuery.validator.addMethod("checkDuplicatedPromptID",
									function(value,element) 
									{

										return this.optional(element) || !checkPromptIdDuplication(value);
									},
									"This id already exists."
									);		
		jQuery.validator.addMethod("telephoneextension",
									function(value,element)
									{
										return this.optional(element) || /^[0-9]{1,10}@(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/i.test(value);
									},
									"Please enter valid extension. e.g 444@255.255.255.255"
									);
      $.validator.addMethod('le', function (value, element, param) {
        return this.optional(element) || parseInt(value) <= parseInt($(param).val());
	  }, 'From should be lesser then To');

	}); 
	